﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StableFluids;


public class ParticleFluidSimDisplayPlane : MonoBehaviour {

    public GameObject FluidSim;

    // Use this for initialization
    void Start () {
    }

    void OnDrawGizmos()
    {
        Vector3 pA = transform.TransformPoint(new Vector3(-0.5f,-0.5f, 0));
        Vector3 pB = transform.TransformPoint(new Vector3( 0.5f,-0.5f, 0));
        Vector3 pC = transform.TransformPoint(new Vector3( 0.5f, 0.5f, 0));
        Vector3 pD = transform.TransformPoint(new Vector3(-0.5f, 0.5f, 0));

        Gizmos.color = new Color32(0, 0, 255, 100);
        Gizmos.DrawLine(pA, pB);
        Gizmos.DrawLine(pB, pC);
        Gizmos.DrawLine(pC, pD);
        Gizmos.DrawLine(pD, pA);
    }

    // Update is called once per frame
    void Update () {
        FluidWithObstacles fluidProcessor = FluidSim.GetComponent(typeof(FluidWithObstacles)) as FluidWithObstacles;
        Renderer rdr = GetComponent<Renderer>();
        rdr.sharedMaterial.SetTexture("_MainTex", fluidProcessor.GetVectorField(StableFluids.FluidWithObstacles.VectorBufferID.Velocity3));
    }
}
