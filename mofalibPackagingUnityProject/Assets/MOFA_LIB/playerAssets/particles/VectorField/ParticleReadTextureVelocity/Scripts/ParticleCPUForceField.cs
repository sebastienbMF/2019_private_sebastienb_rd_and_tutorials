﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StableFluids;

public class ParticleCPUForceField : MonoBehaviour, ITextureGenerator
{
    public GameObject FluidSim;
    public GameObject uvMapping;
    public float Inertia = 0.1f;
    public float SpeedMultiplier = 5.0f;
    public float Margins = 0.005f;

    ParticleSystem myParticleSystem;
    ParticleSystem.Particle[] particleArray;
    FluidWithObstacles fluidProcessor;
    int fluidResolutionX;
    int fluidResolutionY;
    RenderTexture renderTexture_REMOVEME;
    Texture2D forceTexture;

    void Start() {

        fluidProcessor = FluidSim.GetComponent(typeof(FluidWithObstacles)) as FluidWithObstacles;
        myParticleSystem = GetComponent<ParticleSystem>();
        particleArray = new ParticleSystem.Particle[myParticleSystem.main.maxParticles];

        fluidResolutionX = fluidProcessor.ResolutionX;
        fluidResolutionY = fluidProcessor.ResolutionY;
        forceTexture = new Texture2D(fluidResolutionX, fluidResolutionY, TextureFormat.RGBAHalf, false);
        renderTexture_REMOVEME = new RenderTexture(fluidResolutionX, fluidResolutionY, 0, RenderTextureFormat.ARGBFloat);
    }

    Vector3 toWorldSpace(Vector3 systemSpace)
    {
        Vector3 objectSpace = myParticleSystem.transform.TransformPoint(systemSpace);
        return objectSpace;
    }

    Vector3 toVectorFieldSpace(Vector3 systemSpace)
    {
        Vector3 ws = toWorldSpace(systemSpace);
        return uvMapping.transform.InverseTransformPoint(ws);
    }

    Vector2 toVectorFieldUV(Vector3 systemSpace)
    {
        Vector3 vs = toVectorFieldSpace(systemSpace);
        Vector2 bottomLeftCorner_uv = new Vector2(-0.5f, -0.5f);
        Vector2 uv = new Vector2(vs.x, vs.y)-bottomLeftCorner_uv;
        return uv;
    }

    Vector3 uvDirectionToSystemSpace(Vector2 uvDir)
    {
        //Mapping object space to world space
        Vector3 worldDir = uvMapping.transform.TransformDirection(new Vector3(uvDir.x, uvDir.y, 0));
        //World to system space
        Vector3 systemDir = myParticleSystem.transform.InverseTransformVector(worldDir);
        return systemDir;
    }

    byte saturate(int v)
    {
        v = System.Math.Max(v, 0);
        v = System.Math.Min(v, 255);
        return (byte)v;
    }

    byte saturate(double v)
    {
        return saturate((int)v);
    }

    // Update is called once per frame
    void LateUpdate () {

        RenderTexture velocityTexture = fluidProcessor.GetVectorField(StableFluids.FluidWithObstacles.VectorBufferID.Velocity3) as RenderTexture;

        // Backup the currently set RenderTexture
        RenderTexture previousRT = RenderTexture.active;

        //RenderTexture renderTexture = new RenderTexture(velocityTexture.width, velocityTexture.height, 0);
        Graphics.Blit(velocityTexture, renderTexture_REMOVEME);

        // Set the current RenderTexture to the temporary one we created
        RenderTexture.active = renderTexture_REMOVEME;

        // Copy the pixels from the RenderTexture to the new Texture
        forceTexture.ReadPixels(new Rect(0, 0, fluidResolutionX, fluidResolutionY), 0, 0);
        forceTexture.Apply();

        RenderTexture.active = previousRT;

        myParticleSystem.GetParticles(particleArray);
        for (int i=0; i < myParticleSystem.particleCount; ++i)
        {
            Vector2 uv = toVectorFieldUV(particleArray[i].position);

            byte alpha = 255;

            if (uv.x > Margins && uv.x < (1.0f - Margins) &&
                uv.y > Margins && uv.y < (1.0f - Margins) )
            {
                //fluidResolutionX, fluidResolutionY
                Color c = forceTexture.GetPixel((int)(uv.x * forceTexture.width),
                                                (int)(uv.y * forceTexture.height));

                particleArray[i].color = new Color32(saturate(c.r * 255.0 * 3.0),
                                                     saturate(c.g * 255.0 * 3.0), 0, alpha);

                Vector3 vField = uvDirectionToSystemSpace(new Vector2(c.r, c.g)) * SpeedMultiplier;//  new Vector3(c.r * F + 0.01f, 0.0f, c.g * F);
                Vector3 vPrev = particleArray[i].velocity;

                particleArray[i].velocity = Vector3.Lerp(vField, vPrev, Inertia);
            }
            else
            {
                particleArray[i].color = new Color32(255, 255, 255, 128);
            }
        }

        myParticleSystem.SetParticles(particleArray, myParticleSystem.particleCount);
    }

    public Texture GetTexture(uint texNo)
    {
        return forceTexture;
    }
}
