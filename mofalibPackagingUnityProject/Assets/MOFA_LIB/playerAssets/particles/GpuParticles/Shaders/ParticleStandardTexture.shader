﻿Shader "Particles/Custom/StandardTexture"
{
	Properties
	{
		_Color("_Color", Color) = (1,1,1,1)
		_MainTex("_MainTex", 2D) = "white" {}

		_TransparentColor("_TransparentColor", Color) = (0,0,0,1)
		_TransparentTreshold("_TransparentTreshold", Float) = 0.1

		//_MultiplyInputColor("_MultiplyInputColor", Range(0,1)) = 0
		_Glossiness("_Glossiness", Range(0,1)) = 0.5
		_Metallic("_Metallic", Range(0,1)) = 0.0
	}
		
	SubShader
	{
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
		
		ZWrite off
		LOD 200

		CGPROGRAM

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard alpha vertex:vert finalcolor:finalcolor

		sampler2D _MainTex;

		//half _MultiplyInputColor;
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		fixed4 _TransparentColor;
		half _TransparentTreshold;

		// https://docs.unity3d.com/Manual/PartSysVertexStreams.html
		struct appdata_particles
		{
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float4 color : COLOR;
			float4 texcoord : TEXCOORD0;

			// Doest not compile on Android-gles3 if 3 following lines not present:
			float4 texcoord1 : TEXCOORD0;
			float4 texcoord2 : TEXCOORD0;
			UNITY_VERTEX_INPUT_INSTANCE_ID // Macro for gles3?
		};

		struct Input
		{
			float2 uv_MainTex;
			float2 texcoord1;
			float4 color;
		};

		void vert(inout appdata_particles v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input,o);

			//o.uv_MainTex = v.texcoord.xy;
			//o.texcoord1 = v.texcoord.zw;

			o.color = v.color;
		}

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = 2.0f * IN.color * tex2D(_MainTex, IN.uv_MainTex) * _Color;

			o.Albedo = c.rgb;
			o.Alpha = c.a;

			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
		}

		// Final color pass
		void finalcolor(Input IN, SurfaceOutputStandard o, inout fixed4 color)
		{
			// Read color from the texture
			//half4 c = tex2D(_MainTex, IN.uv_MainTex);
			
			//calculate the difference between the texture color and the transparent color
			//note: we use 'dot' instead of length(transparent_diff) as its faster, and
			//although it'll really give the length squared, its good enough for our purposes!
			half3 transparent_diff = color.xyz - _TransparentColor.xyz;
			half transparent_diff_squared = dot(transparent_diff, transparent_diff);

			//if colour is too close to the transparent one, discard it.
			//note: you could do cleverer things like fade out the alpha
			if (transparent_diff_squared < _TransparentTreshold)
				discard;

			//+(IN.color * _MultiplyInputColor)
			color = color;
		}

		ENDCG
	}

	FallBack "Diffuse"
}