﻿// https://docs.unity3d.com/Manual/PartSysInstancing.html
Shader "Particles/Custom/StandardColor"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}

	SubShader
	{
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite off
		LOD 200

		CGPROGRAM

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard alpha vertex:vert

		// https://docs.unity3d.com/Manual/PartSysVertexStreams.html
		struct appdata_particles
		{
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float4 color : COLOR;

			// Doest not compile on Android-gles3 if 3 following lines not present:
			float4 texcoord1 : TEXCOORD0;
			float4 texcoord2 : TEXCOORD0;
			UNITY_VERTEX_INPUT_INSTANCE_ID // Macro for gles3?
		};

		struct Input
		{
			float4 color;
		};

		void vert(inout appdata_particles v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.color = v.color;
		}

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		// https://docs.unity3d.com/Manual/SL-SurfaceShaders.html
		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = 2.0f * IN.color * _Color;

			o.Albedo = c.rgb;
			o.Alpha = c.a;

			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;

		}

		ENDCG
	}

	FallBack "Diffuse"
}