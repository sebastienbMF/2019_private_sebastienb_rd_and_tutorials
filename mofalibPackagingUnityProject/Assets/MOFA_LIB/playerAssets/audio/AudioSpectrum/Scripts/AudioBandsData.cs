﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace MomentFactory.Audio
{
    /// <summary>
    /// Spectrum reader component reading configuration (that can be shared between multiple scenes, etc.)
    /// </summary>
    public class AudioBandsData : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField]
        private AudioBandMeter[] bandMeters;

        private Dictionary<string, AudioBandMeter> bandsByName = new Dictionary<string, AudioBandMeter>();

        public void Read(AudioSpectrumReader spectrumReader, float deltaTime)
        {
            foreach (AudioBandMeter meter in bandMeters)
            {
                meter.Read(spectrumReader, deltaTime);
            }
        }

        public AudioBandMeter GetMeterByName(string name, bool required = true)
        {
            AudioBandMeter band = bandsByName[name];
            if (required && band==null)
            {
                Assert.IsNotNull(band, "BandMeter '" + "' not found in " + this);
            }
            return band;
        }

        public AudioBandMeter[] GetMeters()
        {
            return bandMeters;
        }

        private void OnEnable()
        {
            foreach (AudioBandMeter meter in bandMeters)
            {
                if (bandsByName.ContainsKey(meter.Name))
                {
                    Debug.LogWarning(this + " already had a band named '" + meter.Name + "'");
                    continue;
                }
                bandsByName[meter.Name] = meter;
            }
        }

        #region ISerializationCallbackReceiver
        public void OnBeforeSerialize()
        {
            // TODO: Data validations?
        }

        public void OnAfterDeserialize()
        {
            
        }
        #endregion

    }
}


#if UNITY_EDITOR
namespace MomentFactory.Audio.Edition
{
    using UnityEditor;

    [CustomEditor(typeof(AudioBandsData))]
    public class AudioBandsData_Editor : Editor
    {
        [MenuItem("Assets/Create/MOFA/AudioBandsData")]
        public static void CreateAsset()
        {
            // http://wiki.unity3d.com/index.php/CreateScriptableObjectAsset
            ScriptableObjectUtility.CreateAsset<AudioBandsData>();
        }
        
        /// Portable ScriptableObjectUtility
        public static class ScriptableObjectUtility
        {
            /// <summary>
            //	This makes it easy to create, name and place unique new ScriptableObject asset files.
            /// </summary>
            public static void CreateAsset<T>() where T : ScriptableObject
            {
                T asset = ScriptableObject.CreateInstance<T>();

                string path = AssetDatabase.GetAssetPath(Selection.activeObject);
                if (path == "")
                {
                    path = "Assets";
                }
                else if (System.IO.Path.GetExtension(path) != "")
                {
                    path = path.Replace(System.IO.Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
                }

                string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

                AssetDatabase.CreateAsset(asset, assetPathAndName);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = asset;
            }
        }
    }

    
}
#endif