﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory.Audio
{
    /// <summary>
    /// Transform this component local transform from an audio band level. 
    /// ONLY ADDITIVE for now...
    /// TODO: Rotation/position effect(s)
    /// TODO: Relative/absolute transformations
    /// </summary>
    public class AudioSpectrumTransform : BaseAudioSpectrumComponent
    {
        [Header("Transform Effects")]
        [Tooltip("Additive modifier, multiplied by initial values")]
        public Vector3 Scale = new Vector3(0, 1, 0);

        // TODO:
        // [Tooltip("Additive modifier, multiplied by initial values")]
        // public Vector3 Position = new Vector3(0, 0, 0);

        // TODO:
        // [Tooltip("Additive modifier, multiplied by initial values")]
        // public Vector3 Rotation = new Vector3(0, 0, 0);

        private Vector3 InitialScale;
        //private Vector3 InitialRotation;
        //private Vector3 InitialPosition;

        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            
            SetInitialState();
        }

        void SetInitialState()
        {
            InitialScale = transform.localScale;
            //InitialRotation = transform.localRotation.eulerAngles;
            //InitialPosition = transform.localPosition;
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();

            if(BandMeter != null)
            {
                transform.localScale = InitialScale + CurrentLevel * Vector3.Scale(Scale, InitialScale);
            }
        }
    }
}