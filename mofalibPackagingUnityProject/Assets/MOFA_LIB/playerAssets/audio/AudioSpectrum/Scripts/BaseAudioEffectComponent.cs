﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory.Audio
{
    public class BaseAudioEffectComponent : MonoBehaviour, IAudioEffect
    {
        #region IAudioEffect

        /// <summary>
        /// The current normalized FX amount from 0 to 1. Zero means effect is unactive, 1 means at maximum strength
        /// </summary>
        public virtual float Amount { get; set; }

        #endregion
        // Use this for initialization
        protected virtual void Start()
        {

        }

        // Update is called once per frame
        protected virtual void Update()
        {

        }
    }
}