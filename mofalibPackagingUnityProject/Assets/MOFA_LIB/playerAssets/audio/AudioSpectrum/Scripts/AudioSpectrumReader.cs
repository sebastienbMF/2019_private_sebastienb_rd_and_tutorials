﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory.Audio
{
    /// <summary>
    /// 
    /// Uses AudioListener.GetSpectrumData() and interpret it's data into Octave Bands for visual effects sucha as EQs,etc.
    /// 
    /// Notes
    /// * Make sure to have one, or a strict a minimum of instances!
    /// * You can use AudioReactSpectrumSource.Main to read values from other components.
    /// 
    /// @see https://en.wikipedia.org/wiki/Octave_band
    /// @see https://benjaminbeagley.wordpress.com/tutorials/q-beats-audio-processing-how-i-did-it/part-1-get-spectrum-data/
    /// @see https://answers.unity.com/questions/157940/getoutputdata-and-getspectrumdata-they-represent-t.html
    /// @see https://github.com/keijiro/unity-audio-spectrum
    /// @see https://medium.com/giant-scam/algorithmic-beat-mapping-in-unity-real-time-audio-analysis-using-the-unity-api-6e9595823ce4
    /// 
    /// </summary>
    public class AudioSpectrumReader : MonoBehaviour
    {
        public enum LevelType
        {
            None,
            Peak,
            Average,
            Normalized,
            Filtered
        };

        /// <summary>
        /// DB level when muted
        /// @see https://sound.stackexchange.com/questions/25529/what-is-0-db-in-digital-audio
        /// </summary>
        public const float LevelMuted = -1e12f;

        /// <summary>
        /// Zero DB reference 
        /// </summary>
        public const float LevelZero = 0.70710678118f; // 1/sqrt(2)

        /// <summary>
        /// MAX DB ceiling to draw spectrums 
        /// </summary>
        public const float LevelMax = 0.1f;

        /// <summary>
        /// Null or invalid level
        /// </summary>
        public const float LevelNone = -1000f;


        public static AudioSpectrumReader Main { get; private set; }

        /// <summary>
        /// Octave band type
        /// https://en.wikipedia.org/wiki/Octave_band
        /// </summary>
        public enum BandCountType
        {
            FourBand,
            FourBandVisual,
            EightBand,
            TenBand,
            TwentySixBand,
            ThirtyOneBand
        };

        static float[][] BandTypeMiddleFrequencies = {
            new float[]{ 125.0f, 500, 1000, 2000 },
            new float[]{ 250.0f, 400, 600, 800 },
            new float[]{ 63.0f, 125, 500, 1000, 2000, 4000, 6000, 8000 },
            new float[]{ 31.5f, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000 },
            new float[]{ 25.0f, 31.5f, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000 },
            new float[]{ 20.0f, 25, 31.5f, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000 },
        };

        static float[] BandTypeBandwidth = {
                1.414f, // 2^(1/2)
                1.260f, // 2^(1/3)
                1.414f, // 2^(1/2)
                1.414f, // 2^(1/2)
                1.122f, // 2^(1/6)
                1.122f  // 2^(1/6)
        };

        [Header("Audio Spectrum")]
        [Tooltip("Number of EQ bands to ouput")]
        public BandCountType BandCount = BandCountType.TwentySixBand;
    
        [Tooltip("Length of AudioListener.GetSpectrumData() buffer must be a power of two between 64 and 8192"), Range(64, 2048)]
        public int SampleCount = 1024;

        [Tooltip("The audio channel"), Range(0, 64)]
        public int Channel = 0;

        [Header("Levels")]
        /// <summary>
        /// To achieve subjective 0 to 1 output on NormalizedLevels
        /// </summary>
        [Tooltip("To achieve subjective 0 to 1 output on NormalizedLevels"), Range(1f, 1000f)]
        public float NormalizedMultiplier = 10f;

        [Tooltip("In seconds, use Mathf.SmoothDamp on each normalized band level change"), Range(0f, 0.5f)]
        public float NormalizedDamping = 0.05f;

        [Tooltip("How fast peak levels fall, multiplied by delta time"), Range(0.01f, 0.5f)]
        public float PeakFallSpeed = 0.08f;

        [Tooltip("How much peak sensitivity returns to zero"), Range(1.0f, 20.0f)]
        public float AverageSensitivity = 8.0f;

        [Header("Band Readers")]
        public List<AudioBandsData> BandReadersData;

        [Tooltip("If we run from copies of AudioBandsData, otherwise changes at runtime will be saved in each scriptable object (good for tuning)")]
        public bool InstanciateBandReaders = false;

        /// <summary>
        /// Interpreted spectrum samples in audio levels (averaging between AudioSpectrumReader.LevelMuted to AudioSpectrumReader.LevelMax)
        /// </summary>
        [System.NonSerialized]
        public float[] Levels;

        /// <summary>
        /// Interpreted levels normalized between 0f to 1f
        /// </summary>
        [System.NonSerialized]
        public float[] NormalizedLevels;

        /// <summary>
        /// When NormalizedLevelsDamping > 0, velocity per band
        /// </summary>
        private float[] NormalizedLevelsDampingVelocities;

        /// <summary>
        /// Levels with PeakFallSpeed
        /// </summary>
        [System.NonSerialized]
        public float[] PeakLevels;

        /// <summary>
        /// Average band filtered, return to zero speed using  AverageSensitivity
        /// </summary>
        [System.NonSerialized]
        public float[] AverageLevels;

        /// <summary>
        /// Raw samples values returned from AudioListener.GetSpectrumData()
        /// </summary>
        [System.NonSerialized]
        public float[] RawSpectrum;

        /// <summary>
        /// Current BandCountType bandwidth, from BandTypeMiddleFrequencies
        /// </summary>
        private float[] Middlefrequencies;

        /// <summary>
        /// Current BandCountType bandwidth, from BandTypeBandwidth
        /// </summary>
        private float Bandwidth;

#if UNITY_EDITOR
        [HideInInspector]
        public MomentFactory.Audio.Edition.AudioSpectrumReader_Editor.EditorSettings EditorSettings;
#endif
        

        // Use this for initialization
        void Awake()
        {
            if (Main == null) Main = this;

            // Create scriptable object instances
            if(InstanciateBandReaders && BandReadersData != null)
            {
               
                List<AudioBandsData> BandReaderScriptableObjects = BandReadersData;
                BandReadersData = new List<AudioBandsData>();
                foreach (AudioBandsData bandReaderScriptableObject in BandReaderScriptableObjects)
                {
                    AddBandReader(bandReaderScriptableObject);
                }
            }
        }

        void Start()
        {
            UpdateBands();
        }


        public AudioBandMeter GetBandMeter(string bandMeterName, AudioBandsData scriptableObjectReference = null, bool required = true)
        {
            if(scriptableObjectReference!=null)
            {
                // TODO
            }

            AudioBandMeter meter = null;

            if(BandReadersData==null || BandReadersData.Count ==0)
            {
                Debug.LogWarning("Not BandReadersData set for " + this + ", cannot find band meter '" + bandMeterName + "'");
            }

            foreach (AudioBandsData audioBandsData in BandReadersData)
            {
                if(audioBandsData!=null)
                {
                    meter = audioBandsData.GetMeterByName(bandMeterName, required);
                    if (meter != null) return meter;
                }
            }

            return meter;
        }

        public float[] GetLevels(LevelType levelType)
        {
            switch(levelType)
            {
                case LevelType.Normalized:
                    return NormalizedLevels;

                case LevelType.Average:
                    return AverageLevels;

                case LevelType.Peak:
                    return PeakLevels;

                default:
                    return Levels;
            }
        }

        public float GetLevel(int bandIndex, LevelType levelType = LevelType.None)
        {
            float[] levels = GetLevels(levelType);

            if(levels == null)
            {
                Debug.LogError("Invalid levels for LevelType " + levelType);
                return LevelNone;
            }

            if(bandIndex >= levels.Length)
            {
                Debug.LogWarning("Invalid band index " + bandIndex + " for " + this + ", band count: " + BandCount);
                return LevelNone;
            }

            return levels[bandIndex];
        }

        void Update()
        {
            float deltaTime = Time.deltaTime;

            UpdateBands();
            
            AudioListener.GetSpectrumData(RawSpectrum, Channel, FFTWindow.BlackmanHarris);

            float falldown = PeakFallSpeed * deltaTime;
            float filter = Mathf.Exp(-AverageSensitivity * deltaTime);

            int minIndex;
            int maxIndex;
            float bandMax;
            int n = Levels.Length;
            for (var i = 0; i < n; i++)
            {
                minIndex = FrequencyToSpectrumIndex(Middlefrequencies[i] / Bandwidth);
                maxIndex = FrequencyToSpectrumIndex(Middlefrequencies[i] * Bandwidth);
                bandMax = 0.0f;

                for (var f = minIndex; f <= maxIndex; f++)
                {
                    bandMax = Mathf.Max(bandMax, RawSpectrum[f]);
                }

                Levels[i] = bandMax;

                if(NormalizedDamping>0f)
                {
                    NormalizedLevels[i] = Mathf.SmoothDamp(
                        NormalizedLevels[i], NormalizeLevel(bandMax), ref NormalizedLevelsDampingVelocities[i],
                        NormalizedDamping, Mathf.Infinity, deltaTime);
                } else
                {
                    NormalizedLevels[i] = NormalizeLevel(bandMax);
                }

                PeakLevels[i] = Mathf.Max(PeakLevels[i] - falldown, bandMax) ;
                AverageLevels[i] = (bandMax) - ((bandMax) - AverageLevels[i]) * filter;
            }


            // Read/interpret/interpolate band groups
            foreach (AudioBandsData bandMeterData in BandReadersData)
            {
                bandMeterData.Read(this, Time.deltaTime);
            }
        }

        // Will create an instance of the scriptable object to use at runtime
        public void AddBandReader(AudioBandsData bandReaderScriptableObject)
        {
            if(InstanciateBandReaders)
            {
                AudioBandsData readerData = Instantiate<AudioBandsData>(bandReaderScriptableObject);
                BandReadersData.Add(readerData);
            } else
            {
                BandReadersData.Add(bandReaderScriptableObject);
            }
        }

        public float NormalizeLevel(float dbLevel)
        {
            return Mathf.Clamp01(dbLevel * NormalizedMultiplier);
        }

        private int FrequencyToSpectrumIndex(float f)
        {
            var i = Mathf.FloorToInt(f / AudioSettings.outputSampleRate * 2.0f * RawSpectrum.Length);
            return Mathf.Clamp(i, 0, RawSpectrum.Length - 1);
        }

        private void UpdateBands()
        {
            if (RawSpectrum == null || RawSpectrum.Length != SampleCount)
            {
                RawSpectrum = new float[SampleCount];
            }

            int bandsTypeIndex = (int)BandCount;
            int bandCount = BandTypeMiddleFrequencies[bandsTypeIndex].Length;

            if (Levels == null || Levels.Length != bandCount)
            {
                Levels = new float[bandCount];
                PeakLevels = new float[bandCount];
                AverageLevels = new float[bandCount];

                NormalizedLevels = new float[bandCount];
                NormalizedLevelsDampingVelocities = new float[bandCount];
                for (int i = 0; i < bandCount; i++)
                {
                    NormalizedLevelsDampingVelocities[i] = 0;
                }

                Middlefrequencies = BandTypeMiddleFrequencies[bandsTypeIndex];
                Bandwidth = BandTypeBandwidth[bandsTypeIndex];
            }
        }
    }
}

#if UNITY_EDITOR
namespace MomentFactory.Audio.Edition
{
    using UnityEditor;

    [CustomEditor(typeof(AudioSpectrumReader))]
    public class AudioSpectrumReader_Editor : Editor
    {
        /// <summary>
        /// Specific editor settings, to be serialized in target MonoBehavior, for editor only
        /// </summary>
        [System.Serializable]
        public class EditorSettings
        {
            public AudioSpectrumReader.LevelType LevelType = AudioSpectrumReader.LevelType.Normalized;
        }

        AudioSpectrumReader.LevelType LevelType;
        AnimationCurve curve;

        // Update frequently while it's playing.
        public override bool RequiresConstantRepaint()
        {
            return true;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            AudioSpectrumReader target = this.target as AudioSpectrumReader;

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Runtime Inspector", EditorStyles.boldLabel);

            LevelType = target.EditorSettings.LevelType = (AudioSpectrumReader.LevelType) EditorGUILayout.EnumPopup("Level Type", target.EditorSettings.LevelType);

            // Length of sample buffer must be a power of two between 64 and 8192.
            ClampSamples(target);

            if (Application.isPlaying)
            {
                float[] levels = target.GetLevels(LevelType);

                if(levels!=null)
                {
                    EditorGUI.BeginDisabledGroup(true);

                    bool isNormalized = LevelType == AudioSpectrumReader.LevelType.Normalized;
                    float maxValue = isNormalized ? 1f : AudioSpectrumReader.LevelMax;

                    // EQ Curve (spectrum analyzer)
                    DrawCurve(target, levels, maxValue);

                    // EQ sliders (Using Editor GUI)
                    DrawSliders(target, levels, maxValue);

                    EditorGUI.EndDisabledGroup();

                    EditorGUILayout.Space();
                    EditorGUILayout.Space();

                    DrawBandMeters(target);

                }
                else
                {
                    EditorGUILayout.LabelField("Invalid spectrum data on " + target);
                }
            } else
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Enter playmode to see spectrum analyzers");
            }

            EditorGUILayout.Space();
        }

        void DrawCurve(AudioSpectrumReader target, float[] levels, float maxValue)
        {
            // Create a new curve to update the UI.
            curve = new AnimationCurve();

            // Add keys for the each band.
            int n = levels.Length;
            for (var i = 0; i < n; i++)
            {
                curve.AddKey(1.0f / levels.Length * i, levels[i]);
            }

            EditorGUILayout.CurveField(curve, Color.white, new Rect(0, 0, 1.0f, maxValue), GUILayout.Height(64));
        }

        void DrawSliders(AudioSpectrumReader target, float[] levels, float maxValue)
        {
            Rect SlidersRect = GUILayoutUtility.GetLastRect();

            // Position the sliders floating below the "Add component" button
            SlidersRect.y += SlidersRect.height + 10;
            SlidersRect.height = 120;

            GUI.BeginGroup(SlidersRect);
            EditorGUILayout.BeginHorizontal();

            int n = levels.Length;
            int sliderWidth = Mathf.Min(Mathf.FloorToInt(SlidersRect.width / n), 40);
            for (int i = 0; i < n; i++)
            {
                GUI.VerticalSlider(new Rect(i * sliderWidth, sliderWidth, 100, 80), levels[i], maxValue, 0.0f);
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            GUI.EndGroup();

            GUILayout.Space(100);
        }

        void DrawBandMeters(AudioSpectrumReader target)
        {
            if (target.BandReadersData == null || target.BandReadersData.Count == 0)
            {
                EditorGUILayout.LabelField("BandReadersData not set! Please add one or more!", EditorStyles.boldLabel);
            }
            else
            {
                EditorGUILayout.LabelField("Band Meters", EditorStyles.boldLabel);

                foreach (AudioBandsData bandsData in target.BandReadersData)
                {
                    EditorGUILayout.LabelField(bandsData.name);

                    AudioBandMeter[] bandMeters = bandsData.GetMeters();

                    // Draw each interpreted meter
                    int meterIndex = 0;
                    foreach (AudioBandMeter meter in bandMeters)
                    {
                        EditorGUILayout.Space();
                        EditorGUILayout.BeginHorizontal();
                        bool isMeterNormalized = meter.LevelType == AudioSpectrumReader.LevelType.Normalized;
                        float meterMaxValue = isMeterNormalized ? 1f : meter.MaxLevel;
                        EditorGUILayout.LabelField(string.IsNullOrEmpty(meter.Name) ? meterIndex.ToString() : meter.Name);
                        EditorGUILayout.Slider(meter.CurrentLevel, 0f, meterMaxValue);
                        EditorGUILayout.EndHorizontal();
                        meterIndex++;
                    }

                    EditorGUILayout.Space();
                }
            }
        }

        void ClampSamples(AudioSpectrumReader target)
        {
            int count = target.SampleCount;

            // Round to a power of two
            count--;
            count |= count >> 1;
            count |= count >> 2;
            count |= count >> 4;
            count |= count >> 8;
            count |= count >> 16;

            count++;

            // Length of sample buffer must be a power of two between 64 and 8192.
            count = Mathf.Clamp(count, 64, 8192);

            if (count != target.SampleCount)
            {
                target.SampleCount = count;
                EditorUtility.SetDirty(target.gameObject);
            }
        }

        /*Vector2 GetCurrentGUIGroupWidthHeight()
        {
            Type t = System.Type.GetType("UnityEngine.GUIClip, UnityEngine");
            Rect r = (Rect)t.GetProperty("topmostRect").GetValue(null, null);
            return new Vector2(r.width, r.height);
        }*/
    }
}

#endif