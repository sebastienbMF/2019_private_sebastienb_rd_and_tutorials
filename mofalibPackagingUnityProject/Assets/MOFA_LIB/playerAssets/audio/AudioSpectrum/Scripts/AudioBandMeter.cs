﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory.Audio
{
    /// <summary>
    /// Reads AudioSpectrumReader bars / band using configurable parameters.  
    /// Read() must be called manually:  
    /// @see AudioSpectrumReader component for example use.   
    /// @see AudioSpectrumTransform for example value extration.   
    /// </summary>
    [System.Serializable]
    public class AudioBandMeter
    {
        public string Name;

        [Tooltip("The AudioSpectrumReader.LevelType value reading type")]
        public AudioSpectrumReader.LevelType LevelType = AudioSpectrumReader.LevelType.Normalized;

        [Tooltip("Relative band position in spectrum)"), Range(0, 1)]
        public float Position = 0.25F;

        [Tooltip("Multiplier on AudioSpectrumReader bar levels"), Range(0, 100)]
        public float Gain = 1f;

        [Tooltip("How many bars spread on To read all bands, do not use an AudioBandReader!")]
        public int Width = 1;

        [Tooltip("Relative band positions in spectrum to read (optional)"), Range(0, 1)]
        public float[] AdditionalBands;

        [Tooltip("In seconds, use Mathf.SmoothDamp on level change, zero means instant"), Range(0f, 1f)]
        public float Damping = 0f;

        //public bool ClampNormalizedGain = true;

        [System.NonSerialized]
        public float CurrentLevel;

        /// <summary>
        /// Relative max level by comparing Gain and AudioSpectrumReader.NormalizedLevelMultiplier
        /// </summary>
        [System.NonSerialized]
        public float MaxLevel;

        private float dampingVelocity = 0f;
        

        public float Read(AudioSpectrumReader spectrumReader, float deltaTime)
        {
            int bandIndex = Mathf.RoundToInt(Position * spectrumReader.Levels.Length); // Band index of spectrumReader
            float level = spectrumReader.GetLevel(bandIndex, LevelType) * Gain;
            int totalBands = 1;
            
            // Larger band width
            if (Width > 1f)
            {
                for (int k = 0; k < Width; k++)
                {
                    bandIndex++;
                    if (bandIndex >= spectrumReader.Levels.Length) break;
                    level += spectrumReader.GetLevel(bandIndex, LevelType) * Gain;
                    totalBands++;
                }
            }

            // Additional bands to combine/add
            for (int i = 0; i < AdditionalBands.Length; i++)
            {
                float bandPosition = AdditionalBands[i];

                // Relative band position in spectrum
                float position = Mathf.Clamp01(bandPosition);

                bandIndex = Mathf.RoundToInt(position * spectrumReader.Levels.Length);
                level += spectrumReader.GetLevel(bandIndex, LevelType) * Gain;

                totalBands++;
            }

            // Average levels
            if(totalBands>1)
            {
                level /= (float)totalBands;
            }

            if (LevelType == AudioSpectrumReader.LevelType.Normalized)
            {
                MaxLevel = spectrumReader.NormalizedMultiplier * Gain;

                // TODO
                /*if (ClampNormalizedGain)
                {
                    float Position = Mathf.Clamp01(GetRangePosition(level, 1f, MaxLevel));
                    level = Mathf.Lerp(0f, 1f, Position);
                }*/
            }
            else
            {
                MaxLevel = AudioSpectrumReader.LevelMax * Gain;
            }

            if (Damping > 0f)
            {
                CurrentLevel = Mathf.SmoothDamp(CurrentLevel, level, ref dampingVelocity, Damping, Mathf.Infinity, deltaTime);
            }
            else
            {
                CurrentLevel = level;
            }

            return CurrentLevel;
        }
    }

    /// <summary>
    /// Group of bands that can read a whole spectrum or specific parts of it.  
    /// Read() must be called manually.
    /// </summary>
    [System.Serializable]
    public class AudioBandMeterGroup : ISerializationCallbackReceiver
    {
        public string Name;
        
        public List<AudioBandMeter> Bands = new List<AudioBandMeter>();

        [SerializeField]
        AudioBandMeter[] BandMeters;

        private Dictionary<string, AudioBandMeter> mapByName = new Dictionary<string, AudioBandMeter>();

        public void Read(AudioSpectrumReader spectrumReader, float deltaTime)
        {
            foreach (AudioBandMeter meter in BandMeters)
            {
                meter.Read(spectrumReader, deltaTime);
            }
        }

        public AudioBandMeter GetMeterByName(string name)
        {
            return mapByName[name];
        }

        #region ISerializationCallbackReceiver
        public void OnBeforeSerialize()
        {

        }

        public void OnAfterDeserialize()
        {
            foreach (AudioBandMeter meter in Bands)
            {
                if(mapByName.ContainsKey(meter.Name))
                {
                    Debug.LogWarning(this + " already had a band named '" + meter.Name + "'");
                    continue;
                }
                mapByName[meter.Name] = meter;
            }
        }
        #endregion
    }

}