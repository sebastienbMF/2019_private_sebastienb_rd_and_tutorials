﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory.Audio
{
    public interface IAudioEffect
    {
        /// <summary>
        /// The current normalized FX amount from 0 to 1. Zero means effect is unactive, 1 means at maximum strength
        /// </summary>
        float Amount { get; set; }
    }
}