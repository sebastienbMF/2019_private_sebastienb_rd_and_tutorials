﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory.Audio
{
    public class RandomAudioClip : MonoBehaviour
    {
        public AudioSource Source;

        public List<AudioClip> Clips = new List<AudioClip>();

        public int forceFirstIndex = -1;

        public bool AutoStart = true;

        private int currentIndex = -1;

        [Header("Editor controls")]
        public KeyCode NextSongKey = KeyCode.RightArrow;
        public KeyCode PreviousSongKey = KeyCode.LeftArrow;

        // Use this for initialization
        void Start()
        {
            if (Source == null) Source = GetComponent<AudioSource>();

            if (AutoStart)
            {
                if(forceFirstIndex>=0)
                {
                    PlayClip(Mathf.Min(forceFirstIndex, Clips.Count-1));
                } else
                {
                    PlayRandom();
                }
                
            }
        }

#if UNITY_EDITOR
        private void Update()
        {
            if(Input.GetKeyDown(PreviousSongKey))
            {
                Previous();
            } else if(Input.GetKeyDown(NextSongKey))
            {
                Next();
            }
        }
#endif

        public void PlayRandom()
        {
            if(Source==null)
            {
                Debug.LogError("Missing AudioSource for " + this);
                return;
            }

            PlayClip(GetRandomIndex());
        }

        public int GetRandomIndex()
        {
            int index = Random.Range(0, Clips.Count);

            // Avoid same index
            if(index == currentIndex && Clips.Count > 1)
            {
                return GetRandomIndex();
            }

            return index;
        }

        public void Next()
        {
            int clipIndex = currentIndex + 1;
            if (clipIndex >= Clips.Count) clipIndex = 0;

            PlayClip(clipIndex);
        }

        public void Previous()
        {
            int clipIndex = currentIndex - 1;
            if (clipIndex < 0) clipIndex = Clips.Count - 1;

            PlayClip(clipIndex);
        }


        public void PlayClip(int clipIndex)
        {
            currentIndex = clipIndex;   
            AudioClip clip = Clips[clipIndex];
            Source.clip = clip;
            Source.Play();
        }
    }
}