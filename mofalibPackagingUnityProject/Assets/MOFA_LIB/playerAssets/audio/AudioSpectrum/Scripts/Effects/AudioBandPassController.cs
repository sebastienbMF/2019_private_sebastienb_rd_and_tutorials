﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory.Audio
{
    /// <summary>
    /// Put next to an audio source, will control both a low-pass and hi-pass filters components depending on distance, FOV, etc.
    /// TODO: Find why hi-pass is so clunky in Unity... Maybe next audio engine version will make it better...
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(AudioLowPassFilter))]
    [RequireComponent(typeof(AudioHighPassFilter))]
    [ExecuteInEditMode]
    public class AudioBandPassController : BaseAudioEffectComponent
    {
        public const float FreqMin = 10f;
        public const float FreqMax = 22000f;
        public const float ResMin = 1f;
        public const float ResMax = 10f;

        public static Keyframe[] Keyframes_ZeroOneZero_InOut =
        {
            new Keyframe(0f, 0f, 0f, 0f, 0f, 0.5f),
            new Keyframe(0.5f, 1f, 0f, 0f, 0.5f, 0.5f),
            new Keyframe(1f, 0f, 0f, 0f, 0.5f, 0f),
        };

        public static Keyframe[] Keyframes_ZeroOneZero_Deadzones =
        {
            new Keyframe(0f, 0f, 0f, 0f, 0f, 0f),
            new Keyframe(0.2f, 0f, 0f, 0f, 0f, 0.5f),
            new Keyframe(0.5f, 1f, 0f, 0f, 0.5f, 0.5f),
            new Keyframe(0.8f, 0f, 0, 1f, 0.5f, 0f),
            new Keyframe(1f, 0f, 0f, 0f, 0f, 0f),
        };


        #region IAudioEffect

        /// <summary>
        /// The current normalized FX amount from 0 to 1. Zero means effect is unactive, 1 means at maximum strength
        /// </summary>
        override public float Amount
        {
            get { return 1f - Value; }
            set
            {
                Value = 1f - Mathf.Clamp01(value);
            }
        }

        #endregion

        [Tooltip("Bandpass One-Knob controller, zero means only bass are audible, 1 means all range is audible"), Range(0, 1)]
        public float Value = 1f;

        [Tooltip("Smoothing on Value"), Range(0, 0.5f)]
        public float ValueDamping = 0.1f;

        [Tooltip("For performance concern, when value > 0 do we disable the filters? Disabling when sound is audible could create more audible glitches than in DAWs")]
        public bool TogglesFiltersEnabled = false;

        [Header("Low Pass")]
        [Tooltip(""), Range(FreqMin, FreqMax)]
        public float MinLowPass = 250f;

        [Tooltip(""), Range(FreqMin, FreqMax)]
        public float MaxLowPass = 12800f;

        [Tooltip(""), Range(ResMin, ResMax)]
        public float MaxLowResonance = 1.5f;

        [Tooltip("Amount of resonance to apply when sweeping. Key values between 0 and 1, time between 0 and 1.")]
        public AnimationCurve LowPassResonanceCurve = new AnimationCurve(Keyframes_ZeroOneZero_InOut);

        [Header("High Pass")]
        [Tooltip("High-pass frequency sweeping in Unity is very low quality, so you can toggle it here, only usefull for very slow sweeps")]
        public bool UseHighPass = false;

        [Tooltip("When not active, how much of the low-frequencies will be cut-off"), Range(FreqMin, FreqMax)]
        public float MinHiPass = FreqMin;

        [Tooltip("When Active: how much of the low-frequencies to cut off (in the middle of the sweep range when using default HiPassAmountCurve curve)"), Range(FreqMin, FreqMax)]
        public float MaxHiPass = 5000f;

        [Tooltip("Amount of hi-pass filtering to apply when sweeping,  Key values between 0 and 1, time between 0 and 1.")]
        public AnimationCurve HiPassAmountCurve = new AnimationCurve(Keyframes_ZeroOneZero_Deadzones);

        private AudioLowPassFilter LowPassFilter;
        private AudioHighPassFilter HiPassFilter;

        private float currentValue;
        private float currentValueDampingVelocity = 0f;

        // Use this for initialization
        protected override void Start()
        {
            base.Start();

            LowPassFilter = GetComponent<AudioLowPassFilter>();
            HiPassFilter = GetComponent<AudioHighPassFilter>();
            currentValue = Value;
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();

            if (LowPassFilter == null) return;
            
            if(ValueDamping > 0f)
            {
                currentValue = Mathf.SmoothDamp(currentValue, Mathf.Clamp01(Value), ref currentValueDampingVelocity, ValueDamping, Mathf.Infinity, Time.unscaledDeltaTime);
            } else
            {
                currentValue = Mathf.Clamp01(Value);
            }

            // Low-pass automation
            float lowPassAmount = 1f - currentValue;
            if (TogglesFiltersEnabled) LowPassFilter.enabled = lowPassAmount > 0f;
            if (LowPassFilter.enabled)
            {
                LowPassFilter.cutoffFrequency = Mathf.Lerp(MaxLowPass, MinLowPass, lowPassAmount);

                if(LowPassResonanceCurve != null && LowPassResonanceCurve.keys.Length>1)
                {
                    LowPassFilter.lowpassResonanceQ = ResMin + (Mathf.Clamp01(LowPassResonanceCurve.Evaluate(currentValue)) * (MaxLowResonance - ResMin));
                }
            }

            // Hi-pass automation
            if(UseHighPass)
            {
                HiPassFilter.enabled = true;

                float hiPassAmount;
                if (LowPassResonanceCurve != null && LowPassResonanceCurve.keys.Length > 1)
                {
                    hiPassAmount = Mathf.Clamp01(HiPassAmountCurve.Evaluate(1f - currentValue));
                } else
                {
                    hiPassAmount = 1f - currentValue;
                }

                HiPassFilter.enabled = (TogglesFiltersEnabled) ? hiPassAmount > 0f : true;

                if (HiPassFilter.enabled)
                {
                    HiPassFilter.cutoffFrequency = Mathf.Lerp(MinHiPass, MaxHiPass, hiPassAmount);
                }
            }
            else
            {
                if (HiPassFilter != null) HiPassFilter.enabled = false;
            }
        }


    }
}