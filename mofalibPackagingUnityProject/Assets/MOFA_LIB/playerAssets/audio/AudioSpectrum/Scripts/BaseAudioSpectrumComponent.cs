﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory.Audio
{
    /// <summary>
    /// Component base for things that needs to read A SINGLE AudioBandMeter from a  AudioSpectrumReader
    /// To read all bands data, interact with AudioSpectrumReader directly
    /// </summary>
    public class BaseAudioSpectrumComponent : MonoBehaviour
    {
        [Header("Audio Spectrum Component")]
        [Tooltip("BandReaderGroup reader index, -1 = use CustomBandReader")]
        public string BandMeterName;

        [Header("Optional")]
        [Tooltip("Optional: Specify a band meter scriptable object REFERENCE")]
        public AudioBandsData BandMeterData;
        
        [Tooltip("Optional: will use AudioSpectrumReader.Main if not specified")]
        public AudioSpectrumReader Source;

        [Tooltip("Spectrum Band reading/interpretation")]
        public AudioBandMeter CustomBandMeter;
        
        [Header("Audio Spectrum Component Runtime")]
        [System.NonSerialized, Tooltip("Spectrum Band reading/interpretation")]
        public AudioBandMeter BandMeter;
        
        [System.NonSerialized]
        public float CurrentLevel = 0f;

        bool usingCustomBandReader = false;
        
        // Use this for initialization
        protected virtual void Start()
        {
            SetSource((Source != null) ? Source : AudioSpectrumReader.Main);
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            if (Source == null)
            {
                SetSource(AudioSpectrumReader.Main);
                return;
            }

            if(BandMeter != null)
            {
                if (usingCustomBandReader)
                {
                    if (Source == null) return;
                    BandMeter.Read(Source, Time.unscaledDeltaTime);
                }

                CurrentLevel = BandMeter.Read(Source, Time.deltaTime);
            }
        }

        void SetSource(AudioSpectrumReader SpectrumReader)
        {
            if (SpectrumReader == null) return;
            Source = SpectrumReader;

            if (!string.IsNullOrEmpty(BandMeterName))
            {
                BandMeter = Source.GetBandMeter(BandMeterName, BandMeterData);
            }
            else
            {
                BandMeter = CustomBandMeter;
                usingCustomBandReader = true;
            }
        }

    }
}



#if UNITY_EDITOR
namespace MomentFactory.Audio.Edition
{
    using UnityEditor;

    [CustomEditor(typeof(BaseAudioSpectrumComponent))]
    public class BaseAudioSpectrumComponent_Editor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();


            /// TODO: Add editor BandMeterName dropdown when AudioBandsData is specified... See AudioBandsData for editor class 
        }

    }
}
#endif