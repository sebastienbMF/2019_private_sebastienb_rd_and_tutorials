﻿using System.Collections.Generic;
using UnityEngine;

namespace MofaLib
{
    public class PartoucheInstanceRenderer : MonoBehaviour
    {
        public Mesh InstanceMesh;
        public Material InstanceMaterial;

        public Gradient RandomColorGradient;

        public Transform[] InstancesTransforms;

        // Property block data
        private MaterialPropertyBlock propertyBlock;
        private List<Matrix4x4> transformList;
        private Vector4[] emissionColorArray;
        private float[] tLerpArray;

        // Use this for initialization
        void Start()
        {
            this.transformList = new List<Matrix4x4>();

            this.propertyBlock = new MaterialPropertyBlock();
            this.emissionColorArray = new Vector4[InstancesTransforms.Length];
            this.tLerpArray = new float[InstancesTransforms.Length];

            for (int i = 0; i < InstancesTransforms.Length; ++i)
            {
                this.transformList.Add(InstancesTransforms[i].transform.localToWorldMatrix);

                this.emissionColorArray[i] = RandomColorGradient.Evaluate(Random.value);
                this.tLerpArray[i] = 0.0f;
            }

            this.propertyBlock.SetVectorArray("_EmissionColor", this.emissionColorArray);
        }

        // Update is called once per frame
        void Update()
        {
            for (int i = 0; i < InstancesTransforms.Length; ++i)
            {
                this.transformList[i] = InstancesTransforms[i].transform.localToWorldMatrix;

                this.tLerpArray[i] = Mathf.Abs(Mathf.Sin(Time.time));
            }

            this.propertyBlock.SetFloatArray("_TLerp", tLerpArray);

            Graphics.DrawMeshInstanced(InstanceMesh, 0, InstanceMaterial, this.transformList, this.propertyBlock);
        }
    }
}
