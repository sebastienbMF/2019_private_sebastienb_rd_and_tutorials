﻿Shader "Custom/CutoutShader"
{
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_SineFrequency("Sine Frequency", float) = 100.0
		_SinePhaseShift("Sine Phase Shift", float) = 0.0
		_XWeight("Shrinking position X", Range(-1,1)) = 1.0
		_YWeight("Shrinking position Y", Range(-1,1)) = 1.0
		_ZWeight("Shrinking position Z", Range(-1,1)) = 1.0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		Cull Off
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows addshadow 

		#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
			float3 worldPos;
		};

		// Use shader model 3.0 target, to get nicer looking lighting
		sampler2D _MainTex;
        sampler2D _NoiseTex;

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		float _SineFrequency;
		float _SinePhaseShift;
		float _TessFactor;
		float _XWeight;
		float _YWeight;
		float _ZWeight;

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			float firstSineValue = _SineFrequency * abs(_XWeight * IN.worldPos.x + _YWeight * IN.worldPos.y + _ZWeight * IN.worldPos.z) + _SinePhaseShift;
            float thirdSineValue = _SineFrequency * 0.5 * abs(_XWeight * IN.worldPos.x + _YWeight * IN.worldPos.y + _ZWeight * IN.worldPos.z);
            float fourthSineValue = abs(_XWeight * IN.worldPos.x + _YWeight * IN.worldPos.y + _ZWeight * IN.worldPos.z) + 32.146898129847 * _SinePhaseShift;
            float fifthSineValue = 0.22*_SineFrequency * abs(_XWeight * IN.worldPos.x + _YWeight * IN.worldPos.y + _ZWeight * IN.worldPos.z) + _SinePhaseShift;
            float secondSineValue = _SineFrequency * ((1.0 - _XWeight) * IN.worldPos.x + (1.0 - _YWeight) * IN.worldPos.y + (1.0 - _ZWeight) * IN.worldPos.z);
			
			clip(sin(firstSineValue) + cos(thirdSineValue) + cos(fourthSineValue) + cos(fifthSineValue) + 0 * 0.3 * sin(secondSineValue) - 0.69);

			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
		}
		ENDCG
	}
	Fallback "Diffuse"
}
