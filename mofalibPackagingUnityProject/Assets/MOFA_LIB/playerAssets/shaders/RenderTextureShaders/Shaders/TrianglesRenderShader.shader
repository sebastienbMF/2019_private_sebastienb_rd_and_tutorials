﻿Shader "MofaLib/RenderShaders/TrianglesRenderShader"
{
	Properties
	{
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)

		_Scale("Scale", Float) = 3
		_Speed("Speed", Float) = 1
		[Toggle]_UseGradient("Use Gradient", Float) = 1
		_GradientSpeed("Gradient Speed", Float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag

			#include "UnityCustomRenderTexture.cginc"

			fixed4 _Color1;
			fixed4 _Color2;

			float _Scale;
			float _Speed;
			float _GradientSpeed;
			bool _UseGradient;

			float2 random2(float2 p) {
				return frac(sin(float2(dot(p, float2(127.1, 311.7)), dot(p, float2(269.5, 183.3))))*43758.5453);
			}

			float rand(float2 p)
			{
				return frac(sin(dot(p, float2(12.9898, 78.233))) * 43758.5453);
			}

			float2 uv2tri(float2 uv)
			{
				float sx = uv.x - uv.y / 2; // skewed x
				float offs = step(frac(1 - uv.y), frac(sx));
				return float2(floor(sx) * 2 + offs, floor(uv.y));
			}

			// Based on https://github.com/keijiro/ShaderSketches/blob/master/Fragment/TriLattice8.glsl
			float4 frag(v2f_customrendertexture IN) : COLOR
			{
				float4 color = (float4)0.0;

				float2 uv = IN.globalTexcoord.xy * _Scale;

				float3 p = float3(dot(uv, float2(1, 0.5)), dot(uv, float2(-1, 0.5)), uv.y);
				float3 p1 = frac(+p);
				float3 p2 = frac(-p);

				// distance from borders
				float d1 = min(min(p1.x, p1.y), p1.z);
				float d2 = min(min(p2.x, p2.y), p2.z);
				float d = min(d1, d2);

				// border line
				float c = clamp((d - 0.04) * 1024, 0, 1);

				// gradient inside triangles
				float r = rand(uv2tri(uv));
				c *= abs(0.5 - frac(d + r + _Time.w * (_GradientSpeed * 0.1f) * 0.8)) * 2;

				// color variation
				float cb = sin(_Time.w * (_Speed * 0.1f) * 4.8 + r * 32.984) * 0.5 + 0.5;
				float3 rgb = lerp(_Color1, _Color2, cb);

				color = float4(rgb * (_UseGradient ? c : 1), 1);
				return color;
			}
			ENDCG
		}
	}
}
