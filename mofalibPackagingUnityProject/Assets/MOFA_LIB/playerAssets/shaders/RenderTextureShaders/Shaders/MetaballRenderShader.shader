﻿Shader "MofaLib/RenderShaders/MetaballRenderShader"
{
	Properties
	{
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)

		_Scale("Scale", Float) = 3
		_Speed("Speed", Float) = 1
		_Threshold("Threshold", Range(0,1)) = 0.06
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag

			#include "UnityCustomRenderTexture.cginc"

			fixed4 _Color1;
			fixed4 _Color2;

			float _Scale;
			float _Speed;
			float _Threshold;

			float2 random2(float2 p) {
				return frac(sin(float2(dot(p, float2(127.1, 311.7)), dot(p, float2(269.5, 183.3))))*43758.5453);
			}

			// Based on https://thebookofshaders.com/12/
			float4 frag(v2f_customrendertexture IN) : COLOR
			{
				float4 color = (float4)0.0;

				float2 st = IN.globalTexcoord.xy * _Scale;

				// Tile the space
				float2 i_st = floor(st);
				float2 f_st = frac(st);

				float m_dist = 1.;  // minimun distance
				for (int j = -1; j <= 1; j++) {
					for (int i = -1; i <= 1; i++) {
						// Neighbor place in the grid
						float2 neighbor = float2(float(i), float(j));

						// Random position from current + neighbor place in the grid
						float2 offset = random2(i_st + neighbor);

						// Animate the offset
						offset = 0.5 + 0.5*sin(_Speed * _Time.z + 6.2831*offset);

						// Position of the cell
						float2 pos = neighbor + offset - f_st;

						// Cell distance
						float dist = length(pos);

						// Metaball it!
						m_dist = min(m_dist, m_dist*dist);
					}
				}

				// Draw cells
				float m = step(_Threshold, m_dist);
				color = m * _Color1 + (1 - m) * _Color2;

				//color.rgb = IN.globalTexcoord;
				return color;
			}
			ENDCG
		}
	}
}
