﻿Shader "MofaLib/RenderShaders/MosaicRenderShader"
{
	Properties
	{
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)

		_Scale("Scale", Float) = 3
		_Speed("Speed", Float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag

			#include "UnityCustomRenderTexture.cginc"

			fixed4 _Color1;
			fixed4 _Color2;

			float _Scale;
			float _Speed;

			float random(float2 st) {
				return frac(sin(dot(st.xy, float2(12.9898, 78.233)))* 4.37585453123*_Time.x*_Speed);
			}

			// Based on https://thebookofshaders.com/12/
			float4 frag(v2f_customrendertexture IN) : COLOR
			{
				float2 st = IN.globalTexcoord.xy;

				st *= int(_Scale);
				float2 ipos = floor(st);  // get the integer coords

				float4 color = random(ipos) * _Color1 + (1 - random(ipos)) * _Color2;

				return color;
			}
			ENDCG
		}
	}
}
