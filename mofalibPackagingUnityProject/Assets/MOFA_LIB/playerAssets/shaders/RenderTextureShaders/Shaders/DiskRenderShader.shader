﻿Shader "MofaLib/RenderShaders/DiskRenderShader"
{
	Properties
	{
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)

		_Circle("Circle", Vector) = (0.5, 0.5, 1, 1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag

			#include "UnityCustomRenderTexture.cginc"

			fixed4 _Color1;
			fixed4 _Color2;
			float4 _Circle;

			float4 frag(v2f_customrendertexture IN) : COLOR
			{
				float4 color = _Color1;

				float2 st = IN.globalTexcoord.xy - _Circle.xy;
				if (length(st) > _Circle.z) color = _Color2;

				color.a = 1.0f;
				return color;
			}
			ENDCG
		}
	}
}
