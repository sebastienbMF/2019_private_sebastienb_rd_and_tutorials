﻿Shader "MofaLib/RenderShaders/FbmRenderShader"
{
	Properties
	{
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_Color3("Color3", Color) = (1,1,1,1)
		_Color4("Color4", Color) = (1,1,1,1)

		_NbOctaves("Nb Octaves", Float) = 5
		_Scale("Scale", Float) = 3
		_Speed("Speed", Float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag

			#include "UnityCustomRenderTexture.cginc"

			fixed4 _Color1;
			fixed4 _Color2;
			fixed4 _Color3;
			fixed4 _Color4;
			int _NbOctaves;
			float _Scale;
			float _Speed;

			float random(in float2 st)
			{
				return frac(sin(dot(st, float2(12.9898, 78.233)))*43758.5453123);
			}

			// Based on Morgan McGuire @morgan3d
			// https://www.shadertoy.com/view/4dS3Wd
			float noise(in float2 st)
			{
				float2 i = floor(st);
				float2 f = frac(st);

				// Four corners in 2D of a tile
				float a = random(i);
				float b = random(i + float2(1.0, 0.0));
				float c = random(i + float2(0.0, 1.0));
				float d = random(i + float2(1.0, 1.0));

				float2 u = f * f * (3.0 - 2.0 * f);

				return lerp(a, b, u.x) + (c - a)* u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
			}

			float fbm(in float2 st) 
			{
				float v = 0.0;
				float a = 0.5;
				float2 shift = (float2)100.0;
				// Rotate to reduce axial bias
				float2x2 rot = {cos(0.5), sin(0.5), -sin(0.5), cos(0.5)};

				for (int i = 0; i < _NbOctaves; ++i) 
				{
					v += a * noise(st);
					st = mul(rot, st);
					st = st* 2.0 + shift;
					a *= 0.5;
				}
				return v;
			}

			// Based on https://thebookofshaders.com/13/
			float4 frag(v2f_customrendertexture IN) : COLOR
			{
				float4 color = (float4)0.0;

				float2 st = IN.globalTexcoord.xy * _Scale;

				float2 q = (float2)0;
				q.x = fbm(st + 0.0*_Time.z);
				q.y = fbm(st + (float2)1.0);

				float2 r = (float2)0.0;
				r.x = fbm(st + 1.0*q + float2(1.7, 9.2) + 0.150*_Time.z*_Speed);
				r.y = fbm(st + 1.0*q + float2(8.3, 2.8) + 0.126*_Time.z*_Speed);

				float f = fbm(st + r);

				color = lerp(_Color1, _Color2, saturate((f*f)*4.0));
				color = lerp(color, _Color3, saturate(length(q)));
				color = lerp(color, _Color4, saturate(length(r)));
				color = (f*f*f + .6*f*f + .5*f)*color;

				color.a = 1.0;
				return color;
			}
			ENDCG
		}
	}
}
