﻿Shader "MofaLib/RenderShaders/SunRenderShader"
{
	Properties
	{
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_Intensity("Intensity", Float) = 2

		_Scale("Scale", Float) = 3
		_Speed("Speed", Float) = 1
		_SpeedRotation("SpeedRotation", Float) = 1
		_CenterDistFactor("Center Dist Factor", Float) = 1.5
	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag

			#include "UnityCustomRenderTexture.cginc"

			fixed4 _Color1;
			fixed4 _Color2;

			float _Scale;
			float _Speed;
			float _SpeedRotation;
			float _Intensity;
			float _CenterDistFactor;

			float2 random2(float2 p) 
			{
				return frac(sin(float2(dot(p, float2(127.1, 311.7)), dot(p, float2(269.5, 183.3))))*43758.5453);
			}


			float4 frag(v2f_customrendertexture IN) : COLOR
			{
				float COSPI4 = 0.707107;

				float4 color = _Color1;

				float2 uv = IN.globalTexcoord.xy - _Speed * float2(_SinTime.x*0.5 + 0.5, _CosTime.x*0.5+0.5);
				uv = IN.globalTexcoord.xy - float2(_CenterDistFactor * sin(_Speed * _Time.x)*0.5 + 0.5, _CenterDistFactor * cos(_Speed*_Time.x)*0.5 + 0.5);

				float angle = atan2(uv.x, uv.y) + _SpeedRotation * _Time.x;
				angle /= 3.14;

				float blur = frac(angle * (int)_Scale);
				angle = floor(angle * (int)_Scale);
				
				if (angle % 2 == 0) color = _Color2 * blur * (1-blur) * _Intensity;

				return color;
			}
			ENDCG
		}
	}
}
