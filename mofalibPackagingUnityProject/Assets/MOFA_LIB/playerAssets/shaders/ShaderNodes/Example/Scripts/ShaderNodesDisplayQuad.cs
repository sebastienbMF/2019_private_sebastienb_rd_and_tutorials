﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderNodesDisplayQuad : MonoBehaviour
{
    public MonoBehaviourTextureGenerator TextureGenerator;
    
    private Material m;

    // Start is called before the first frame update
    void Start()
    {
        m = new Material(Shader.Find("Unlit/TextureMultiply"));
        GetComponent<Renderer>().sharedMaterial = m;
    }

    // Update is called once per frame
    void Update()
    {
        if (m == null)
            return;
        if (TextureGenerator == null)
            return;

        m.SetTexture("_MainTex", TextureGenerator.GetTexture(0));
    }
}
