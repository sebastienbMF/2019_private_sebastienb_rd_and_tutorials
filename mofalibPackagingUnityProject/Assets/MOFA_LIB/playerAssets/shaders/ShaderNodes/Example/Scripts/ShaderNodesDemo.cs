﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mofalib.Rendering.ShaderPassAPI;
using mofalib.Rendering.Filters;
using mofalib.Rendering.ShaderNodes;
using mofalib.Rendering.Physics;

public class ShaderNodesDemo : MonoBehaviourTextureGenerator
{
    [Range(0, 1)]
    public float DisplayTextureSelector = 0;
    public int TextureIndex = 0;
    public float RampUpSpeed = 1.0F;
    public float RampDownSpeedExp = 0.99F;
    public float RampDownSpeedLin = 0.0001F;
    public Texture InputTexture;
    public ComputeShader fluidCompute;

    private List<ITextureGenerator> shaderPasses;

    public Shader externalForces;
    public Shader LineDrawingShader;
    public Shader TrailAccumulationShader;

    MousePaintPass inputPass;
    TextureAccumulationPass accumulationPass;
    BlurPass blurPass;
    GradientPass gradPass;
    FluidSimProcessor fluidSimPass;
    ReactionDiffusionPass reactPass;

    //New passes, from Panasonic Dev:
    DampingPass dampingPass;
    SimpleShaderPass lineDrawingPass;
    DoubleBufferShaderPass trailAccumulationPass;
    MultiInputPaintPass multiInPass;

    //Beta
    public PositionHistoryMonoBehaviour TrackerHistory;

    // Start is called before the first frame update
    void Start()
    {
        int ResX = Screen.width / 2;
        int ResY = Screen.height / 2;
        inputPass = new MousePaintPass(ResX, ResY, InputTexture);
        blurPass = new BlurPass(ResX, ResY, 5, 2, RenderTextureFormat.R8);
        gradPass = new GradientPass(ResX, ResY, RenderTextureFormat.RGFloat);
        accumulationPass = new TextureAccumulationPass(Screen.width, Screen.height, RenderTextureFormat.R16);
        fluidSimPass = new FluidSimProcessor(ResX, ResY, fluidCompute, externalForces);
        reactPass = new ReactionDiffusionPass(ResX, ResY, RenderTextureFormat.ARGBFloat);

        //New passes, from Panasonic Dev:
        lineDrawingPass = new SimpleShaderPass(LineDrawingShader, ResX, ResY, RenderTextureFormat.RG16);
        dampingPass = new DampingPass(ResX, ResY, RenderTextureFormat.R16);
        trailAccumulationPass = new DoubleBufferShaderPass(TrailAccumulationShader, ResX, ResY, RenderTextureFormat.R16);
        multiInPass = new MultiInputPaintPass(ResX, ResY);

        shaderPasses = new List<ITextureGenerator>();
        shaderPasses.Add(multiInPass);
        shaderPasses.Add(trailAccumulationPass);
        shaderPasses.Add(inputPass);
        shaderPasses.Add(blurPass);
        shaderPasses.Add(gradPass);
        shaderPasses.Add(accumulationPass);
        shaderPasses.Add(fluidSimPass);
        shaderPasses.Add(reactPass);
        shaderPasses.Add(lineDrawingPass);
        shaderPasses.Add(dampingPass);
    }

    public override Texture GetTexture(uint texNo)
    {
        if (shaderPasses ==null || shaderPasses.Count == 0)
            return null;

        return shaderPasses[TextureIndex].GetTexture(0);
    }

    void UpdateParameters()
    {
        if (shaderPasses == null || shaderPasses.Count == 0)
            return;

        TextureIndex = (int)(DisplayTextureSelector * (shaderPasses.Count - 1));

        inputPass.TextureScale = 0.5F;

        accumulationPass.RampDownSpeedExp = RampDownSpeedExp;
        accumulationPass.RampDownSpeedLin = RampDownSpeedLin;
        accumulationPass.RampUpSpeed = RampUpSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateParameters();

        List<Vector2> inputList = new List<Vector2>();
        inputList.Add(new Vector2(Input.mousePosition.x / Screen.width,
                                  Input.mousePosition.y / Screen.height));
        inputList.Add(new Vector2(Input.mousePosition.x / Screen.width+0.1F,
                                  Input.mousePosition.y / Screen.height));

        multiInPass.Update(inputList);

        inputPass.Update();

        blurPass.Update(delegate (Material m)
        {
            m.SetTexture("_MainTex", inputPass.GetTexture(0));
        });

        gradPass.Update(delegate (Material m)
        {
            m.SetTexture("_MainTex", blurPass.GetTexture(0));
        });

        accumulationPass.Update(delegate (Material m)
        {
            m.SetTexture("_MainTex", blurPass.GetTexture(0));
        });

        fluidSimPass.Update(delegate (Material m)
        {
            m.SetTexture("_ObstacleTex", blurPass.GetTexture(0));
            m.SetTexture("_GradientTex", gradPass.GetTexture(0));
        });

        reactPass.Update(delegate (Material m)
        {
            m.SetTexture("_MainTex", blurPass.GetTexture(0));
        });

        lineDrawingPass.Update(delegate (Material m)
        {
            var h = TrackerHistory.GetHistory();
            if (h.Count >= 2)
            {
                var itA = TrackerHistory.GetHistory().First;
                var itB = itA.Next;
                m.SetVector("_LineP1P2", new Vector4(itA.Value.x, itA.Value.y, itB.Value.x, itB.Value.y));
            }
        });

        dampingPass.Update(delegate (Material m)
        {
            m.SetTexture("_MainTex", lineDrawingPass.GetTexture(0));
        });

        trailAccumulationPass.Update(delegate (Material m)
        {
            //m.SetVector("_Mouse", new Vector4((float)(Input.mousePosition.x) / (float)Screen.width,
            //                                (float)(Input.mousePosition.y) / (float)Screen.height, 1.0F, 1.0F));

            m.SetTexture("_MainTex", multiInPass.GetTexture(0));
        });
    }
}
