﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetaballCompute : MonoBehaviour
{
    public RenderTexture TextureToCompute;
    public ComputeShader MetaballComputeShader;

    public int GridDim = 64;

    public float[] SpheresRadius;
    private ComputeBuffer _spheresRadiusBuffer;

    private int _numberOfSpheres = 4;
    public Vector3[] SpheresPositions;
    private ComputeBuffer _spheresPositionsBuffer;

    public MarchingCubes MarchingCubesToUse;
    // Start is called before the first frame update
    void Start()
    {
        _numberOfSpheres = SpheresPositions.Length;
        _spheresPositionsBuffer = new ComputeBuffer(_numberOfSpheres, sizeof(float) * 3);
        _spheresPositionsBuffer.SetData(SpheresPositions);

        _spheresRadiusBuffer = new ComputeBuffer(_numberOfSpheres, sizeof(float));
        _spheresRadiusBuffer.SetData(SpheresRadius);

        RenderTextureDescriptor descriptor = new RenderTextureDescriptor();
        descriptor.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        descriptor.width = GridDim;
        descriptor.height = GridDim;
        descriptor.volumeDepth = GridDim;
        descriptor.enableRandomWrite = true;
        descriptor.colorFormat = RenderTextureFormat.ARGBFloat;  //use ARGB32 for better performance but less precise gradient & normal computation
        descriptor.msaaSamples = 1;

        TextureToCompute = new RenderTexture(descriptor);
        TextureToCompute.name = "MetaBalls";
        TextureToCompute.anisoLevel = 0;
        TextureToCompute.filterMode = FilterMode.Bilinear;
        TextureToCompute.Create();

        ComputeShaderDispatch();
    }

    // Update is called once per frame
    void Update()
    {
        ComputeShaderDispatch();
        MarchingCubesToUse.TextureToRender = TextureToCompute;
    }

    private void ComputeShaderDispatch()
    {
        int kernelHandle = MetaballComputeShader.FindKernel("CSMain");
        
        _spheresPositionsBuffer.SetData(SpheresPositions);
        _spheresRadiusBuffer.SetData(SpheresRadius);

        MetaballComputeShader.SetFloat("_TimeSec", Time.time);
        MetaballComputeShader.SetInt("_GridDim", GridDim);
        MetaballComputeShader.SetInt("_NumberOfSpheres", _numberOfSpheres);
        MetaballComputeShader.SetBuffer(kernelHandle, "_SpheresPositionsBuffer", _spheresPositionsBuffer);
        MetaballComputeShader.SetBuffer(kernelHandle, "_SpheresRadiusBuffer", _spheresRadiusBuffer);
        MetaballComputeShader.SetTexture(kernelHandle, "Result", TextureToCompute);
        MetaballComputeShader.Dispatch(kernelHandle, GridDim / 8, GridDim / 8, GridDim / 8);
    }

    private void OnDestroy()
    {
        _spheresPositionsBuffer.Release();
        _spheresRadiusBuffer.Release();
    }
}
