Shader "Custom/MarchingCubes"
{
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Cull Off

        CGPROGRAM

        #pragma surface surf Standard fullforwardshadows vertex:vert
        #pragma target 5.0

        #include "UnityCG.cginc"

        struct appdata
        {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
            float4 tangent : TANGENT;
            float4 color : COLOR;
            float4 texcoord1 : TEXCOORD1;
            float4 texcoord2 : TEXCOORD2;
            uint vid : SV_VertexID;
        };

        struct Input
        {
            float vface : VFACE;
            float4 color : COLOR;
        };

        half4 _Color;
        half _Smoothness;
        half _Metallic;
        half3 _Emission;


        struct Vert
        {
            float4 position;
            float3 normal;
        };

        #ifdef SHADER_API_D3D11
                uniform StructuredBuffer<Vert> _Buffer;
        #endif

        void vert(inout appdata v)
        {
            #ifdef SHADER_API_D3D11
                Vert vert = _Buffer[v.vid];
                v.vertex = vert.position;
                v.normal = vert.normal;
                v.color.xyz = float3(1.0,1.0,1.0);
            #endif
        }

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            o.Albedo = _Color;
            o.Metallic = _Metallic;
            o.Smoothness = _Smoothness;
            o.Emission = _Emission;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
