﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRenderQueueLayer : MonoBehaviour
{
    public string TagName = "";

    // [Tooltip("Background=1000, Geometry=2000, AlphaTest=2450, Transparent=3000, Overlay=4000")]
    public int RenderQueueOrder = 3005;

    void Start()
    {
        AddThisScriptLayerToList(this);
        GetRenderOrder();
    }

    // just for a ref when we want to update all layer object after a list change
    public void AddThisScriptLayerToList(SetRenderQueueLayer layerScript)
    {
        if (RenderQueueLayerManager.Instance._setLayerRefList.Contains(layerScript))
        {
            Debug.LogWarningFormat("SetLayer has already been added to the list!", gameObject.name);
        }
        else
        {
            RenderQueueLayerManager.Instance._setLayerRefList.Add(layerScript);
        }
    }

    public void GetRenderOrder(RenderQueueLayerManager manager = null)
    {
        if (manager == null) manager = RenderQueueLayerManager.Instance;

        int orderIndex = manager.list.Count - manager.list.FindIndex(v => v.stringvalue.ToLower() == TagName.ToLower());
        if (orderIndex == -1) { Debug.LogError("GO: " + gameObject.name + " - LAYER TAG NOT FOUND!"); return; }

        Renderer renderer = GetComponent<Renderer>();
        if (renderer || renderer.sharedMaterial)
        {
            renderer.sharedMaterial.renderQueue = manager.BaseRenderQueueOrder;
            for (int i = 0; i < renderer.sharedMaterials.Length; i++)
            {
                renderer.sharedMaterials[i].renderQueue = manager.BaseRenderQueueOrder + orderIndex;
            }
        }
    }
}