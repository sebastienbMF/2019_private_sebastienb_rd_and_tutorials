﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(RenderQueueLayerManager))]
public class RenderQueueLayerManagerEditor : Editor
{
    public ReorderableList reorderableList = null;

    private RenderQueueLayerManager layerManager
    {
        get
        {
            return target as RenderQueueLayerManager;
        }
    }

    private void OnEnable()
    {
        reorderableList = new ReorderableList(layerManager.list, typeof(LayerListClass), true, true, true, true);

        reorderableList.drawHeaderCallback += DrawHeader;
        reorderableList.drawElementCallback += DrawElement;

        reorderableList.onAddCallback += AddItem;
        reorderableList.onRemoveCallback += RemoveItem;
    }

    private void OnDisable()
    {
        // Make sure we don't get memory leaks etc.
        if (reorderableList != null)
        {
            reorderableList.drawHeaderCallback -= DrawHeader;
            reorderableList.drawElementCallback -= DrawElement;

            reorderableList.onAddCallback -= AddItem;
            reorderableList.onRemoveCallback -= RemoveItem;
        }
    }

    /// <summary>
    /// Draws the header of the list
    /// </summary>
    /// <param name="rect"></param>
    private void DrawHeader(Rect rect)
    {
        GUI.Label(rect, "LAYERS");
    }

    /// <summary>
    /// Draws one element of the list (ListItemExample)
    /// </summary>
    /// <param name="rect"></param>
    /// <param name="index"></param>
    /// <param name="active"></param>
    /// <param name="focused"></param>
    private void DrawElement(Rect rect, int index, bool active, bool focused)
    {
        LayerListClass item = layerManager.list[index];

        EditorGUI.BeginChangeCheck();
        item.stringvalue = EditorGUI.TextField(new Rect(rect.x, rect.y, rect.width, rect.height), item.stringvalue);
        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(target);
        }
    }

    private void AddItem(ReorderableList list)
    {
        layerManager.list.Add(new LayerListClass());
        EditorUtility.SetDirty(target);
    }

    private void RemoveItem(ReorderableList list)
    {
        layerManager.list.RemoveAt(list.index);
        EditorUtility.SetDirty(target);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();
        if (reorderableList != null) reorderableList.DoLayoutList();

        EditorGUILayout.Space();
        if (GUILayout.Button("Update Renderer Queues"))
        {
            RenderQueueLayerManager myTarget = (RenderQueueLayerManager)target;
            if (myTarget != null)
            {
                myTarget._setLayerRefList.Clear();
                object[] obj = GameObject.FindObjectsOfType(typeof(SetRenderQueueLayer));
                foreach (object o in obj)
                {
                    myTarget._setLayerRefList.Add((SetRenderQueueLayer)o);
                }
                myTarget.RefreshLayerRenderQueues();
            }
        }
    }
}