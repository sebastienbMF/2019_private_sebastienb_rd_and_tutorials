﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LayerListClass
{
    [SerializeField]
    public string stringvalue;
}

public class RenderQueueLayerManager : Singleton<RenderQueueLayerManager>
{

    [HideInInspector]
    public List<LayerListClass> list;

    [HideInInspector]
    public List<SetRenderQueueLayer> _setLayerRefList;

    bool _lateStartDone = false;

    public int BaseRenderQueueOrder = 3005;


    void Start()
    {
        RefreshLayerRenderQueues();
    }

    void Update()
    {
        // Be sure that the layer are not overrided on start
        if (!_lateStartDone)
        {
            RefreshLayerRenderQueues();
            _lateStartDone = true;
        }
    }

    public void RefreshLayerRenderQueues()
    {
        for (int i = 0; i <= _setLayerRefList.Count - 1; i++)
        {
            if (_setLayerRefList[i] != null)
                _setLayerRefList[i].GetRenderOrder(this);
        }
    }


}
