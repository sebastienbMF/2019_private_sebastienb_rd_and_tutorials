﻿using System.Collections;
using System.Collections.Generic;
using System;

using UnityEngine;


public class ComputeImageGradient : MonoBehaviour, ITextureGenerator
{
    public enum Kernels
    {
       BlurX = 0,
       BlurY = 1,
       Gradient = 2
    }

    public ComputeShader Compute;
    public Texture InputTexture;

    [Range(0, 250)]
    public int SampleRad;
    [Range(0, 250)]
    public float GaussSigma;

    private float[] coeffs;
    RenderTexture BlurX;
    RenderTexture BlurXY;
    RenderTexture GradientResult;

    RenderTexture AllocateBuffer(int width, int height, RenderTextureFormat fmt)
    {
        var rt = new RenderTexture(width, height, 0, fmt);
        rt.enableRandomWrite = true;
        rt.Create();
        return rt;
    }

    void Start ()
    {
        BlurX = AllocateBuffer(InputTexture.width, InputTexture.height, RenderTextureFormat.RFloat);
        BlurXY = AllocateBuffer(InputTexture.width, InputTexture.height, RenderTextureFormat.RFloat);
        GradientResult = AllocateBuffer(InputTexture.width, InputTexture.height, RenderTextureFormat.RGFloat);
    }

    void OnDestroy()
    {
        Destroy(BlurX);
        Destroy(BlurXY);
        Destroy(GradientResult);
    }
    
    private float[] CreateGaussianCoefficients(int RAD, float Sigma)
    {
        Sigma = Math.Max(Sigma, 0.01f);
        float[] coeffs = new float[RAD + 1];
        float denom = 1 / (2 * (float)Math.PI);

        for (int i = 0; i <= RAD; ++i)
        {
            coeffs[i] = (float)(denom * Math.Exp(-0.5 * (float)(i * i) / (Sigma * Sigma)) / Sigma);
        }
        return coeffs;
    }
    
    void Update ()
    {
        int ThreadCountX = (InputTexture.width + 7) / 8;
        int ThreadCountY = (InputTexture.height + 7) / 8;
        
        float[] coeffs = CreateGaussianCoefficients(SampleRad, GaussSigma);
        
        //Blur X
        Compute.SetFloats("Coeffs", coeffs);
        Compute.SetInt("RAD", SampleRad);
        Compute.SetTexture((int)Kernels.BlurX, "InputImage", InputTexture);
        Compute.SetTexture((int)Kernels.BlurX, "Result", BlurX);
        Compute.Dispatch((int)Kernels.BlurX, ThreadCountX, ThreadCountY, 1);

        //Blur Y
        Compute.SetTexture((int)Kernels.BlurY, "InputImage", BlurX);
        Compute.SetTexture((int)Kernels.BlurY, "Result", BlurXY);
        Compute.Dispatch((int)Kernels.BlurY, ThreadCountX, ThreadCountY, 1);

        //Gradient
        BlurXY.wrapMode = TextureWrapMode.Clamp;
        Compute.SetTexture((int)Kernels.Gradient, "InputImage", BlurXY);
        Compute.SetTexture((int)Kernels.Gradient, "Result", GradientResult);
        Compute.Dispatch((int)Kernels.Gradient, ThreadCountX, ThreadCountY, 1);
    }

    public Texture GetTexture(uint texNo)
    {
        if(texNo==0)
            return GradientResult;
        else
            return BlurXY;
    }
}