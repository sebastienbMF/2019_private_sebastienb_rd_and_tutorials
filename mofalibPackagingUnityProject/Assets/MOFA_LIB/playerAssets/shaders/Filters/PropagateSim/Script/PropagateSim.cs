﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropagateSim : MonoBehaviour
{
    public Texture mask;
    private float lastUpdate;
    private mofalib.Rendering.ShaderPassAPI.DoubleBufferShaderPass shaderPass;

    // Start is called before the first frame update
    void Start()
    {
        lastUpdate = Time.time;
        shaderPass = new mofalib.Rendering.ShaderPassAPI.DoubleBufferShaderPass("Unlit/PropagateSim", 512, 512);
    }

    // Update is called once per frame
    void Update()
    {
        if((Time.time-lastUpdate)>0.010)
        {
            print("UpdateShader!");
            shaderPass.GetMaterial().SetTexture("_maskTex", mask);
            shaderPass.Update();
            lastUpdate = Time.time;
        }
            
        GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex", shaderPass.GetTexture(0));
    }
}
