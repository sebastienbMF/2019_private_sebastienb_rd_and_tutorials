using UnityEngine;

[ExecuteInEditMode]
public class SinglePostProcess : MonoBehaviour 
{
	public Material PostProcessingMaterial;

	// Postprocess the image
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{
		Graphics.Blit (source, destination, PostProcessingMaterial);
	}	
}
