﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ComputeWaterRipples : MonoBehaviour
{
    public ComputeShader Compute;
    public int ComputeResolutionX;
    public int ComputeResolutionY;

    enum Kernels
    {
        Ripples = 0
    }

    private RenderTexture ripplesTexA;
    private RenderTexture ripplesTexB;
    private int renderCount;

    RenderTexture AllocateBuffer(int width, int height, RenderTextureFormat fmt)
    {
        var rt = new RenderTexture(width, height, 0, fmt);
        rt.enableRandomWrite = true;
        rt.Create();
        return rt;
    }

    void Start()
    {
        renderCount = 0;
        ripplesTexA = AllocateBuffer(ComputeResolutionX, ComputeResolutionY, RenderTextureFormat.RGFloat);
        ripplesTexB = AllocateBuffer(ComputeResolutionX, ComputeResolutionY, RenderTextureFormat.RGFloat);
    }

    void OnDestroy()
    {
        Destroy(ripplesTexA);
        Destroy(ripplesTexB);
    }

    public RenderTexture RipplesInput()
    {
        return (renderCount % 2 == 1) ? ripplesTexA : ripplesTexB;
    }

    public RenderTexture RipplesOutput()
    {
        return (renderCount % 2 == 1) ? ripplesTexB : ripplesTexA;
    }

    void Update()
    {
        int ThreadCountX = (ComputeResolutionX + 7) / 8;
        int ThreadCountY = (ComputeResolutionY + 7) / 8;

        //Ripples processing
        ++renderCount;
        
        Compute.SetFloat("StartTimeSec", Time.time);
        Compute.SetVector("MouseState", new Vector4(Input.mousePosition.x * RipplesInput().width / ((float)Screen.width),
                                                    Input.mousePosition.y * RipplesInput().height / ((float)Screen.height),
                                                    Input.GetMouseButton(0) ? 1.0f : 0.0f,
                                                    Input.GetMouseButton(1) ? 1.0f : 0.0f));
        Compute.SetTexture((int)Kernels.Ripples, "RipplesIn", RipplesInput());
        Compute.SetTexture((int)Kernels.Ripples, "RipplesOut", RipplesOutput());
        Compute.Dispatch((int)Kernels.Ripples, ThreadCountX, ThreadCountY, 1);
    }

    public Texture getOutputTexture()
    {
       return RipplesOutput();
    }
}
