﻿Shader "Unlit/WaterRippleReflect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _WaterHeightMap ("Water Height Map", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _WaterHeightMap;
            float4 _HeightMapResolution;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float waterHeight(float2 uv)
            {
                return tex2D(_WaterHeightMap, uv).r/*+ambientNoise(uv, _Time.y)*/;
            }
            
            float3 grad(float2 uv, float2 iResolution)
            {
                float3 e = float3(float2(1,1)/iResolution.xy,0.);
                float p10 = waterHeight(uv-e.zy).x;
                float p01 = waterHeight(uv-e.xz).x;
                float p21 = waterHeight(uv+e.xz).x;
                float p12 = waterHeight(uv+e.zy).x;
                return normalize(float3(p21 - p01, p12 - p10, 1.));
            }
            
            float dLine(float2 p, float2 a, float2 b){
                float2 ab = b-a;
                return length(p-a-clamp(dot(p-a,ab)/dot(ab,ab),0.0,1.0)*ab);
            }

            float smoothInternalQuadGrad(float2 uv, float2 c1, float2 c2, float2 c3, float2 c4)
            {
                float da = dLine(uv, c3, c2);
                float db = dLine(uv, c2, c1);
                float dc = dLine(uv, c1, c4);
                float dd = dLine(uv, c4, c3);
                
                //Note : a parameter controls smoothness (and scales value)
                float a = 0.01;
                float NORMALIZATION_TERM = log((1.+a)/a);
                da = log((da+a)/a)/NORMALIZATION_TERM;
                db = log((db+a)/a)/NORMALIZATION_TERM;
                dc = log((dc+a)/a)/NORMALIZATION_TERM;
                dd = log((dd+a)/a)/NORMALIZATION_TERM;
                return da*db*dc*dd;
            }
            
            fixed4 frag(v2f i) : SV_Target
            {
                float2 iResolution = _HeightMapResolution;
                
                // sample the texture
                float2 uv = i.uv;
                fixed4 col = tex2D(_MainTex, uv);
                float h = tex2D(_WaterHeightMap, uv).x;
                float3 g = grad(uv,iResolution.xy);
                
                float dc = smoothInternalQuadGrad(uv, float2(1,1),float2(1,0),float2(0,0),float2(0,1));
                float4 c = tex2D(_MainTex, uv + g.xy*1.75*max(dc-0.1,0.));
                
                // Totally fake displacement and shading:
                float3 light = normalize(float3(.2,-.5,.7));
                float diffuse = dot(g,light);
                float spec = (0.25+0.75*dc)*pow(max(0.,-reflect(light,g).z),32.);
                
                col = lerp(c,float4(.4,.75,1.,1.),.25*dc)*max(diffuse,0.) + spec;
                
                float2 uvVignette = uv * 4. - 5./ _HeightMapResolution;

                if(uvVignette.x>0.&& uvVignette.x < 1. && uvVignette.y > 0. && uvVignette.y < 1.)
                    return tex2D(_WaterHeightMap, uvVignette);
                
                return col;
            }
            ENDCG
        }
    }
}
