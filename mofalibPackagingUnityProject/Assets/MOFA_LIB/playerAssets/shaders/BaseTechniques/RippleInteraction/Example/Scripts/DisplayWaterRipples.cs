﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayWaterRipples : MonoBehaviour {

    public GameObject ComputeShader;
    public Shader ReflectShader;
    public Texture InputTexure;
    private Material reflectMaterial;

    void Start ()
    {
        reflectMaterial = new Material(ReflectShader);
    }
   
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        ComputeWaterRipples compShader = ComputeShader.GetComponent(typeof(ComputeWaterRipples)) as ComputeWaterRipples;

        reflectMaterial.SetVector("_HeightMapResolution", new Vector4(compShader.getOutputTexture().width,
                                                                      compShader.getOutputTexture().height));
        reflectMaterial.SetTexture("_WaterHeightMap", compShader.getOutputTexture());

        Graphics.Blit(InputTexure, destination, reflectMaterial, 0);
    }
}

