﻿Shader "Unlit/s_seeThrough001"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 200

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Debug/debugUtilities.hlsl"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/ProceduralTextures/Checkerboard.hlsl"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Raytracing/intersections.hlsl"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/Cg/utilities.cginc"
            
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 screenuv : TEXCOORD1;
                float3 viewDir : TEXCOORD2; //as seen in FXWaterPro.shader (Standard Asset - Water)
                float4 vertex : SV_POSITION;
            };

            sampler2D _CameraDepthTexture;
            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.screenuv = ComputeScreenPos(o.vertex);
                o.viewDir.xyz = WorldSpaceViewDir(v.vertex);
                return o;
            }

            float3 add(const float3 a, const float3 b) {
                if (a.x < b.x)
                    return a;
                return b;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                //Raytracing parameters
                float3 ro = _WorldSpaceCameraPos;     //ro = Ray origin
                float3 rd = normalize(-i.viewDir.xyz);//rd = Ray direction
                float3 camDir = mofalib_CameraDirection ();

                //Existing (rendered) opaque scene items
                float opaqueZ = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenuv)).r);
                float dOpaque = opaqueZ / dot(camDir,rd); //Opaque Item distance from camera

                //Display camera direction coordinates (text color)
                float4 cText = mofalib_printFloat3(i.uv, camDir, 5.0, _ScreenParams.xy);
                if (cText.a > 0)
                    return cText;

                //<XYZ(RGB) debug axis>
                {
                    float3 p = float3(0, 0, 0); //Position of the x,y,z raytraced gizmo
                    if (mofalib_RayBoxIntersecUV(ro - p - float3(0.5,0,0),rd,float3(0.50,0.05,0.05)).x < MOFALIB_MAX_INTERSECTION_DIST) return fixed4(1,0,0,1);
                    if (mofalib_RayBoxIntersecUV(ro - p - float3(0,0.5,0),rd,float3(0.05,0.50,0.05)).x < MOFALIB_MAX_INTERSECTION_DIST) return fixed4(0,1,0,1);
                    if (mofalib_RayBoxIntersecUV(ro - p - float3(0,0,0.5),rd,float3(0.05,0.05,0.50)).x < MOFALIB_MAX_INTERSECTION_DIST) return fixed4(0,0,1,1);
                }
                //</XYZ(RGB) debug axis>

                //<Raytraced items>
                //h = hit information (distance,uvx,uvy)
                float3 planeUVScale = float3(1, 10, 10); //This vector scales the plane (UVx, UVy) by a factor of 10.
                float3 h = float3(1, 5, 5)*mofalib_RaySphIntersecUV(ro - float3(-2.00, 0,  0),rd,1.0);
                h = mofalib_addPrimitive(h, float3(1, 5, 5)*mofalib_RaySphIntersecUV(ro - float3( 2.25, 1, -2),rd,0.501));
                h = mofalib_addPrimitive(h, float3(1,20, 1)*mofalib_RayBoxIntersecUV(ro - float3( 0.00, 0,  0),rd,float3(10.5,0.5,0.5)));
                h = mofalib_addPrimitive(h, mofalib_RayPlaneXZIntersecUV(ro,rd,float2(10,10))*planeUVScale);
                //</Raytraced items>

                //Composition (mixing elements)
                float dRayTraced = h.x; //Raytraced item distance from camera
                float2 hit_uv = h.yz/5.0;

                //If raytraced items are in front of opaque scene items
                if (dRayTraced < dOpaque)
                {
                    float3 c = (float3)(mofalib_Checkerboard(hit_uv));
                    return fixed4(c, 1);
                }
                
                //Opaque item is in front, return zero alpha to prevent masking it.
                return dOpaque<100.?float4(0,0,0,0): float4(0,0,0,1);
            }
            ENDCG
        }
    }
}
