﻿Shader "Unlit/s_plane"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Raytracing/intersections.hlsl"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Debug/debugUtilities.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float3 viewDir : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.viewDir.xyz = WorldSpaceViewDir(v.vertex);
                return o;
            }

            float3 add(const float3 a, const float3 b) {
                if (a.x<b.x)
                    return a;
                return b;
            }

            float3 r(float3 p)
            {
                float a = _Time.y;
                float b = _Time.y*.25;
                return p; //Comment to test rotation
                p.xz = float2(p.x*cos(a) + p.z*sin(a),
                              p.z*cos(a) - p.x*sin(a));

                p.yz = float2(p.y*cos(b) + p.z*sin(b),
                              p.z*cos(b) - p.y*sin(b));

                return p;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //Raytracing parameters
                float3 ro = _WorldSpaceCameraPos;
                float3 rd = normalize(-i.viewDir.xyz);
                float3 camDir = -UNITY_MATRIX_V[2].xyz; //View Matrix trick : extract look at (-Z)

                //Raytraced objects positions
                float3 sphPos1  = float3( 2.00,-2.00,-15.00);
                float3 sphPos2  = float3( 2.00,-4.00,-15.00);
                float3 cylPos1  = float3(-4.65, 3.27, -0.64);
                float3 boxPos1  = float3( 2.56, 1.23, -2.89);
                float3 boxPos2  = float3( 2.56, 4.00, -2.89);
                float3 planePos = float3(-2.56,-1.00, -2.89);

                //Note h = hit info (hit distance, uvx, uvy)
                float3 h = float3(mofalib_RaySphIntersecUV(r(ro - sphPos1), r(rd), 1.));
                h = add(h, float3(mofalib_RaySphIntersecUV(r(ro - sphPos2), r(rd), 1.)));
                h = add(h, float3(mofalib_RayCylIntersecUV(r(ro - cylPos1), r(rd), float3(0, 1, 0), float3(0, -1, 0), 1.)));
                h = add(h, float3(mofalib_RayBoxIntersecUV(r(ro - boxPos1), r(rd), (float3)1.5)));
                h = add(h, float3(mofalib_RayBoxIntersecUV(r(ro - boxPos2), r(rd), (float3)0.5)));
                h = add(h, float3(mofalib_RayPlaneXZIntersecUV(r(ro - planePos), r(rd), .1*float2(16,9))));

                float distanceToFloor = mofalib_RayPlaneIntersec(ro, rd, float3(0, -30, 0), float3(0, 1, 0));
                
                //If an object hit position (h) is in front of the floor
                if (h.x < MOFALIB_MAX_INTERSECTION_DIST && h.x < distanceToFloor)
                {
                    float2 uv = h.yz;
                    return float4(uv, h.x / 30., 1);
                }

                //Display camera direction coordinates
                float4 cText = mofalib_printFloat3(i.uv, camDir, 5., _ScreenParams.xy);
                if (cText.a > 0)
                {
                    return cText;
                }

                //Draw gray gradient floor (fade with distance)
                if (distanceToFloor < h.x)
                    return float4((float3)saturate(1. - distanceToFloor / 150), 1);
                
                return fixed4(0, 0, 0, 1);
            }
            ENDCG
        }
    }
}
