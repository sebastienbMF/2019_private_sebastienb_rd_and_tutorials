﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkerboard : MonoBehaviour {

    public RenderTexture CheckerboardTexture;
    public Shader CheckerboardShader;

    private Material mat;
    // Use this for initialization

    void Start () {
        mat = new Material(CheckerboardShader);
    }
    
    // Update is called once per frame
    void Update () {

        Graphics.Blit(null, CheckerboardTexture, mat, 0);
    }
}
