﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplaySimpleComputeShader : MonoBehaviour {

    public GameObject _computeShader;
    public Shader _displayShader;
    Material _matDisplay;
    
    void Start ()
    {
        _matDisplay = new Material(_displayShader);
    }
   
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        SimpleComputeShader compShader = _computeShader.GetComponent(typeof(SimpleComputeShader)) as SimpleComputeShader;
        Graphics.Blit(compShader.getOutputTexture(), destination, _matDisplay, 0);
    }
}
