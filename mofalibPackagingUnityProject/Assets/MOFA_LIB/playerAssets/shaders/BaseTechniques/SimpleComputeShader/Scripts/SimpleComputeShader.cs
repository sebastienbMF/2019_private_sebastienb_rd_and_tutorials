﻿using System.Collections;
using System.Collections.Generic;
using System;

using UnityEngine;

static class Kernels
{
    public const int BlurX = 0;
    public const int BlurY = 1;
}

public class SimpleComputeShader : MonoBehaviour {

    public ComputeShader Compute;
    public Texture InputTexture;

    [Range(0, 250)]
    public int SampleRad;
    [Range(0, 250)]
    public float GaussSigma;

    private float[] coeffs;
    RenderTexture outputTextureA;
    RenderTexture outputTextureB;

    RenderTexture AllocateARGBBuffer(int width, int height)
    {
        var rt = new RenderTexture(width, height, 0, RenderTextureFormat.ARGBHalf);
        rt.enableRandomWrite = true;
        rt.Create();
        return rt;
    }

    void Start ()
    {
        outputTextureA = AllocateARGBBuffer(InputTexture.width, InputTexture.height);
        outputTextureB = AllocateARGBBuffer(InputTexture.width, InputTexture.height);
    }

    void OnDestroy()
    {
        Destroy(outputTextureA);
        Destroy(outputTextureB);
    }
    
    private float[] CreateGaussianCoefficients(int RAD, float Sigma)
    {
        Sigma = Math.Max(Sigma, 0.01f);
        float[] coeffs = new float[RAD + 1];
        float denom = 1 / (2 * (float)Math.PI);

        for (int i = 0; i <= RAD; ++i)
        {
            coeffs[i] = (float)(denom * Math.Exp(-0.5 * (float)(i * i) / (Sigma * Sigma)) / Sigma);
        }
        return coeffs;
    }
    
    void Update ()
    {
        int ThreadCountX = (InputTexture.width + 7) / 8;
        int ThreadCountY = (InputTexture.height + 7) / 8;
        
        float[] coeffs = CreateGaussianCoefficients(SampleRad, GaussSigma);
        
        //Blur X
        Compute.SetFloats("Coeffs", coeffs);
        Compute.SetInt("RAD", SampleRad);
        Compute.SetTexture(Kernels.BlurX, "InputImage", InputTexture);
        Compute.SetTexture(Kernels.BlurX, "Result", outputTextureA);
        Compute.Dispatch(Kernels.BlurX, ThreadCountX, ThreadCountY, 1);

        //Blur Y
        Compute.SetTexture(Kernels.BlurY, "InputImage", outputTextureA);
        Compute.SetTexture(Kernels.BlurY, "Result", outputTextureB);
        Compute.Dispatch(Kernels.BlurY, ThreadCountX, ThreadCountY, 1);
    }

    public Texture getOutputTexture()
    {
        return outputTextureB;
    }
}