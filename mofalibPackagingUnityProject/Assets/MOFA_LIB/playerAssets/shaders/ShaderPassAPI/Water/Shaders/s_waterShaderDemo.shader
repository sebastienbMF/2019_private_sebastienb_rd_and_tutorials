﻿Shader "Unlit/s_waterShaderDemo"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 200
        
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        GrabPass{
        // "_BGTex"
        // if the name is assigned to the grab pass
        // all objects that use this shader also use a shared
        // texture grabbed before rendering them.
        // otherwise a new _GrabTexture will be created for each object.
        }
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/Cg/utilities.cginc"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Noise/basicNoise.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 screenuv : TEXCOORD1;
                float4 grabScreenuv : TEXCOORD2;
                float3 viewDir : TEXCOORD3; //as seen in FXWaterPro.shader (Standard Asset - Water)
                float3 worldPos : TEXCOORD4;
                UNITY_FOG_COORDS(1) //Necessary?
                float4 pos : SV_POSITION;
            };

            sampler2D _CameraDepthTexture;
            sampler2D _GrabTexture;
            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert(appdata v)
            {
              v2f o;
              //Q : Raymarch here?

              o.pos = UnityObjectToClipPos(v.vertex);
              //Slight difference between ComputeScreenPos() and ComputeGrabScreenPos()
              //See Program Files\Unity\Hub\Editor\2019.1.10f1\Editor\Data\CGIncludes\UnityCG.cginc
              o.screenuv = ComputeScreenPos(o.pos);
              o.grabScreenuv = ComputeGrabScreenPos(o.pos);
              o.uv = TRANSFORM_TEX(v.uv, _MainTex);
              o.viewDir.xyz = WorldSpaceViewDir(v.vertex);
              o.worldPos = mul(unity_ObjectToWorld, v.vertex);
              UNITY_TRANSFER_FOG(o, o.vertex);
              return o;
            }

            float ambientNoise(float2 uv, float fTime)
            {
              float2x2 rMat = float2x2(0.8, -0.6, 0.6, 0.8);
              uv *= 0.5;
              float f1a = 1.5*mofalib_BicubicNoise(uv + fTime*1.0) ;
              float f1b = 1.5*mofalib_BicubicNoise(uv - fTime*1.0) ;
              uv *= 3.;
              uv = mul(uv, rMat);
              float f2a = 0.3*mofalib_BicubicNoise(uv + float2(fTime*5., 0));
              float f2b = 0.3*mofalib_BicubicNoise(uv - float2(fTime*5.1, 0));
              uv *= 2.857;
              uv = mul(uv, rMat);
              float f3a = 0.15*mofalib_BicubicNoise(uv + float2(fTime*4., 0));
              float f3b = 0.15*mofalib_BicubicNoise(uv - float2(fTime*4., 0));
              return (f1a + f1b + f2a + f2b + f3a + f3b)*1.35;
            }

            float waterHeight(float2 uv)
            {
              return ambientNoise(uv, _Time.y);
            }

            float3 waterNormal(float2 uv, float2 iResolution)
            {
              float normalScale = 0.02f;
              float3 e = float3(float2(1, 1) / iResolution.xy, 0.);
              float p10 = waterHeight(uv - e.zy).x;
              float p01 = waterHeight(uv - e.xz).x;
              float p21 = waterHeight(uv + e.xz).x;
              float p12 = waterHeight(uv + e.zy).x;
              return normalize(float3(p21 - p01, normalScale, p12 - p10));
            }

            //This was partly validated for the depth aligned with the pixel being rendered.
            //TODO (sol a): Simplify this, where the ray origin and direction is expressed
            //       in view space so that z value can be used just like height is in a typical
            //       height map raymarching shader.
            //       (same as raymarching height)
            //TODO (sol b): Knowing the frustom plane would probably make this process faster
            float distanceToDepthBuffer(float3 pWorld, float3 camPos, float3 camDir)
            {
              float4 clipPos = UnityWorldToClipPos(pWorld);
              float4 screenuv = ComputeScreenPos(clipPos);
              float zDepth = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(screenuv)).r);
              float dFromCam = zDepth / dot(camDir, normalize(pWorld - camPos));
              return dFromCam - length(pWorld - camPos);
            }

            //TODO : Fix and move to common shaders
            float rayMarchDepthBuffer(float3 ro, float3 rd)
            {
              float3 camDir = mofalib_CameraDirection();
              float3 camPos = _WorldSpaceCameraPos;

              float3 p = ro;
              float d = 0.;

              for (int i = 0; i < 10; ++i)
              {
                float h = distanceToDepthBuffer(ro + d * rd, camPos, camDir);
                d += h;
                p = ro + rd * d;
              }
              return d;
            }

              //mofalib_TricubicNoise

            fixed4 frag (v2f i) : SV_Target
            {
                //Raytracing parameters
                float3 ro = _WorldSpaceCameraPos;
                float3 rd = normalize(-i.viewDir.xyz);
                float3 camDir = mofalib_CameraDirection();

                float3 pWaterSurfaceWorld = i.worldPos;

                //Existing (rendered) opaque scene items
                //From https://forum.unity.com/threads/accessing-depth-buffer-from-a-surface-shader.404380/>
                float opaqueZ = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenuv)).r);
                float dOpaque = opaqueZ / dot(camDir,rd);

                float3 surfaceNormal = waterNormal(pWaterSurfaceWorld.xz, float2(512, 512));
                float3 rRefract = refract(rd, surfaceNormal, 1. / 1.3);

                float4 screenuv = i.screenuv;
                {
                  float epsilon = 0.0001;
                  float dRayMarch = max(epsilon, rayMarchDepthBuffer(pWaterSurfaceWorld, rRefract));
                  
                  const float MAX_DEPTH = 5.0;
                  float relativeDepth = dRayMarch / MAX_DEPTH;
                  float3 pDepth = pWaterSurfaceWorld + rRefract * dRayMarch;
                  float4 clipPos = UnityWorldToClipPos(pDepth);
                  screenuv = ComputeScreenPos(clipPos);
                }
                float f = waterHeight(pWaterSurfaceWorld.xz);
                //return float4((float3)f, 1);
                bool isUnderwater = true;
                float3 cScreenSpace = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(isUnderwater ? screenuv : i.screenuv)).rgb;
                //cScreenSpace = waterNormal(pWaterSurfaceWorld.xz, float2(512,512));
                return float4(cScreenSpace,1);
            }
            ENDCG
        }
    }
}
