﻿Shader "ShaderPassAPI_Examples/ObstacleDetection"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DepthTex("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Mapping/zBuffer.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _DepthTex;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _zBufferParams;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float zVal = tex2D(_DepthTex, i.uv).x;
                float distanceFromCamera = mofalib_LinearEyeDepth(zVal, _zBufferParams);
                return float4( (float3)(distanceFromCamera<100?1.0:0.0),1);
            }
            ENDCG
        }
    }
}
