﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mofalib.Rendering.Filters;

[ExecuteAlways]
public class shaderPassAPI_minimalistDemo : MonoBehaviour
{
    public Camera secondaryCamera;

    mofalib.Rendering.RenderCameraToTexture secondaryCameraRenderPass;
    mofalib.Rendering.ShaderPassAPI.SimpleShaderPass obstaclesDetectionPass;
    mofalib.Rendering.ShaderNodes.BlurPass blurPass;

    void Awake()
    {
        secondaryCameraRenderPass = new mofalib.Rendering.RenderCameraToTexture(secondaryCamera, 512, 512);
        obstaclesDetectionPass = new mofalib.Rendering.ShaderPassAPI.SimpleShaderPass("ShaderPassAPI_Examples/ObstacleDetection", 512, 512, RenderTextureFormat.ARGB32,delegate (Material m) 
        {
            m.SetTexture("_DepthTex", secondaryCameraRenderPass.GetDepthTexture());
            m.SetVector("_zBufferParams", secondaryCameraRenderPass.GetZBufferParams());
        });
        blurPass = new mofalib.Rendering.ShaderNodes.BlurPass(512, 512, 50, 25, RenderTextureFormat.ARGB32, delegate (Material m)
        {
            m.SetTexture("_MainTex", obstaclesDetectionPass.GetTexture(0));
        });

    }

    public void OnGUI()
    {   
        GUI.DrawTexture(new Rect(0, 0, 256, 256), secondaryCameraRenderPass.GetColorTexture());
        GUI.DrawTexture(new Rect(256, 0, 256, 256), obstaclesDetectionPass.GetTexture(0));
        GUI.DrawTexture(new Rect(512, 0, 256, 256), blurPass.GetTexture(0));
    }

    // Update is called once per frame
    void Update()
    {
        secondaryCameraRenderPass.Update();
        obstaclesDetectionPass.Update();
        blurPass.Update();
    }
}
