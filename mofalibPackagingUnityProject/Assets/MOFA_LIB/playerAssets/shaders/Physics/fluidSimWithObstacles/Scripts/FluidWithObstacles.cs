﻿// FluidWithObstacles 
//  Jos Stam's Stable Fluids
//  Original implementation in Unity by Keijiro Takahashi (https://github.com/keijiro/StableFluids)
//  Modified by Moment Factory to add obstacles texture input
//    * Velocity redirection around obstacles
//
using UnityEngine;

namespace StableFluids
{
    public class FluidWithObstacles : MonoBehaviour, ITextureGenerator
    {
        #region Editable attributes

        [SerializeField] int _resolutionX = 512;
        [SerializeField] int _resolutionY = 512;
        [SerializeField] float _viscosity = 1e-6f;
        [SerializeField] float _force = 300;
        [SerializeField] float _exponent = 200;
        /*[SerializeField] Texture2D _initial;
        [SerializeField] Texture2D _obstacles;
        */
        public Texture2D _initial;
        public Texture2D _obstacles;
        public GameObject _gradientProcessor;

        #endregion

        #region Internal resources

        [SerializeField, HideInInspector] ComputeShader _compute;
        [SerializeField, HideInInspector] Shader _shader;

        #endregion

        public Shader _forceShader;

        #region Private members

        Material _shaderSheet;
        Material _forceShaderSheet;
        Vector2 _previousInput;

        static class Kernels
        {
            public const int Advect = 0;
            public const int Force = 1;
            public const int PSetup = 2;
            public const int PFinish = 3;
            public const int Jacobi1 = 4;
            public const int Jacobi2 = 5;
        }

        int ThreadCountX { get { return (_resolutionX + 7) / 8; } }
        int ThreadCountY { get { return (_resolutionY + 7) / 8; } }

        public int ResolutionX { get { return ThreadCountX * 8; } }
        public int ResolutionY { get { return ThreadCountY * 8; } }

        public enum VectorBufferID
        {
            Velocity1 = 0,
            Velocity2 = 1,
            Velocity3 = 2,
            Pressure1 = 3,
            Pressure2 = 4,
            Force = 5,
            Obstacles = 6,
        }

        // Vector field buffers
        static class VFB
        {
            public static RenderTexture V1;
            public static RenderTexture V2;
            public static RenderTexture V3;
            public static RenderTexture P1;
            public static RenderTexture P2;
            public static RenderTexture F1;
        }

        // Color buffers (for double buffering)
        RenderTexture _colorRT1;
        RenderTexture _colorRT2;

        RenderTexture AllocateBuffer(int componentCount, int width = 0, int height = 0)
        {
            var format = RenderTextureFormat.ARGBHalf;
            if (componentCount == 1) format = RenderTextureFormat.RHalf;
            if (componentCount == 2) format = RenderTextureFormat.RGHalf;

            if (width == 0) width = ResolutionX;
            if (height == 0) height = ResolutionY;

            var rt = new RenderTexture(width, height, 0, format);
            rt.enableRandomWrite = true;
            rt.Create();
            return rt;
        }

        #endregion

        #region MonoBehaviour implementation

        void OnValidate()
        {
            _resolutionX = Mathf.Max(_resolutionX, 8);
            _resolutionY = Mathf.Max(_resolutionY, 8);
        }

        void Start()
        {
            _shaderSheet = new Material(_shader);
            _forceShaderSheet = new Material(_forceShader); //This should be a compute shader

            VFB.V1 = AllocateBuffer(2);
            VFB.V2 = AllocateBuffer(2);
            VFB.V3 = AllocateBuffer(2);
            VFB.P1 = AllocateBuffer(1);
            VFB.P2 = AllocateBuffer(1);
            VFB.F1 = AllocateBuffer(2);

            //Optional Color buffers
            if (_initial != null)
            {
                _colorRT1 = AllocateBuffer(4, Screen.width, Screen.height);
                _colorRT2 = AllocateBuffer(4, Screen.width, Screen.height);
                Graphics.Blit(_initial, _colorRT1);
            }

#if UNITY_IOS
            Application.targetFrameRate = 60;
#endif
        }

        void OnDestroy()
        {
            Destroy(_shaderSheet);
            Destroy(_forceShaderSheet); //This should be a compute shader.

            Destroy(VFB.V1);
            Destroy(VFB.V2);
            Destroy(VFB.V3);
            Destroy(VFB.P1);
            Destroy(VFB.P2);
            Destroy(VFB.F1);

            Destroy(_colorRT1);
            Destroy(_colorRT2);
        }

        Texture gradientTexture()
        {
            ITextureGenerator texGen = _gradientProcessor.GetComponent(typeof(ITextureGenerator)) as ITextureGenerator;
            return texGen.GetTexture(0/*0=Gradient*/);
        }

        Texture blurTexture()
        {
            ITextureGenerator texGen = _gradientProcessor.GetComponent(typeof(ITextureGenerator)) as ITextureGenerator;
            return texGen.GetTexture(1/*1=Blur*/);
        }

        void Update()
        {
            var dt = Time.deltaTime;
            var dx = 1.0f / ResolutionY;

            // Input point
            var input = new Vector2(
                (Input.mousePosition.x - Screen.width * 0.5f) / Screen.height,
                (Input.mousePosition.y - Screen.height * 0.5f) / Screen.height
            );

            // Common variables
            _compute.SetFloat("Time", Time.time);
            _compute.SetFloat("DeltaTime", dt);

            // Advection
            _compute.SetTexture(Kernels.Advect, "U_in", VFB.V1);
            _compute.SetTexture(Kernels.Advect, "W_out", VFB.V2);
            _compute.Dispatch(Kernels.Advect, ThreadCountX, ThreadCountY, 1);

            // Diffuse setup
            var dif_alpha = dx * dx / (_viscosity * dt);
            _compute.SetFloat("Alpha", dif_alpha);
            _compute.SetFloat("Beta", 4 + dif_alpha);
            Graphics.CopyTexture(VFB.V2, VFB.V1);
            _compute.SetTexture(Kernels.Jacobi2, "B2_in", VFB.V1);

            // Jacobi iteration
            for (var i = 0; i < 20; i++)
            {
                _compute.SetTexture(Kernels.Jacobi2, "X2_in", VFB.V2);
                _compute.SetTexture(Kernels.Jacobi2, "X2_out", VFB.V3);
                _compute.Dispatch(Kernels.Jacobi2, ThreadCountX, ThreadCountY, 1);

                _compute.SetTexture(Kernels.Jacobi2, "X2_in", VFB.V3);
                _compute.SetTexture(Kernels.Jacobi2, "X2_out", VFB.V2);
                _compute.Dispatch(Kernels.Jacobi2, ThreadCountX, ThreadCountY, 1);
            }

            // Add external force
            _forceShaderSheet.SetTexture("_VelocityTex", VFB.V1);
            _forceShaderSheet.SetTexture("_ObstacleTex", blurTexture());
            _forceShaderSheet.SetTexture("_GradientTex", gradientTexture());
            Graphics.Blit(null, VFB.F1, _forceShaderSheet, 0);

            // TODO : Add input texture for external force!
            _compute.SetVector("ForceOrigin", input);
            _compute.SetFloat("ForceExponent", _exponent);
            _compute.SetTexture(Kernels.Force, "F_in", VFB.F1); //External Force texture
            _compute.SetTexture(Kernels.Force, "W_in", VFB.V2); //Pressure?
            _compute.SetTexture(Kernels.Force, "W_out", VFB.V3); //Velocity

            if (Input.GetMouseButton(1))
                // Random push
                _compute.SetVector("ForceVector", Random.insideUnitCircle * _force * 0.025f);
            else if (Input.GetMouseButton(0))
                // Mouse drag
                _compute.SetVector("ForceVector", (input - _previousInput) * _force);
            else
                _compute.SetVector("ForceVector", Vector4.zero);

            _compute.Dispatch(Kernels.Force, ThreadCountX, ThreadCountY, 1);

            // Projection setup
            _compute.SetTexture(Kernels.PSetup, "W_in", VFB.V3);
            _compute.SetTexture(Kernels.PSetup, "DivW_out", VFB.V2);
            _compute.SetTexture(Kernels.PSetup, "P_out", VFB.P1);
            _compute.Dispatch(Kernels.PSetup, ThreadCountX, ThreadCountY, 1);

            // Jacobi iteration
            _compute.SetFloat("Alpha", -dx * dx);
            _compute.SetFloat("Beta", 4);
            _compute.SetTexture(Kernels.Jacobi1, "B1_in", VFB.V2);

            for (var i = 0; i < 20; i++)
            {
                _compute.SetTexture(Kernels.Jacobi1, "X1_in", VFB.P1);
                _compute.SetTexture(Kernels.Jacobi1, "X1_out", VFB.P2);
                _compute.Dispatch(Kernels.Jacobi1, ThreadCountX, ThreadCountY, 1);

                _compute.SetTexture(Kernels.Jacobi1, "X1_in", VFB.P2);
                _compute.SetTexture(Kernels.Jacobi1, "X1_out", VFB.P1);
                _compute.Dispatch(Kernels.Jacobi1, ThreadCountX, ThreadCountY, 1);
            }

            // Projection finish
            _compute.SetTexture(Kernels.PFinish, "W_in", VFB.V3);
            _compute.SetTexture(Kernels.PFinish, "P_in", VFB.P1);
            _compute.SetTexture(Kernels.PFinish, "U_out", VFB.V1);
            _compute.Dispatch(Kernels.PFinish, ThreadCountX, ThreadCountY, 1);

            //Process color buffers
            if (_colorRT1 != null)
            {
                // Apply the velocity field to the color buffer.
                var offs = Vector2.one * (Input.GetMouseButton(1) ? 0 : 1e+7f);
                _shaderSheet.SetVector("_ForceOrigin", input + offs);
                _shaderSheet.SetFloat("_ForceExponent", _exponent);
                _shaderSheet.SetTexture("_VelocityField", VFB.V1);
                Graphics.Blit(_colorRT1, _colorRT2, _shaderSheet, 0);

                // Swap the color buffers.
                var temp = _colorRT1;
                _colorRT1 = _colorRT2;
                _colorRT2 = temp;
            }

            _previousInput = input;
        }

        //Generic interface
        public Texture GetTexture(uint texNo)
        {
            return GetVectorField((VectorBufferID)texNo);
        }

        //Specific interface
        public Texture GetVectorField(VectorBufferID id)
        {
            Texture[] vfArray = { VFB.V1,
                                  VFB.V2,
                                  VFB.V3,
                                  VFB.P1,
                                  VFB.P2,
                                  VFB.F1,
                                  _obstacles };

            return vfArray[(uint)id];
        }

        public Texture GetObstacleTex()
        {
            return _obstacles;
        }

        public RenderTexture GetColor()
        {
            return _colorRT1;
        }

        #endregion
    }
}
