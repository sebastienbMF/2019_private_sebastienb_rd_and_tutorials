﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StableFluids;

public class DisplayFluidSimWithObstacles : MonoBehaviour
{

    public GameObject FluidSim;
    public GameObject GradientObj;

    private Material matDrawBuffers;

    void Start()
    {
        matDrawBuffers = new Material(Shader.Find("Unlit/DrawFluidWithObstacles"));
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        FluidWithObstacles fluidProcessor = FluidSim.GetComponent(typeof(FluidWithObstacles)) as FluidWithObstacles;
        ComputeImageGradient gradientProcessor = GradientObj.GetComponent(typeof(ComputeImageGradient)) as ComputeImageGradient;

        matDrawBuffers.SetTexture("_VelocityTex", fluidProcessor.GetVectorField(StableFluids.FluidWithObstacles.VectorBufferID.Velocity3));
        matDrawBuffers.SetTexture("_PressureTex", fluidProcessor.GetVectorField(StableFluids.FluidWithObstacles.VectorBufferID.Pressure1));
        matDrawBuffers.SetTexture("_ObstacleTex", fluidProcessor.GetVectorField(StableFluids.FluidWithObstacles.VectorBufferID.Obstacles));

        matDrawBuffers.SetTexture("_GradientTex", gradientProcessor.GetTexture(0));
        matDrawBuffers.SetTexture("_SoftObstaclesTex", gradientProcessor.GetTexture(1));
        
        Graphics.Blit(fluidProcessor.GetColor(), destination, matDrawBuffers, 0);
    }
}
