﻿Shader "Unlit/ForceGeneration"
{
    Properties
    {
        _VelocityTex ("Velocity", 2D) = "black" {}
        _ObstacleTex("Obstacle", 2D) = "black" {}
        _GradientTex("Gradient", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _VelocityTex;
            float4 _VelocityTex_ST;
            sampler2D _ObstacleTex;
            sampler2D _GradientTex;

            //TODO : Move to common shaders (Noise).
            float hash13(float3 p3) {
                p3 = frac(p3 * .1031);
                p3 += dot(p3, p3.yzx + 33.33);
                return frac((p3.x + p3.y) * p3.z);
                //NOTE : Changed amplitude into [0-1] range (was [-1,1])
            }

            //TODO : Move to common shaders (mofalibCG.cginc).
            float bilinearInterpolation(float2 u, float a, float b, float c, float d) {
                return float(a + (b - a)*u.x + (c - a)*u.y + (a - b - c + d)*u.x*u.y);
            }

            //TODO : Move to common shaders (Noise).
            float waveNoise01(in float2 p, float fTime)
            {
                p.xy += fTime;
                float2 i = floor(p); float2 v = frac(p);
                float2 u = v * v*(3.0 - 2.0*v);

                float t = fTime / 30.;

                float a = hash13(float3(i + float2(0.0, 0.0), floor(t)));
                float b = hash13(float3(i + float2(1.0, 0.0), floor(t)));
                float c = hash13(float3(i + float2(0.0, 1.0), floor(t)));
                float d = hash13(float3(i + float2(1.0, 1.0), floor(t)));

                float L1 = bilinearInterpolation(u, a, b, c, d);

                float e = hash13(float3(i + float2(0.0, 0.0), ceil(t)));
                float f = hash13(float3(i + float2(1.0, 0.0), ceil(t)));
                float g = hash13(float3(i + float2(0.0, 1.0), ceil(t)));
                float h = hash13(float3(i + float2(1.0, 1.0), ceil(t)));

                float L2 = bilinearInterpolation(u, e, f, g, h);

                t = frac(t);
                t = t * t*(3.0 - 2.0*t);

                return lerp(L1, L2, frac(t));
            }

            float ambientNoise(float2 uv)
            {
                float fTime = _Time.y;

                uv *= 15.;
                float f1a = waveNoise01(uv + fTime * 2., 0.) - 0.5;
                float f1b = waveNoise01(uv - fTime * 2.3, 0.) - 0.5;
                uv *= 2.;
                float f2a = 0.3*waveNoise01(uv + float2(fTime*1., 0), 0.) - 0.5;
                float f2b = 0.3*waveNoise01(uv - float2(fTime*1., 0), 0.) - 0.5;
                uv *= 1.457;
                float f3a = 0.15*waveNoise01(uv + float2(fTime*1., 0), 0.) - 0.5;
                float f3b = 0.15*waveNoise01(uv - float2(fTime*1., 0), 0.) - 0.5;
                return (f1a + f1b + f2a + f2b + f3a + f3b)*0.35;
            }
            
            float3 noiseGrad(float2 uv)
            {
                float eps = 0.01;
                float dx = ambientNoise(uv + float2(eps, 0)) - ambientNoise(uv - float2(eps, 0));
                float dy = ambientNoise(uv + float2(0, eps)) - ambientNoise(uv - float2(0, eps));

                return normalize(float3(dx, dy,1.))+float3(0.1,0,0);
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _VelocityTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                float4 vel = tex2D(_VelocityTex, i.uv);
                float2 uvCenterA = float2(0.75,0.85);
                float2 uvCenterB = float2(0.75, 0.72);
                
                float isObstacle = tex2D(_ObstacleTex, i.uv).x;
                float2 grad = -tex2D(_GradientTex, i.uv).rg*0.05;


                float2 forceXY = grad;// float2(0, 0);

                float2 obstacleBrake = -vel * 1.0;
                float2 noiseFlow = noiseGrad(i.uv*float2(1, 1)).xy*float2(2, 5)*0.01;

                forceXY += lerp(noiseFlow, obstacleBrake, (isObstacle > 0.5)?0.9:0.);
                float minStuckSpeed = 2.0;
                float unstuckFlowRate = 0.75;
                forceXY = lerp(forceXY, noiseFlow+grad, length(vel.xy) < minStuckSpeed ? unstuckFlowRate : 0);
                
                return float4(forceXY, 0, 1);
            }
            ENDCG
        }
    }
}
