﻿Shader "Unlit/DrawFluidWithObstacles"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _VelocityTex ("Velocity", 2D) = "black" {}
        _PressureTex("Pressure", 2D) = "black" {}
        _ObstacleTex("Obstacles", 2D) = "black" {}
        _SoftObstaclesTex("SoftObstacles", 2D) = "black" {}
        _GradientTex("Gradient", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _VelocityTex;
            sampler2D _PressureTex;
            sampler2D _ObstacleTex;
            sampler2D _GradientTex;
            sampler2D _SoftObstaclesTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_VelocityTex, i.uv/.666666); //Velocity occupies 2/3 of the Render Target

                float2 uv = 3.*i.uv;

                if (     uv.x < 1. && uv.y > 2.) col = tex2D(_ObstacleTex,      uv - float2(0, 2));
                else if (uv.x < 2. && uv.y > 2.) col = tex2D(_SoftObstaclesTex, uv - float2(1, 2));
                else if (uv.x > 2. && uv.y > 2.) col = tex2D(_GradientTex,      uv - float2(2, 2))*10.; //Multiply gradient to better see it
                else if (uv.x > 2. && uv.y > 1.) col = tex2D(_MainTex,          uv - float2(2, 1));
                else if (uv.x > 2. && uv.y > 0.) col = tex2D(_PressureTex,      uv - float2(2, 0))*35000.; //Pressure has super low amplitude, must be cranked up to be visible.
                return col;
            }
            ENDCG
        }
    }
}
