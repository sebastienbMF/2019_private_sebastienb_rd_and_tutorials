﻿Shader "FluidWithTurbulence/DrawResult"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _VelocityTex ("Velocity", 2D) = "black" {}
        _PressureTex("Pressure", 2D) = "black" {}
        _ObstacleTex("Obstacles", 2D) = "black" {}
        _SoftObstaclesTex("SoftObstacles", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _VelocityTex;
            sampler2D _PressureTex;
            sampler2D _ObstacleTex;
            sampler2D _SoftObstaclesTex;
            float4 _MainTex_ST;

            float3 _TexWeights;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float totalWeights = _TexWeights.x + _TexWeights.y + _TexWeights.z;
                _TexWeights.x /= totalWeights;
                _TexWeights.y /= totalWeights;
                _TexWeights.z /= totalWeights;

                float isObstacle = tex2D(_SoftObstaclesTex, i.uv).x;

                if (isObstacle > .5)
                {
                    return float4(0.5, .35, .25, 1);
                }

                return tex2D(_MainTex, i.uv);
            }
            ENDCG
        }
    }
}
