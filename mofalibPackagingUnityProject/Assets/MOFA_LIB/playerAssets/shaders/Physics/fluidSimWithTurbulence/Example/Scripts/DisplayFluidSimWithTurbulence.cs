﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using StableFluids;
using mofalib.Rendering.ShaderPassAPI;

namespace mofalib
{
    namespace Rendering
    {
        namespace Examples
        {
            public class DisplayFluidSimWithTurbulence : MonoBehaviour
            {
                public GameObject FluidSim;
                public GameObject GradientObj;
                public Shader DrawResultShader;
                private Material matDrawBuffers;
                
                void Start()
                {
                    matDrawBuffers = new Material(DrawResultShader);
                }

                void OnDestroy()
                {
                    Destroy(matDrawBuffers);
                }

                void OnRenderImage(RenderTexture source, RenderTexture destination)
                {
                    FluidWithTurbulence fluidProcessor = FluidSim.GetComponent(typeof(FluidWithTurbulence)) as FluidWithTurbulence;
                    ComputeImageGradient gradientProcessor = GradientObj.GetComponent(typeof(ComputeImageGradient)) as ComputeImageGradient;
                    
                    matDrawBuffers.SetTexture("_VelocityTex", fluidProcessor.GetVectorField(mofalib.Rendering.Physics.FluidSimProcessor.VectorBufferID.Velocity3));
                    matDrawBuffers.SetTexture("_PressureTex", fluidProcessor.GetVectorField(mofalib.Rendering.Physics.FluidSimProcessor.VectorBufferID.Pressure1));
                    matDrawBuffers.SetTexture("_ObstacleTex", gradientProcessor.InputTexture);
                    matDrawBuffers.SetTexture("_SoftObstaclesTex", gradientProcessor.GetTexture(1));

                    Graphics.Blit(fluidProcessor.DistortedTexture(), destination, matDrawBuffers, 0);
                }
            }
        }
    }
}