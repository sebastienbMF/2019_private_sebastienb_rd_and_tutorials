﻿// FluidWithTurbulence : Fluid Sim + distortion cycle
//  
using UnityEngine;
using mofalib.Rendering.ShaderPassAPI;
using mofalib.Rendering.Physics;

namespace mofalib
{
    namespace Rendering
    {
        namespace Examples
        {
            public class FluidWithTurbulence : MonoBehaviour, ITextureGenerator
            {
                [SerializeField, HideInInspector]
                ComputeShader _compute;
                [SerializeField, HideInInspector]
                Shader _shader;
                [SerializeField]
                int _resolutionX = 512;
                [SerializeField]
                int _resolutionY = 512;
                [SerializeField]
                public Texture2D _initial;
                public GameObject _gradientProcessor;
                public Shader _forceShader;
                
                #region Private members

                private FluidSimProcessor _fluidSimProcessor;
                private InterleavedDistortionPass _distorsionPass;

                #endregion
                
                void Start()
                {
                    _fluidSimProcessor = new FluidSimProcessor(_resolutionX, _resolutionY, _compute, _forceShader, null);
                    _distorsionPass = new InterleavedDistortionPass(180, _initial);
                }

                void OnDestroy()
                {
                    _fluidSimProcessor.ReleaseResources();
                    _distorsionPass.ReleaseResources();
                }

                public Texture GradientTexture()
                {
                    ITextureGenerator texGen = _gradientProcessor.GetComponent(typeof(ITextureGenerator)) as ITextureGenerator;
                    return texGen.GetTexture(0/*0=Gradient*/);
                }

                public Texture BlurTexture()
                {
                    ITextureGenerator texGen = _gradientProcessor.GetComponent(typeof(ITextureGenerator)) as ITextureGenerator;
                    return texGen.GetTexture(1/*1=Blur*/);
                }

                public Texture DistortedTexture()
                {
                    return _distorsionPass.GetTexture(0);
                }

                void Update()
                {
                    //Update Fluid Sim velocity
                    _fluidSimProcessor.Update(delegate (Material m)
                    {
                        m.SetTexture("_VelocityTex", _fluidSimProcessor.GetVectorField(FluidSimProcessor.VectorBufferID.Velocity1));
                        m.SetTexture("_ObstacleTex", BlurTexture());
                        m.SetTexture("_GradientTex", GradientTexture());
                    });
                    
                    //Use Fluid Sim velocity to distort the initial texture
                    _distorsionPass.Update(delegate (Material m)
                    {
                        Texture velTex = _fluidSimProcessor.GetVectorField(FluidSimProcessor.VectorBufferID.Velocity3);//ANSME : Should we use Velocity1 here? Which is more recent, stable?

                        var input = new Vector2(
                            (Input.mousePosition.x - Screen.width * 0.5f) / Screen.height,
                            (Input.mousePosition.y - Screen.height * 0.5f) / Screen.height
                        );

                        // Move color injection super far if the mouse button is not pressed
                        var offs = Vector2.one * (Input.GetMouseButton(1) ? 0 : 1e+7f);

                        m.SetVector("_ForceOrigin", input + offs);
                        m.SetFloat("_ForceExponent", 200);
                        m.SetTexture("_VelocityField", velTex);
                    });
                }

                //Generic interface
                public Texture GetTexture(uint texNo)
                {
                    return _fluidSimProcessor.GetTexture(texNo);
                }

                //Specific interface
                public Texture GetVectorField(mofalib.Rendering.Physics.FluidSimProcessor.VectorBufferID id)
                {
                    return _fluidSimProcessor.GetVectorField(id);
                }
            }
        }
    }
}