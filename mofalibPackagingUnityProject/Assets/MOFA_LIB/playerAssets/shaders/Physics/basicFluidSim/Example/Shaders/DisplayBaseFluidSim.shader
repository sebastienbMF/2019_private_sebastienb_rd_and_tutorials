﻿Shader "Unlit/DisplayBaseFluidSim"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _V1("V1", 2D) = "black" {}
        _V2("V2", 2D) = "black" {}
        _P1("P1", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _V1;
            sampler2D _V2;
            sampler2D _P1;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = 2.*i.uv;

                fixed4 col = tex2D(_MainTex, uv);
                
                //Velocity
                if (uv.x > 1. && uv.y < 1.)
                    col = tex2D(_V1, uv-float2(1,0));
                //Velocity 2 (delta/compensation?)
                if (uv.x > 1. && uv.y > 1.)
                    col = tex2D(_V2, uv - float2(1, 1));
                //Pressure 1
                if (uv.x < 1. && uv.y > 1.)
                    col = tex2D(_P1, uv - float2(0, 1))*35000.;
                
                return col;
            }
            ENDCG
        }
    }
}
