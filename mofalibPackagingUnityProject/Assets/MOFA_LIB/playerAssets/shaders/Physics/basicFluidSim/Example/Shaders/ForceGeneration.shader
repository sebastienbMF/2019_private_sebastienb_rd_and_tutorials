﻿Shader "BaseFluidSimExample/ForceGeneration"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            // External force parameters
            float2 _ForceOrigin;
            float2 _ForceVector;
            float _ForceExponent;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                /*
                float2 pos = (tid + 0.5 - dim * 0.5) / dim.y;
                float amp = exp(-ForceExponent * distance(ForceOrigin, pos));
                W_out[tid] = W_in[tid] + ForceVector * amp + F_in[tid];
                */

                float amp = exp(-_ForceExponent * distance(_ForceOrigin, i.uv));
                return fixed4(_ForceVector * amp,0,1);
            }
            ENDCG
        }
    }
}
