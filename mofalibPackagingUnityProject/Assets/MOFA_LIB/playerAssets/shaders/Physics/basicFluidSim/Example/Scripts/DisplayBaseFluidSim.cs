﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StableFluids;

public class DisplayBaseFluidSim : MonoBehaviour
{

    public GameObject FluidSim;

    private Material matDrawBuffers;

    void Start()
    {
        matDrawBuffers = new Material(Shader.Find("Unlit/DisplayBaseFluidSim"));
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        BaseFluidSim fluidProcessor = FluidSim.GetComponent(typeof(BaseFluidSim)) as BaseFluidSim;

        matDrawBuffers.SetTexture("_V1", fluidProcessor.GetTexture(0));
        matDrawBuffers.SetTexture("_V2", fluidProcessor.GetTexture(5));
        matDrawBuffers.SetTexture("_P1", fluidProcessor.GetTexture(3));
        Graphics.Blit(fluidProcessor.getColor(), destination, matDrawBuffers, 0); 
    }
}
