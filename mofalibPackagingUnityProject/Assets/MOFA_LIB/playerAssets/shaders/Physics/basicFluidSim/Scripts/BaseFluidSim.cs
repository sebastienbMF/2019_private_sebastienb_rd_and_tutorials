﻿// StableFluids - A GPU implementation of Jos Stam's Stable Fluids on Unity
// https://github.com/keijiro/StableFluids

using UnityEngine;
using System;

namespace StableFluids
{
  public class BaseFluidSim : MonoBehaviour
  {
    [SerializeField] int _resolution = 512;
    [SerializeField] float _viscosity = 1e-6f;
    [SerializeField] float force = 300;
    [SerializeField] float exponent = 200;
    [SerializeField] Texture2D _initial;
    
    [SerializeField, HideInInspector] ComputeShader _compute;
    [SerializeField, HideInInspector] Shader _shader;

    public Shader forceShader;

    private Vector2 previousInput;
    private mofalib.Rendering.ShaderPassAPI.InkDispersionPass _inkDispersion;
    private mofalib.Rendering.Physics.FluidSimProcessor _fluidSimProcessor;
    
    void Start()
    {
        _fluidSimProcessor = new mofalib.Rendering.Physics.FluidSimProcessor(_resolution, _resolution, _compute, forceShader);
        _inkDispersion = new mofalib.Rendering.ShaderPassAPI.InkDispersionPass(Screen.width, Screen.height);
        _inkDispersion.setInitialTexture(_initial);
    }

    void OnDestroy()
    {
        _fluidSimProcessor.ReleaseResources();
        _inkDispersion.ReleaseResources();
    }
    
    void Update()
    {
      _fluidSimProcessor.viscosity = _viscosity;

      // Apply the velocity field to the color buffer.
      var offs = Vector2.one * (Input.GetMouseButton(1) ? 0 : 1e+7f);
      
      // Input point
      var input = new Vector2(
          (Input.mousePosition.x - Screen.width * 0.5f) / Screen.height,
          (Input.mousePosition.y - Screen.height * 0.5f) / Screen.height
      );
      
      _fluidSimProcessor.Update(delegate (Material m)
      {
          if (Input.GetMouseButton(1))
              // Random push
              m.SetVector("_ForceVector", UnityEngine.Random.insideUnitCircle * force * 0.025f);
          else if (Input.GetMouseButton(0))
              // Mouse drag
              m.SetVector("_ForceVector", (input - previousInput) * force);
          else
              m.SetVector("_ForceVector", Vector4.zero);

          m.SetFloat("_ForceExponent", exponent);
          m.SetVector("_ForceOrigin", input);
      });
        
      _inkDispersion.Update(delegate (Material m)
      {
        m.SetVector("_ForceOrigin", input + offs);
        m.SetFloat("_ForceExponent", exponent);
        m.SetTexture("_VelocityField", _fluidSimProcessor.GetVectorField(0));
        //m.SetTexture("_MainTex", _inkDispersion.GetTexture(0));
      });

            previousInput = input;
    }

    public Texture GetTexture(uint id)
    {
      return _fluidSimProcessor.GetTexture(id);
    }

    public Texture getColor()
    {
      return _inkDispersion.GetTexture(0);
    }
  }
}
