﻿Shader "MofaLib/Pastille/BorderShader"
{
	Properties
	{
		_Radius("Radius", Range(0, 0.5)) = 0.5
		_BorderWidth("Border Width", Range(0, 0.5)) = 0.05

		_BaseColor("Base Color", Color) = (1, 1, 1, 1)
		_PulseColor("Pulse Color", Color) = (1, 1, 1, 1)
		_MainTex("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags{ "RenderType" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag


			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			fixed4 _BaseColor;
			fixed4 _PulseColor;
			float _Radius;
			float _BorderWidth;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float dist = length(i.uv - float2(0.5, 0.5));

				fixed4 col = dist < _Radius - _BorderWidth ? _BaseColor: _PulseColor;
				if (dist > _Radius)
				{
					col.a = 0;
				}

				return col;
			}
			ENDCG
		}
	}
}
