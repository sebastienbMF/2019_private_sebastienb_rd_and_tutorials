﻿Shader "MofaLib/Pastille/PulsingPastilleShader"
{
	Properties
	{
		_NbCircles("Nb Circles", Int) = 3
		_PulseSpeed("Pulse Speed", Float) = 1
		_Radius("Radius", Range(0, 0.5)) = 0.5

		[Toggle] _InverseAlpha("Inverse Alpha", Float) = 0
		_BaseColor("Base Color", Color) = (1, 1, 1, 1)
		_PulseColor("Pulse Color", Color) = (1, 1, 1, 1)

		_AlphaTex ("Alpha Texture", 2D) = "white" {}
		_AlphaOffset("Alpha Offset", Float) = 0.3
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" }
		LOD 100

		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _AlphaTex;
			float4 _AlphaTex_ST;
			float _AlphaOffset;

			fixed4 _BaseColor;
			fixed4 _PulseColor;
			float _Radius;
			int _NbCircles;
			float _PulseSpeed;
			float _InverseAlpha;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _AlphaTex);

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_AlphaTex, i.uv);
				float dist = length(i.uv - float2(0.5, 0.5));
				float alpha = saturate(col.a + _AlphaOffset);

				if (dist > _Radius)
				{
					col.a = 0;
				}
				else
				{
					dist /= _Radius;

					dist *= _NbCircles;
					dist -= _Time.y*_PulseSpeed;
					dist = frac(dist);

					col = lerp(_BaseColor, _PulseColor, _InverseAlpha * dist + (1 - _InverseAlpha) * (1.0 - dist));
				}

				col.a *= alpha;
				return col;
			}
			ENDCG
		}
	}
}
