﻿Shader "MofaLib/Pastille/TargetClockShader"
{
	Properties
	{
		_NbCircles("Nb Circles", Int) = 3
		_Color1("Color 1", Color) = (1, 1, 1, 1)
		_Color2("Color2", Color) = (1, 1, 1, 1)
		_MainTex("Texture", 2D) = "white" {}
		_Factor("Step", Range(0,1)) = 0.5

		_Angle("Angle", Float) = 6.28
	}
	SubShader
	{
		Tags{ "RenderType" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag


			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			fixed4 _Color1;
			fixed4 _Color2;
			int _NbCircles;
			float _Factor;
			fixed4 _LineColor;
			float _Angle;
			
			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{


				// sample the texture
				fixed4 col = (fixed4)1;
				float2 vec = i.uv - float2(0.5, 0.5);
				float dist = length(vec);

				float angle = atan2(vec.y, vec.x) + 3.14;

				if (dist > 0.5 || angle > _Angle)
				{
					col.a = 0;
				}
				else
				{
					dist /= 0.5;
					dist *= _NbCircles;
					dist = frac(dist);

					col = lerp(_Color1, _Color2, step(_Factor, dist));
				}

				

				return col;
			}
			ENDCG
		}
	}
}
