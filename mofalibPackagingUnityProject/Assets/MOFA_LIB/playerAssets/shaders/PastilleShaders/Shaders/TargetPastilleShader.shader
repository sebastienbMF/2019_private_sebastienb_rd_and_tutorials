﻿Shader "MofaLib/Pastille/TargetShader"
{
	Properties
	{
		_NbCircles("Nb Circles", Int) = 3
		_Radius("Radius", Range(0, 0.5)) = 0.5
		[Toggle] _InverseAlpha("Inverse Alpha", Float) = 0
		_BaseColor("Base Color", Color) = (1, 1, 1, 1)
		_PulseColor("Pulse Color", Color) = (1, 1, 1, 1)
		_MainTex("Texture", 2D) = "white" {}
		_Factor("Step", Range(0,1)) = 0.5
		_LineColor("Line Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		Tags{ "RenderType" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag


			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			fixed4 _BaseColor;
			fixed4 _PulseColor;
			float _Radius;
			int _NbCircles;
			float _PulseSpeed;
			float _InverseAlpha;
			float _Factor;
			fixed4 _LineColor;
			
			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				float dist = length(i.uv - float2(0.5, 0.5));

				if (dist > _Radius)
				{
					col.a = 0;
				}
				else
				{
					dist /= _Radius;

					dist *= _NbCircles;
					dist = frac(dist);

					// Gradient 
					col = lerp(_BaseColor, _PulseColor, _InverseAlpha * dist + (1 - _InverseAlpha) * (1.0 - dist));
					
					// Line
					col = lerp(col, _LineColor, step(_Factor, dist));
				}

				return col;
			}
			ENDCG
		}
	}
}
