﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace MofaLib
{
    public class TrackSegmentControllerBase : MonoBehaviour
    {

        public int ModelId = -1;
        public int SegmentWidth = 10;
        public Transform[] SegmentModels;


        public float InitSegment(Vector3 pos)
        {
            transform.position = pos;
            ModelId = UnityEngine.Random.Range(0, SegmentModels.Length);

            // Hide Other Models
            for (int i = 0; i <= SegmentModels.Length - 1; i++)
                SegmentModels[i].gameObject.SetActive(false);

            // Show Selected Model
            SegmentModels[ModelId].gameObject.SetActive(true);
            return GetSegmentWidth();
        }


        public float GetSegmentWidth()
        {
            return SegmentModels[ModelId].GetComponent<TrackSegmentWidthOverride>() ? SegmentModels[ModelId].GetComponent<TrackSegmentWidthOverride>().SegmentWidth : SegmentWidth;
        }


        public virtual void DestroySegment()
        {
            Destroy(gameObject);
        }
    }
}
