﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MofaLib
{
    public class TrackSegmentWidthOverride : MonoBehaviour
    {
        public int SegmentWidth = 10;
    }
}
