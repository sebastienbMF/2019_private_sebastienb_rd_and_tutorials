﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MofaLib
{
    public class InfiniteLinearWorldGenerator : MonoBehaviour
    {

        [Header("GENERAL")]
        public Transform PlayerTrans;
        public Transform TrackSegmentContainer;
        public Transform AdditiveTrackSegmentContainer;
        private float currentSegmentDistance = 0;
        private float totalSegmentDistance = 0;
        private float currentZoneDistance;


        public TrackSegmentController CurrentSegment { get { return segmentList[0]; } }
        protected List<TrackSegmentController> segmentList = new List<TrackSegmentController>();
        protected List<TrackSegmentController> additiveLayerList = new List<TrackSegmentController>();


        [Header("LEVEL PARTS")]
        public GameObject SegmentPrefab;
        public float LevelMoveSpeed = 100.0f;
        public float RoadOffset = -20;
        public int DesiredSegmentCount = 10;


        [Header("SIDE ROAD PROPS")]
        public GameObject[] SideRoadPropPrefabList;
        public int SidePropSpawnChance = 30; // %
        public bool UseRoadPropSpawner = true;
        private int currentSegmentIndex;
        private Vector3 NewSegmentSpawnPosition
        {
            get
            {
                var pos = transform.position;
                pos.z += currentSegmentDistance + RoadOffset;
                return pos;
            }
        }


        void Start()
        {
            ResetTrack();
        }


        void Update()
        {
            // Create segments
            while (segmentList.Count < DesiredSegmentCount)
            {
                SpawnNewSegment();
            }

            // Re-use Track Segment
            if (segmentList[currentSegmentIndex].transform.position.z - RoadOffset <= PlayerTrans.position.z)
            {
                // Re-use Track Segment
                segmentList[currentSegmentIndex].InitSegment(NewSegmentSpawnPosition);
                UpdateDistances(segmentList[currentSegmentIndex].GetSegmentWidth());
                currentSegmentIndex = (currentSegmentIndex + 1) % segmentList.Count;

                // Spawn Props
                if (UseRoadPropSpawner && UnityEngine.Random.Range(0, 100) <= Mathf.Clamp(SidePropSpawnChance, 0, 100)) SpawnRandomSideRoadProp(NewSegmentSpawnPosition);
            }

            // Move Track Elements
            MoveTrack(LevelMoveSpeed);
        }


        public void InitTrack()
        {
            currentZoneDistance = 0;
        }


        public void ResetTrack()
        {
            DestroyAllSegments();
            InitTrack();
        }


        public void MoveTrack(float moveFrame)
        {
            // track segment parts
            for (int i = 0; i <= segmentList.Count - 1; i++)
            {
                segmentList[i].transform.position = new Vector3(segmentList[i].transform.position.x, segmentList[i].transform.position.y, segmentList[i].transform.position.z - moveFrame);
            }

            // Additive layers (like finish line, road mark, etc..)
            for (int i = 0; i < additiveLayerList.Count; i++)
            {
                additiveLayerList[i].transform.position = new Vector3(additiveLayerList[i].transform.position.x, additiveLayerList[i].transform.position.y, additiveLayerList[i].transform.position.z - moveFrame);

                // Destroy when reach the limit
                if (additiveLayerList[i].transform.position.z - RoadOffset <= PlayerTrans.position.z)
                    RemoveAdditiveLayer(additiveLayerList[i].transform);
            }

            currentSegmentDistance -= moveFrame;
        }


        public void SpawnNewSegment()
        {
            GameObject newSegment = Instantiate(SegmentPrefab);
            newSegment.transform.parent = TrackSegmentContainer;
            newSegment.GetComponent<TrackSegmentController>().InitSegment(NewSegmentSpawnPosition);
            segmentList.Add(newSegment.GetComponent<TrackSegmentController>());
            UpdateDistances(newSegment.GetComponent<TrackSegmentController>().GetSegmentWidth());

            // Spawn Props
            if (UseRoadPropSpawner && UnityEngine.Random.Range(0, 100) <= Mathf.Clamp(SidePropSpawnChance, 0, 100)) SpawnRandomSideRoadProp(NewSegmentSpawnPosition);
        }


        private void UpdateDistances(float dist)
        {
            currentSegmentDistance += dist;
            totalSegmentDistance += dist;
        }


        public void SpawnRandomSideRoadProp(Vector3 pos)
        {
            if(SideRoadPropPrefabList.Length <= 0) return;
            
            int randomId = UnityEngine.Random.Range(0, SideRoadPropPrefabList.Length);
            GameObject newSegment = Instantiate(SideRoadPropPrefabList[randomId], pos, Quaternion.identity, AdditiveTrackSegmentContainer) as GameObject;
            additiveLayerList.Add(newSegment.GetComponent<TrackSegmentController>());
            newSegment.GetComponent<TrackSegmentController>().InitSegment(pos);
        }


        public void RemoveAdditiveLayer(Transform trans)
        {
            additiveLayerList.Remove(trans.GetComponent<TrackSegmentController>());
            Destroy(trans.gameObject);
        }


        public void DestroyAllSegments()
        {
            currentSegmentDistance = 0;

            // Track Segments
            for (int i = 0; i <= segmentList.Count - 1; i++)
            {
                segmentList[i].DestroySegment();
            }
            segmentList.Clear();

            // Track Additive layers
            for (int i = 0; i <= additiveLayerList.Count - 1; i++)
            {
                additiveLayerList[i].DestroySegment();
            }
            additiveLayerList.Clear();

            // Clean the segment container
            foreach (Transform child in TrackSegmentContainer.transform)
                Destroy(child.gameObject);
        }

    }
}
