﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlatMesh : MonoBehaviour
{

    public bool OnUpdate = false;

    [HideInInspector]
    public Transform ZoneMeshPointsTransform;
    private Mesh _lastGeneratedMesh = null;

    void Start()
    {
        // Generate Mesh
        GenerateZoneMesh();
    }


    void Update()
    {
        // Re-Generate Mesh
        if (OnUpdate)
        {
            GenerateZoneMesh();
        }
    }


    #region Gizmos

    void OnDrawGizmosSelected()
    {
        Transform[] posTransList = transform.GetComponentsInChildren<Transform>();
        for (int i = 0; i <= posTransList.Length - 1; i++)
        {
            if (posTransList[i].transform != transform && posTransList[i].transform != transform)
                Gizmos.DrawSphere(new Vector3(posTransList[i].position.x, posTransList[i].position.y, posTransList[i].position.z), 0.25f);
        }
    }

    #endregion




    #region MESH GENERATION

    public void GenerateZoneMesh(bool fromEditor = false)
    {
        if (transform == null) return;

        // Get all child positions
        Transform[] posTransList = transform.GetComponentsInChildren<Transform>();
        List<Vector2> meshUVs = new List<Vector2>();

        // Convert positions
        List<Vector2> convertedPos = new List<Vector2>();
        for (int i = 0; i <= posTransList.Length - 1; i++)
        {
            if (posTransList[i].transform != transform && posTransList[i].transform != transform)
            {
                convertedPos.Add(new Vector2(posTransList[i].localPosition.x, posTransList[i].localPosition.y));
                meshUVs.Add(new Vector2(posTransList[i].localPosition.x / 50.0f, posTransList[i].localPosition.y / 50.0f));
            }
        }

        // Use the triangulator to get indices for creating triangles
        FlatMeshCalculator tr = new FlatMeshCalculator(convertedPos.ToArray());
        int[] indices = tr.Triangulate();

        // Create the Vector3 vertices
        Vector3[] vertices = new Vector3[convertedPos.Count];
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = new Vector3(convertedPos[i].x, convertedPos[i].y, 0);
        }

        // Prevent memory leaks
        if (_lastGeneratedMesh != null)
        {
            _lastGeneratedMesh.Clear();
            if (!fromEditor)
                Destroy(_lastGeneratedMesh);
            else
                DestroyImmediate(_lastGeneratedMesh);
            _lastGeneratedMesh = null;
        }

        // Create the new mesh
        _lastGeneratedMesh = new Mesh();
        _lastGeneratedMesh.vertices = vertices;
        _lastGeneratedMesh.triangles = indices;
        _lastGeneratedMesh.uv = meshUVs.ToArray();
        _lastGeneratedMesh.RecalculateNormals();
        _lastGeneratedMesh.RecalculateBounds();

        // Apply new mesh
        if (!fromEditor)
        {
            Destroy(transform.GetComponent<MeshFilter>().mesh);
            transform.GetComponent<MeshFilter>().mesh = _lastGeneratedMesh;
        }
        else
        {
            // Editor
            DestroyImmediate(transform.GetComponent<MeshFilter>().sharedMesh);
            transform.GetComponent<MeshFilter>().sharedMesh = _lastGeneratedMesh;
        }

    }


    public void DestroyZoneMesh(bool fromEditor = false)
    {
        // Prevent memory leaks
        if (_lastGeneratedMesh != null)
        {
            _lastGeneratedMesh.Clear();
            if (!fromEditor)
                Destroy(_lastGeneratedMesh);
            else
                DestroyImmediate(_lastGeneratedMesh);

            _lastGeneratedMesh = null;
        }

        // Apply new mesh
        if (!fromEditor)
        {
            Destroy(transform.GetComponent<MeshFilter>().mesh);
            transform.GetComponent<MeshFilter>().mesh = null;
        }
        else
        {
            // Editor
            DestroyImmediate(transform.GetComponent<MeshFilter>().sharedMesh);
            transform.GetComponent<MeshFilter>().sharedMesh = null;
        }
    }

    #endregion
}
