﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FlatMesh)), CanEditMultipleObjects]
public class FlatMeshEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        FlatMesh myTarget = (FlatMesh)target;
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        if (GUILayout.Button("Generate Mesh"))
        {
            myTarget.GenerateZoneMesh(true);
            myTarget.transform.GetComponent<Renderer>().enabled = true;
        }

        if (GUILayout.Button("Destroy Mesh"))
        {
            myTarget.DestroyZoneMesh(true);
        }
        EditorGUILayout.Space();
    }
}
