﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendDataButton : MonoBehaviour
{
    public OSC_Sender Sender;
    public void SendData()
    {
        /*
        float[] fData1 = new float[] { 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f };
        float[] fData2 = new float[] { 0.11f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f };
        float[] fData3 = new float[] { 0.12f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f };
        float[] fData4 = new float[] { 0.13f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f };

        Sender.SendFloatArray("/fundfreq/channelA", fData1);
        Sender.SendFloatArray("/fundfreq/channelB", fData2);
        Sender.SendFloatArray("/fundfreq/channelC", fData3);
        Sender.SendFloatArray("/fundfreq/channelD", fData4);
        */

        Sender.SendFloat("/fundfreq/channelA", 0.10F);
        Sender.SendFloat("/fundfreq/channelA", 0.11F);
        Sender.SendFloat("/fundfreq/channelA", 0.12F);
        Sender.SendFloat("/fundfreq/channelA", 0.13F);
        Sender.SendFloat("/fundfreq/channelA", 0.20F);
        Sender.SendFloat("/fundfreq/channelA", 0.21F);
        Sender.SendFloat("/fundfreq/channelA", 0.22F);
        Sender.SendFloat("/fundfreq/channelA", 0.23F);
        Sender.SendFloat("/fundfreq/channelA", 0.30F);
        Sender.SendFloat("/fundfreq/channelA", 0.31F);
        Sender.SendFloat("/fundfreq/channelA", 0.32F);
        Sender.SendFloat("/fundfreq/channelA", 0.33F);
        Sender.SendFloat("/fundfreq/channelA", 0.40F);
        Sender.SendFloat("/fundfreq/channelA", 0.41F);
        Sender.SendFloat("/fundfreq/channelA", 0.42F);
        Sender.SendFloat("/fundfreq/channelA", 0.43F);
        Sender.SendFloat("/fundfreq/channelA", 0.50F);
        Sender.SendFloat("/fundfreq/channelA", 0.51F);
        Sender.SendFloat("/fundfreq/channelA", 0.52F);
        Sender.SendFloat("/fundfreq/channelA", 0.53F);
        Sender.SendFloat("/fundfreq/channelA", 0.60F);
        Sender.SendFloat("/fundfreq/channelA", 0.61F);
        Sender.SendFloat("/fundfreq/channelA", 0.62F);
        Sender.SendFloat("/fundfreq/channelA", 0.63F);
        Sender.SendFloat("/fundfreq/channelA", 0.70F);
        Sender.SendFloat("/fundfreq/channelA", 0.71F);
        Sender.SendFloat("/fundfreq/channelA", 0.72F);
        Sender.SendFloat("/fundfreq/channelA", 0.73F);
        Sender.SendFloat("/fundfreq/channelA", 0.80F);
        Sender.SendFloat("/fundfreq/channelA", 0.81F);
        Sender.SendFloat("/fundfreq/channelA", 0.82F);
        Sender.SendFloat("/fundfreq/channelA", 0.83F);

        //Debug.Log("SendDataButton: " + "/fundfreq/channel1" + fData1);
    }

    public void SendChannelEndData()
    {
        Sender.SendInt("/fundfreq/channelend", 1);
        Debug.Log("SendDataButton: " + "/fundfreq/channelend" );
    }
}
