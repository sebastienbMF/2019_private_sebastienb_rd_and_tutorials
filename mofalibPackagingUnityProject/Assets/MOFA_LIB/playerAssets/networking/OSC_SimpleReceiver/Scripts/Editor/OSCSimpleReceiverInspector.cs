﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(OSC_SimpleReceiver))]
public class OSCSimpleReceiverInspector : Editor 
{

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
        
        OSC_SimpleReceiver myScript = (OSC_SimpleReceiver)target;
        
        GUILayout.Space(10);
        if(GUILayout.Button("Save Config"))
        {
            myScript.SaveConfig();
        }
	}
}
