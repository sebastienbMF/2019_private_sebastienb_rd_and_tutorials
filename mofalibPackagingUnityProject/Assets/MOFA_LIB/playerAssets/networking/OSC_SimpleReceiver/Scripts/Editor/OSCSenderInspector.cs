﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(OSC_Sender))]
public class OSCSenderInspector : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
        
        OSC_Sender myScript = (OSC_Sender)target;
        
        GUILayout.Space(10);
        if(GUILayout.Button("Save Config"))
        {
            myScript.SaveConfig();
        }
	}
}
