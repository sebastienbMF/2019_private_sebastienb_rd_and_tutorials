﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using OSC.NET;

public class OSC_Sender : MonoBehaviour
{
	public OSCTransmitter _transmitter;
	public string ConfigFileName = "OscSenderConfig.json";
	[SerializeField]
	OSCSenderConfig _config;

	[Header ("Optional Info Display")]
	public Text _ipConfigText;
	public Text _portConfigText;


	// Use this for initialization
	void Start () 
	{
		LoadConfig();
		NewOscTransmitter();
	}

	void UpdateInfoDisplay()
	{
		if(_ipConfigText != null)
		{
			_ipConfigText.text = "IP: " + _config.Ip.ToString();
		}
		if(_portConfigText != null)
		{
			_portConfigText.text = "PORT: " + _config.Port.ToString();
		}
	}

	public void ChangePort(string newPort)
	{
		if(newPort != null && newPort != "")
		{
			_config.Port = Convert.ToInt32 (newPort);
			NewOscTransmitter ();
		}
	}

	public void ChangeIP(string ip)
	{
		if(ip != null && ip != "")
		{
			_config.Ip = ip;
			NewOscTransmitter();
		}
	}

	public void NewOscTransmitter()
	{
        try
        {
			if(_transmitter != null)
			{
            	_transmitter.Close();
			}
        }
        catch (Exception e)
        {
            print(e);
        }
		
		_transmitter = new OSCTransmitter (_config.Ip, _config.Port);
		UpdateInfoDisplay();
	}

	public void SendFloat(string adress, float f)
	{
		OSCMessage message = new OSCMessage(adress,f);
		_transmitter.Send (message);
		
	}

	public void SendFloatArray(string adress, float[] array)
	{
		OSCMessage message = new OSCMessage(adress,array[0]);
		for(int i = 1; i< array.Length; i++)
		{
			message.Append(array[i]);
		}
		_transmitter.Send (message);
	}
	

	public void SendInt(string adress, int i)
	{
		OSCMessage message = new OSCMessage(adress,i);
		_transmitter.Send (message);
	}

	public void SendVector3(string adress, Vector3 vector)
	{
		OSCMessage message = new OSCMessage(adress,vector.x*-1);
		message.Append (vector.y);
		message.Append (vector.z);
		
		_transmitter.Send (message);
	}
	

	public void SendColor(string adress, Vector3 vector)
	{
		OSCMessage message = new OSCMessage(adress,vector.x);
		message.Append (vector.y);
		message.Append (vector.z);
		
		_transmitter.Send (message);
		
	}

	public void SendString(string adress, string value)
	{
		OSCMessage message = new OSCMessage(adress,value);
	
		_transmitter.Send(message);
		
	}

	void SaveSettings()
	{
		PlayerPrefs.SetString ("ip", _config.Ip);
		PlayerPrefs.SetInt ("port", _config.Port);
	}

	void LoadConfig()
	{
		string json = File.ReadAllText(Application.streamingAssetsPath + "/" + ConfigFileName);
		_config = JsonUtility.FromJson<OSCSenderConfig>(json);
	}

	public void SaveConfig()
	{
		string json = JsonUtility.ToJson(_config, true);
		string path = Application.streamingAssetsPath + "/" + ConfigFileName;
		File.WriteAllText(path, json);
		Debug.Log("Config Saved at: " + path);

	}
}

[Serializable]
public struct OSCSenderConfig
{
	public string Ip;
	public int Port;
}
