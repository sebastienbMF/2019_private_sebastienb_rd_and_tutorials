﻿using UnityEngine;
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using OSC.NET;

public class OSC_SimpleReceiver : MonoBehaviour
{

	public OSCSimpleReceiverConfig Config;
	public string ConfigFileName;

	bool _connected = false;
    public bool _interactivity = true;
	OSCReceiver _receiver;
	Thread _thread;
	string _connectMessage = "";
	public string ConnectMessage { get {return _connectMessage;}}
	List<OSCMessage> _messageQueue = new List<OSCMessage>();

    public void Connect()
	{
		try 
		{
			_connected = true;
			_receiver = new OSCReceiver(Config.Port);
			_thread = new Thread(new ThreadStart(listen));
			_thread.Start();
			Debug.Log("OSC reciever "+ gameObject.name +" connected to port " + Config.Port );
			_connectMessage = "OSC reciever connected to port " + Config.Port.ToString();
		}
		catch (Exception e) 
		{
			Debug.Log("Failed to connect to port " + Config.Port);
			_connectMessage = "Failed to connect to port "+ Config.Port;
			Debug.Log(e.Message);
		}
	}

	public int getPort() 
	{
		return Config.Port;
	}

	public bool isConnected() 
	{
		return _connected; 
	}

    public void StopInteractivity()
    {
        _interactivity = false;
    }

    public void StartInteractivity()
    {
        _interactivity = true;
    }
	
	// Update is called once per frame
	void Update () 
	{
		//processMessages has to be called on the main thread
		//so we used a shared proccessQueue full of OSC Messages
		lock(_messageQueue)
		{
            foreach (OSCMessage message in _messageQueue)
            {
                //print(message.Address);
                if (_interactivity)
                {
                    switch (message.Address)
                    {
                        
                        case "/example/dostuff":
                            // do stuff
                            break;
                    }
                }

                
            }
			_messageQueue.Clear();
		}
	}

	string ParseMessage(OSCMessage message)
	{
		string output = message.Address + " : ";
		for (int i = 0; i< message.Values.Count; i++)
		{
			try
			{
				output += message.Values[i].ToString() + " : ";
			}
			catch(Exception e)
			{
				Debug.Log(e);
			}
		}
		//_messageCount += 1;
		return output + " "; //+ _messageCount;
	}

	public void OnApplicationQuit()
	{
		disconnect();
	}
	
	public void disconnect() 
	{
		if (_receiver!=null)
		{
			_receiver.Close();
		}
		
		_receiver = null;
		_connected = false;
		Debug.Log("Osc Reciever Disconnected");
	}
	
	private void listen() 
	{
		while(_connected) 
		{
			try 
			{
				OSCPacket packet = _receiver.Receive();
				if (packet!=null) 
				{
					lock (_messageQueue)
					{

						if (packet.IsBundle()) 
						{
							foreach (OSCMessage message in packet.Values)
							{
								_messageQueue.Add(message as OSCMessage );
							}
						} 
						else
						{
							_messageQueue.Add(packet as OSCMessage);
						}
					}
				} 
				else Console.WriteLine("null packet");
			} 
			catch (Exception e) 
			{ 
				Debug.Log( e.Message);
				Console.WriteLine(e.Message); 
			}
		}
	}

	void LoadConfig()
	{
		string json = File.ReadAllText(Application.streamingAssetsPath + "/" + ConfigFileName);
		Config = JsonUtility.FromJson<OSCSimpleReceiverConfig>(json);
	}

	public void SaveConfig()
	{
		string json = JsonUtility.ToJson(Config, true);
		string path = Application.streamingAssetsPath + "/" + ConfigFileName;
		File.WriteAllText(path, json);
		Debug.Log("Config Saved at: " + path);
	}
}

[Serializable]
public struct OSCSimpleReceiverConfig
{
	public int Port;
}
