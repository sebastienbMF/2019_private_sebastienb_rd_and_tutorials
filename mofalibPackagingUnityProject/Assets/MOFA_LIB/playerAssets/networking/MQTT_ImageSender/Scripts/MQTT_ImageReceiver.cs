﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using uPLibrary.Networking.M2Mqtt.Messages;
using UnityEngine.UI;

namespace MofaLib
{
    public class MQTT_ImageReceiver : MonoBehaviour
    {

        public AsyncMqtt MqttManager;
        [HideInInspector] public Texture2D TextureRef = null;

        private Sprite newSprite = null;


        void Start()
        {
            MqttManager.OnMqttMessageReceived += AddReceivedMqttMessage;
        }


        public void AddReceivedMqttMessage(MqttMsgPublishEventArgs e)
        {
            if (e.Topic.Contains("/debug/unityframe"))
            {
                if (TextureRef != null) Object.Destroy(TextureRef);
                TextureRef = new Texture2D(1, 1);
                TextureRef.LoadImage(e.Message);
                TextureRef.Apply();

                // Use Material
                if (GetComponent<Renderer>())
                {
                    GetComponent<Renderer>().material.SetTexture("_MainTex", TextureRef);
                }

                // Use Sprite / UI
                if (GetComponent<Image>())
                {
                    if (newSprite != null) Object.Destroy(newSprite);
                    newSprite = Sprite.Create(TextureRef, new Rect(0.0f, 0.0f, TextureRef.width, TextureRef.height), new Vector2(0.5f, 0.5f), 100.0f);
                    GetComponent<Image>().sprite = newSprite;
                }
            }
        }
    }
}
