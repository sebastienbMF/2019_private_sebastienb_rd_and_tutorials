﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;

namespace MofaLib
{
    public class MQTT_ImageSender : MonoBehaviour
    {

        public AsyncMqtt MqttManager;
        public KeyCode DebugKey = KeyCode.Space;
        public bool UseUpdate = false;
        public float RefreshTime = 1.0f;

        private Coroutine curCoroutine = null;


        void Update()
        {
            if (UseUpdate)
                if (curCoroutine == null)
                    curCoroutine = StartCoroutine(sendImageFrame());

            if (Input.GetKeyDown(DebugKey)) StartCoroutine(sendImageFrame());
        }


        public IEnumerator sendImageFrame()
        {
            yield return new WaitForEndOfFrame();

            // Create a texture the size of the screen, RGB24 format
            int width = Screen.width;
            int height = Screen.height;
            Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
            tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
            tex.Apply();

            MqttManager.sendMqttMsgToTopic(MqttManager._config._rootTopic + "/debug/unityframe", tex.EncodeToPNG());

            yield return new WaitForSeconds(RefreshTime);
            curCoroutine = null;
        }
    }
}
