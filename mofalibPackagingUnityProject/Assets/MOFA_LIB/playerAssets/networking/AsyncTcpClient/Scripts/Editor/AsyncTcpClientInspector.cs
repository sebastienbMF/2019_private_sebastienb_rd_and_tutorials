﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MofaLib;

[CustomEditor(typeof(AsyncTCPClient))]
public class AsyncTcpClientInspector : Editor
{
    public override void OnInspectorGUI()
    {
        AsyncTCPClient myTarget = (AsyncTCPClient)target;
		
		DrawDefaultInspector();
		
		GUILayout.Space(20);
		if(GUILayout.Button("Save Config"))
        {
            myTarget.SaveConfig(myTarget.Config);
        }
		if(GUILayout.Button("Load Config"))
        {
            myTarget.Config = myTarget.LoadConfig(myTarget.Config);
        }
    }
}
