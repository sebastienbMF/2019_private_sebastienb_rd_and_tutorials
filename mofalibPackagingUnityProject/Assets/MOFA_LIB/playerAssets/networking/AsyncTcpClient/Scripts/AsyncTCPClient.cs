﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

//Component that establishes a TCP connection to a server, and then receives TCP messages (null-terminated strings) asynchronously.
//Messages will then be sent to anything registered as a delegate, from the main thread (so you can update game engine stuff)
namespace MofaLib
{
    public class AsyncTCPClient : LoadExternalConfig
    {
        //Can be either an IP address or hostname

        public TcpClientConfig Config;

        public delegate void TCPMessageReceivedDelegate(string message);
        public TCPMessageReceivedDelegate OnTcpMessageReceived;

        private TcpClient tcpClient = new TcpClient();

        private Thread clientReceiveThread;
        private bool running = false;

        private List<string> incomingMessages = new List<string>();

        void Start()
        {
            ConnectToTcpServer();
        }

        void OnEnable()
        {
            ConnectToTcpServer();
        }

        void OnDisable()
        {
            DisconnectFromTcpServer();
        }

        void OnDestroy()
        {
            DisconnectFromTcpServer();
        }

        void OnAppplicationQuit()
        {
            DisconnectFromTcpServer();
        }

        void Update()
        {
            lock (this.incomingMessages)
            {
                foreach (string tcpMessage in this.incomingMessages)
                {
                    Debug.Log("AsyncTCPClient Got A Message!: " + tcpMessage);
                    if(OnTcpMessageReceived != null)
                    {
                        OnTcpMessageReceived(tcpMessage);
                    }
                }
                this.incomingMessages.Clear();
            }
        }

        private void ConnectToTcpServer()
        {
            if (!this.running)
            {
                this.running = true;

                //Add load config from json

                try
                {
                    this.clientReceiveThread = new Thread(new ThreadStart(ListenForData));
                    this.clientReceiveThread.IsBackground = true;
                    this.clientReceiveThread.Start();
                }
                catch (Exception e)
                {
                    Debug.Log("AsyncTCPClient ConnectToTcpServer exception: " + e);
                    return;
                }
            }
        }

        private void DisconnectFromTcpServer()
        {
            if (this.running)
            {
                //This will cause the TCP receiving thread to exit eventually
                this.running = false;
                try
                {
                    if (this.tcpClient.Connected)
                    {
                        this.tcpClient.GetStream().Close();
                        this.tcpClient.Close();
                    }
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("TCP Disconnect Exception: {0}", e);
                }
            }
        }

        private void ListenForData()
        {
            while (this.running)
            {
                try
                {
                    //This seems to be the only way to reconnect a disconnected socket, you cannot reuse the broken socket to do a new connection
                    this.tcpClient = new TcpClient();

                    this.tcpClient.Connect(Config.TcpServerIP, Config.TcpServerPort);

                    Byte[] bytes = new Byte[this.tcpClient.ReceiveBufferSize];

                    //According to doc, if Read returns 0, socket is in an error state
                    int bytesRead;
                    while (this.running &&
                        this.tcpClient.Connected &&
                        (bytesRead = this.tcpClient.GetStream().Read(bytes, 0, (int)this.tcpClient.ReceiveBufferSize)) > 0)
                    {
                        string message = Encoding.ASCII.GetString(bytes);
                        lock (incomingMessages)
                        {
                            incomingMessages.Add(message);
                        }
                    }
                }
                catch (SocketException socketException)
                {
                    Debug.Log("AsyncTCPClient Socket receive SocketException: " + socketException);
                }
                catch (Exception e)
                {
                    Debug.Log("AsyncTCPClient Socket receive Exception: " + e);
                }
            }
        }

        public void TCPSend(string message)
        {
            message += '\0';

            try
            {
                if (this.running && this.tcpClient.Connected)
                {
                    byte[] byteData = Encoding.ASCII.GetBytes(message);
                    this.tcpClient.GetStream().Write(byteData, 0, message.Length);
                }
                else
                {
                    Debug.Log("AsyncTCPClient Socket not created, dropping message: " + message);
                }
            }
            catch (Exception exception)
            {
                Debug.Log("AsyncTCPClient Socket send exception: " + exception + " while sending message: " + message);
            }
        }
    }

    [Serializable]
    public class TcpClientConfig
    {
        public string TcpServerIP;
        public int TcpServerPort;
    }
}
