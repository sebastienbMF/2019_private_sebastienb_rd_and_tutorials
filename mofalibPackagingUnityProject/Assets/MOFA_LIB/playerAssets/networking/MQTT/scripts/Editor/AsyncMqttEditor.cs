﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(AsyncMqtt))]
public class AsyncMqttEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		AsyncMqtt myTarget = (AsyncMqtt)target;
		EditorGUILayout.Space();
		EditorGUILayout.Space();

		if(GUILayout.Button("SaveConfig"))
        {
            myTarget.SaveConfig();
        }
		EditorGUILayout.Space();

		if(GUILayout.Button("LoadConfig"))
        {
            myTarget.LoadConfig();
        }
		EditorGUILayout.Space();
		
		if(GUILayout.Button("Test"))
        {
            myTarget.TestMessages();
        }
		EditorGUILayout.Space();
	}
}
