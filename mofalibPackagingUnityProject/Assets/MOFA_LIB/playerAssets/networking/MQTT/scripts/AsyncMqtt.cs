using UnityEngine;
using System;
using System.Net;
using System.Diagnostics;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;


// dependencies: MFVariousExtensions

public enum MQTTlogStateEnum
{
    Everything = 0,
    Warnings = 1,
    Error = 2,
    Silent = 3
}

public class AsyncMqtt : MonoBehaviour
{
    public bool _connected = false;
    public MqttConfig _config;

    string _clientId;
    MqttClient _client;
    List<MqttMsgPublishEventArgs> _messageList = new List<MqttMsgPublishEventArgs>();
    X509Certificate _certificate;

    public delegate void MessageDelegate(MqttMsgPublishEventArgs newMQTTmsg);
    public MessageDelegate OnMqttMessageReceived; // Usage:  GetComponent<AsyncMqtt>()._MQTT_messageListener += AddReceivedMqttMessage; (OnEnable & OnDisable)
    [SerializeField]
    string[] _subscribedTopics;

    void Start()
    {
        ConnectToBroker();
        OnMqttMessageReceived += ChangeMQTTLogState;
    }

    void OnDisable()
    {
        if (_connected)
        {
            Application.logMessageReceived -= LogToMQTT;
            _connected = false;
            _client.Disconnect();
        }
    }

    public void SaveConfig()
    {
        _config.SaveJson("MQTTConfig.json");
    }

    public void LoadConfig()
    {
        _config = _config.LoadJson("MQTTConfig.json");
    }

    void ConnectToBroker()
    {
        MqttConfig config = new MqttConfig();
        config = config.LoadJson("MQTTConfig.json");
        if(config._loadFromFile)
        {
            _config = config;
        }
        
        _config._rootTopic = string.Format(_config._rootTopic, Environment.MachineName.ToLower());

        try
        {
            UnityEngine.Debug.Log("Try connect to mqtt broker...");
            _clientId = Guid.NewGuid().ToString();
            if(_config._useSSL)
            {
                // Load new certificate
                byte[] certStr;
                if(_config._cert_loadString == "")
                {
                    _config._cert_loadFromStreamingAssets = true;
                }
                if (_config._cert_loadFromStreamingAssets)
                {
                    _certificate = new System.Security.Cryptography.X509Certificates.X509Certificate();
                    _certificate.Import(Application.streamingAssetsPath + "/mofa_studio_ca_cert.ca");
                }
                else
                {
                    // Load certificate from string
                    certStr = System.Text.Encoding.UTF8.GetBytes(_config._cert_loadString);
                    _certificate = new System.Security.Cryptography.X509Certificates.X509Certificate(certStr);
                }

                // Skip certificate verification in case server is misconfigured or has a self-signed cert.
                if (_config._verifyServerCertificate)
                {
                    _client = new MqttClient(_config._address, _config._port, true, _certificate, null, MqttSslProtocols.TLSv1_0);
                }
                else
                {
                    _client = new MqttClient(_config._address, _config._port, true, _certificate, null, MqttSslProtocols.TLSv1_0, SkipCertificateVerificationCallback);
                }

                _client.Connect(_clientId, "connectedfactory", "UhkzYQgubAouyV4uQEULadAA");

            }
            else
            {
                _client = new MqttClient(_config._address);
                _client.Connect(_clientId);
            }
            
            _client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            _connected = true;
             Application.logMessageReceived += LogToMQTT;
             
            if (_config._topicsGroup.Length > 0)
            {
                UnityEngine.Debug.Log("try subscribe");
                SubscribeToTopic(_config._topicsGroup);
            }
           
            SendMqttMessage("Unity Connected!");
            UnityEngine.Debug.Log("Connected to broker");
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError("MQTT ERROR" + e);
        }
    }

    bool SkipCertificateVerificationCallback(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
        return true;
    }

    public void SubscribeToTopic(string[] topics)
    {
        if (_connected)
        {
            _subscribedTopics = new string[topics.Length];
            for(int i = 0; i< topics.Length; i++)
            {
                _subscribedTopics[i] = _config._rootTopic + topics[i];
                UnityEngine.Debug.Log("MQTT Sbscribing to: " + _subscribedTopics[i]);
               _client.Subscribe(new string[] {_subscribedTopics[i]} , new byte[] { _config._qos_level });
            }
        }
    }

    void Update()
    {
        if (_connected)
        {
            try
            {
                 ParseMqttMessages();
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log("MQTT MESSAGE ERROR: " + e);
                lock(_messageList)
                _messageList.Clear();
            }
        }
    }

    public void TestMessages()
    {
        UnityEngine.Debug.Log("small shit!");
        UnityEngine.Debug.LogWarning("bigger shit!");
        UnityEngine.Debug.LogError("BIG BIG SHIT!");
    }


    void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 
        lock(_messageList)
        {
            _messageList.Add(e);
        }
    }

    // sends unity log to MQTT broker according to log level
    public void LogToMQTT(string logString, string stackTrace, LogType type)
    {
        string ts = DateTime.Now.ToString();
        switch (_config._logState)
        {
            // everything
            case MQTTlogStateEnum.Everything:
                _client.Publish(_config._rootTopic + _config._logTopic, System.Text.Encoding.UTF8.GetBytes(ts + ": " + logString), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, false);
                break;
            
            // errors and exceptions
            case MQTTlogStateEnum.Warnings:
                if (type == LogType.Error || type == LogType.Exception || type == LogType.Warning)
                {
                    _client.Publish(_config._rootTopic + _config._logTopic, System.Text.Encoding.UTF8.GetBytes(ts + ": " + logString), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE,false);
                }
                break;

            // errors only
            case MQTTlogStateEnum.Error:
                if (type == LogType.Error || type == LogType.Exception)
                {
                    _client.Publish(_config._rootTopic + _config._logTopic, System.Text.Encoding.UTF8.GetBytes(ts + ": " + logString), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE,false);
                }
                break;

            // no logs
            case MQTTlogStateEnum.Silent:
                break;
        }
    }

    public void SendMqttMessage(string message)
    {
        if (_connected)
        {
            _client.Publish(_config._logTopic, System.Text.Encoding.UTF8.GetBytes(message), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, false);
        }
    }

    void ParseMqttMessages()
    {
        lock(_messageList)
        foreach (MqttMsgPublishEventArgs msg in _messageList)
        {
            string msgString = System.Text.Encoding.UTF8.GetString(msg.Message);
            print ("MQTT Received: " + msgString);

            // Register Messages
            if(OnMqttMessageReceived != null)
            {
                OnMqttMessageReceived(msg); 
            }
        }
        _messageList.Clear();
    }

    public void ChangeMQTTLogState(MqttMsgPublishEventArgs msg)
    {
        // Update Log State
        if(msg.Topic != (_config._rootTopic + _config._logTopic))
        {
            return;
        }

        string payload = System.Text.Encoding.UTF8.GetString(msg.Message);

        if (payload.Contains("log0"))
            {
                _config._logState = MQTTlogStateEnum.Everything;
                sendMqttMsgToTopic(_config._rootTopic + _config._logTopic, "Log State changed to everything");
            }
            if (payload.Contains("log1"))
            {
                _config._logState = MQTTlogStateEnum.Warnings;
                sendMqttMsgToTopic(_config._rootTopic + _config._logTopic, "Log State changed to warning and errors");
            }
            if (payload.Contains("log2"))
            {
                _config._logState = MQTTlogStateEnum.Error;
                sendMqttMsgToTopic(_config._rootTopic + _config._logTopic, "Log State changed to errors only");
            }
             if (payload.Contains("log3"))
            {
                _config._logState = MQTTlogStateEnum.Silent;
                sendMqttMsgToTopic(_config._rootTopic + _config._logTopic, "Log State changed to silent");
            }
    }

    public void sendMqttMsgToTopic(string topic, string message)
    {
        UnityEngine.Debug.Log("SENDING MESSAGE: " + topic + " " + message);
        if (_connected)
            _client.Publish(topic, System.Text.Encoding.UTF8.GetBytes(message), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, false);
    }

    public void sendMqttMsgToTopic(string topic, byte[] payload)
    {
        UnityEngine.Debug.Log("SENDING MESSAGE: " + topic + " " + payload);
        if (_connected)
            _client.Publish(topic, payload, MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, false);
    }

}

[Serializable]
public class MqttConfig
{
    public bool _loadFromFile;
    [Header("Server")]
    [Tooltip ("defaut MF broker: connectmqtt.mofa.studio")] 
    public string _address = "connectmqtt.mofa.studio"; 
    [Tooltip ("default MF port:  8883")]
    public int _port = 8883; 
    
    [Header("SSL")]
    public bool _useSSL;
    public bool _cert_loadFromStreamingAssets = false;
    public string _cert_loadString = "";
    [Tooltip("Uncheck at your own risk! This may allow server impersonation.")]
    public bool _verifyServerCertificate = true;

    [Header ("Subscribtions")]
    [Tooltip ("Should be nameOfProject/computer/{0}/unity where {0} is gonna be replaced by the computer's name")]
    public string _rootTopic; 
    [Tooltip ("to be appended to root topic")]
    public string[] _topicsGroup; 

    [Header ("QOS")]
    [Tooltip("0 = at least once, 1 = at most once, 2 = exactly once")] 
    public byte _qos_level = 2;

    [Header ("Error logs")]
    public string _logTopic = "logs";
    public MQTTlogStateEnum _logState = MQTTlogStateEnum.Everything;
}

