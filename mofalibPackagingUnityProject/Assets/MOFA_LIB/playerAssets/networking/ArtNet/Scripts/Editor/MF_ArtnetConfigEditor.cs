﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(MF_ArtnetConfigManager))]
public class MF_ArtnetConfigEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		MF_ArtnetConfigManager myTarget = (MF_ArtnetConfigManager)target;
		EditorGUILayout.Space();
		EditorGUILayout.Space();

		if(GUILayout.Button("SaveConfig"))
        {
            myTarget.SaveConfig();
        }
		EditorGUILayout.Space();
	}
}
