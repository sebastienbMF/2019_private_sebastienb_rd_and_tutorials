﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Diagnostics;
using System.Net;
using ArtNet.Sockets;
using ArtNet.Packets;

public class MF_ArtNetSender : MonoBehaviour
{
    [Header("Artnet Config")]
    public string _localIp = "10.10.80.49"; // should be localhost
    public string _remoteEndPoint = "10.10.80.49"; // the remote Ip
    public string _locaSubnetMask = "255.255.0.0";
    public int _universeToSend = 0;
    public bool _connected = false;
    public string _deviceName;

    [Header("color test")]
    public Color[] _testColor;

    ArtNetSocket _socket;

    void Start()
    {
        ArtnetConfigJson config = new ArtnetConfigJson();
        config = config.LoadJson("artnetconfig.json");
        _localIp = config.LocalIp;
        ArtnetConfig specs = config.ConfigSpecs.SingleOrDefault(item => item.DeviceName == _deviceName);
        _remoteEndPoint = specs.RemoteIp;
        _locaSubnetMask = specs.SubnetMask;
        _universeToSend = specs.UniverseToSend;
        ConnectArtnet();
    }
   
    void ConnectArtnet()
    {
        try
        {
            UnityEngine.Debug.Log("Try to open artnet socket...");
            _socket = new ArtNetSocket();
            _socket.EnableBroadcast = false;
            _socket.Open(IPAddress.Parse(_localIp), IPAddress.Parse(_locaSubnetMask));
            _connected = true;
            print("Artnet Connected");
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogWarning("--- ARTNET ERROR: " + e);
        }

        // ça prendrait un timeout
    }

    void CloseArtnet()
    {
        _socket.Close();
    }

    void OnDisable()
    {
        if (_socket != null)
        {
            _socket.Close();
        }
    }

    void Update()
    {
        if (_connected)
        {
            Color[] col = _testColor;

            byte[] values = new byte[col.Length * 3];
            int index = 0;
            for (int i = 0; i < col.Length; i++)
            {
                Color32 color = col[i];
                values[index] = color.r;
                values[index + 1] = color.g;
                values[index + 2] = color.b;
                
                index += 3;
            }
            SendArtnet(_universeToSend, values);
        }
    }

    void SendArtnet(int universe, byte[] data)
    {
        ArtNetDmxPacket toSend = new ArtNetDmxPacket();
        toSend.DmxData = data;
        toSend.Universe = (short)universe;
        _socket.SendTo(toSend.ToArray(), new IPEndPoint(IPAddress.Parse(_remoteEndPoint), 6454));
    }
}



