﻿using UnityEngine;

namespace MomentFactory
{
    /// <summary>
    /// Self contained first person camera controller.
    /// Mostly for vizualization and in-editor needs like simulating VR/AR views and movement
    /// </summary>
    public class FirstPersonCamera : MonoBehaviour
    {
        public bool EditorOnly = false;

        [Tooltip("Clicking in viewport with left mouse button will toggle this camera controller on/off")]
        public bool LeftClickToggleActivation = false;

        [Tooltip("Holding the right mouse button will activate this camera (like Unity Scene camera, but for runtime)")]
        public bool RightClickHoldActivation = false;

        public bool UseScaledDeltaTime = false;

        [Tooltip("This controller will keep controlling the target camera even when controls not active")]
        public bool TranformWhenInactive = true;

        [Tooltip("Optional: The camera to control")]
        public Camera TargetCamera;

        [Tooltip("Optional: the transform to apply to the camera (only if camera is not parented to this transform)")]
        public Transform TransformToApply;

        [Header("Vertical Movement")]

        [Tooltip("If the camera will fly directly where the user points it to, otherwise the camera will be clipped to XZ plane")]
        public bool FreeFlyEnabled = false;

        public KeyCode ToggleFreeFlyKey = KeyCode.F;

        [Tooltip("If we can Y axis movement when using Q or E")]
        public bool ClimbingEnabled = true;

        public KeyCode ClimbUpKey = KeyCode.E;
        public KeyCode ClimbDownKey = KeyCode.Q;

        [Header("Movement")]
        [Tooltip("How much units movement per second"), Range(1f, 100f)]
        public float MovementSpeed = 10.0f;

        [Tooltip("How much Y axis movement when using Q or E"), Range(1f, 100f)]
        public float ClimbSpeed = 5.0f;

        [Tooltip("Multiplier on MovementSpeed when shift key is held"), Range(0.1f, 10f)]
        public float TurboMultiplier = 2.5f;

        [Tooltip("Multiplier on MovementSpeed when alt key is held"), Range(0.1f, 1f)]
        public float SlowmoMultiplier = 0.5f;

        /// @see https://docs.unity3d.com/ScriptReference/CharacterController.html
        [Tooltip("If we control a Unity CharacterController instead of the camera directly (will resolve wall collisions, etc.")]
        public CharacterController TargetCharacter;

        //float JumpSpeed = 8.0F;

        //float AdditionalGravity = 20.0F;

        [Header("Rotation"), Tooltip("How much degrees per second multiplied on rotation axises values")]
        public float RotationSpeed = 180f;

        [Tooltip("SmoothDamp in seconds for rotation"), Range(0f, 0.5f)]
        public float RotationDamping = 0.07f;

        [Tooltip("Multipliers on each XY rotation axises, zero will prevent any rotation on this axis")]
        public Vector2 RotationAmount = new Vector2(1, 0.75f);

        private Vector3 RotationVelocity = Vector3.zero;

        private Vector2 inputDirection = Vector2.zero;
        private Vector3 moveDirection = Vector3.zero;

        float MinVerticalAngle = -45f;
        float MaxVerticalAngle = 45f;

        bool IsTargetCameraParented;

        private Vector3 TargetAngles;

        private bool isActive = true;

        private void Awake()
        {
#if !UNITY_EDITOR
        if (EditorOnly)
        {
            Destroy(this);
        }
#endif
        }

        void Start()
        {
            TargetCharacter = GetComponent<CharacterController>();

            if (TargetCamera == null)
            {
                TargetCamera = Camera.main;
                if (TargetCamera != null) Debug.Log("Using Main Camera for " + this);
            }

            if (TargetCamera == null)
            {
                Debug.LogWarning("Main camera not found for " + this + " looking for any camera object...");
                TargetCamera = FindObjectOfType<Camera>();
            }

            IsTargetCameraParented = TargetCamera.transform.IsChildOf(transform);

            // Sync to camera
            if (!IsTargetCameraParented)
            {
                transform.SetPositionAndRotation(TargetCamera.transform.position, TargetCamera.transform.rotation);
            }

            TargetAngles = transform.localEulerAngles;

            if (LeftClickToggleActivation || RightClickHoldActivation)
            {
                if (RightClickHoldActivation)
                {
                    Debug.LogWarning("Hold Right Click on viewport to activate " + this);
                }
                if (LeftClickToggleActivation)
                {
                    Debug.LogWarning("Click on viewport to activate " + this);
                }

                isActive = false;
            }
        }

        void Update()
        {
            if (TargetCamera == null || !Application.isFocused) return;

            if (!isActive)
            {
                if ((LeftClickToggleActivation && Input.GetMouseButtonUp(0)) || (RightClickHoldActivation && Input.GetMouseButtonDown(1)))
                {
                    isActive = true;
                    Debug.Log("Activating " + this);
                }
                else
                {
                    return;
                }
            }
            else if ((LeftClickToggleActivation && Input.GetMouseButtonUp(0)) || (RightClickHoldActivation && Input.GetMouseButtonUp(1)))
            {
                isActive = false;
                Debug.Log("Deactivating " + this);
                return;
            }

            if (Input.GetKeyUp(ToggleFreeFlyKey))
            {
                FreeFlyEnabled = !FreeFlyEnabled;
                Debug.LogWarning("AllowFreeFly: " + FreeFlyEnabled + " on " + this);
            }

            // Scaled/unscaled delta time, smooth for SmoothDamp() and such does not like large delta times
            float deltaTime = Mathf.Min(UseScaledDeltaTime ? Time.deltaTime : Time.unscaledDeltaTime, 0.1f);

            Vector3 DeltaAngles = new Vector3(
                -Input.GetAxis("Mouse Y") * RotationSpeed * deltaTime * RotationAmount.y,
                Input.GetAxis("Mouse X") * RotationSpeed * deltaTime * RotationAmount.x,
                0
            );

            Vector3 CurrentAngles = TargetAngles;

            TargetAngles = new Vector3(
                CurrentAngles.x + Mathf.DeltaAngle(CurrentAngles.x, CurrentAngles.x + DeltaAngles.x),
                CurrentAngles.y + Mathf.DeltaAngle(CurrentAngles.y, CurrentAngles.y + DeltaAngles.y),
                CurrentAngles.z + Mathf.DeltaAngle(CurrentAngles.z, CurrentAngles.z + DeltaAngles.z)
            );

            // Simple way to go around clamping vertical axis
            if (TargetAngles.x < 0f)
            {
                TargetAngles.x = Mathf.Max(TargetAngles.x, MinVerticalAngle);
            }
            else
            {
                // Over 180f means rotator axis is flipping from zero to 360, so 180 degrees will split clamping logic fine 
                TargetAngles.x = TargetAngles.x > 180f ? Mathf.Max(TargetAngles.x, 360 - MaxVerticalAngle) : Mathf.Min(TargetAngles.x, -MinVerticalAngle);
            }


            if (RotationDamping > 0f)
            {
                Vector3 velocity = this.RotationVelocity;

                transform.localEulerAngles = new Vector3(
                    Mathf.SmoothDampAngle(transform.localEulerAngles.x, TargetAngles.x, ref velocity.x, RotationDamping, Mathf.Infinity, deltaTime),
                    Mathf.SmoothDampAngle(transform.localEulerAngles.y, TargetAngles.y, ref velocity.y, RotationDamping, Mathf.Infinity, deltaTime),
                    Mathf.SmoothDampAngle(transform.localEulerAngles.z, TargetAngles.z, ref velocity.z, RotationDamping, Mathf.Infinity, deltaTime)
                );

                this.RotationVelocity = velocity;
            }
            else
            {
                transform.localEulerAngles = TargetAngles;
            }

            inputDirection = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            if (FreeFlyEnabled)
            {
                moveDirection = transform.right * inputDirection.x + transform.forward * inputDirection.y;
            }
            else
            {
                Vector3 right = transform.right;
                right.y = 0;

                Vector3 forward = transform.forward;
                forward.y = 0;

                moveDirection = right * inputDirection.x + forward * inputDirection.y;
            }


            float speed = MovementSpeed;
            float climbSpeed = ClimbSpeed;
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                speed *= TurboMultiplier;
                climbSpeed *= TurboMultiplier;
            }
            else if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
            {
                speed *= SlowmoMultiplier;
                climbSpeed *= SlowmoMultiplier;
            }

            Vector3 worldMovement = moveDirection * speed * deltaTime;

            // Vertical climb E/Q
            if (ClimbingEnabled)
            {
                if (Input.GetKey(ClimbDownKey))
                {
                    worldMovement.y -= climbSpeed * deltaTime;
                }
                else if (Input.GetKey(ClimbUpKey))
                {
                    worldMovement.y += climbSpeed * deltaTime;
                }
            }

            // Use Unity Character Contoller?
            if (TargetCharacter != null)
            {
                // TODO: Jump, etc. 
                /*if (TargetCharacter.isGrounded)
                {
                    if (Input.GetButton("Jump"))
                    {
                        moveDirection.y = JumpSpeed;
                    }
                }*/

                //moveDirection.y -= gravity * Time.deltaTime;

                TargetCharacter.Move(worldMovement);
            }
            else
            {
                transform.position += worldMovement;

                // TODO: Clip to ground raycast when not flying
                // if (!CanFly)
            }
        }

        // Leave time for transform modifiers / physics before transfering transform state to camera
        void LateUpdate()
        {
            if (TargetCamera == null) return;
            if (!TranformWhenInactive && !isActive) return;

            // Control external camera
            if (!IsTargetCameraParented)
            {
                if (TransformToApply == null) TransformToApply = transform;
                TargetCamera.transform.SetPositionAndRotation(TransformToApply.position, TransformToApply.rotation);
            }
        }

        public static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360F)
                angle += 360F;
            if (angle > 360F)
                angle -= 360F;
            return Mathf.Clamp(angle, min, max);
        }

    }
}