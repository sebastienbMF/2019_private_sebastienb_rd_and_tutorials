﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory
{
    /// <summary>
    /// Aligns a QUAD in front of a camera
    /// Notes: 
    /// * Stretching is absolute for now: it will not keep any aspect ratio
    /// </summary>
    [ExecuteInEditMode, RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class CameraAlignedPlane : MonoBehaviour
    {
        public enum MeshFilterType
        {
            Quad,
            Plane
        }

        const float DefaultOffsetZ = 0.01f;

        [Tooltip("The camera to bind to (can be changed at runtime)")]
        public Camera TargetCamera;

        [Range(0f, 0.5f), Tooltip("Grayscale effect intensity.")]
        public float OffsetZ = DefaultOffsetZ;

        [Tooltip("Plane has more triangles, but less UV distortion")]
        public MeshFilterType MeshType = MeshFilterType.Plane;

        private MeshFilterType CurrentMeshType;

        // Use this for initialization
        void Start()
        {
            CurrentMeshType = MeshType;

#if UNITY_EDITOR
            // Edit mode
            if (!Application.isPlaying)
            {
                if (GetComponent<MeshFilter>().sharedMesh == null)
                {
                    SetDefaultMesh();
                }

                return;
            }
#endif

            if (TargetCamera == null) GetCamera();

        }

        void GetCamera()
        {
            TargetCamera = GetComponent<Camera>();
            if (TargetCamera == null && transform.parent != null) transform.parent.GetComponent<Camera>();
            if (TargetCamera == null) TargetCamera = Camera.main;
        }

        void SetDefaultMesh()
        {
            MeshFilter mesh = GetComponent<MeshFilter>();
            GameObject go;
            if (MeshType == MeshFilterType.Plane)
            {
                go = GameObject.CreatePrimitive(PrimitiveType.Plane);
            }
            else
            {
                go = GameObject.CreatePrimitive(PrimitiveType.Quad);
            }

            mesh.sharedMesh = go.GetComponent<MeshFilter>().sharedMesh;

            if (!Application.isPlaying)
            {
                DestroyImmediate(go);
            }
            else
            {
                Destroy(go);
            }
        }

        // Update is called once per frame
        void Update()
        {
#if UNITY_EDITOR
            // Edit mode
            if (!Application.isPlaying)
            {
                if (CurrentMeshType != MeshType)
                {
                    CurrentMeshType = MeshType;
                    SetDefaultMesh();
                }

                if (TargetCamera == null)
                {
                    GetCamera();
                    return;
                }
            }
#endif

            if (TargetCamera == null)
            {
                Debug.LogWarning("Target camera not found");
                GetCamera();
                return;
            }

            float pos = (TargetCamera.nearClipPlane + OffsetZ);

            Vector3 position = TargetCamera.transform.position + TargetCamera.transform.forward * pos;
            Quaternion rotation;


            // The unity plane is 10 times bigger and rotated 90 degrees around the x axis
            if (MeshType == MeshFilterType.Plane)
            {
                rotation = Quaternion.LookRotation(TargetCamera.transform.forward * -1, TargetCamera.transform.up) * Quaternion.Euler(90.0f, 0.0f, 0.0f);

                float h = (Mathf.Tan(TargetCamera.fieldOfView * Mathf.Deg2Rad * 0.5f) * pos * 2f) / 10.0f;
                transform.localScale = new Vector3(h * TargetCamera.aspect, 1.0f, h);
            }
            else
            {
                rotation = Quaternion.LookRotation(TargetCamera.transform.forward, TargetCamera.transform.up);

                float h = (Mathf.Tan(TargetCamera.fieldOfView * Mathf.Deg2Rad * 0.5f) * pos * 2f);
                transform.localScale = new Vector3(h * TargetCamera.aspect, 1.0f, h);
            }

            transform.SetPositionAndRotation(position, rotation);
        }


    }
}