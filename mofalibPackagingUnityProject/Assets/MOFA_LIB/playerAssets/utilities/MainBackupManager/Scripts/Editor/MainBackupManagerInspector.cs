﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MofaLib;

[CustomEditor(typeof(MainBackupManager))]
public class MainBackupManagerInspector : Editor 
{
	private string testTag;

	public override void OnInspectorGUI()
	{
		MainBackupManager myTarget = (MainBackupManager)target;
		
		EditorGUILayout.HelpBox("Singleton", UnityEditor.MessageType.Info);

		DrawDefaultInspector();
		
		GUILayout.Space(20);
		if(GUILayout.Button("Save Config"))
        {
            myTarget.SaveConfig();
        }

		GUILayout.Space(20);
		GUILayout.Label("This part is for local testing");
		GUILayout.Space(5);
		testTag = EditorGUILayout.TextField("Machine tag to test:", testTag);
		if(GUILayout.Button("Switch Mode"))
        {
            myTarget.SwitchMainBackup(testTag);
        }
	}

}
