﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBackupSwitchComponent : MainBackupSwitchBehavior
{
	public MonoBehaviour MyComponent;
	private IMainBackupSwitchable switchable;
	public bool ToggleEnable = true;

	protected override void OnEnable()
	{
		base.OnEnable();
		
		if(MyComponent.GetComponent<IMainBackupSwitchable>() != null)
		{
			this.switchable = MyComponent.GetComponent<IMainBackupSwitchable>();
		}

		if(!IsActive)
		{
			OnSetAsleep();
		}
	}

	protected override void OnSetActive()
	{
		base.OnSetActive();
		if(ToggleEnable)
		{
			MyComponent.enabled = true;
		}
		if(this.switchable != null)
		{
			this.switchable.OnSetActive();
		}
	}

	protected override void OnSetAsleep()
	{
		base.OnSetAsleep();
		if(this.switchable != null)
		{
			this.switchable.OnSetAsleep();
		}
		if(ToggleEnable)
		{
			MyComponent.enabled = false;
		}
		
	}
}
