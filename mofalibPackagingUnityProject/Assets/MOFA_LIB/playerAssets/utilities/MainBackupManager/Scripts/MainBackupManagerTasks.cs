﻿
// to use the task, make sure you have imported XagoraShowControl and uncomment the line below

//#define USE_XASHOWCONTROL 
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if (USE_XASHOWCONTROL)
using XagoraShowControl;

public class MainBackupManagerTasks : TaskBehaviour 
{
    [SerializeField]
	private MainBackupManager manager;
    
    [Conditional ("USE_SHOWCONTROL")]
    protected override void Start()
    {
        base.Start();
        if(MainBackupManager.Instance != null)
        {
            manager = MainBackupManager.Instance;
        }
    }

    [TaskAttributes("Pour transferer du backup au main ou vice et versa", "Main ou Backup")]
    public void MainBackupSwitch(string machineTag)
    {
        manager.SwitchMainBackup(machineTag);
    }
}
#endif

