﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MofaLib;

public class MainBackupSwitchBehavior : MonoBehaviour 
{
    protected MainBackupManager _mainBackupManager;
    public bool IsActive {get{return IsActiveMachine();}}

    protected virtual void OnEnable()
    {
        if(!ValidateMainBackupManager())
        {
            return;
        }

        _mainBackupManager.SwitchToActiveEvent += OnSetActive;
        _mainBackupManager.SwitchToSleepEvent += OnSetAsleep;
    }

    protected virtual void OnDisable()
    {
        if(!ValidateMainBackupManager())
        {
            return;
        }
        _mainBackupManager.SwitchToActiveEvent -= OnSetActive;
        _mainBackupManager.SwitchToSleepEvent -= OnSetAsleep;
    }

    // called by the Switch to active event
    protected virtual void OnSetActive()
    {

    }

    // called by the Switch to sleep event
    protected virtual void OnSetAsleep()
    {

    }

    protected bool IsActiveMachine()
    {
        if(ValidateMainBackupManager())
        {
            return _mainBackupManager.IsActiveMachine;
        }
        // By default if there is no backup manager the machine is considered to be active.
        return true;
    }

    protected bool ValidateMainBackupManager()
    {   
        if(_mainBackupManager == null)
        {
            if(MainBackupManager.Instance == null)
            {
                Debug.LogWarning("Could not find MainBackupManager Instance....");
                return false;
            }

            _mainBackupManager = MainBackupManager.Instance;
            Debug.Log("MainBackupManager Instance successfully found!");
        }       
        
        return true;
    }
}
