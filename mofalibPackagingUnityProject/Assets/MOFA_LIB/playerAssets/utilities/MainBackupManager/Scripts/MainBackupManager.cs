﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//------------------------------------------------------------------------------//
// DEPENDENCIES: Singleton                                                      //
//                                                                              //
// Make sure you change the execution order of this script so                   //
// the Singleton gets created before any other script tries to access it.       //
// Not doing this step may result in other components failing to register       //
// to events properly and not executing when required.                         //
// You can access this by going to Edit/Project Settings/Script Execution Order //
//                                                                              //
// It is a good idea to use the sleep icon to show the state (active/asleep)    //
// of the program on screen. This can help with debugging and prevent some       //
// confusion.                                                                   //
//------------------------------------------------------------------------------//

namespace MofaLib
{
    public class MainBackupManager  : Singleton<MainBackupManager>
    {
        public MainBackupConfig LocalConfig;

        public bool IsActiveMachine {get {return LocalConfig.IsActiveMachine;}}

        public Image SleepIcon;

        public delegate void SwitchToActiveDelegate();
        public event SwitchToActiveDelegate SwitchToActiveEvent;

        public delegate void SwitchToSleepDelegate();
        public event SwitchToSleepDelegate SwitchToSleepEvent;

        public void OnEnable()
        {
            SwitchToActiveEvent += FlagAsActive;
            SwitchToSleepEvent += FlagAsSleep;
        }

        void Start()
        {
            LoadConfig();
            if(IsActiveMachine)
            {
                SwitchToActiveEvent();
            }
            else
            {
                SwitchToSleepEvent();
            }
            DontDestroyOnLoad(this.gameObject);
        }

        public void SaveConfig()
        {
            LocalConfig.SaveJson("MainBackupConfig.json");
        }

        public void LoadConfig()
        {
            LocalConfig = LocalConfig.LoadJson("MainBackupConfig.json");
        } 
        
        public void SwitchMainBackup(string machineTag)
        {
            Debug.Log("Main Backup Switch received: " + machineTag);
            if(machineTag == LocalConfig.MachineTag)
            {
                LocalConfig.IsActiveMachine = true;
                if(SwitchToActiveEvent != null)
                {
                    SwitchToActiveEvent();
                }
            }

            if(machineTag != LocalConfig.MachineTag)
            {
                LocalConfig.IsActiveMachine = false;
                if(SwitchToSleepEvent != null)
                {
                    SwitchToSleepEvent();
                }
            }		
        }

        void FlagAsActive()
        {
            AudioListener.volume = 1;
            Debug.Log("I'm now the active machine!");
            if(SleepIcon == null)
            {
                Debug.LogWarning("you should think about implementing the sleep icon...");
                return;
            }
            SleepIcon.enabled = false;
        }

        void FlagAsSleep()
        {
            AudioListener.volume = 0;
            Debug.Log("Going to sleep... zzzz");
            if(SleepIcon == null)
            {
                Debug.LogWarning("you should think about implementing the sleep icon...");
                return;
            }
            SleepIcon.enabled = true;

        }
    }

    [Serializable]
    public struct MainBackupConfig
    {
        public bool IsActiveMachine;
        public string MachineTag;
    }
}
