﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBackupComponentExample : MonoBehaviour, IMainBackupSwitchable
{

	public float MyTimeUp;

		
	void Update () 
	{
		MyTimeUp += Time.deltaTime;	
	}

	public void OnSetActive()
	{
		MyTimeUp = 0;
	}
	

	public void OnSetAsleep()
	{
		print("Stop Counting time and going to bed....");
	}
}
