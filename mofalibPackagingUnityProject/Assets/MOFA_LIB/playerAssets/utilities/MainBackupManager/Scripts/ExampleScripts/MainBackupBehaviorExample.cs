﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBackupBehaviorExample : MainBackupSwitchBehavior 
{

	public Light MyLight;

	// called by the Switch to active event
	protected override void OnSetActive()
    {
		base.OnSetActive();
		MyLight.intensity = 1;
		MyLight.color = Color.red;
    }

    // called by the Switch to sleep event
    protected override void OnSetAsleep()
    {
        base.OnSetAsleep();
		MyLight.intensity = 0.25f;
		MyLight.color = Color.blue;
    }


}
