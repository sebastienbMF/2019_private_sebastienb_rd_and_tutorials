﻿Shader "Custom/SkyPlaneShader" 
{
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_EmissionColor("Color Emission", Color) = (0,0,0,0)
		_EmissionMap("Tex Emission", 2D) = "black" {}
		_EmissionIntensity("Emission Intensity", Float) = 1.0

		_TexScrollSpeed("Tex Scroll Speed X Y", Vector) = (0,0,0,0)
	}
	SubShader {
		Tags { "Queue"="Transparent" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha:fade

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _EmissionMap;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;
		fixed4 _EmissionColor;
		float2 _TexScrollSpeed;
		float _EmissionIntensity;

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			float2 uv = IN.uv_MainTex + _TexScrollSpeed * _Time.y;

			fixed4 c = tex2D (_MainTex, uv) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;

			fixed4 ce = tex2D(_EmissionMap, uv);
			o.Emission = _EmissionColor * ce * _EmissionIntensity;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
