﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace MofaLib
{
    public class DayCycleController : MonoBehaviour
    {
        [Header("Timings")]
        public bool BeginsWithDay = true;

        public float DawnToDayDuration = 10.0f;
        public float DayDuration = 10.0f;
        public float DayToDuskDuration = 10.0f;
        public float DuskDuration = 10.0f;
        public float DuskToNightDuration = 10.0f;
        public float NightDuration = 10.0f;
        public float NightToDawnDuration = 10.0f;
        public float DawnDuration = 10.0f;

        [Header("Lighting Data")]
        public Light SunDirectionalLight;

        [Space(5)]
        public DayTimeData DawnData;
        [Space(5)]
        public DayTimeData DayData;
        [Space(5)]
        public DayTimeData DuskData;
        [Space(5)]
        public DayTimeData NightData;

        public enum DayTimeName
        {
            Dawn,
            Day,
            Dusk,
            Night
        };

        public DayTimeName CurrentDayTime { get; private set; }

        [Serializable]
        public struct DayTimeData
        {
            public DayTimeName Name;

            [Header("Env")]
            public Color EnvironmentColor;

            [Header("Sun")]
            public Color SunColor;
            public float SunIntensity;
            public Vector3 SunRotation;

            [Header("Fog")]
            public Color FogColor;
            public float FogDensity;

            // All the following lists of InterpolationBase could be merged in two big On and Off lists.
            // However I think maybe it's clearer for the user to have them separated so I kept them as is.
            // It also means we can use any class derived from InterpolationBase, for example not just 
            // a LightIntensityInterpolator for the light, but also LightGradientCololrInterpolator,
            // and same things for the Materials etc.

            [Header("Lights")]
            public List<InterpolationBase> LightsToSwitchOn;
            public List<InterpolationBase> LightsToSwitchOff;

            [Header("Materials")]
            public List<InterpolationBase> EmissionToTurnOn;
            public List<InterpolationBase> EmissionToTurnOff;
            [Space(5)]
            public List<InterpolationBase> AlphaToTurnOn;
            public List<InterpolationBase> AlphaToTurnOff;

            [Header("Particles")]
            public List<ParticleSystem> PlayParticles;
            public List<ParticleSystem> StopParticles;
        };

        private struct TransitionData
        {
            public DayTimeData From;
            public DayTimeData To;

            public float Duration;

            public TransitionData(DayTimeData from, DayTimeData to, float duration)
            {
                From = from;
                To = to;
                Duration = duration;
            }
        }

        private List<TransitionData> _dayTransitions;

        // Cache sun quaternions
        private Quaternion _sunFrom;
        private Quaternion _sunTo;

        void Start()
        {
            // Setup of the day cycle states in a list.
            _dayTransitions = new List<TransitionData>();
            _dayTransitions.Add(new TransitionData(DawnData, DayData, DawnToDayDuration));
            _dayTransitions.Add(new TransitionData(DayData, DayData, DayDuration));
            _dayTransitions.Add(new TransitionData(DayData, DuskData, DayToDuskDuration));
            _dayTransitions.Add(new TransitionData(DuskData, DuskData, DuskDuration));
            _dayTransitions.Add(new TransitionData(DuskData, NightData, DuskToNightDuration));
            _dayTransitions.Add(new TransitionData(NightData, NightData, NightDuration));
            _dayTransitions.Add(new TransitionData(NightData, DawnData, NightToDawnDuration));
            _dayTransitions.Add(new TransitionData(DawnData, DawnData, DawnDuration));

            _sunFrom = new Quaternion();
            _sunTo = new Quaternion();

            // If Begins with day begins with cycle dawn to day, else dusk to night.
            StartCoroutine("CycleCoroutine", BeginsWithDay ? 7 : 3);
        }

        private IEnumerator CycleCoroutine(int transitionIndex)
        {
            if (_dayTransitions[transitionIndex].Duration <= 0.0f)
            {
                StartCoroutine("CycleCoroutine", (transitionIndex + 1) % _dayTransitions.Count);
                yield break;
            }

            var from = _dayTransitions[transitionIndex].From;
            var to = _dayTransitions[transitionIndex].To;

            for (float t = 0; t < 1.0f; t += Time.deltaTime / _dayTransitions[transitionIndex].Duration)
            {
                DoTransition(from, to, t);
                yield return null;
            }

            DoTransition(from, to, 1.0f);

            StartCoroutine("CycleCoroutine", (transitionIndex + 1) % _dayTransitions.Count);
        }

        private void DoTransition(DayTimeData from, DayTimeData to, float t)
        {
            if (from.Name == to.Name) return;

            RenderSettings.ambientLight = Vector4.Lerp(from.EnvironmentColor, to.EnvironmentColor, t);
            RenderSettings.fogColor = Vector4.Lerp(from.FogColor, to.FogColor, t);
            RenderSettings.fogDensity = Mathf.Lerp(from.FogDensity, to.FogDensity, t);

            UpdateSun(from, to, t);

            // Lights
            for (int i = 0; i < to.LightsToSwitchOn.Count; ++i)
            {
                to.LightsToSwitchOn[i].TInterpolation = t;
            }
            for (int i = 0; i < to.LightsToSwitchOff.Count; ++i)
            {
                to.LightsToSwitchOff[i].TInterpolation = 1.0f - t;
            }

            // Materials
            for (int i = 0; i < to.EmissionToTurnOn.Count; ++i)
            {
                to.EmissionToTurnOn[i].TInterpolation = t;
            }
            for (int i = 0; i < to.EmissionToTurnOff.Count; ++i)
            {
                to.EmissionToTurnOff[i].TInterpolation = 1.0f - t;
            }

            for (int i = 0; i < to.AlphaToTurnOn.Count; ++i)
            {
                to.AlphaToTurnOn[i].TInterpolation = t;
            }
            for (int i = 0; i < to.AlphaToTurnOff.Count; ++i)
            {
                to.AlphaToTurnOff[i].TInterpolation = 1.0f - t;
            }

            // Particles
            for (int p = 0; p < to.PlayParticles.Count; ++p)
            {
                to.PlayParticles[p].Play();
            }
            for (int p = 0; p < to.StopParticles.Count; ++p)
            {
                to.StopParticles[p].Stop();
            }

            CurrentDayTime = to.Name;
        }

        private void UpdateSun(DayTimeData from, DayTimeData to, float t)
        {
            if (SunDirectionalLight == null)
            {
                Debug.LogWarning("Light cycle controller has no directional light assigned to sun.");
                return;
            }

            SunDirectionalLight.intensity = Mathf.Lerp(from.SunIntensity, to.SunIntensity, t);
            SunDirectionalLight.color = Vector4.Lerp(from.SunColor, to.SunColor, t);
            _sunFrom = Quaternion.Euler(from.SunRotation);
            _sunTo = Quaternion.Euler(to.SunRotation);
            SunDirectionalLight.transform.rotation = Quaternion.Slerp(_sunFrom, _sunTo, t);
        }
    }
}
