/**
    prototypes.cginc
    Purpose: Collection of helper functions that are in a *beta* state.
             Parameters can change, an their reliability is not yet prooven
             in all scenarios.
    
    @author Sebastien Berube
*/

/*! \brief Point Distance To Depth Buffer Along (pWorld-pCamera) Ray.
 *
 *  Return the distance between the point and the depth buffer, along the camera-to-pWorld ray.
 * 
 *  Notes : This was partly validated for the depth aligned with the pixel being rendered.
 *          TODO (sol a): Simplify this, where the ray origin and direction is expressed
 *                        in view space so that z value can be used just like height is in a typical
 *                        height map raymarching shader (same as raymarching height).
 *       
 *          TODO (sol b): Knowing the frustom plane, there is possibly a more optimal process.
 */ 
float mofalib_DistanceToDepthBuffer(float3 pWorld, float3 pCamera, float3 camDir)
{
    float4 clipPos = UnityWorldToClipPos(pWorld);
    float4 screenuv = ComputeScreenPos(clipPos);
    float zDepth = LinearEyeDepth (tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(screenuv)).r);
    float dFromCam = zDepth/dot(camDir,normalize(pWorld-pCamera));
    return dFromCam-length(pWorld-pCamera);
}
