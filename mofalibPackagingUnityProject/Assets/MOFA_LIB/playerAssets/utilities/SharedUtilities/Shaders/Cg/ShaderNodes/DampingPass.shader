﻿Shader "ShaderNodes/DampingPass"
{
    Properties
    {
        _MainTex("Texture", 2D) = "black" {}
        _SingleFrameWeight("SingleFrameWeight", Range(0.0,1.0)) = 0.1
    }

    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
                
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _SelfTex;
            float _SingleFrameWeight;
            int _FrameNo;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float4 cMain = tex2D(_MainTex, i.uv);
                float4 cSelf = tex2D(_SelfTex, i.uv);
                if (_FrameNo == 0)
                  return cMain;
                return lerp(cSelf, cMain, _SingleFrameWeight);
            }
            ENDCG
        }
    }
}
