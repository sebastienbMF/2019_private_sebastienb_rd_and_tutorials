﻿Shader "Unlit/DrawLine"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            float4 _LineP1P2;
            float4 _Resolution;

            #include <Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/DistanceFields/distanceFunctions.hlsl>

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 px = i.uv *_Resolution.xy;
                float2 pa = _LineP1P2.xy*_Resolution.xy;
                float2 pb = _LineP1P2.zw*_Resolution.xy;
                
                float d = dLine(px, pa, pb);
                float I = (d < 20.) ? 1. : 0.;
                return float4(I, d/_Resolution.x, 0, 1);
            }
            ENDCG
        }
    }
}
