/**
    utilities.cginc
    Purpose: Various helpers to brighten your day while shading with NVIDIA Cg.
    
    @author Sebastien Berube
*/

/*! \brief Get Matrix Translation.
 *
 *  Extract XYZ translation components from an homogeneous coordinates matrix.
 */
float3 mofalib_ExtractTranslation(float4x4 m)
{
    return float3(m[0][3], m[1][3], m[2][3]);
}

/*! \brief Get Object World Position.
 *
 *  Get current rendered object world position (from object world space matrix)
 */
float3 mofalib_ObjectWorldPos()
{
    return mofalib_ExtractTranslation(unity_ObjectToWorld);
}

/*! \brief Get Camera LookAt Direction
 *
 *  Get world-space camera direction (lookAt), using the current view matrix components
 */
float3 mofalib_CameraDirection()
{
    //Q : Is there any possibility for this to return a camera direction that is not normalized?
    //    The way I see it, the view matrix should not contain any scale, so we don't have to normalize it.
    return normalize(-UNITY_MATRIX_V[2].xyz);
}
