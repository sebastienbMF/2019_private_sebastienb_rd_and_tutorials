﻿Shader "ShaderNodes/DirectionalBlur"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Coeffs[250]; //Gaussian filter precomputed coefficients
            int _RAD; //Gaussian filter sample range (box half-extent)
            float4 _BlurDirection;

            float4 directionalGaussianBlur(float2 uv, float2 dir) {
                float4 sum = (float4)0;
                float totalW = 0.0;
                for (int i = -_RAD; i <= _RAD; ++i)
                {
                    float2 uvFetch = uv + dir * float(i);
                    
                    //Border management : do not sum anything outside of usable image range.
                    if (uvFetch.x>0.0 && uvFetch.y>0.0&&
                        uvFetch.x<1.0 && uvFetch.y<1.0 )
                    {
                        float weight = _Coeffs[abs(i)];
                        float4 col = tex2D(_MainTex, uvFetch);
                        sum += weight * col;
                        totalW += weight;
                    }
                }
                return sum / totalW;
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float4 c = directionalGaussianBlur(i.uv, _BlurDirection);
                return float4(c.rgb, 1);
            }
            ENDCG
        }
    }
}
