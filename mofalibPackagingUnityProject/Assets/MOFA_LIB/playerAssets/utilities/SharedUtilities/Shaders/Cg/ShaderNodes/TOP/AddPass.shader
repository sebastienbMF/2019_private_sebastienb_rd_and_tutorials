﻿Shader "TOP/AddPass"
{
    Properties
    {
        _InputA("_InputA", 2D) = "black" {}
        _InputB("_InputB", 2D) = "black" {}
        _MultA("_MultA", Float) = 1.0
        _MultB("_MultB", Float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _InputA;
            sampler2D _InputB;
            float _MultA;
            float _MultB;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _MultA*tex2D(_InputA, i.uv)+_MultB*tex2D(_InputB, i.uv);
            }
            ENDCG
        }
    }
}
