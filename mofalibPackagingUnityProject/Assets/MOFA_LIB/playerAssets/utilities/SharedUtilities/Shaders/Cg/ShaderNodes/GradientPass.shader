﻿Shader "ShaderNodes/GradientPass"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _Resolution;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
                float dx = 1.0 / _Resolution.x;
                float dy = 1.0 / _Resolution.y;

                float pX1 = tex2D(_MainTex, i.uv - float2(dx, 0));
                float pX2 = tex2D(_MainTex, i.uv + float2(dx, 0));
                float pY1 = tex2D(_MainTex, i.uv - float2(0, dy));
                float pY2 = tex2D(_MainTex, i.uv + float2(0, dy));

                return float4(pX2 - pX1, pY2 - pY1, 1, 1);
            }
            ENDCG
        }
    }
}
