﻿Shader "Unlit/TrailFeedback"
{
    Properties
    {
        _Mouse("Mouse", Vector) = (0,0,0,0)
        _MainTex("Texture", 2D) = "white" {}
        _RampUpSpeed("RampUpSpeed", Range(0.0,1.0)) = 0.04
        _RampDownSpeed("RampDownSpeed", Range(0.0,1.0)) = 0.99
    }
    
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
        
            #include "UnityCG.cginc"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Noise/basicNoise.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _SelfTex;
            float4 _Mouse;
            float4 _Resolution;
            float _RampUpSpeed;
            float _RampDownSpeed;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float progressiveFade(float current, float target, float rate)
            {
                float delta = target - current;
                return current + delta * rate;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 noiseLg = mofalib_TricubicRGBNoise(float3(i.uv*1.00, _Time.y*0.25)).xy;
                float2 noiseSm = mofalib_TricubicRGBNoise(float3(i.uv*2.13, _Time.y*0.15)).xy;

                float2 uv = i.uv + 1.5*noiseLg + 0.75*noiseSm;

                float scl = 1.0;
                float spd = 0.3;
                float2 uvTarget = float2(_Mouse.x, 0.5 + 0.07*sin(scl*uv.x*10.+ _Time.y*spd)
                                                       + 0.07*sin(scl*uv.x*17.14 - _Time.y*spd*1.12)
                                                       + 0.03*sin(scl*uv.x*23.14 + _Time.y*spd*0.78));

                //float dTgt = abs(uv.y - uvTarget.y);
                float dTgt = tex2D(_MainTex, i.uv + (noiseLg - 0.5)*0.001).g;

                float falloffRange = 0.30;
                float dInput = 30.*tex2D(_MainTex, i.uv + (noiseLg - 0.5)*0.001).g;
                dInput = dInput > 1.0 ? 1.0 : 0.0;


                float I = saturate(lerp(1.0, 0.0, dInput));
                //_MainTex
                _RampUpSpeed = 1.0;
                
                float ISelf = tex2D(_SelfTex, i.uv+ (noiseLg-0.5)*0.001).r;
                float newVal = ISelf * _RampDownSpeed + _RampUpSpeed * I;
                //return float4(0, 0, 0, 1);
                //return float4(dTgt,1,1, 1);
                return float4(newVal,1,1,1);
            }
            ENDCG
        }
    }
}
