﻿Shader "ShaderNodes/DrawTexture"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _TextureScale("TextureScale", Vector) = (1.0,1.0,0,0)
        _InputPosition("InputPosition", Vector) = (0.5,0.5,0,0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _InputPosition;
            float4 _TextureScale;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float4 inPos = _InputPosition;
                float4 col = tex2D(_MainTex, (i.uv - _InputPosition.xy)/_TextureScale+float2(0.5,0.5));
                return float4(col.rgb, 1) * col.a *_InputPosition.z;
            }
            ENDCG
        }
    }
}
