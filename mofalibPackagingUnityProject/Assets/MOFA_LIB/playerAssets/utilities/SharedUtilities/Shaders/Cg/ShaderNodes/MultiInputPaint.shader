﻿Shader "ShaderNodes/MultiInputPaint"
{
    Properties
    {
        _PixelRad("Pixel Radius", Float) = 20.0
        _Softness("Softness", Float) = 0.05
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            float4 _Inputs[20];
            float _PixelRad;
            float _Softness;
            float4 _Resolution;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 px = i.uv*_Resolution.xy;
                
                float sum = 0.;
                float fadeStart = _PixelRad - _PixelRad*_Softness;
                float fadeEnd   = _PixelRad + _PixelRad*_Softness;
                
                float dToInput = 999999.;

                for (int i = 0; i < 20; ++i)
                {
                    float2 pt = _Inputs[i].xy*_Resolution.xy;
                    float2 d = length(pt - px);
                    dToInput = min(dToInput, d);

                    sum += smoothstep(fadeEnd, fadeStart,  d) * _Inputs[i].z;
                }

                return float4(sum, dToInput/_Resolution.x, 0, 1);
            }
            ENDCG
        }
    }
}
