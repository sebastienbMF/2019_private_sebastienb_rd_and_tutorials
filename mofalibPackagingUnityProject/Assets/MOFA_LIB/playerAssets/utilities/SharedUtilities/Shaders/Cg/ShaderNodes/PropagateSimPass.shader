﻿Shader "ShaderNodes/PropagateSim"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "black" {}
        _SelfTex("Texture", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Mapping/coordinateSystems.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _SelfTex;
            sampler2D _maskTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float maskedSample(float2 px, float2 iRes)
            {
                float2 uv = mofalib_pixelToUV(px, iRes);
                float tSelf = tex2D(_SelfTex, uv).r;
                
                return tSelf;// *tMask;
            }
            

            fixed4 frag (v2f i) : SV_Target
            {
                //Coordinates
                float2 iRes = float2(512, 512);
                float2 px = mofalib_uvToPixel(i.uv, iRes);
                float centerValue = tex2D(_SelfTex, i.uv).r;

                float v = centerValue;
				
				//Fake Temp Init
                /*if (px.x < 3. && px.y < 3.)
                    v = 1.0;*/

                if (tex2D(_maskTex, i.uv).r < 0.1)
                {
                    return float4(0, 0, 0, 1);
                } 

                float propagationStrenght = 1.0;
                v = max(v, maskedSample(px + float2(-1, 0), iRes)*propagationStrenght);
                v = max(v, maskedSample(px + float2(0, +1), iRes)*propagationStrenght);
                v = max(v, maskedSample(px + float2(0, -1), iRes)*propagationStrenght);
                v = max(v, maskedSample(px + float2(+1, 0), iRes)*propagationStrenght);

                return float4((float3)v, 1);
            }
            ENDCG
        }
    }
}
