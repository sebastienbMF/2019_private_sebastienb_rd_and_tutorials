﻿Shader "ShaderNodes/ReactionDiffusion"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
    }


    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Noise/basicNoise.hlsl"
            #include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Debug/debugUtilities.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _SelfTex;
            float4 _Resolution;
            int _FrameNo;
            float aTest;

            /*
                A Fitzhugh-Nagumo reaction-diffusion system.
                See this paper for additional information:

                http://arxiv.org/pdf/patt-sol/9401002.pdf

                A large timestep is used to make the system evolve at an interactive rate when limited to 60 FPS.
                The system is unstable using a large timestep with simple Euler integration, so instead it is
                updated with an exponentially-weighted moving average of the gradient (with time constant tc).
            */
            float4 selfSample(float2 uv)
            {
                return tex2D(_SelfTex, uv);
            }

            //https://www.shadertoy.com/view/XdcXDN
            float4 reactionDiffusion(float2 fragCoord)
            {
                const float _K0 = -20.0 / 6.0; // center weight
                const float _K1 = 4.0 / 6.0; // edge-neighbors
                const float _K2 = 1.0 / 6.0; // vertex-neighbors
                const float timestep = 0.7;
                const float a0 = -0.1;
                const float a1 = 2.0;
                const float epsilon = 0.05;
                const float delta = 4.0;
                const float k1 = 1.0;
                const float k2 = 0.0;
                const float k3 = 1.0;
                const float tc = 0.8;

                float2 vUv = fragCoord.xy / _Resolution.xy;
                float2 texel = 1. / _Resolution.xy;

                // 3x3 neighborhood coordinates
                float step_x = texel.x;
                float step_y = texel.y;
                float2 n = float2(0.0, step_y);
                float2 ne = float2(step_x, step_y);
                float2 e = float2(step_x, 0.0);
                float2 se = float2(step_x, -step_y);
                float2 s = float2(0.0, -step_y);
                float2 sw = float2(-step_x, -step_y);
                float2 w = float2(-step_x, 0.0);
                float2 nw = float2(-step_x, step_y);

                float4 uv = selfSample(vUv);
                float4 uv_n = selfSample(vUv + n);
                float4 uv_e = selfSample(vUv + e);
                float4 uv_s = selfSample(vUv + s);
                float4 uv_w = selfSample(vUv + w);
                float4 uv_nw = selfSample(vUv + nw);
                float4 uv_sw = selfSample(vUv + sw);
                float4 uv_ne = selfSample(vUv + ne);
                float4 uv_se = selfSample(vUv + se);

                // laplacian of all components
                float4 lapl = _K0 * uv + _K1 * (uv_n + uv_e + uv_w + uv_s) + _K2 * (uv_nw + uv_sw + uv_ne + uv_se);

                float a = uv.x;
                float b = uv.y;
                float c = uv.z;
                float d = uv.w;

                float d_a = k1 * a - k2 * a*a - a * a*a - b + lapl.x;
                float d_b = epsilon * (k3*a - a1 * b - a0) + delta * lapl.y;
                c = tc * c + (1.0 - tc) * d_a;
                d = tc * d + (1.0 - tc) * d_b;

                a = a + timestep * c;
                b = b + timestep * d;

                float aInput = tex2D(_MainTex, vUv).r;
                //if (iMouse.z > 0.0)
                {
                    a += aInput;
                }

                // initialize with noise
                if (_FrameNo < 10) {
                    return -.5 + mofalib_BicubicRGBANoise(fragCoord);
                }
                else {
                    return clamp(float4(a, b, c, d), -1., 1.);
                }
            }

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag(v2f i) : SV_Target
            {
                float2 fragCoord = i.uv*_Resolution.xy;
                float4 v = reactionDiffusion(fragCoord);
                return v;
            }
            ENDCG
        }
    }
}
