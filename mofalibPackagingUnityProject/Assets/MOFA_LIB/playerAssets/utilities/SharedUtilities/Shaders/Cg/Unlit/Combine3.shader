﻿Shader "Filters/Combine3"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _t0;
            sampler2D _t1;
            sampler2D _t2;
            float3 _TexWeights;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float totalWeights = _TexWeights.x + _TexWeights.y + _TexWeights.z;
                _TexWeights.x /= totalWeights;
                _TexWeights.y /= totalWeights;
                _TexWeights.z /= totalWeights;

                float4 finalColor = (_TexWeights.x *tex2D(_t0, i.uv) 
                                   + _TexWeights.y *tex2D(_t1, i.uv)
                                   + _TexWeights.z *tex2D(_t2, i.uv));

                return finalColor;
            }
            ENDCG
        }
    }
}
