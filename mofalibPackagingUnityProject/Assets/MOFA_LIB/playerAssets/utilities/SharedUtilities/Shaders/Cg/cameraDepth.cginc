/**
    utilities.cginc
    Purpose: Camera depth related utilities.
    
    @author Sebastien Berube
*/

#include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/Cg/utilities.cginc"

/*! \brief Get Pixel Z-Buffer World Position
 *
 *  Using the given pixel direction from camera origin (which varies from pixel to pixel, unlike view direction),
 *  the world-space position corresponding to the depth buffer pixel hit position is resolved and returned.
 *
 *  Shader usage example 1: (using vertex shader output)
 *   v2f vert (appdata v)
 *   {
 *     v2f o;
 *     o.viewDir.xyz = WorldSpaceViewDir(v.vertex); //Varying from pixel to pixel (if projective)
 *     o.screenuv = ComputeScreenPos(UnityObjectToClipPos(v.vertex));
 *     return o;
 *   }
 *   
 *   fixed4 frag (v2f i) : SV_Target
 *   {
 *     //Note : WorldSpaceViewDir returns world space direction (not normalized) from given object
 *     //       space vertex position towards the camera. Hence the sign flip.
 *     //       see https://docs.unity3d.com/Manual/SL-BuiltinFunctions.html
 *     float3 worldZPos = mofalib_getZBufferPosWorld(normalize(-i.viewDir.xyz), i.screenuv);
 *     ...
 *   }
 *  
 *  Shader usage example 2: (computing neccessary input values locally)
 *   fixed4 frag (v2f i) : SV_Target
 *   {
 *     float3 pWorld = ...[some function returning a world position];
 *     float4 clipPos = UnityWorldToClipPos(pWorld);
 *     float4 screenuv = ComputeScreenPos(clipPos);
 *     float3 rayDirection = normalize(pWorld-camPos); //Ray direction
 *     float3 worldZPos = mofalib_getZBufferPosWorld(rayDirection, screenuv);
 *     ...
 *   }
 */
float3 mofalib_GetZBufferWorldPos(float3 rayDirection, float4 screenUV)
{
	float3 ro = _WorldSpaceCameraPos; //Ray origin
	float viewSpaceZ = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(screenUV)).r);
	float3 camDir = mofalib_CameraDirection();
	float distanceFromOrigin = viewSpaceZ / dot(camDir, rayDirection); //Convert from Z value to distance value (equal if orthographic)
	return ro + rayDirection * distanceFromOrigin;
}