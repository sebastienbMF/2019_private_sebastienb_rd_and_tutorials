﻿Shader "ShaderNodes/TextureAccumulationPass"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _RampUpSpeed("RampUpSpeed", Range(0.0,1.0)) = 0.02
        _RampDownSpeedExp("RampDownSpeedExp", Range(0.0,1.0)) = 0.99
        _RampDownSpeedLin("RampDownSpeedLin", Range(0.0,1.0)) = 0.0001
    }

    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
                
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _SelfTex;
            float4 _Mouse;
            float4 _Resolution;
            float _RampUpSpeed;
            float _RampDownSpeedExp;
            float _RampDownSpeedLin;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float4 cMain = tex2D(_MainTex, i.uv);
                float4 cSelf = tex2D(_SelfTex, i.uv);
                float4 newVal = max(float4(0,0,0,0), cSelf * _RampDownSpeedExp - _RampDownSpeedLin + _RampUpSpeed * cMain);
                return float4(newVal.rgb,1);
            }
            ENDCG
        }
    }
}
