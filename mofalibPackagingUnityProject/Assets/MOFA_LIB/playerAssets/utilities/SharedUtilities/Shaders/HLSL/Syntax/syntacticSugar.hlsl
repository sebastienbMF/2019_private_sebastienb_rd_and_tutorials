/**
    syntacticSugar.cginc
    Purpose: Various helpers for shader language conversion (e.g. webGL)
*/

bool3 lessThan(float3 a, float3 b)
{
    return bool3(a.x<b.x,a.y<b.y,a.z<b.z);
}

bool2 lessThan(float2 a, float2 b)
{
    return bool2(a.x<b.x,a.y<b.y);
}