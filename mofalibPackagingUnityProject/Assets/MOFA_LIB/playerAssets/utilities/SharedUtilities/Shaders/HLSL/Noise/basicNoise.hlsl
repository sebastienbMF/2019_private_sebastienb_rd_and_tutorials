/**
    basicNoise.hlsl
    Purpose: Collection of basic generative noise functions.
*/


/*! \brief 1D Random value from 1D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float mofalib_Hash11(float p)
{
    p = frac(p * .1031);
    p *= p + 33.33;
    p *= p + p;
    return frac(p);
}

/*! \brief 1D Random value from 2D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float mofalib_Hash12(float2 p)
{
    float3 p3 = frac(float3(p.xyx) * .1031);
    p3 += dot(p3, p3.yzx + 33.33);
    return frac((p3.x + p3.y) * p3.z);
}

/*! \brief 1D Random value from 3D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float mofalib_Hash13(float3 p3)
{
    p3 = frac(p3 * .1031);
    p3 += dot(p3, p3.yzx + 33.33);
    return frac((p3.x + p3.y) * p3.z);
    //NOTE : Changed amplitude into [0-1] range (was [-1,1])
}

/*! \brief 2D Random value from 1D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float2 mofalib_Hash21(float p)
{
    float3 p3 = frac(float3(p,p,p) * float3(.1031, .1030, .0973));
    p3 += dot(p3, p3.yzx + 33.33);
    return frac((p3.xx + p3.yz) * p3.zy);

}

/*! \brief 2D Random value from 2D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float2 mofalib_Hash22(float2 p)
{
    float3 p3 = frac(float3(p.xyx) * float3(.1031, .1030, .0973));
    p3 += dot(p3, p3.yzx + 33.33);
    return frac((p3.xx + p3.yz) * p3.zy);

}

/*! \brief 2D Random value from 3D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float2 mofalib_Hash23(float3 p3)
{
    p3 = frac(p3 * float3(.1031, .1030, .0973));
    p3 += dot(p3, p3.yzx + 33.33);
    return frac((p3.xx + p3.yz) * p3.zy);
}

/*! \brief 3D Random value from 1D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float3 mofalib_Hash31(float p)
{
    float3 p3 = frac(float3(p,p,p) * float3(.1031, .1030, .0973));
    p3 += dot(p3, p3.yzx + 33.33);
    return frac((p3.xxy + p3.yzz) * p3.zyx);
}

/*! \brief 3D Random value from 2D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float3 mofalib_Hash32(float2 p)
{
    float3 p3 = frac(float3(p.xyx) * float3(.1031, .1030, .0973));
    p3 += dot(p3, p3.yxz + 33.33);
    return frac((p3.xxy + p3.yzz) * p3.zyx);
}

/*! \brief 3D Random value from 3D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float3 mofalib_Hash33(float3 p3)
{
    p3 = frac(p3 * float3(.1031, .1030, .0973));
    p3 += dot(p3, p3.yxz + 33.33);
    return frac((p3.xxy + p3.yxx) * p3.zyx);
}

/*! \brief 4D Random value from 1D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float4 mofalib_Hash41(float p)
{
    float4 p4 = frac(float4(p,p,p,p) * float4(.1031, .1030, .0973, .1099));
    p4 += dot(p4, p4.wzxy + 33.33);
    return frac((p4.xxyz + p4.yzzw) * p4.zywx);
}

/*! \brief 4D Random value from 2D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float4 mofalib_Hash42(float2 p)
{
    float4 p4 = frac(float4(p.xyxy) * float4(.1031, .1030, .0973, .1099));
    p4 += dot(p4, p4.wzxy + 33.33);
    return frac((p4.xxyz + p4.yzzw) * p4.zywx);
}

/*! \brief 4D Random value from 3D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float4 mofalib_Hash43(float3 p)
{
    float4 p4 = frac(float4(p.xyzx) * float4(.1031, .1030, .0973, .1099));
    p4 += dot(p4, p4.wzxy + 33.33);
    return frac((p4.xxyz + p4.yzzw) * p4.zywx);
}

/*! \brief 4D Random value from 4D seed input
 * 
 * Ref: https://www.shadertoy.com/view/4djSRW
 */
float4 mofalib_Hash44(float4 p4)
{
    p4 = frac(p4 * float4(.1031, .1030, .0973, .1099));
    p4 += dot(p4, p4.wzxy + 33.33);
    return frac((p4.xxyz + p4.yzzw) * p4.zywx);
}


/*! \brief Bilinear Interpolation of 4 values.
 *
 *  c------------d
 *  |            |
 *  |  U : Horiz |
 *  |  V : Vert  |
 *  |            |
 *  a------------b
 *
 * @param u   Interpolation factors (u.x horizontal, u.y vertical)
 * @param a   Bottom left value (conceptually, according to ascii art)
 * @param b   Bottom right value
 * @param c   Top left value
 * @param d   Top right value
 *
 * @return    Interpolated Value
 *
 * Ref: None (sebastienb@momentfactory.com)
 */
float mofalib_BilinearInterpolation(float2 u, float a, float b, float c, float d)
{
    return float(a + (b - a)*u.x + (c - a)*u.y + (a - b - c + d)*u.x*u.y);
}

//Same as above, with RGB noise rather than single-component noise
float3 mofalib_BilinearInterpolation(float2 u, float3 a, float3 b, float3 c, float3 d)
{
    return float3(a + (b - a)*u.x + (c - a)*u.y + (a - b - c + d)*u.x*u.y);
}

//Same as above, with RGBA noise rather than single-component noise
float4 mofalib_BilinearInterpolation(float2 u, float4 a, float4 b, float4 c, float4 d)
{
    return float4(a + (b - a)*u.x + (c - a)*u.y + (a - b - c + d)*u.x*u.y);
}

/*! \brief Continuous bicubic-interpolated generative noise
 *
 * This function returns a smooth, continuous noise value.
 * It is essentially just bilinearly interpolated fixed noise sampled
 * with an additional ease-in/ease-out applied on the uv coordinates,
 * to keep the value - and its derivatives - continous.
 *
 * Ref: https://en.wikipedia.org/wiki/Bicubic_interpolation
 */
float mofalib_BicubicNoise(float2 p)
{
    float2 i = floor(p);
    float2 v = frac(p);
    float2 u = v * v * (3.0 - 2.0 * v); //Cubic interpolation (ease-in, ease-out)

    float a = mofalib_Hash12(i + float2(0.0, 0.0));
    float b = mofalib_Hash12(i + float2(1.0, 0.0));
    float c = mofalib_Hash12(i + float2(0.0, 1.0));
    float d = mofalib_Hash12(i + float2(1.0, 1.0));
    return mofalib_BilinearInterpolation(u.xy, a, b, c, d);
}

float4 mofalib_BicubicRGBANoise(float2 p)
{
    float2 i = floor(p);
    float2 v = frac(p);
    float2 u = v * v * (3.0 - 2.0 * v); //Cubic interpolation (ease-in, ease-out)

    float4 a = mofalib_Hash42(i + float2(0.0, 0.0));
    float4 b = mofalib_Hash42(i + float2(1.0, 0.0));
    float4 c = mofalib_Hash42(i + float2(0.0, 1.0));
    float4 d = mofalib_Hash42(i + float2(1.0, 1.0));
    return mofalib_BilinearInterpolation(u.xy, a, b, c, d);
}

//GLSL-type mod (note : different from HLSL fmod with negative values!)
float mofalib_Modulo(float  v, float m)
{
    return v - m * floor(v / m);
}

//For a given cyclic time T, the following rules must 
//be respected by the parameters 'p' and 'period' for this
//noise to cycle seamlessly over time:
// p  : source uv or position can be scaled arbitrarely,
//          e.g. mofalib_PeriodicBicubicNoise(uv*13.234234, T);
//      HOWEVER, if the noise is scrolling in a time-dependent manner,
//      the scrolling speed must be an intergral multiplier of t (time).
//          e.g. mofalib_PeriodicBicubicNoise(uv*13.234234+t*a, T); //where a belongs to Z, this will cycle seamlessly
// period : The period can be repeated more frequently than the
//          cycle time T, as long as it is a factor of T.
//            e.g.  mofalib_PeriodicBicubicNoise(uv*13.234234+t*a, T/b); //where a and (T/b) belongs to Z, this will cycle seamlessly
//

float mofalib_PeriodicBicubicNoise(float2 p, float2 period)
{
    float2 i = floor(p);
    float2 f = frac(p);
    float2 u = f * f * (3.0 - 2.0 * f); //Cubic interpolation (ease-in, ease-out)

    //Make sample seed periodic
    float x0 = mofalib_Modulo(i.x, period);
    float x1 = mofalib_Modulo(i.x + 1, period);
    float y0 = mofalib_Modulo(i.y, period);
    float y1 = mofalib_Modulo(i.y + 1, period);

    float a = mofalib_Hash12(float2(x0, y0));
    float b = mofalib_Hash12(float2(x1, y0));
    float c = mofalib_Hash12(float2(x0, y1));
    float d = mofalib_Hash12(float2(x1, y1));
    return mofalib_BilinearInterpolation(u.xy, a, b, c, d);
}

/*! \brief Continuous tricubic-interpolated generative noise
 *
 * This function returns a smooth, continuous noise value.
 * Just feed the function your 3D seed (e.g. position in space, or float3(UV,time), etc.).
 * Same principle as bicubic noise, with an extra dimension.
 *
 * Ref: None https://en.wikipedia.org/wiki/Bicubic_interpolation
 */
 //https://en.wikipedia.org/wiki/Multivariate_interpolation
float mofalib_TricubicNoise(float3 p)
{
    float3 i = floor(p);
    float3 v = frac(p);
    float3 u = v * v * (3.0 - 2.0 * v); //Cubic interpolation (ease-in, ease-out)

    float a = mofalib_Hash13(i + float3(0.0, 0.0, 0.0));
    float b = mofalib_Hash13(i + float3(1.0, 0.0, 0.0));
    float c = mofalib_Hash13(i + float3(0.0, 1.0, 0.0));
    float d = mofalib_Hash13(i + float3(1.0, 1.0, 0.0));
    float L1 = mofalib_BilinearInterpolation(u.xy, a, b, c, d);

    float e = mofalib_Hash13(i + float3(0.0, 0.0, 1.0));
    float f = mofalib_Hash13(i + float3(1.0, 0.0, 1.0));
    float g = mofalib_Hash13(i + float3(0.0, 1.0, 1.0));
    float h = mofalib_Hash13(i + float3(1.0, 1.0, 1.0));
    float L2 = mofalib_BilinearInterpolation(u.xy, e, f, g, h);

    return lerp(L1, L2, u.z);
}

/*! \brief Continuous tricubic-interpolated generative float3 noise
*
* This function returns a smooth, continuous float3 noise value.
* Just feed the function your 3D seed (e.g. position in space, or float3(UV,time), etc.).
* Same principle as mofalib_TricubicNoise, with a noise triplet rathan than a single scalar
*/
float3 mofalib_TricubicRGBNoise(float3 p)
{
    float3 i = floor(p);
    float3 v = frac(p);
    float3 u = v * v * (3.0 - 2.0 * v); //Cubic interpolation (ease-in, ease-out)

    float3 a = mofalib_Hash33(i + float3(0.0, 0.0, 0.0));
    float3 b = mofalib_Hash33(i + float3(1.0, 0.0, 0.0));
    float3 c = mofalib_Hash33(i + float3(0.0, 1.0, 0.0));
    float3 d = mofalib_Hash33(i + float3(1.0, 1.0, 0.0));
    float3 L1 = mofalib_BilinearInterpolation(u.xy, a, b, c, d);

    float3 e = mofalib_Hash33(i + float3(0.0, 0.0, 1.0));
    float3 f = mofalib_Hash33(i + float3(1.0, 0.0, 1.0));
    float3 g = mofalib_Hash33(i + float3(0.0, 1.0, 1.0));
    float3 h = mofalib_Hash33(i + float3(1.0, 1.0, 1.0));
    float3 L2 = mofalib_BilinearInterpolation(u.xy, e, f, g, h);

    return lerp(L1, L2, u.z);
}