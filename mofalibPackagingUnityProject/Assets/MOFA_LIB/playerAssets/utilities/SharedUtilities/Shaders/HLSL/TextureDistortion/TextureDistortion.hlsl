#if !defined(TEXTURE_DISTORTION_INCLUDED)
#define TEXTURE_DISTORTION_INCLUDED


/************************************************************************************************************************************************************************************************
uv          : These are the original uv coordinates that you want distorted.

flowVector  : This is the vector that will distort the original uv.

jump        : This adds a jump offset to the uv for every phases (each time the weight is zero). This allows the distortion to repeat itself less often by adding more phases.

flowOffset  : This offsets the value at which we start our distortion. By default, the value of the distortion progresses from 0 to 1. For example, you could make it start at -0.5 up to 0.5.

tiling      : This makes it possible to tile the distortion of the uvs (and its corresponding texture).

time        : This is the time used to compute the distortion. It can be custom, or you can pass one of the default available _Time shader variable.

flowB       : This indicates whether or not the computation should have the offset of the second texture to make sure they are synched (and hide the �reset� of the distortion).

phaseOffset : This is a normalized progression phase offset. Range = [0,1]
************************************************************************************************************************************************************************************************/
float3 DistortUVW(float2 uv, float2 flowVector, float2 jump, float flowOffset, float tiling, float time, float phaseOffset)
{
    float progress = frac(time + phaseOffset);
    float3 uvw = float3(0,0,0);
    uvw.xy = uv - flowVector * (progress + flowOffset);
    uvw.xy *= tiling;
    uvw.xy += phaseOffset;
    uvw.xy += (time - progress) * jump;
    uvw.z = 1 - abs(1 - 2 * progress);
    return uvw;
}

#endif