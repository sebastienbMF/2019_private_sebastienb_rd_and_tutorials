/**
    distanceFunctions.hlsl
    Purpose: Collection of distance functions.

    Ref: https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
*/

/*! \brief Distance From 3D Point to Sphere
 *
 * @param p Point to get the distance from.
 * @param r Radius of the sphere
 * @return Distance from primitive
 *
 * Note : This one is a bit on the simple side...
 *
 * See : https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 *       https://www.shadertoy.com/view/Xds3zN
 */
float sdSphere( float3 p, float r )
{
  return length(p)-r;
}

/*! \brief Distance From 3D Point to Axis-Aligned Box
 *
 * @param p Point to get the distance from.
 * @param b Box half-extent size
 * @return Distance from primitive
 *
 * See : https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 *       https://www.shadertoy.com/view/Xds3zN
 */
float sdBox(float3 p, float3 b)
{
    float3 d = abs(p) - b;
    return length(max(d, 0.0))
        + min(max(d.x, max(d.y, d.z)), 0.0);
}

/*! \brief Distance From 3D Point to Axis-Aligned Rounded Box
 *
 * @param p Point to get the distance from.
 * @param b Box half-extent size
 * @param b Rounded edge radius
 * @return Distance from primitive
 *
 * See : https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 *       https://www.shadertoy.com/view/Xds3zN
 */
float sdRoundBox(float3 p, float3 b, float r)
{
    float3 d = abs(p) - b;
    return length(max(d, 0.0)) - r
        + min(max(d.x, max(d.y, d.z)), 0.0); // Note: the line adds the full internal signed distance. It can be removed as an optimization for a partially signed sdf (don't modify here! Copy it if you want a version optimized for your usage).
}

/*! \brief Distance From 3D Point to Axis-Aligned Cone (Y-Up)
 *
 * @param p Point to get the distance from.
 * @param c c is the sin/cos of the angle
 * @return Distance from primitive
 *
 * See : https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 *       https://www.shadertoy.com/view/Xds3zN
 */
float sdCone(in float3 p, in float2 c)
{
    float q = length(p.xy);
    return dot(c, float2(q, p.z));
}

/*! \brief Distance From 3D Point to Capsule
 *
 * @param p Point to get the distance from.
 * @param a Endpoint 1
 * @param b Endpoint 2
 * @param b Capsule radius
 * @return Distance from primitive
 *
 * See : https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 *       https://www.shadertoy.com/view/Xds3zN
 */
float sdCapsule(float3 p, float3 a, float3 b, float r)
{
    float3 pa = p - a, ba = b - a;
    float h = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);
    return length(pa - ba * h) - r;
}

/*! \brief Distance From 3D Point to Axis-Aligned Cylinder (Y-Up)
 *
 * @param p Point to get the distance from.
 * @param h Height
 * @param r Radius
 * @return Distance from primitive
 *
 * See : https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 *       https://www.shadertoy.com/view/Xds3zN
 */
float sdCappedCylinder(float3 p, float h, float r)
{
    float2 d = abs(float2(length(p.xz), p.y)) - float2(h, r);
    return min(max(d.x, d.y), 0.0) + length(max(d, 0.0));
}

/*! \brief Distance From 3D Point to Capped Cylinder
 *
 * @param p Point to get the distance from.
 * @param a Endpoint 1
 * @param b Endpoint 2
 * @param r Radius
 * @return Distance from primitive
 *
 * See : https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 *       https://www.shadertoy.com/view/Xds3zN
 */
float sdCappedCylinder(float3 p, float3 a, float3 b, float r)
{
    float3  ba = b - a;
    float3  pa = p - a;
    float baba = dot(ba, ba);
    float paba = dot(pa, ba);
    float x = length(pa*baba - ba * paba) - r * baba;
    float y = abs(paba - baba * 0.5) - baba * 0.5;
    float x2 = x * x;
    float y2 = y * y*baba;
    float d = (max(x, y) < 0.0) ? -min(x2, y2) : (((x > 0.0) ? x2 : 0.0) + ((y > 0.0) ? y2 : 0.0));
    return sign(d)*sqrt(abs(d)) / baba;
}

/*! \brief Distance From 3D Point to Cone
 *
 * @param p Point to get the distance from.
 * @param t [x:main radius, y:secondary radius] 
 * @return Distance from primitive
 *
 * See : https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 *       https://www.shadertoy.com/view/Xds3zN
 */
float sdTorus(float3 p, float2 t)
{
    float2 q = float2(length(p.xz) - t.x, p.y);
    return length(q) - t.y;
}

float dLine(float2 p, float2 a, float2 b) {
    float2 ab = b - a;
    return length(p - a - clamp(dot(p - a, ab) / dot(ab, ab), 0.0, 1.0)*ab);
}
