/**
    coordinateSystems.hlsl
    Purpose: Various coordinate system conversion utilities
*/

/*! \brief Convert From Euclidian to Spherical coordinates (physics mapping)
 *
 * When converting to spherical coordinates, BEWARE of the convention:
 * Spherical coordinates (r, theta, phi) as commonly used in physics (ISO convention): 
 *      radial distance r
 *      polar angle theta
 *      and azimuthal angle phi
 *
 * See : https://en.wikipedia.org/wiki/Spherical_coordinate_system
 */
float3 mofalib_EuclidianToSpherical(float3 p)
{
    //Physical mapping (Z-up), XY for polar
    float r = length(p); //rad
    float pol = atan2(p.y, p.x); //theta
    float azi = acos(p.z / r); //phi
    return float3(r, pol, azi);
}

float2 mofalib_pixelToUV(float2 pixelCoord, float2 iRes)
{
    float2 uv = (pixelCoord + 0.5) / iRes.xy;
    return uv;
}

float2 mofalib_uvToPixel(float2 uv, float2 iRes)
{
    float2 pixelCoord = uv * iRes.xy - 0.5;
    return pixelCoord;
}

float2 mofalib_pixelSize(float2 iRes)
{
    return float2(1,1)/iRes;
}

float PRIVATE_distanceToLineSeg(float2 p, float2 a, float2 b)
{
  //e = capped [0,1] orthogonal projection of ap on ab
  //       p
  //      /
  //     /
  //    a--e-------b
  float2 ap = p - a;
  float2 ab = b - a;
  float2 e = a + clamp(dot(ap, ab) / dot(ab, ab), 0.0, 1.0)*ab;
  return length(p - e);
}

bool PRIVATE_isOutside(float2 uv, float2 c1, float2 c2, float2 c3, float2 c4)
{
  return dot((c1 - c2).yx*float2(-1, 1), uv - c1) < 0.
    || dot((c2 - c3).yx*float2(-1, 1), uv - c2) < 0.
    || dot((c3 - c4).yx*float2(-1, 1), uv - c3) < 0.
    || dot((c4 - c1).yx*float2(-1, 1), uv - c4) < 0.;
}

float mofalib_smoothInternalQuadGrad(float2 uv, float2 c1, float2 c2, float2 c3, float2 c4)
{
  if (PRIVATE_isOutside(uv, c1, c2, c3, c4))
    return 0.;

  float da = PRIVATE_distanceToLineSeg(uv, c3, c2);
  float db = PRIVATE_distanceToLineSeg(uv, c2, c1);
  float dc = PRIVATE_distanceToLineSeg(uv, c1, c4);
  float dd = PRIVATE_distanceToLineSeg(uv, c4, c3);

  //Note : a parameter controls smoothness (and scales value)
  float a = 0.01;
  float NORMALIZATION_TERM = log((1. + a) / a);
  da = log((da + a) / a) / NORMALIZATION_TERM;
  db = log((db + a) / a) / NORMALIZATION_TERM;
  dc = log((dc + a) / a) / NORMALIZATION_TERM;
  dd = log((dd + a) / a) / NORMALIZATION_TERM;

  return da*db*dc*dd;
}