/*! \brief mofalib_Checkerboard
 *
 * This function generates and returns a checkerboard.
 * Useful as a quick debug texture, to add some spatial awareness
 * in a scene without addind an actual external texture.
 *
 * @param uv   Pixel shader uv (0-1, typically)
 * @return     The checkboard texture, at the given coordinate (8x8)
 *
 * Usage example:
 *
 * float3 color = (float3)mofalib_Checkerboard(uv);
 *
 * Ref : https://www.shadertoy.com/view/4sBSWW
 *
 */
float mofalib_Checkerboard(float2 uv)
{
    uv = uv*4.-0.25; //Scale to 8x8 (smoothstep divides in 2x2)
    const float eps = 0.01; //Transition width (pseudo-antialiasing)
    float c =  smoothstep(0.25 - eps, 0.25 + eps, abs(frac(uv.x) - 0.5))*2. - 1.;
          c *= smoothstep(0.25 - eps, 0.25 + eps, abs(frac(uv.y) - 0.5))*2. - 1.;
    return max(c, 0.);
}