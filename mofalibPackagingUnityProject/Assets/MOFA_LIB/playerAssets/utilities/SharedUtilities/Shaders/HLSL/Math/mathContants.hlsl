/**
    mathConstants.hlsl
    Purpose: Collection of math constants
    
    Note: No static const allowed here, it will otherwise create shader
          variable redefinion errors whenever the file is included
          more than once.
*/

//Note : float is ~7 digits of precision.
#define MF_PI  3.1415926
#define MF_2PI 6.2831853