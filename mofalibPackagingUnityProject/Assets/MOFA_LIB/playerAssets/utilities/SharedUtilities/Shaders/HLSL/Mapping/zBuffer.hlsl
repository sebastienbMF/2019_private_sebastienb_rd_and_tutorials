/**
    zBuffer.hlsl
    Purpose: Various zBuffer related utilities
*/


/*! \brief Convert Depth Buffer pixel value to 0-1 range
 *
 * @param zBufParams Parameters used for quick conversion, derived from camera far and near plane.
 *                   See MofaLib.Rendering.CameraUtilities.ComputeZBufferParams (cs script)
 * @return Depth value, scaled in 0-1 range (1 being the farthest)
 *
 * References : https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html (information is deprecated for ReversedZBuffer since Unity 5.5)
 *              [InstallationFolder]/Editor/Data/CGIncludes/UnityCG.cginc
 *              [InstallationFolder]/Editor/Data/CGIncludes/UnityShaderVariables.cginc
 */
inline float mofalib_Linear01Depth(float z, float4 zBufParams)
{
    return 1.0 / (zBufParams.x * z + zBufParams.y);
}

/*! \brief Convert Depth Buffer pixel value to linear camera space units
 *
 * @param zBufParams Parameters used for quick conversion, derived from camera far and near plane.
 *                   See MofaLib.Rendering.CameraUtilities.ComputeZBufferParams (cs script)
 * @return Z Depth value, aligned and centered on the camera coordinate system
 *
 * References : https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html (information is deprecated for ReversedZBuffer since Unity 5.5)
 *              [InstallationFolder]/Editor/Data/CGIncludes/UnityCG.cginc
 *              [InstallationFolder]/Editor/Data/CGIncludes/UnityShaderVariables.cginc
 */
inline float mofalib_LinearEyeDepth(float z, float4 zBufParams)
{
    return 1.0 / (zBufParams.z * z + zBufParams.w);
}

/*! \brief Generate ZBuffer params
 *
 * Note : It is preferable to use MofaLib.Rendering.CameraUtilities.ComputeZBufferParams (cs script),
 *        instead of rebuilding parameters for each frag shader program.
 *
 * 
 *  From : [InstallationFolder]/Editor/Data/CGIncludes/UnityShaderVariables.cginc
 * 
 *      With reversed depth buffer(DirectX 11, DirectX 12, PS4, Xbox One, Metal), _ZBufferParams contain :
 *          x = -1 + far / near
 *          y = 1
 *          z = x / far
 *          w = 1 / far
 *          and Clip space range is[near, 0] instead of[0, far].
 *      
 *      The Traditional direction(OpenGL, D9) : _ZBufferParams contain :
 *          x is(1 - far / near),
 *          y is(far / near),
 *          z is(x / far) and
 *          w is(y / fa)
 *
 * @param near Camera near plane
 * @param near Camera far plane
 * @return ZBuffer parameters
 *
 * Referemces : https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html (information is deprecated for ReversedZBuffer since Unity 5.5)
 *              http://www.laiqifang.com/2017/09/15/_zbufferparams-in-reversed-depth-buffer/
 *              https://forum.unity.com/threads/_zbufferparams-values.39332/
 *              [InstallationFolder]/Editor/Data/CGIncludes/UnityCG.cginc
 *              [InstallationFolder]/Editor/Data/CGIncludes/UnityShaderVariables.cginc
 */
inline float4 mofalib_generateZBufferParams(float near, float far)
{
#if defined(UNITY_REVERSED_Z)
    float x = -1.0 + (far / near);
    float y = 1.0;
    return float4(x, y, (x / far), (1.0 / far));
#else
    float x = 1.0 - (far / near);
    float y = (far / near);
    return float4(x, y, (x / far), (y / far));
#endif
}