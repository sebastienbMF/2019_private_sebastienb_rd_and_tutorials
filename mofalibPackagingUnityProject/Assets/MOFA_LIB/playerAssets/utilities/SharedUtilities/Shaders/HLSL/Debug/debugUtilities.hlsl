/**
    debugUtilities.hlsl
    Purpose: Helper functions and instrumentation, useful for debugging but not
             intended for content production.
*/

#include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Math/mathContants.hlsl"

/*! \brief Private Function (condensed digit bitmap - bitfield matrix)
 *
 *  For a given input digit (0-9), this function will return
 *  a float representation of a bitfield
 *  (atypical, probably to stay with floating points).
 *
 * Ref : https://www.shadertoy.com/view/4sBSWW
 *
 */
float PRIVATE_DigitBin(const int x)
{
    return x == 0 ? 480599.0 :
           x == 1 ? 139810.0 :
           x == 2 ? 476951.0 :
           x == 3 ? 476999.0 :
           x == 4 ? 350020.0 :
           x == 5 ? 464711.0 :
           x == 6 ? 464727.0 :
           x == 7 ? 476228.0 :
           x == 8 ? 481111.0 :
           x == 9 ? 481095.0 : 0.0;
}

/*! \brief Print Value
 *
 * This function allows to print a value directly from shader code.
 * Useful for debugging matrices, uniforms, or other non-varying values.
 * 
 * @param screenCoords   Screen coordinates
 * @param fValue         The value to display
 * @param fMaxDigits     The maximum number of digits
 * @param fDecimalPlaces Number of decimal digits
 * @return  > 0. if the pixel should be lit to display the value.
 * 
 * Usage example:
 * 
   float2 vFontSize = float2(8.0, 10.0); // Multiples of 4x5 work best
   float3 vDebug = _WorldSpaceCameraPos;
   if (mofalib_PrintValue((_ScreenParams.xy*i.uv - float2(  0, 5)) / vFontSize, vDebug.x, 6.0, 2.0) > 0.1)
     return float4(1, 0.3, 0.3, 1);
   if (mofalib_PrintValue((_ScreenParams.xy*i.uv - float2(100, 5)) / vFontSize, vDebug.y, 6.0, 2.0) > 0.1)
     return float4(0.3, 1, 0.3, 1);
   if (mofalib_PrintValue((_ScreenParams.xy*i.uv - float2(200, 5)) / vFontSize, vDebug.z, 6.0, 2.0) > 0.1)
     return float4(0.3, 0.3, 1, 1);
   return fixed4(0, 0, 0, 1);
 * 
 * Ref : https://www.shadertoy.com/view/4sBSWW
 *
 */
float mofalib_PrintValue(float2 screenCoords, float fValue, float fMaxDigits, float fDecimalPlaces)
{
    if ((screenCoords.y < 0.0) || (screenCoords.y >= 1.0))
    {
        return 0.0;
    } 
    bool bNeg = (fValue < 0.0);
    fValue = abs(fValue);
    float fLog10Value = log2(abs(fValue)) / log2(10.0);
    float fBiggestIndex = max(floor(fLog10Value), 0.0);
    float fDigitIndex = fMaxDigits - floor(screenCoords.x);
    float fCharBin = 0.0;
    if (fDigitIndex > (-fDecimalPlaces - 1.01))
    {
        if (fDigitIndex > fBiggestIndex)
        {
            if ((bNeg) && (fDigitIndex < (fBiggestIndex + 1.5)))
            {
                fCharBin = 1792.0;
            }
        }
        else
        {
            if (fDigitIndex == -1.0)
            {
                if (fDecimalPlaces > 0.0)
                {
                    fCharBin = 2.0;
                }
            }
            else
            {
                float fReducedRangeValue = fValue;
                if (fDigitIndex < 0.0)
                {
                    fReducedRangeValue = frac(fValue);
                    fDigitIndex += 1.0;
                }
                float fDigitValue = (abs(fReducedRangeValue / (pow(10.0, fDigitIndex))));
                fCharBin = PRIVATE_DigitBin(int(floor(fmod(fDigitValue, 10.0))));
            }
        }
    }
    return floor(fmod((fCharBin / pow(2.0, floor(frac(screenCoords.x) * 4.0) + (floor(screenCoords.y * 5.0) * 4.0))), 2.0));
}

/*! \brief Print Float 3 Value
 *
 * This function allows to print a float3 value directly from shader code.
 * Useful for debugging matrices, uniforms, or other non-varying values.
 * Same as above, repeated 3 times with less parameters.
 *
 * @param uv   Pixel shader uv (0-1, typically)
 * @param f3   The float3 value to print
 * @param scl  Text scaling factor. 1.0 is the minimum readable size.
 * @param v    Render target resolution
 * @return     Color value, where alpha == 0 if the pixel is not part of the text.
 *
 * Usage example:
 *
   //Display camera direction coordinates
   float4 cText = mofalib_printFloat3(i.uv, camDir, 5., _ScreenParams.xy);
   if (cText.a > 0)
     return cText;
 *
 * Ref : https://www.shadertoy.com/view/4sBSWW
 *
 */
float4 mofalib_printFloat3(float2 uv, float3 f3, float scl, float2 iResolution)
{
    float2 vFontSize = float2(4.0, 5.0)*scl; // Multiples of 4x5 work best

    if (mofalib_PrintValue((iResolution*uv - float2(0.*scl, 5)) / vFontSize, f3.x, 6.0, 2.0) > 0.1)
        return float4(1, 0.3, 0.3, 1);
    if (mofalib_PrintValue((iResolution*uv - float2(50.*scl, 5)) / vFontSize, f3.y, 6.0, 2.0) > 0.1)
        return float4(0.3, 1, 0.3, 1);
    if (mofalib_PrintValue((iResolution*uv - float2(100.*scl, 5)) / vFontSize, f3.z, 6.0, 2.0) > 0.1)
        return float4(0.3, 0.3, 1, 1);

    return float4(0, 0, 0, 0);
}

/*! \brief Print Float 4 Value
 *
 * This function allows to print a float4 value directly from shader code.
 * Useful for debugging matrices, uniforms, or other non-varying values.
 * Same as above, repeated 4 times with less parameters.
 *
 * @param uv   Pixel shader uv (0-1, typically)
 * @param f4   The float4 value to print
 * @param scl  Text scaling factor. 1.0 is the minimum readable size.
 * @param v    Render target resolution
 * @return     Color value, where alpha == 0 if the pixel is not part of the text.
 *
 * Usage example:
 *
   //Display camera direction coordinates
   float4 cText = mofalib_printFloat3(i.uv, _ZBufferParams, 5., _ScreenParams.xy);
   if (cText.a > 0)
     return cText;
 *
 * Ref : https://www.shadertoy.com/view/4sBSWW
 *
 */
float4 mofalib_printFloat4(float2 uv, float4 f4, float scl, float2 iResolution)
{
    float2 vFontSize = float2(4.0, 5.0)*scl; // Multiples of 4x5 work best

    if (mofalib_PrintValue((iResolution*uv - float2(0.*scl, 5)) / vFontSize, f4.x, 6.0, 4.0) > 0.1)
        return float4(1, 0.3, 0.3, 1);
    if (mofalib_PrintValue((iResolution*uv - float2(50.*scl, 5)) / vFontSize, f4.y, 6.0, 4.0) > 0.1)
        return float4(0.3, 1, 0.3, 1);
    if (mofalib_PrintValue((iResolution*uv - float2(100.*scl, 5)) / vFontSize, f4.z, 6.0, 4.0) > 0.1)
        return float4(0.3, 0.3, 1, 1);
    if (mofalib_PrintValue((iResolution*uv - float2(150.*scl, 5)) / vFontSize, f4.w, 6.0, 4.0) > 0.1)
        return float4(1, 0, 1, 1);

    return float4(0, 0, 0, 0);
}

/*! \brief mofalib_Dial
 *
 * This function generates some kind of black and white dial.
 * This is useful for visually debugging timers, for example.
 *
 * @param p   Pixel position (centered on (0,0))
 * @param t   Dial value (cycles on [0-1])
 * @param r   Radius of the dial.
 * @return    Pixel color (black or white)
 *
 * Usage example:
 *
 * return float4((float3)mofalib_Dial((uv - 0.5), _Time.y / 4, 0.5),1.);
 *
 * Ref : https://www.shadertoy.com/view/4sBSWW
 *
 */
float mofalib_Dial(float2 p, float t, float r)
{
    float fT = atan2(p.x, p.y) / 6.28318 + 0.5;
    float dc = length(p);
    float eps = 0.005*r; //Transition width (pseudo-antialiasing)
    float eps_rad = eps / (MF_PI*dc);
    float c =  smoothstep(frac(t) - eps_rad, frac(t) + eps_rad, fT);
          c *= smoothstep(r + eps, r - eps, dc);
    return max(c, 0.);
}