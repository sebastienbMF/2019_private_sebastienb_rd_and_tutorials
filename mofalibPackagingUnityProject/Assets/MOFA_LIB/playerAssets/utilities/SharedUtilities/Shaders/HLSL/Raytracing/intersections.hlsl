/**
    intersections.hlsl
    Purpose: Ray-Object intersection function collection.

    Useful links (variants for other shapes):
    http://www.iquilezles.org/www/articles/intersectors/intersectors.htm
    https://www.shadertoy.com/view/tl23Rm (Ray Tracing - Primitives)
    https://www.shadertoy.com/view/4s23DR (ray/cone and ray/frustum)
*/

#include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Syntax/syntacticSugar.hlsl"
#include "Assets/MOFA_LIB/playerAssets/utilities/SharedUtilities/Shaders/HLSL/Math/mathContants.hlsl"

static const float MOFALIB_MAX_INTERSECTION_DIST = 9000000.; //Arbitrary.

/*! \brief Macro to combine primitives.
 *
 * This macros is intended to be used with intersection functions below,
 * where the first component of the returned value is the distance from ray origin
 * or camera.
 * In other words, it returns the shape that is in front of the other for the given ray.
 *
 * Usage example :
 *  //Note h  = hit info (hit distance, uvx, uvy)
 *  //     ro = ray origin
 *  //     rd = ray direction
 *  float3 h =                  float3(mofalib_RaySphIntersecUV(ro - float3(1,0,0), rd, 1.));
 *  h = mofalib_addPrimitive(h, float3(mofalib_RaySphIntersecUV(ro - float3(2,0,0), rd, 1.)));
 *  h = mofalib_addPrimitive(h, float3(mofalib_RayCylIntersecUV(ro - float3(3,0,0), rd, float3(0, 1, 0), float3(0, -1, 0), 1.)));
 *  h = mofalib_addPrimitive(h, float3(mofalib_RayBoxIntersecUV(ro - float3(4,0,0), rd, (float3)1.5)));
 *  h = mofalib_addPrimitive(h, float3(mofalib_RayBoxIntersecUV(ro - float3(5,0,0), rd, (float3)0.5)));
 *  h = mofalib_addPrimitive(h, float3(mofalib_RayPlaneXZIntersecUV(ro - (float3)0, rd, .1*float2(16,9))));
 *
 *  float3 hitPosition = ro+rd*h.x;
 *  float2 uv = h.yz;
 */
#define mofalib_addPrimitive(a, b) (((a).x < (b).x) ? a : b)

/*! \brief Ray-Sphere Intersection (with UVs)
 *
 * Computes and returns the intersection of a ray with a sphere.
 * Returns the distance from ray origin along ray direction 
 * if there is a hit, or -1 otherwise.
 *
 * Usage example 1:
 *  float3 hitInfo = mofalib_raySphIntersecUV(ro, rd, radius);
 *  float3 hitPosition = ro+rd*hitInfo.x;
 *
 * @param ro   Ray origin
 * @param rd   Ray direction
 * @param sr   Sphere radius
 * @return     float3(distance from ro, uvx, uvy)
 *
 * Ref : http://www.iquilezles.org/blog/?p=2411
 *          (Modified to return UVs)
 */
float3 mofalib_RaySphIntersecUV(float3 ro, float3 rd, float sr)
{
    float b = dot(rd, ro);
    float c = dot(ro, ro) - sr * sr;
    float t = b * b - c;
    if (t > 0.0)
    {
        t = -b - sqrt(t);
        float3 p = ro + t * rd;
        return float3(t, float2((atan2(p.x, p.z) + MF_PI) / (2.*MF_PI), acos(p.y / sr) / MF_PI));
    }
    return float3(MOFALIB_MAX_INTERSECTION_DIST, 0, 0);
}

/*! \brief Uncapped Ray-Cylinder Intersection (with UVs)
 *
 * Computes and returns the intersection of a ray with a cylinder.
 * Returns the distance from ray origin along ray direction
 * if there is a hit, or -1 otherwise.
 * 
 * Usage example 1:
 *  float3 hitInfo = mofalib_rayCylIntersecUV(ro, rd, float3(0, 1, 0), float3(0, -1, 0), radius);
 *  float3 hitPosition = ro+rd*hitInfo.x;
 *
 * @param ro   Ray origin
 * @param rd   Ray direction
 * @param p1   The first end of the cylinder
 * @param p2   The second end of the cylinder
 * @param r    Cylinder radius
 * @return     float3(distance from ro, uvx, uvy)
 * 
 * Ref : sebastienb@momentfactory.com (probably not optimal!)
 * Note : For a version returning surface normal rather than UVs, see :
 *        http://www.iquilezles.org/www/articles/intersectors/intersectors.htm
 */
float3 mofalib_RayCylIntersecUV(float3 ro, float3 rd, float3 p1, float3 p2, float r)
{
    float r2 = r * r;
    float3 nLateral=cross(p2-p1,rd);//Billboard plane
    float3 nPlane = cross(p2-p1,nLateral);
    float a = dot(p1-ro,nPlane)/dot(rd,nPlane);
    float3 pBillbrd = ro+a*rd;
    float3 vToCenter = dot(p2-pBillbrd,nLateral)/dot(nLateral,nLateral)*nLateral;
    float dist2 = dot(vToCenter,vToCenter);
    if(dist2<r2&&a>0.)
    {   //OPTME : The part below could most probably be optimized. Too many sqrt happening there.
        float k = sqrt(r2-dist2);
        float t = 1.0/abs(dot(normalize(nPlane),rd));
        float theta = atan2(k,sqrt(dist2)*(dot(vToCenter,nLateral)>0.0?-1.0:+1.0));
        a = a-k*t;
        float3 p = ro+a*rd;
        float fHeight = dot(p-p1,p2-p1);
        if(fHeight>=0.0 && fHeight <= dot(p2-p1,p2-p1))
            return float3(a,float2(theta,dot(p-p1,normalize(p2-p1))));
    }
    return float3(MOFALIB_MAX_INTERSECTION_DIST,float2(0,0));
}

/*! \brief Axis-Aligned Uncapped Ray-Cylinder Intersection (with UVs)
 *
 * Computes and returns the intersection of a ray with a cylinder.
 * Returns the distance from ray origin along ray direction
 * if there is a hit, or MOFALIB_MAX_INTERSECTION_DIST otherwise.
 *
 * Usage example 1:
 *  float3 hitInfo = mofalib_rayCylIntersecUV(ro, rd, 10.0, 0.1);
 *  float3 hitPosition = ro+rd*hitInfo.x;
 *
 * @param ro   Ray origin
 * @param rd   Ray direction
 * @param h    Height of the cylinder
 * @param r    Radius of the cylinder
 * @return     float3(distance from ro, uvx, uvy)
 *
 * Ref : stevenb@momentfactory.com
 * Note : For a version returning surface normal rather than UVs, see :
 *        http://www.iquilezles.org/www/articles/intersectors/intersectors.htm
 */
float3 mofalib_RayCylIntersecUV(float3 ro, float3 rd, float h, float r)
{
    float2 rd2dnorm = normalize(rd.xz);
    float2 ro2d = ro.xz;
    float b = dot(rd2dnorm, ro2d);
    float c = dot(ro2d, ro2d) - r * r;
    float t = b * b - c;
    if (t > 0.0)
    {
        t = (-b - sqrt(t)) / length(rd.xz);
        float3 p = ro + t * rd;
        if (p.y >= -h && p.y <= h)
        {
            return float3(t, float2((atan2(p.x, p.z) + MF_PI) / (2.*MF_PI), p.y + h / (2 * h)));
        }
    }
    return float3(MOFALIB_MAX_INTERSECTION_DIST, float2(0, 0));
}

/*! \brief Uncapped Ray-Box Intersection (with UVs)
 *
 * Computes and returns the intersection of a ray with a box.
 * Returns the distance from ray origin along ray direction
 * if there is a hit, or -1 otherwise.
 *
 * @param ro   Ray origin
 * @param rd   Ray direction
 * @param size Size along each axis (half-extent)
 * @return     float3(distance from ro, uvx, uvy)
 *
 * Usage example 1:
 *  float3 hitInfo = mofalib_rayBoxIntersecUV(ro, rd, float3(2, 3, 2.5));
 *  float3 hitPosition = ro+rd*hitInfo.x;
 *
 * Usage example 2 (rotated, translated):
 *  float3 objectPos = float3(2,1,3);
 *  //transpose to inverse : the view rather the object is rotated.
 *  float3x3 mRot = transpose(getRotationMatrix());
 *  float3x3 rotation = getRotationMatrix();
 *  float3 hitInfo = mofalib_rayBoxIntersecUV(mul(mRot,ro-objectPos), mul(mRot,rd), float3(2, 3, 2.5));
 *
 * Ref : sebastienb@momentfactory.com (probably not optimal!)
 *          https://www.shadertoy.com/view/MscSzn
 */
float3 mofalib_RayBoxIntersecUV(float3 ro, float3 rd, float3 size)
{
    float cullingDir = all(lessThan(abs(ro), size)) ? 1. : -1.;
    float3 viewSign = cullingDir * sign(rd);
    float3 t = (-sign(rd)*size - ro) / rd;
    float2 uvx = (ro.zy+t.x*rd.zy)/size.zy;
    float2 uvy = (ro.xz+t.y*rd.xz)/size.xz;
    float2 uvz = (ro.xy+t.z*rd.xy)/size.xy;
    //Note : This is for left-handed coordinate systems. Invert viewSign if needed to flip UVs.
    if(      all(lessThan(abs(uvx),float2(1,1))) && t.x > 0.) return float3(t.x,(float2( viewSign.x, 1)*uvx+1.)/2.);
    else if( all(lessThan(abs(uvy),float2(1,1))) && t.y > 0.) return float3(t.y,(float2( 1, viewSign.y)*uvy+1.)/2.);
    else if( all(lessThan(abs(uvz),float2(1,1))) && t.z > 0.) return float3(t.z,(float2(-viewSign.z, 1)*uvz+1.)/2.);
    return float3(MOFALIB_MAX_INTERSECTION_DIST,float2(0,0));
}

/*! \brief Ray-XZPlane Intersection (with UVs)
 *
 * Computes and returns the intersection of a ray with a XZ oriented plane.
 * Returns the distance from ray origin along ray direction
 * if there is a hit, or -1 otherwise.
 *
 * @param ro   Ray origin
 * @param rd   Ray direction
 * @param size Plane half-extent size, along X and Z axis
 * @return     float3(distance from ro, uvx, uvy)
 *
 * Usage example 1:
 *  float3 hitInfo = mofalib_rayPlaneXZIntersecUV(ro, rd, float3(16, 9));
 *  float3 hitPosition = ro+rd*hitInfo.x;
 *
 * Usage example 2 (rotated, translated):
 *  float3 objectPos = float3(2,1,3);
 *  //transpose to inverse : the view rather the object is rotated.
 *  float3x3 mRot = transpose(getRotationMatrix());
 *  float3x3 rotation = getRotationMatrix();
 *  float3 hitInfo = mofalib_rayPlaneXZIntersecUV(mul(mRot,ro-objectPos), mul(mRot,rd), float3(16, 9));
 *
 * Ref : sebastienb@momentfactory.com (probably not optimal!)
 */
float3 mofalib_RayPlaneXZIntersecUV(float3 ro, float3 rd, float2 size)
{
    float d = -ro.y/rd.y;
    float3 p = ro+rd*d;
    if(abs(p.x)<size[0]&&abs(p.z)<size[1]&&d>0.)
    {
        return float3(d,(float2(.5,.5)*p.xz/size+float2(.5,.5)));
    }
    return float3(MOFALIB_MAX_INTERSECTION_DIST,float2(0,0));
}

/*! \brief Ray-Plane Intersection
 *
 * Computes and returns the intersection of a ray with an arbitrarily oriented plane.
 * Returns the distance from ray origin along ray direction
 * if there is a hit, or -1 otherwise.
 *
 * @param ro   Ray origin
 * @param rd   Ray direction
 * @param po   Arbitrary point on the plane
 * @param pn   Plane normal
 * @return     Intersection distance from ro, along rd
 *
 * Usage example 1:
 *  float hitDistance= mofalib_rayPlaneIntersec(ro, rd, float3(0, 0, 0), float3(0,1,0));
 *  float3 hitPosition = ro+rd*hitDistance;
 */
float mofalib_RayPlaneIntersec(float3 ro, float3 rd, float3 po, float3 pn)
{
    float t = dot(po - ro, pn) / dot(rd, pn);
    return t > 0. ? t : MOFALIB_MAX_INTERSECTION_DIST;
}