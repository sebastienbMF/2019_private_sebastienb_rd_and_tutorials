﻿// FluidSimProcessor
//  History:
//    Jos Stam's Stable Fluids
//    Then implemented in Unity by Keijiro Takahashi (https://github.com/keijiro/StableFluids)
//    Modification MF : add external forces
//    Modification MF : better components packaging
using UnityEngine;
using System;

namespace mofalib
{
  namespace Rendering
  {
    namespace Physics
    {
      public class FluidSimProcessor : ITextureGenerator, mofalib.Rendering.ShaderPassAPI.ShaderPassInterface
      {
        public float viscosity = 1e-6f;
        
        private int _resolutionX = 512;
        private int _resolutionY = 512;
        
        private ComputeShader compute;
        private Shader forceShader = null;
        private Action<Material> updateForceFunc;
        private Material externalForceMaterial = null;

        static class Kernels
        {
          public const int Advect = 0;
          public const int Force = 1;
          public const int PSetup = 2;
          public const int PFinish = 3;
          public const int Jacobi1 = 4;
          public const int Jacobi2 = 5;
        }

        private int ThreadCountX { get { return (_resolutionX + 7) / 8; } }
        private int ThreadCountY { get { return (_resolutionY + 7) / 8; } }
        public int ResolutionX { get { return ThreadCountX * 8; } }
        public int ResolutionY { get { return ThreadCountY * 8; } }

        public enum VectorBufferID
        {
          Velocity1 = 0,
          Velocity2 = 1,
          Velocity3 = 2,
          Pressure1 = 3,
          Pressure2 = 4,
          Force = 5
        }

        // Vector field buffers
        static class VFB
        {
          public static RenderTexture V1;
          public static RenderTexture V2;
          public static RenderTexture V3;
          public static RenderTexture P1;
          public static RenderTexture P2;
          public static RenderTexture F1;
        }

        private RenderTexture AllocateBuffer(int componentCount, int width = 0, int height = 0)
        {
          var format = RenderTextureFormat.ARGBHalf;
          if (componentCount == 1) format = RenderTextureFormat.RHalf;
          if (componentCount == 2) format = RenderTextureFormat.RGHalf;

          if (width == 0) width = ResolutionX;
          if (height == 0) height = ResolutionY;

          var rt = new RenderTexture(width, height, 0, format);
          rt.enableRandomWrite = true;
          rt.Create();
          return rt;
        }

        public FluidSimProcessor(int resolutionX, int resolutionY, ComputeShader fluidCompute, Shader externalForces = null, Action<Material> updateForceFunction = null)
        {
          _resolutionX = Mathf.Max(resolutionX, 8);
          _resolutionY = Mathf.Max(resolutionY, 8);
          updateForceFunc = updateForceFunction;
          compute = fluidCompute;
          forceShader = externalForces;

          if(forceShader!=null)
            externalForceMaterial = new Material(forceShader); //This should be a compute shader

          VFB.V1 = AllocateBuffer(2);
          VFB.V2 = AllocateBuffer(2);
          VFB.V3 = AllocateBuffer(2);
          VFB.P1 = AllocateBuffer(1);
          VFB.P2 = AllocateBuffer(1);
          VFB.F1 = AllocateBuffer(2); //TODO: conditional allocation. Not needed if no external forces.
        }
        
        public void ReleaseResources()
        {
          UnityEngine.Object.Destroy(externalForceMaterial); //This should be a compute shader.

          UnityEngine.Object.Destroy(VFB.V1);
          UnityEngine.Object.Destroy(VFB.V2);
          UnityEngine.Object.Destroy(VFB.V3);
          UnityEngine.Object.Destroy(VFB.P1);
          UnityEngine.Object.Destroy(VFB.P2);
          UnityEngine.Object.Destroy(VFB.F1);
        }
        
        virtual public Material GetMaterial()
        {
          return externalForceMaterial;
        }

        public void Update()
        {
            Update(updateForceFunc);
        }

        public void Update(Action<Material> updateForceFunction)
        {
          var dt = Time.deltaTime;
          var dx = 1.0f / ResolutionY;

          // Common variables
          compute.SetFloat("Time", Time.time);
          compute.SetFloat("DeltaTime", dt);

          // Advection
          compute.SetTexture(Kernels.Advect, "U_in", VFB.V1);
          compute.SetTexture(Kernels.Advect, "W_out", VFB.V2);
          compute.Dispatch(Kernels.Advect, ThreadCountX, ThreadCountY, 1);

          // Diffuse setup
          var dif_alpha = dx * dx / (viscosity * dt);
          compute.SetFloat("Alpha", dif_alpha);
          compute.SetFloat("Beta", 4 + dif_alpha);
          Graphics.CopyTexture(VFB.V2, VFB.V1);
          compute.SetTexture(Kernels.Jacobi2, "B2_in", VFB.V1);

          // Jacobi iteration
          for (var i = 0; i < 20; i++)
          {
            compute.SetTexture(Kernels.Jacobi2, "X2_in", VFB.V2);
            compute.SetTexture(Kernels.Jacobi2, "X2_out", VFB.V3);
            compute.Dispatch(Kernels.Jacobi2, ThreadCountX, ThreadCountY, 1);

            compute.SetTexture(Kernels.Jacobi2, "X2_in", VFB.V3);
            compute.SetTexture(Kernels.Jacobi2, "X2_out", VFB.V2);
            compute.Dispatch(Kernels.Jacobi2, ThreadCountX, ThreadCountY, 1);
          }

          // Add external force
          if (externalForceMaterial != null)
          {
            updateForceFunction.Invoke(externalForceMaterial);
            externalForceMaterial.SetTexture("_VelocityTex", VFB.V1);
            externalForceMaterial.SetVector("_Resolution", new Vector4(ResolutionX, ResolutionY,0,0));
            Graphics.Blit(null, VFB.F1, externalForceMaterial, 0);
          }
          
          compute.SetTexture(Kernels.Force, "F_in", VFB.F1);
          compute.SetTexture(Kernels.Force, "W_in", VFB.V2);
          compute.SetTexture(Kernels.Force, "W_out", VFB.V3);
          compute.Dispatch(Kernels.Force, ThreadCountX, ThreadCountY, 1);

          // Projection setup
          compute.SetTexture(Kernels.PSetup, "W_in", VFB.V3);
          compute.SetTexture(Kernels.PSetup, "DivW_out", VFB.V2);
          compute.SetTexture(Kernels.PSetup, "P_out", VFB.P1);
          compute.Dispatch(Kernels.PSetup, ThreadCountX, ThreadCountY, 1);

          // Jacobi iteration
          compute.SetFloat("Alpha", -dx * dx);
          compute.SetFloat("Beta", 4);
          compute.SetTexture(Kernels.Jacobi1, "B1_in", VFB.V2);

          for (var i = 0; i < 20; i++)
          {
            compute.SetTexture(Kernels.Jacobi1, "X1_in", VFB.P1);
            compute.SetTexture(Kernels.Jacobi1, "X1_out", VFB.P2);
            compute.Dispatch(Kernels.Jacobi1, ThreadCountX, ThreadCountY, 1);

            compute.SetTexture(Kernels.Jacobi1, "X1_in", VFB.P2);
            compute.SetTexture(Kernels.Jacobi1, "X1_out", VFB.P1);
            compute.Dispatch(Kernels.Jacobi1, ThreadCountX, ThreadCountY, 1);
          }

          // Projection finish
          compute.SetTexture(Kernels.PFinish, "W_in", VFB.V3);
          compute.SetTexture(Kernels.PFinish, "P_in", VFB.P1);
          compute.SetTexture(Kernels.PFinish, "U_out", VFB.V1);
          compute.Dispatch(Kernels.PFinish, ThreadCountX, ThreadCountY, 1);
        }

        //Generic interface
        public Texture GetTexture(uint texNo)
        {
          return GetVectorField((VectorBufferID)texNo);
        }

        //Specific interface
        public Texture GetVectorField(VectorBufferID id)
        {
          Texture[] vfArray = { VFB.V1,
                                VFB.V2,
                                VFB.V3,
                                VFB.P1,
                                VFB.P2,
                                VFB.F1 };
          return vfArray[(uint)id];
        }
      }
    }
  }
}