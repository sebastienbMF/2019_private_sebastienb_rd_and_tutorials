﻿using UnityEngine;

public abstract class MonoBehaviourTextureGenerator : MonoBehaviour, ITextureGenerator
{
    public abstract Texture GetTexture(uint texNo);
}

public interface ITextureGenerator
{
    Texture GetTexture(uint texNo);
}