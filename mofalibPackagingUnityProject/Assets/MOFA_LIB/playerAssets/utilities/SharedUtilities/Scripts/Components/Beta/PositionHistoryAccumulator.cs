﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PositionHistory
{
    LinkedList<Vector2> GetHistory();
}

public abstract class PositionHistoryMonoBehaviour : MonoBehaviour, PositionHistory
{
    public abstract LinkedList<Vector2> GetHistory();
}

public class PositionHistoryAccumulator : PositionHistoryMonoBehaviour
{
    public int MaxHistorySize = 500;
    LinkedList<Vector2> positions;
    // Start is called before the first frame update
    void Start()
    {
        positions = new LinkedList<Vector2>();
    }

    // Update is called once per frame
    void Update()
    {
        positions.AddFirst(new Vector2(Input.mousePosition.x / Screen.width,
                                       Input.mousePosition.y / Screen.height));

        while (positions.Count > MaxHistorySize)
            positions.RemoveLast();
    }

    public override LinkedList<Vector2> GetHistory()
    {
        return positions;
    }
}
