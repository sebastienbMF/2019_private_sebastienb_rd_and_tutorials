﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mofalib.Rendering.ShaderNodes;

public class MousePainterComponent : MonoBehaviourTextureGenerator
{
    public Texture InputTexture;
    public bool Hold = false;
    private MousePaintPass inputPass;

    // Start is called before the first frame update
    void Start()
    {
        int ResX = Screen.width / 2;
        int ResY = Screen.height / 2;
        inputPass = new MousePaintPass(ResX, ResY, InputTexture);
    }

    // Update is called once per frame
    void Update()
    {
        inputPass.Hold = Hold;
        inputPass.TextureScale = 0.5F;
        inputPass.Update();
    }
    
    override public Texture GetTexture(uint texNo)
    {
        return inputPass.GetTexture(0);
    }
}
