﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayTextureGeneratorComponent : MonoBehaviour
{
    public MonoBehaviourTextureGenerator TextureGenerator;
    public uint TextureGeneratorSelector = 0;
    public Texture TextureOverride = null;
    
    private Material m;

    // Start is called before the first frame update
    void Start()
    {
        m = new Material(Shader.Find("Unlit/TextureMultiply"));
        GetComponent<Renderer>().sharedMaterial = m;
    }

    // Update is called once per frame
    void Update()
    {
        if (m == null)
        {
            return;
        }

        Texture inputTex = (TextureOverride != null) ? TextureOverride : TextureGenerator.GetTexture(TextureGeneratorSelector);

        m.SetTexture("_MainTex", inputTex);
    }
}
