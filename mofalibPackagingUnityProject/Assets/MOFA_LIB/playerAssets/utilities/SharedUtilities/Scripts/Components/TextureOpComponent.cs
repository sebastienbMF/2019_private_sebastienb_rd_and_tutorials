﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mofalib.Rendering.ShaderPassAPI;

public class TextureOpComponent : MonoBehaviourTextureGenerator
{
    public int ResX = 640;
    public int ResY = 360;
    public MonoBehaviourTextureGenerator InputA;
    public MonoBehaviourTextureGenerator InputB;
    public Shader OperationShader;
    public RenderTextureFormat Format = RenderTextureFormat.R8;

    private SimpleShaderPass operationPass;

    // Start is called before the first frame update
    void Start()
    {
        operationPass = new SimpleShaderPass(OperationShader, ResX, ResY, Format);
    }

    // Update is called once per frame
    void Update()
    {
        operationPass.Update(delegate(Material m)
        {
            m.SetTexture("_InputA", InputA.GetTexture(0));
            m.SetTexture("_InputB", InputB.GetTexture(0));
        });
    }
    
    override public Texture GetTexture(uint texNo)
    {
        return operationPass.GetTexture(0);
    }
}
