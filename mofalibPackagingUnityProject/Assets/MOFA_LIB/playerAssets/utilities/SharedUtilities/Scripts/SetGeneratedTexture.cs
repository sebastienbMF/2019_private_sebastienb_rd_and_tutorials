﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGeneratedTexture : MonoBehaviour {

    public Texture TextureSource;
    public GameObject TextureGeneratorObj;
    public Shader DisplayShader;
    public uint TextureID = 0;
    public float TextureValueMultiplier = 1;
    
    private Material showTextureMat;
    public ITextureGenerator textureGen;

    public void Awake()
    {
        if (DisplayShader == null)
        {
            DisplayShader = Shader.Find("Unlit/TextureMultiply");
        }
    }

    public void Start()
    {
        showTextureMat = new Material(DisplayShader);
        if (TextureGeneratorObj != null)
        {
            textureGen = TextureGeneratorObj.GetComponent<ITextureGenerator>() as ITextureGenerator;
        }
    }

    Texture getDisplayTexture()
    {
        if (TextureSource != null)
        {
            return TextureSource;
        }
        else
        {
            return textureGen.GetTexture(TextureID);
        }
    }

    void setupMaterial(Material mat)
    {
        mat.SetTexture("_MainTex", getDisplayTexture());
        mat.SetFloat("_Multiply", TextureValueMultiplier);
    }

    //Use case : Camera
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        setupMaterial(showTextureMat);
        Graphics.Blit(getDisplayTexture(), destination, showTextureMat, 0);
    }

    //Use case : Rendered object 
    private void Update()
    {
        Renderer rdr = GetComponent<Renderer>();
        if (rdr != null)
        {
            setupMaterial(rdr.material);
        }
    }
}
