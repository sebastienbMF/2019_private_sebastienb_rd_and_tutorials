﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mofalib
{
    namespace Rendering
    {
        public static partial class CameraUtilities
        {
            //This function makes it possible to access a depth buffer texture
            //generated from a different camera than the active one.
            //See shader instruction "mofalib_LinearEyeDepth()", making use of
            //these values.
            public static Vector4 ComputeZBufferParams(float near, float far)
            {
                //See 
                //https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html
                //http://www.laiqifang.com/2017/09/15/_zbufferparams-in-reversed-depth-buffer/
                //https://forum.unity.com/threads/_zbufferparams-values.39332/
                //See also : Editor/Data/CGIncludes/UnityShaderVariables.cginc
                /*
                With reversed depth buffer(DirectX 11, DirectX 12, PS4, Xbox One, Metal), _ZBufferParams contain :
                    x = -1 + far / near
                    y = 1
                    z = x / far
                    w = 1 / far
                    and Clip space range is[near, 0] instead of[0, far].

                The Traditional direction(OpenGL, D9) : _ZBufferParams contain :
                    x is(1 - far / near),
                    y is(far / near),
                    z is(x / far) and
                    w is(y / fa)
                */
                if (SystemInfo.usesReversedZBuffer)
                {
                    float x = -1.0f + (far / near);
                    float y = 1.0f;
                    return new Vector4(x, y, (x / far), (1.0f / far));
                }
                else
                {
                    float x = 1.0f - (far / near);
                    float y = (far / near);
                    return new Vector4(x, y, (x / far), (y / far));
                }
            }
        }
    }
}
