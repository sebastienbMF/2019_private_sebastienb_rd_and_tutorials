﻿using UnityEngine;
using System;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderNodes
        {
            class GradientPass : mofalib.Rendering.ShaderPassAPI.SimpleShaderPass
            {
                public GradientPass(int resX, int resY, RenderTextureFormat fmt = RenderTextureFormat.R16)
                : base("ShaderNodes/GradientPass", resX, resY, fmt)
                {
                }
            }
        }
    }
}