using System;
using UnityEngine;
using System.Collections.Generic;
using mofalib.Rendering.ShaderPassAPI;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderNodes
        {
            public class MultiInputPaintPass : mofalib.Rendering.ShaderPassAPI.SimpleShaderPass
            {
                public MultiInputPaintPass(int resX, int resY, RenderTextureFormat fmt = RenderTextureFormat.RG16)
                  : base("ShaderNodes/MultiInputPaint", resX, resY, fmt)
                {
                }

                public void Update(List<Vector2> points)
                {
                    const int MAX_INPUTS = 20;
                    Vector4[] shaderInputs = new Vector4[MAX_INPUTS];

                    for(int i=0; i< MAX_INPUTS; ++i)
                    {
                        shaderInputs[i] = Vector4.zero;

                        if(i<points.Count)
                        {
                            Vector2 p = points[i];
                            shaderInputs[i].x = p.x;
                            shaderInputs[i].y = p.y;
                            shaderInputs[i].z = 1;
                            shaderInputs[i].w = 1;
                        }
                    }

                    base.Update(delegate (Material m)
                    {
                        m.SetVectorArray("_Inputs", shaderInputs);
                    });
                }
            }
        }
    }
}