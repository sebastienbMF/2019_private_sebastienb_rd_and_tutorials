using System;
using UnityEngine;

namespace mofalib
{
    namespace Rendering
    {
        namespace Filters
        {
            public class GradientImageProcessor : ITextureGenerator
            {
                public enum OutputTextureID
                {
                    Gradient = 0,
                    Blur = 1
                }

                public enum Kernels
                {
                    BlurX = 0,
                    BlurY = 1,
                    Gradient = 2
                }

                private float[] coeffs;
                private ComputeShader compute;
                private RenderTexture blurX;
                private RenderTexture blurXY;
                private RenderTexture gradientResult;

                public GradientImageProcessor(ComputeShader computeGradientShader, int resX, int resY)
                {
                    compute = computeGradientShader;
                    blurX = AllocateBuffer(resX, resY, RenderTextureFormat.RFloat);
                    blurXY = AllocateBuffer(resX, resY, RenderTextureFormat.RFloat);
                    gradientResult = AllocateBuffer(resX, resY, RenderTextureFormat.RGFloat);
                }

                public void ReleaseResources()
                {
                    UnityEngine.Object.Destroy(blurX);
                    UnityEngine.Object.Destroy(blurXY);
                    UnityEngine.Object.Destroy(gradientResult);
                    coeffs = null;
                }

                public Texture GetTexture(uint texNo)
                {
                    if ((uint)OutputTextureID.Gradient == texNo)
                        return gradientResult;
                    else
                        return blurXY;
                }

                private RenderTexture AllocateBuffer(int width, int height, RenderTextureFormat fmt)
                {
                    var rt = new RenderTexture(width, height, 0, fmt);
                    rt.enableRandomWrite = true;
                    rt.Create();
                    return rt;
                }

                private void CreateGaussianCoefficients(int RAD, float Sigma)
                {
                    Sigma = Math.Max(Sigma, 0.01f);
                    int N = RAD + 1;
                    if (coeffs.Length != N)
                        coeffs = new float[RAD + 1];

                    float denom = 1 / (2 * (float)Math.PI);

                    for (int i = 0; i <= RAD; ++i)
                    {
                        coeffs[i] = (float)(denom * Math.Exp(-0.5 * (float)(i * i) / (Sigma * Sigma)) / Sigma);
                    }
                }

                public void GenerateGradient(Texture InputTexture, int SampleRad, float GaussSigma)
                {
                    int ThreadCountX = (InputTexture.width + 7) / 8;
                    int ThreadCountY = (InputTexture.height + 7) / 8;

                    CreateGaussianCoefficients(SampleRad, GaussSigma);

                    //Blur X
                    compute.SetFloats("Coeffs", coeffs);
                    compute.SetInt("RAD", SampleRad);
                    compute.SetTexture((int)Kernels.BlurX, "InputImage", InputTexture);
                    compute.SetTexture((int)Kernels.BlurX, "Result", blurX);
                    compute.Dispatch((int)Kernels.BlurX, ThreadCountX, ThreadCountY, 1);

                    //Blur Y
                    compute.SetTexture((int)Kernels.BlurY, "InputImage", blurX);
                    compute.SetTexture((int)Kernels.BlurY, "Result", blurXY);
                    compute.Dispatch((int)Kernels.BlurY, ThreadCountX, ThreadCountY, 1);

                    //Gradient
                    blurXY.wrapMode = TextureWrapMode.Clamp;
                    compute.SetTexture((int)Kernels.Gradient, "InputImage", blurXY);
                    compute.SetTexture((int)Kernels.Gradient, "Result", gradientResult);
                    compute.Dispatch((int)Kernels.Gradient, ThreadCountX, ThreadCountY, 1);
                }
            }
        }
    }
}