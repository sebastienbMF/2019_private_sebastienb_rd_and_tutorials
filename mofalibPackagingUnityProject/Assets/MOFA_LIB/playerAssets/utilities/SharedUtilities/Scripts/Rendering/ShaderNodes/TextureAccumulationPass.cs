using System;
using UnityEngine;
using mofalib.Rendering.ShaderPassAPI;

namespace mofalib
{
  namespace Rendering
  {
    namespace ShaderNodes
    {
      public class TextureAccumulationPass : DoubleBufferShaderPass
      {
        public float RampUpSpeed = 0.02F; //Linear speed at which the value ramps up
        public float RampDownSpeedExp = 0.99F; //"Exponential" speed at which the value fades out (% of current value). Must be < 1.0.
        public float RampDownSpeedLin = 0.0001F; //Linear speed at which the value ramps down. This is important to avoid value getting "stuck" where the ramp down % is smaller than bit precision.

        public TextureAccumulationPass(int resX, int resY, RenderTextureFormat fmt = RenderTextureFormat.R16)
          : base("ShaderNodes/TextureAccumulationPass", resX, resY, fmt)
        {
        }
        public void Update(Texture t)
        {
            base.Update(delegate (Material m)
            {
                m.SetFloat("_RampUpSpeed", RampUpSpeed);
                m.SetFloat("_RampDownSpeedExp", RampDownSpeedExp);
                m.SetFloat("_RampDownSpeedLin", RampDownSpeedLin);
                m.SetTexture("_MainTex", t);
            });
        }
      }
    }
  }
}