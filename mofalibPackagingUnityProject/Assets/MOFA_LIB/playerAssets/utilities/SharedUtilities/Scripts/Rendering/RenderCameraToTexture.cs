using UnityEngine;

namespace mofalib
{
    namespace Rendering
    {
        class RenderCameraToTexture
        {
            private Camera camera;
            private RenderTexture colorBuffer;
            private RenderTexture depthBuffer;

            public RenderCameraToTexture(Camera cam, int ResX, int ResY, int targetDisplay = -1)
            {
                camera = cam;

                //Changing the target display here
                if( targetDisplay == -1 && camera.targetDisplay == 0)
                {
                    Debug.LogError("Beware! RenderCameraToTexture set to Target Display 0 will prevent GUI from being displayed by the main camera. To remove this error, change the Camera object Target Display or provide an argument at construction.");
                }
                else
                {
                    camera.targetDisplay = targetDisplay;
                }

                colorBuffer = new RenderTexture(ResX, ResY, 32, RenderTextureFormat.ARGB32);
                colorBuffer.antiAliasing = 1; //Lowest level
                colorBuffer.useMipMap = false;

                depthBuffer = new RenderTexture(ResX, ResY, 32, RenderTextureFormat.Depth);
                depthBuffer.antiAliasing = 1; //Lowest level
                depthBuffer.useMipMap = false;
            }

            ~RenderCameraToTexture()
            {
                //ReleaseResources();
            }

            public void ReleaseResources()
            {
                UnityEngine.Object.Destroy(colorBuffer);
                UnityEngine.Object.Destroy(depthBuffer);
            }

            public void Update()
            {
                RenderTexture prevTarget = RenderTexture.active;
                camera.SetTargetBuffers(colorBuffer.colorBuffer, depthBuffer.depthBuffer);
                camera.Render();
                RenderTexture.active = prevTarget;
            }

            public Vector4 GetZBufferParams()
            {
                return CameraUtilities.ComputeZBufferParams(camera.nearClipPlane, camera.farClipPlane);
            }

            public Texture GetDepthTexture()
            {
                return depthBuffer;
            }

            public Texture GetColorTexture()
            {
                return colorBuffer;
            }
        }
    }
}
