using System;
using UnityEngine;
using mofalib.Rendering.ShaderPassAPI;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderNodes
        {
            public class DampingPass : DoubleBufferShaderPass
            {
                public float SingleFrameWeight = 0.1F;
                private int frameNo = 0;

                //Note : This shader reuses the damping pass, which can have other purposes.
                public DampingPass(int resX, int resY, RenderTextureFormat fmt = RenderTextureFormat.R16)
                    : base("ShaderNodes/DampingPass", resX, resY, fmt)
                {
                    frameNo = 0;
                }

                override public void Update(Action<Material> updateFunc)
                {
                    base.Update(delegate (Material m)
                    {
                        if (updateFunc != null)
                            updateFunc(m);

                        //Note : First frame has 100% to initialize properly.
                        m.SetFloat("_SingleFrameWeight", (frameNo == 0) ? 1.0F : SingleFrameWeight);
                        m.SetFloat("_FrameNo", frameNo); //Not really necessary, but added as an additional safety for the 
                        ++frameNo;
                    });
                }
            }
        }
    }
}