using UnityEngine;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderPassAPI
        {
            interface ShaderPassInterface : ITextureGenerator
            {
               void Update();
               void ReleaseResources();
               Material GetMaterial();
            }
        }
    }
}