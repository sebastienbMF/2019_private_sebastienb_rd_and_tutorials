using System;
using UnityEngine;
using mofalib.Rendering.ShaderPassAPI;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderNodes
        {
            //Usage Example : 
            //MousePaintPass pass = new MousePaintPass(Screen.width / 8, Screen.height / 8  , Resources.Load<Texture2D>("Textures/texture01"));
            public class MousePaintPass : mofalib.Rendering.ShaderPassAPI.SimpleShaderPass
            {
                public float TextureScale;
                public Texture Tex;
                public bool Hold = false;
                
                public MousePaintPass(int resX, int resY, Texture texture, RenderTextureFormat fmt = RenderTextureFormat.R8)
                  : base("ShaderNodes/DrawTexture", resX, resY, fmt)
                {
                    Tex = texture;
                    TextureScale = 0.5F;
                }

                override public void Update()
                {
                    Tex.wrapMode = TextureWrapMode.Clamp;
                    base.Update(delegate (Material m)
                    {
                        if(Input.GetMouseButton(0) || !Hold)
                        {
                            float imageAR = Mathf.Max(1, (float)Tex.width) / Mathf.Max(1, (float)Tex.height);
                            float screenAR = Mathf.Max(1, (float)Screen.width) / Mathf.Max(1, (float)Screen.height);
                            m.SetTexture("_MainTex", Tex);
                            m.SetVector("_TextureScale", new Vector4(imageAR * TextureScale / screenAR, TextureScale, 1, 1));
                            m.SetVector("_InputPosition", new Vector4(Input.mousePosition.x / Screen.width,
                                                                      Input.mousePosition.y / Screen.height,
                                                                      Input.GetMouseButton(0) ? 1 : 0,
                                                                      Input.GetMouseButton(1) ? 1 : 0));
                        }
                        
                    });
                }
            }
        }
    }
}