using UnityEngine;
using System;
using mofalib.Rendering.ShaderPassAPI;

namespace mofalib
{
  namespace Rendering
  {
    namespace ShaderPassAPI
    {
      class InkDispersionPass : DoubleBufferShaderPass
      {
        public InkDispersionPass(int resX, int resY, RenderTextureFormat fmt = RenderTextureFormat.ARGB32)
          : base("FluidSim/InkDispersion", resX, resY, fmt) //TODO : Change shader here. Rename to something else.
        {
        }
        
        public void setInitialTexture(Texture src) //TODO: Rename to resetTexture
        {
          //TODO : One of then is unnecessary. Clean this up.
          Graphics.Blit(src, (RenderTexture)GetTexture(0));
          Graphics.Blit(src, (RenderTexture)GetTexture(1));
        }

        public override void Update(Action<Material> updateFunc)
        {
          GetMaterial().SetTexture("_MainTex", GetTexture(0));
          base.Update(updateFunc);
        }
      }
    }
  }
}