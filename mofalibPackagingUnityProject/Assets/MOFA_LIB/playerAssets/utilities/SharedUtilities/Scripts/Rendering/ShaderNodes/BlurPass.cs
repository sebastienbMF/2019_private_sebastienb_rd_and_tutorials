﻿using UnityEngine;
using System;
using mofalib.Rendering.ShaderPassAPI;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderNodes
        {
            class BlurPass : ShaderPassInterface
            {
                private SimpleShaderPass blurPassX;
                private SimpleShaderPass blurPassY;
                private Action<Material> setSourceTexture;
                private float[] coeffs;
                public int sampleRAD;
                public float gaussSigma;
                int resX;
                int resY;

                public BlurPass(int resolutionX, int resolutionY, int NSamples, float fSigma, RenderTextureFormat fmt = RenderTextureFormat.ARGB32, Action<Material> updateFunc = null)
                {
                    resX = resolutionX;
                    resY = resolutionY;
                    setSourceTexture = updateFunc;
                    sampleRAD = NSamples;
                    gaussSigma = fSigma;

                    blurPassX = new SimpleShaderPass("ShaderNodes/DirectionalBlur", resolutionX, resolutionY, fmt);
                    blurPassY = new SimpleShaderPass("ShaderNodes/DirectionalBlur", resolutionX, resolutionY, fmt);
                }

                private float[] CreateGaussianCoefficients(int RAD, float Sigma)
                {
                    //OPTME : Do not recalculate if already computed.
                    Sigma = Math.Max(Sigma, 0.01f);
                    float[] gaussCoeffs = new float[RAD + 1];
                    float denom = 1 / (2 * (float)Math.PI);

                    for (int i = 0; i <= RAD; ++i)
                    {
                        gaussCoeffs[i] = (float)(denom * Math.Exp(-0.5 * (float)(i * i) / (Sigma * Sigma)) / Sigma);
                    }
                    return gaussCoeffs;
                }

                public virtual void Update()
                {
                    Update(setSourceTexture);
                }

                public virtual void Update(Action<Material> updateFunc)
                {
                    Update(updateFunc, null);
                }
                
                public virtual void Update(Action<Material> updateFunc, RenderTexture destination)
                {
                    RenderTexture prevTarget = blurPassY.target;
                    if (destination != null)
                        blurPassY.target = destination;

                    coeffs = CreateGaussianCoefficients(sampleRAD, gaussSigma);
                    blurPassX.Update(
                        delegate (Material m)
                        {
                            if (updateFunc != null)
                            {
                                updateFunc(m);
                            }
                            m.SetFloatArray("_Coeffs", coeffs);
                            m.SetInt("_RAD", sampleRAD);
                            m.SetVector("_BlurDirection", new Vector4(1.0F / (float)resX, 0, 0, 0));
                        }
                    );

                    blurPassY.Update(
                        delegate (Material m)
                        {
                            m.SetTexture("_MainTex", blurPassX.GetTexture(0));
                            m.SetFloatArray("_Coeffs", coeffs);
                            m.SetInt("_RAD", sampleRAD);
                            m.SetVector("_BlurDirection", new Vector4(0, 1.0F / (float)resY, 0, 0));
                        }
                    );

                    if (destination != null)
                        blurPassY.target = prevTarget;
                }
                public virtual void ReleaseResources()
                {
                    if (blurPassX != null)
                    {
                        blurPassX.ReleaseResources();
                    }
                    if (blurPassY != null)
                    {
                        blurPassY.ReleaseResources();
                    }
                }
                public virtual Material GetMaterial()
                {
                    if (blurPassX != null)
                    {
                        return blurPassX.GetMaterial();
                    }
                    return null;
                }
                public Texture GetTexture(uint texNo)
                {
                    if (blurPassY != null)
                    {
                        return blurPassY.GetTexture(0);
                    }
                    return null;
                }
            }
        }
    }
}