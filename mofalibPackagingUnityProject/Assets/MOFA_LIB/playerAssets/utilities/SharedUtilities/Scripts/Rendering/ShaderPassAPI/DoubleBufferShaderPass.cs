﻿using UnityEngine;
using System;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderPassAPI
        {
            public class DoubleBufferShaderPass : ShaderPassInterface
            {
                private Shader shader;
                private Material internalMaterial;
                private RenderTexture bufferA;
                private RenderTexture bufferB;
                private bool isPair = false;
                Action<Material> updateAction = null;

                public DoubleBufferShaderPass(string shaderName, int resolutionX, int resolutionY, RenderTextureFormat fmt = RenderTextureFormat.ARGB32, Action<Material> updateFunc = null)
                {
                    updateAction = updateFunc;
                    shader = Shader.Find(shaderName);
                    internalMaterial = new Material(shader);
                    bufferA = new RenderTexture(resolutionX, resolutionY, 0, fmt);
                    bufferB = new RenderTexture(resolutionX, resolutionY, 0, fmt);
                }

                public DoubleBufferShaderPass(Shader externalShader, int resolutionX, int resolutionY, RenderTextureFormat fmt = RenderTextureFormat.ARGB32, Action<Material> updateFunc = null)
                {
                    updateAction = updateFunc;
                    shader = externalShader;
                    internalMaterial = new Material(shader);
                    bufferA = new RenderTexture(resolutionX, resolutionY, 0, fmt);
                    bufferB = new RenderTexture(resolutionX, resolutionY, 0, fmt);
                }

                virtual public void ReleaseResources()
                {
                    UnityEngine.Object.Destroy(internalMaterial);
                    UnityEngine.Object.Destroy(bufferA);
                    UnityEngine.Object.Destroy(bufferB);
                }

                virtual public Material GetMaterial()
                {
                    return internalMaterial;
                }

                virtual public void Update()
                {
                    Update(updateAction);
                }

                virtual public void Update(Action<Material> updateFunc)
                {
                    if (updateFunc != null)
                        updateFunc(internalMaterial);

                    isPair = !isPair;
                    internalMaterial.SetTexture("_SelfTex", inputTex());
                    internalMaterial.SetVector("_Resolution", new Vector4(bufferA.width, bufferB.height, 0));
                    Graphics.Blit(null, outputTex(), internalMaterial, 0);
                }

                virtual public Texture GetTexture(uint texNo)
                {
                    return outputTex();
                }

                private Texture inputTex()
                {
                    return isPair ? bufferA : bufferB;
                }

                private RenderTexture outputTex()
                {
                    return isPair ? bufferB : bufferA;
                }
            }
        }
    }
}
