using UnityEngine;
using System;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderPassAPI
        {
            //Usage Example:
            /*
                    float myShaderInputValue = 0.77;
                    Texture2D t = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Textures/texture.jpg", typeof(Texture2D));

                    shaderPassTest = new SimpleShaderPass("Unlit/MyShader", 256, 256, RenderTextureFormat.ARGB32, delegate (Material m)
                    {
                        m.SetTexture("_InputTexture", t);
                        m.SetFloat("_Intensity", myShaderInputValue);
                    });
            */
            public class SimpleShaderPass : ShaderPassInterface
            {
                private Shader shader;
                private Material internalMaterial;
                public RenderTexture target;
                Action<Material> updateAction;

                public SimpleShaderPass(string shaderName, int resolutionX, int resolutionY, RenderTextureFormat fmt = RenderTextureFormat.ARGB32, Action<Material> updateFunc = null)
                {
                    updateAction = updateFunc;
                    shader = Shader.Find(shaderName);
                    internalMaterial = new Material(shader);
                    target = new RenderTexture(resolutionX, resolutionY, 0, fmt);
                }

                public SimpleShaderPass(Shader externalShader, int resolutionX, int resolutionY, RenderTextureFormat fmt = RenderTextureFormat.ARGB32, Action<Material> updateFunc = null)
                {
                    updateAction = updateFunc;
                    shader = externalShader;
                    internalMaterial = new Material(shader);
                    target = new RenderTexture(resolutionX, resolutionY, 0, fmt);
                }

                virtual public void Update(/*For external RT TODO : RenderTexture = null*/)
                {
                    Update(updateAction);
                }

                virtual public void Update(Action<Material> updateFunc /*For external RT TODO : , RenderTexture = null*/)
                {
                    if (updateFunc != null)
                        updateFunc(internalMaterial);

                    internalMaterial.SetVector("_Resolution", new Vector4(target.width, target.height, 0));
                    Graphics.Blit(null, target, internalMaterial, 0);
                }

                virtual public Texture GetTexture(uint texNo)
                {
                    return target;
                }

                virtual public void ReleaseResources()
                {
                    UnityEngine.Object.Destroy(internalMaterial);
                    UnityEngine.Object.Destroy(target);
                }

                virtual public Material GetMaterial()
                {
                    return internalMaterial;
                }
            }
        }
    }
}
