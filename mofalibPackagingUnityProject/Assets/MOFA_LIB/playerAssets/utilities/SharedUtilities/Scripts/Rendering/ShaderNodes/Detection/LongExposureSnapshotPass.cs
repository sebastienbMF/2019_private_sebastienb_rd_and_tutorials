﻿using UnityEngine;
using System;
using mofalib.Rendering.ShaderPassAPI;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderNodes
        {
            public class LongExposureSnapshotPass : ShaderPassInterface
            {
                DampingPass dampingPass;
                
                public float SingleFrameWeight = 0.1F;
                public float ExposureTimeSec = 10.0F;
                public bool FreeMemoryOnSnapshot = true;
                private float startTimeSec = -1.0F;
                private bool snapshotTaken = false;
                private RenderTextureFormat snapshotFmt;
                private RenderTexture snapshotTexture = null;
                private Action<Material> updateFunc;
                private Action<Texture> snapshotTakenCbk;

                //Note : This shader reuses the damping pass, which can have other purposes.
                //Note : The process shader squeezes bit depth, its RenderTextureFormat should therefore be 
                //       be higher than the result format.
                public LongExposureSnapshotPass(int resX, int resY,
                                                RenderTextureFormat processFmt = RenderTextureFormat.R16,
                                                RenderTextureFormat resultFmt = RenderTextureFormat.R8,
                                                Action<Texture> snapshotTakenCallback = null,
                                                Action<Material> updateFuncDelegate = null)
                {
                    dampingPass = new DampingPass(resX, resY, processFmt);
                    dampingPass.SingleFrameWeight = SingleFrameWeight;
                    updateFunc = updateFuncDelegate;
                    snapshotTakenCbk = snapshotTakenCallback;
                    snapshotFmt = resultFmt;
                }

                public virtual void Update()
                {
                    Update(updateFunc);
                }
                
                public void takeSnapshot()
                {
                    Texture tSrc = dampingPass.GetTexture(0);

                    if (snapshotTexture == null)
                    {
                        snapshotTexture = new RenderTexture(tSrc.width, tSrc.height, 0, snapshotFmt);
                    }

                    Graphics.Blit(tSrc, snapshotTexture);
                    snapshotTaken = true;
                    
                    if(FreeMemoryOnSnapshot)
                    {
                        dampingPass = null;
                    }

                    snapshotTakenCbk(snapshotTexture);
                }

                public Material GetMaterial()
                {
                    //GetMaterial() should ne be called directly on this class, as the
                    //material can be released once the snapshot is taken.
                    //Use Update(Action<Material> updateFunc) to inject material parameters instead.
                    if (dampingPass != null)
                    {
                        return dampingPass.GetMaterial();
                    }
                    return null;
                }

                public void Update(Action<Material> updateFunc)
                {
                    if (snapshotTaken)
                    {
                        return;
                    }

                    //Initialize start time. Note: Better do it here, since we don't want to count the start time.
                    if (startTimeSec < 0.0)
                    {
                        startTimeSec = Time.time;
                    }

                    float elapsedTime = Time.time - startTimeSec;
                    if (elapsedTime < ExposureTimeSec)
                    {
                        //Keep exposing
                        dampingPass.Update(updateFunc);
                    }
                    
                    else
                    {
                        //Capture snapshot
                        takeSnapshot();
                    }
                }

                public Texture GetTexture(uint texNo)
                {
                    //Return wither final or intermediate result.
                    return snapshotTaken ? snapshotTexture : dampingPass.GetTexture(0);
                }

                public virtual void ReleaseResources()
                {
                    if (dampingPass != null)
                        dampingPass.ReleaseResources();
                    
                }
            }
        }
    }
}