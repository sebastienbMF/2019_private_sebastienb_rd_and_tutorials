﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mofalib
{
  namespace Rendering
  {
    public class CameraFullscreenBlit : MonoBehaviour
    {
      public GameObject TextureGenerator;

      private ITextureGenerator src;
      private Material m;

      void Start()
      {
        //m = new Material(new Shader("Unlit/TextureMultiply"));
        src = TextureGenerator.GetComponent<ITextureGenerator>();
      }

      void OnRenderImage(RenderTexture source, RenderTexture destination)
      {
        Graphics.Blit(src.GetTexture(0), destination);//, matDrawBuffers, 0);
      }
    }
  }
}