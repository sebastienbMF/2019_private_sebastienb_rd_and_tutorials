﻿using System;
using UnityEngine;
using mofalib.Rendering.ShaderPassAPI;

namespace mofalib
{
  namespace Rendering
  {
    namespace ShaderNodes
    {
      public class PropagateSimPass : DoubleBufferShaderPass
      {
        private int frameNo = 0;

        public PropagateSimPass(int resX, int resY, RenderTextureFormat fmt)
          : base("ShaderNodes/PropagateSimPass", resX, resY, fmt)
        {
            frameNo = 0;
        }

        override public void Update(Action<Material> updateFunc)
        {
            base.Update(delegate (Material m)
            {
                if(updateFunc!=null)
                    updateFunc(m);

				//m.SetTexture("_maskTex", )
                m.SetFloat("_FrameNo", ++frameNo);
            });
        }
      }
    }
  }
}