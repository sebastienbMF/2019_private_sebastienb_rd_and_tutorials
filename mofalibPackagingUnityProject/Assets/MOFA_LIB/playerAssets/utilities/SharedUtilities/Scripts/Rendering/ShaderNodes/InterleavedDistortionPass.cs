using UnityEngine;
using System;

namespace mofalib
{
    namespace Rendering
    {
        namespace ShaderPassAPI
        {
            public class InterleavedDistortionPass
            {
                private InkDispersionPass[] _inkDispersionPass;
                public int FrameCountUpdate;
                private SimpleShaderPass combinePass;
                private Texture initialTex;
                private Action<Material> updateFunc;

                public InterleavedDistortionPass(int frameCountUpdate, Texture initial, RenderTextureFormat fmt = RenderTextureFormat.ARGB32, Action<Material> updateFunction = null)
                {
                    updateFunc = updateFunction;
                    initialTex = initial;
                    combinePass = new SimpleShaderPass("Filters/Combine3", Screen.width, Screen.height);
                    FrameCountUpdate = frameCountUpdate;
                    _inkDispersionPass = new InkDispersionPass[3];
                    for (int i = 0; i < _inkDispersionPass.Length; ++i)
                    {
                        _inkDispersionPass[i] = new InkDispersionPass(Screen.width, Screen.height, fmt);
                        _inkDispersionPass[i].setInitialTexture(initial);
                    }
                }

                public void ReleaseResources()
                {
                    for (int i = 0; i < _inkDispersionPass.Length; ++i)
                    {
                        _inkDispersionPass[i].ReleaseResources();
                    }
                }

                public Texture GetTexture(uint texNo)
                {
                    return combinePass.GetTexture(0);
                }

                public float GetProgress()
                {
                    float progress = (float)((Time.frameCount % (FrameCountUpdate / 2)) / (float)(FrameCountUpdate / 2 - 1));

                    if ((Time.frameCount) % (FrameCountUpdate) >= (FrameCountUpdate / 2))
                    {
                        progress = 1.0f - progress;
                    }
                    Debug.Log(progress);
                    return progress;
                }

                private Vector3 GetWeights()
                {
                    Vector3 result = new Vector3(0.0f, 0.0f, 0.0f);
                    float progress = (float)(((Time.frameCount + 0 * FrameCountUpdate / 3) % (FrameCountUpdate / 2)) / (float)(FrameCountUpdate / 2 - 1));

                    if ((Time.frameCount + 0 * FrameCountUpdate / 3) % (FrameCountUpdate) >= (FrameCountUpdate / 2))
                    {
                        progress = 1.0f - progress;
                    }
                    result.x = progress;

                    progress = (float)(((Time.frameCount + 1 * FrameCountUpdate / 3) % (FrameCountUpdate / 2)) / (float)(FrameCountUpdate / 2 - 1));

                    if ((Time.frameCount + 1 * FrameCountUpdate / 3) % (FrameCountUpdate) >= (FrameCountUpdate / 2))
                    {
                        progress = 1.0f - progress;
                    }
                    result.y = progress;

                    progress = (float)(((Time.frameCount + 2 * FrameCountUpdate / 3) % (FrameCountUpdate / 2)) / (float)(FrameCountUpdate / 2 - 1));

                    if ((Time.frameCount + 2 * FrameCountUpdate / 3) % (FrameCountUpdate) >= (FrameCountUpdate / 2))
                    {
                        progress = 1.0f - progress;
                    }
                    result.z = progress;

                    return result;
                }

                public void Update()
                {
                    Update(updateFunc);
                }

                //Texture velocity, Vector2 forceOrigin, float forceExponent
                public void Update(Action<Material> updateFunction)
                {
                    for (int i = 0; i < _inkDispersionPass.Length; ++i)
                    {
                        _inkDispersionPass[i].Update(updateFunction); 
                    }

                    if (Time.frameCount % FrameCountUpdate == 0)
                    {
                        _inkDispersionPass[0].setInitialTexture(initialTex);
                    }
                    if ((Time.frameCount + FrameCountUpdate / 3) % FrameCountUpdate == 0)
                    {
                        _inkDispersionPass[1].setInitialTexture(initialTex);
                    }
                    if ((Time.frameCount + 2 * FrameCountUpdate / 3) % FrameCountUpdate == 0)
                    {
                        _inkDispersionPass[2].setInitialTexture(initialTex);
                    }

                    Vector3 w = GetWeights();

                    combinePass.Update(delegate (Material m)
                    {
                        m.SetTexture("_t0", _inkDispersionPass[0].GetTexture(0));
                        m.SetTexture("_t1", _inkDispersionPass[1].GetTexture(0));
                        m.SetTexture("_t2", _inkDispersionPass[2].GetTexture(0));
                        m.SetVector("_TexWeights", w);
                    });
                }
            }
        }
    }
}
