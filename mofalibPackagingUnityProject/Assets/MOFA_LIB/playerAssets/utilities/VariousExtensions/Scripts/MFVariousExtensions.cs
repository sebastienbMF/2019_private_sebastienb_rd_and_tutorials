﻿using System;
using System.IO;
using UnityEngine;


public static class MFVariousExtensions
{

#region jsonLoadSave

    public static T LoadJson<T>(this T myParam, string filename)
    {
        string json = File.ReadAllText(Application.streamingAssetsPath + "/"+ filename);
        T config = JsonUtility.FromJson<T>(json);
        return config;
    }

    public static void SaveJson<T>(this T param, string filename)
    {
        string path = Application.streamingAssetsPath + "/" + filename;
        string json = JsonUtility.ToJson(param, true);
        File.WriteAllText(path,json);
    }
#endregion

#region parsing

    // try to convert a string to a float, handles parsing error by returning the value used before
    public static float Param_ParseToFloat(this string text, float initVal)
    {
        float f = initVal;
        try
        {
            f = Convert.ToSingle(text);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log("Ui Error: " + e);
        }
        return f;
    }

    // try to convert a string to an int, handles parsing error by returning the value used before
    public static int Param_ParseToInt(this string text, int initVal)
    {
        int i = initVal;
        try
        {
            i = Convert.ToInt32(text);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log("Ui Error: " + e);
        }
        return i;
    }
#endregion

   
#region gradient

    // Converts all the keys in a gradient to a color array
    public static Color32[] KeycolToColorArray(this Gradient gradient)
    {
        Color32[] cols = new Color32[gradient.colorKeys.Length];
        for (int i = 0; i < cols.Length; i++)
        {
            cols[i] = gradient.colorKeys[i].color;
        }
        return cols;
    }

    // convert all the gradient time keys to a float array
    public static float[] KeytimeToFloatArray(this Gradient gradient)
    {
        float[] pos = new float[gradient.colorKeys.Length];
        for (int i = 0; i < pos.Length; i++)
        {
            pos[i] = gradient.colorKeys[i].time;
        }
        return pos;
    }
#endregion

#region other
    // normalize a value
    public static float Normalize(this float val, float min, float max)
    {
        float n = (val - min) / (max - min);
        return n;
    }
#endregion

}
