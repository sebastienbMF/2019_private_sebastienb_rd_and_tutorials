﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using MofaLib;

public class Protov3TCPController : MonoBehaviour
{
    public CanvasGroup CanvasGroup = null;
    public AsyncTCPClient TcpClient = null;
    public string GameSceneName;

    [DllImport("user32.dll")]
    private static extern IntPtr GetActiveWindow();
  
    [DllImport("user32.dll")]
    private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    private const int SW_HIDE = 0;
    private const int SW_SHOW = 5;

    private const float FADE_SECS = 2.0f;

    private IntPtr hwnd;

    private Coroutine fadeCoroutine;

    void Start()
    {
        if (TcpClient != null)
        {
            TcpClient.OnTcpMessageReceived += ParseMessage;
        }
        else
        {
            Debug.Log("TCPClient is null, world will not be controllable by the protoframework");
        }

        if (CanvasGroup == null)
        {
            Debug.Log("CanvasGroup is null, fading will not work");
        }

        hwnd = GetActiveWindow();
        if (!Application.isEditor)
        {
            if (CanvasGroup != null)
            {
                CanvasGroup.alpha = 1.0f;
            }
            ShowWindow(hwnd, SW_HIDE);
        }

        StartCoroutine(LoadScene());
    }

    private void OnDestroy()
    {
        if (TcpClient != null)
        {
            TcpClient.OnTcpMessageReceived -= ParseMessage;
        }
    }


    private void ParseMessage(string message)
    {
        Debug.Log("handling message: " + message);

        if (message.StartsWith("STARTGAME"))
        {
            if (!Application.isEditor)
            {
                ShowWindow(hwnd, SW_SHOW);
            }
            if (fadeCoroutine != null)
            {
                StopCoroutine(fadeCoroutine);
            }
            fadeCoroutine = StartCoroutine(FadeIn());
        }
        else if (message.StartsWith("STOPGAME"))
        {
            if (fadeCoroutine != null)
            {
                StopCoroutine(fadeCoroutine);
            }
            fadeCoroutine = StartCoroutine(FadeOut());
        }
        else
        { 
            Debug.Log("unknown message: " + message);
        }
    }

    private IEnumerator FadeIn()
    {
        if (CanvasGroup != null)
        {
            while (CanvasGroup.alpha > 0.0f)
            {
                CanvasGroup.alpha -= Time.deltaTime / FADE_SECS;
                yield return null;
            }
            CanvasGroup.alpha = 0.0f;
        }
        TcpClient.TCPSend("GAMESTARTED");
        yield return null;
    }

    private IEnumerator FadeOut()
    {
        if (CanvasGroup != null)
        {
            while (CanvasGroup.alpha < 1.0f)
            {
                CanvasGroup.alpha += Time.deltaTime / FADE_SECS;
                yield return null;
            }
            CanvasGroup.alpha = 1.0f;
        }
        TcpClient.TCPSend("GAMESTOPPED");
        yield return null;
    }

    IEnumerator LoadScene()
    {
        yield return null;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(GameSceneName, LoadSceneMode.Additive);
        while (!asyncOperation.isDone)
        {
            Debug.Log("Loading progress: " + (asyncOperation.progress * 100) + "%");
            if (TcpClient != null)
            {
                TcpClient.TCPSend("LOADPROGRESS;" + (int)(asyncOperation.progress * 100000.0));
            }
            yield return null;
        }

        Debug.Log("Loading Done");
        if (TcpClient != null)
        {
            TcpClient.TCPSend("LOADINGDONE");
        }
        yield return null;
    }
}