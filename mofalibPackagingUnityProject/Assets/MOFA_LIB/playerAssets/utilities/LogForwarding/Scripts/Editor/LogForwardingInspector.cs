﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MofaLib;

[CustomEditor(typeof(LogForwarding), true)]
public class LogForwardingInspector : Editor
{
    public override void OnInspectorGUI()
	{
		LogForwarding myTarget = (LogForwarding)target;
		
		DrawDefaultInspector();
		
		GUILayout.Space(20);
		if(GUILayout.Button("Save Config"))
        {
            myTarget.SaveConfig();
        }
		if(GUILayout.Button("Load Config"))
        {
            myTarget.LoadConfig();
        }
	}
}
