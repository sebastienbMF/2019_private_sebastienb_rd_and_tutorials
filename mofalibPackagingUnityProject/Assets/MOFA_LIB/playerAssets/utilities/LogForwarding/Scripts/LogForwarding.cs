﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Globalization;
using UnityEngine;
using System.Collections;

namespace MofaLib
{
    public class LogForwarding : MonoBehaviour 
    {
        public LogJson Log;
        public LogForwardingConfig Config;
        
        private UdpClient client;

        const string configFileName = "LogForwardingConfig.json";

        public virtual void Start()
        {
            LoadConfig();
            Log = new LogJson(GetDateTimeIso8601(), "info", Environment.MachineName, "log forwarding active");
            CreateUdpClient();
            
        }

        void OnEnable()
        {
            Application.logMessageReceived += HandleLog;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }
      
        void CreateUdpClient()
        {
            client = new UdpClient(Config.remoteHost, Config.remotePort);
            Debug.Log("Log forwarding active!");
        }

        void HandleLog(string logString, string stackTrace, LogType type)
        {
            string level = LogtypeConverter(type);
            string time = GetDateTimeIso8601();
            Log.UpdateMessage(time, level, logString);
            SendLog();
        }

        void SendLog()
        {
            string serializedMessage = JsonUtility.ToJson(Log);
            Byte[] jsonUdp = Encoding.ASCII.GetBytes(serializedMessage);
            client.Send(jsonUdp, jsonUdp.Length);
        }

        string GetDateTimeIso8601()
        {
            return DateTime.Now.ToString("o", CultureInfo.InvariantCulture);
        }

        string LogtypeConverter(LogType type)
        {
            string level = "info";
            if(type == LogType.Warning)
            {
                return "warning";
            }
            if(type == LogType.Error)
            {
                return "error";
            }
            if(type == LogType.Exception)
            {
                return "error";
            }

            return level;
        }


        public void SaveConfig()
        {
            Config.SaveJson(configFileName);
        }

        public void LoadConfig()
        {
            Config = Config.LoadJson(configFileName);
        }
    }

    [Serializable]
    public class LogJson
    {
        public string time;
        public string level;
        public string application = "unity";
        public string hostname;
        public string user = "n/a";
        public string platform = "windows 10";
        public string MFNotifMessage;

        public LogJson(string time, string level, string hostName, string MFNotifMessage)
        {
            this.time = time;
            this.level = level;
            this.hostname = hostName;
            this.MFNotifMessage = MFNotifMessage;
        }

        public void UpdateMessage(string time, string level, string MFNotifMessage)
        {
            this.time = time;
            this.level = level;
            this.MFNotifMessage = MFNotifMessage;
        }
    }

    [Serializable]
    public class LogForwardingConfig
    {
        public string remoteHost;
        public int remotePort;
    }
}
