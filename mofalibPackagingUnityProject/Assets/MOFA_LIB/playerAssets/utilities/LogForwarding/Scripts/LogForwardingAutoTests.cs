﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MofaLib;

public class LogForwardingAutoTests : LogForwarding 
{
    [Header ("For Status Feedback")]
    public Renderer NiceCube;
    public bool autoLogEnabled;


    public override void Start()
    {
        base.Start();
        NiceCube.material.color = Color.red;
        if(autoLogEnabled)
        {
            StartCoroutine(AutoLogCoroutine());
        }
    }


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(autoLogEnabled)
            {
                StopAutoLog();
                return;
            }
                StartAutoLog();
        }
    }

    public void StartAutoLog()
    {
        StartCoroutine(AutoLogCoroutine());
    }

    public void StopAutoLog()
    {
        NiceCube.material.color = Color.red;
        autoLogEnabled = false;
    }

    IEnumerator AutoLogCoroutine()
    {
        autoLogEnabled = true;
        NiceCube.material.color = Color.green;
        while(autoLogEnabled)
        {
            Debug.Log("Don't worry, i'm a simple message!");
            yield return new WaitForSeconds(1);
            Debug.LogWarning("I'm a warning, you should be a bit worried...");
            yield return new WaitForSeconds(1);
            Debug.LogError("I'm a really mean error, you should be scared!!!");
            yield return new WaitForSeconds(1);
        }
    }
	
}
