﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class ExportableScriptableObject : ScriptableObject 
{
	[HideInInspector]
	public string path;

	public virtual void ExportJson<T>(T scriptableO)
	{
		if(path == "")
		{
			Debug.Log("No File name to save : " + this.name);
			return;
		}

        string json = JsonUtility.ToJson(scriptableO, true);
		Directory.CreateDirectory(GetFolderPath());
        File.WriteAllText(path, json);
	}

	public virtual void ImportJson()
	{
		var xso =  this;
		if(path == "")
		{
			Debug.Log("No File name to load : " + this.name);
			return;
		}

		string json = File.ReadAllText(path);
        JsonUtility.FromJsonOverwrite(json, xso);
	}

	public string GetFolderPath()
	{
		if(path == "" || path == null)
		{
			Directory.CreateDirectory(Application.streamingAssetsPath);
			return Application.streamingAssetsPath;
		}

		if(!File.Exists(path))
		{
			Directory.CreateDirectory(Application.streamingAssetsPath);
			return Application.streamingAssetsPath;
		}
		
		string[] pathSplit = path.Split('/');
		string fileName = pathSplit[pathSplit.Length - 1];
		string folderPath = path.Replace("/" + fileName, "");
		return folderPath;
	}

	public string GetFileName()
	{
		if(path == "" || path == null)
		{
			return null;
		}
		
		string[] pathSplit = path.Split('/');
		string fileName = pathSplit[pathSplit.Length - 1];
		return fileName;
	}

}
