﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;

[CustomEditor(typeof(ExportableScriptableObject), true)]
public class ExportableScriptableObjectEditor : Editor {

	public override void OnInspectorGUI()
	{
		ExportableScriptableObject myTarget = (ExportableScriptableObject)target;

		if(GUILayout.Button("Export"))
		{
			myTarget.path =  EditorUtility.SaveFilePanel("Export Preset", myTarget.GetFolderPath(), "Settings", "json");
			myTarget.ExportJson(myTarget);
		}

		if(GUILayout.Button("Import"))
		{
			myTarget.path =  EditorUtility.OpenFilePanel("Import Presets", myTarget.GetFolderPath(), "json");
			if(myTarget.path != "" && myTarget.path.Contains("json"))
			{
				myTarget.ImportJson();
			}
		}

		GUILayout.Space(10);
		if(GUILayout.Button("Overwrite"))
		{
			string title = "Overwrite Settings";
			if(myTarget.GetFileName() == null)
			{
				if(EditorUtility.DisplayDialog(title, "File Do Not Exists Yet!" , "Ok"))
				{
					return;
				}
			}

			if(!File.Exists(myTarget.path))
			{
				if(EditorUtility.DisplayDialog(title, "File Do Not Exists Yet!" , "Ok"))
				{
					return;
				}
			}

			if(EditorUtility.DisplayDialog(title, "Are you sure you want to OVERWRITE " + myTarget.GetFileName() + "?", "Yes", "No"))
			{
				myTarget.ExportJson(myTarget);
			}
		}

		if(GUILayout.Button("Reload"))
		{
			string title = "Reload Settings";
			if(myTarget.GetFileName() == null)
			{
				if(EditorUtility.DisplayDialog(title, "File Do Not Exists Yet!" , "Ok"))
				{
					return;
				}
			}

			if(!File.Exists(myTarget.path))
			{
				if(EditorUtility.DisplayDialog(title, "File Do Not Exists Yet!" , "Ok"))
				{
					return;
				}
			}

			if(EditorUtility.DisplayDialog(title, "Are you sure you want to RELOAD " + myTarget.GetFileName() + "? You will LOSE current settings!", "Yes", "No"))
			{
				myTarget.ImportJson();
			}
		}
		GUILayout.Space(5);
		GUILayout.Label("File name: " + myTarget.GetFileName());

		GUILayout.Label("----------------------------------");

		DrawDefaultInspector();
	}
}

