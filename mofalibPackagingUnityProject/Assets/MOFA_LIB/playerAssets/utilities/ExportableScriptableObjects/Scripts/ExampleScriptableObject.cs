﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ExampleSO", menuName = "ScriptableObjects/Test Serializable")]
[System.Serializable]
public class ExampleScriptableObject : ExportableScriptableObject
{
	public int Number;
	public float Size;
	public Color Col;
}
