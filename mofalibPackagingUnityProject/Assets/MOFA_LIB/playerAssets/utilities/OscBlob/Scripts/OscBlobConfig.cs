﻿using System;

namespace MofaLib.OscBlob
{
    [Serializable]
    public class OscBlobConfig
    {
        public int Port;
        public string MessageAddress = "/scanblob/unity";
        
        public float XValueScale;
        public float YValueScale;
        public float BlobLife;
        public float MinBlobSize;
        public float BlobSmoothing;

        public bool DebugOscMessages;

        public bool FlipX;
        public bool FlipY;
        public bool SwapXY;

        public bool IsWall;
        public bool ShowBlob;

        public bool ClickMode;
        public float ClickDelay;
        public float ClickPositionThreshold;
    }
}
