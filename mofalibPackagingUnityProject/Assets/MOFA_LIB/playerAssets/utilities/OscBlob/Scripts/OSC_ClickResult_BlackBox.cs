﻿using UnityEngine;
using System.Collections;

public class OSC_ClickResult_BlackBox : OSC_ClickResult 
{

	void Start()
	{
		StartCoroutine (LoadTexture ());
	}


	public override void Click (int blobId)
	{

		base.Click (blobId);
		StartCoroutine (CloseApplication ());

	}

	IEnumerator CloseApplication()
	{
		Vector3 scale = transform.localScale;
		Vector3 gotoScale = scale * 1.3f;
		for (float t = 0; t <= 1; t += Time.deltaTime * 10)
		{
			Vector3 newScale = Vector3.Lerp (scale, gotoScale, t);
			transform.localScale = newScale;
			yield return null;
		}
		transform.localScale = gotoScale;
		for (float t = 0; t <= 1; t += Time.deltaTime * 10)
		{
			Vector3 newScale = Vector3.Lerp (gotoScale, scale, t);
			transform.localScale = newScale;
			yield return null;
		}
		transform.localScale = scale;
		Application.Quit ();
	}


	IEnumerator LoadTexture()
	{
		string path = "file://" + Application.streamingAssetsPath + @"/BlackBox_BackButton.png";
		Debug.Log("Streaming assets path: " + path);
		WWW www = new WWW (path);
		yield return www;
		Texture2D buttonTexture = new Texture2D (512,512,TextureFormat.ARGB32,false);
		www.LoadImageIntoTexture (buttonTexture);
		GetComponent<Renderer> ().material.mainTexture = buttonTexture;
	}
}
