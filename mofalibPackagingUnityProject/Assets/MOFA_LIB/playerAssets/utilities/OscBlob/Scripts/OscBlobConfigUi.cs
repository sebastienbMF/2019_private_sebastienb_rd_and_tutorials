﻿using System;
using UnityEngine;
using UnityEngine.UI;
using MofaLib.OscBlob;

public class OscBlobConfigUi : MonoBehaviour 
{
    public GameObject MenuAnchor;
    public OscBlobReceiver BlobReceiver;
    
	private OscBlobConfig config;

    // UI Components
    public Toggle ShowMessages;
    public InputField Port;
    public InputField MessageAddress;
    public InputField XScale;
    public InputField YScale;
    public InputField BlobLife;
    public InputField MinBlobSize;
    public InputField BlobSmoothing;
    public Toggle FlipX;
    public Toggle FlipY;
    public Toggle SwapXY;
    public Toggle IsWall;
    public Toggle ShowBlob;
    public Toggle ClickMode;
    public InputField ClickDelay;
    public InputField ClickPosThreshold;

    const int uiMovementSpeed = 5;
    
	// Update is called once per frame
	void Update () 
    {
		if(Input.GetKey(KeyCode.LeftShift))
        {
            if(Input.GetKeyDown(KeyCode.C))
            {
                if(MenuAnchor.activeSelf)
                {
                    HideConfigMenu();
                }
                else
                {
                    ShowConfigMenu();
                }
            }

            if(Input.GetKey(KeyCode.LeftArrow))
            {
               if(MenuAnchor.activeSelf)
               {
                   MenuAnchor.transform.position += new Vector3(-uiMovementSpeed,0,0);
               }
            }

            if(Input.GetKey(KeyCode.RightArrow))
            {
               if(MenuAnchor.activeSelf)
               {
                   MenuAnchor.transform.position += new Vector3(uiMovementSpeed,0,0);
               }
            }

            if(Input.GetKey(KeyCode.UpArrow))
            {
               if(MenuAnchor.activeSelf)
               {
                   MenuAnchor.transform.position += new Vector3(0,uiMovementSpeed,0);
               }
            }

            if(Input.GetKey(KeyCode.DownArrow))
            {
               if(MenuAnchor.activeSelf)
               {
                   MenuAnchor.transform.position += new Vector3(0,-uiMovementSpeed,0);
               }
            }
        }
	}


    public void SetPort(string input)
    {
        config.Port = Convert.ToInt32(input);
    }

    public void SetMessageAddress(string input)
    {
        config.MessageAddress = input;
    }

    public void SetXValueScale(string input)
    {
        config.XValueScale = Convert.ToSingle(input);
    }

    public void SetYValueScale(string input)
    {
        config.YValueScale = Convert.ToSingle(input);
    }

    public void SetBlobLife(string input)
    {
        config.BlobLife = Convert.ToSingle(input);
    }

    public void SetMinBlobSize(string input)
    {
        config.MinBlobSize = Convert.ToSingle(input);
    }

    public void SetBlobSmoothing(string input)
    {
        config.BlobSmoothing = Convert.ToSingle(input);
    }

    public void SetClickDelay(string input)
    {
        config.ClickDelay = Convert.ToSingle(input);
    }

    public void SetClickPosThreshold(string input)
    {
        config.ClickPositionThreshold = Convert.ToSingle(input);
    }

    public void SetShowOscMessages(bool b)
    {
        config.DebugOscMessages = b;
    }

    public void SetFlipX(bool b)
    {
        config.FlipX = b;
    }

    public void SetFlipY(bool b)
    {
        config.FlipY = b;
    }

    public void SetSwapXY(bool b)
    {
        config.SwapXY = b;
    }

    public void SetIsWall(bool b)
    {
        config.IsWall = b;
    }

    public void SetShowBlob(bool b)
    {
        config.ShowBlob = b;
    }

    public void SetClickMode(bool b)
    {
        config.ClickMode = b;
    }

    public void SaveConfig()
    {
       BlobReceiver.SaveConfig(config);
    }

    void ShowConfigMenu()
    {
        config = BlobReceiver.Config;
        MenuAnchor.SetActive(true);
        ShowMessages.isOn = config.DebugOscMessages;
        Port.text = config.Port.ToString();
        MessageAddress.text = config.MessageAddress;
        XScale.text = config.XValueScale.ToString();
        YScale.text = config.YValueScale.ToString();
        BlobLife.text = config.BlobLife.ToString();
        MinBlobSize.text = config.MinBlobSize.ToString();
        BlobSmoothing.text = config.BlobSmoothing.ToString();
        FlipX.isOn = config.FlipX;
        FlipY.isOn = config.FlipY;
        SwapXY.isOn = config.SwapXY;
        IsWall.isOn = config.IsWall;
        ShowBlob.isOn = config.ShowBlob;
        ClickMode.isOn = config.ClickMode;
        ClickDelay.text = config.ClickDelay.ToString();
        ClickPosThreshold.text = config.ClickPositionThreshold.ToString();
    }

    void HideConfigMenu()
    {
        MenuAnchor.SetActive(false);
    }
}
