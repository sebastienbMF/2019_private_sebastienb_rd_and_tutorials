﻿using UnityEngine;
using System.Collections.Generic;
using System;
using OSC.NET;

namespace MofaLib.OscBlob
{
    [RequireComponent(typeof(OscBlobReceiver))]
    public class OscBlobManager : MonoBehaviour {


        public LayerMask Layermask;
        public GameObject BlobPrefab;
        public OscBlobZone Zone;
        public List<GameObject> BlobList = new List<GameObject>();
        public float ClickDelay = 5f;
        public float ClickMaxDistance = 0.3f;
        public Camera RayCamera;

        [HideInInspector]
        public bool ClickMode;
        [HideInInspector]
        public bool FilterSize;
        [HideInInspector]
        public float MinBlobSize =  0;

        private OscBlobReceiver BlobReceiver;


        void Start()
        {
            if(!RayCamera)
            {
                RayCamera = Camera.main;
            }
            Zone = GetComponent<OscBlobZone> ();
            BlobReceiver = GetComponent<OscBlobReceiver>();
            BlobReceiver.OnOscBlobMessageReceived += OSCMessageReceived;
        }

        void OnDisable()
        {
            BlobReceiver.OnOscBlobMessageReceived -= OSCMessageReceived;
        }

        public void OSCMessageReceived(OSCMessage message)
        {
            if(ValidateMessage(message))
            {
                ManageBlob(message);
            }
        }

        void ManageBlob(OSCMessage message)
        {
            string blobId = message.Values [0].ToString ();

            if (Convert.ToSingle (blobId) >= 0) 
            {
                Vector2 blobPos = new Vector2 (Convert.ToSingle(message.Values [1]), Convert.ToSingle(message.Values [2]));

                if(!BlobList.Exists (x => x.GetComponent<OscBlob> ().Id == blobId))
                {
                    if(!FilterSize)
                    {
                        CreateNewBlob(blobId, blobPos);
                    }
                    else if(ValidateSize(Convert.ToSingle(message.Values[3]),Convert.ToSingle(message.Values[4])))
                    {
                        CreateNewBlob(blobId, blobPos);
                    }
                }
                
                // if blob exists, pass values
                if (BlobList.Exists (x => x.GetComponent<OscBlob> ().Id == blobId)) 
                {
                    int index = BlobList.FindIndex (x => x.GetComponent<OscBlob> ().Id == blobId);
                    GameObject blob = BlobList [index];
                    blob.GetComponent<OscBlob> ().Move (blobPos);
                }
            }
        }

        bool ValidateSize(float width, float height)
        {
            if(width * height >= MinBlobSize)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool ValidateMessage(OSCMessage message)
        {
            bool valid = false;
            for(int i = 0; i< message.Values.Count; i++)
            {
                if(message.Values[i] != null)
                {
                    valid = true;
                }
                else
                {
                    valid = false;
                    break;
                }
            }
            return valid;
        }

        void CreateNewBlob(string blobId, Vector2 blobPos)
        {
            GameObject newBlob = Instantiate (BlobPrefab) as GameObject;
            newBlob.name = "Blob_" + blobId;
            OscBlob blobScript = newBlob.GetComponent<OscBlob>();
            blobScript.Id = blobId;
            blobScript.BlobManager = GetComponent<OscBlobManager>();
            blobScript.Zone = Zone;
            blobScript.LifeTime = Zone.Config.BlobLife;
            blobScript.LayerMask = Layermask.value;
            blobScript.ShowBlob = Zone.Config.ShowBlob;
            blobScript.RayCamera = RayCamera;
            blobScript.Move (blobPos);
            blobScript.Smooth = Zone.Config.BlobSmoothing;
            BlobList.Add(newBlob);

            if(ClickMode)
            {
                blobScript.BlobClick(ClickDelay);
            }
            else
            blobScript.ShowTheBlob();
        }

        void Click(RaycastHit hit, int blobId)
        {
            //Debug.Log ("Hitted Object: " + hit.transform.gameObject.name);
            if(hit.transform.gameObject.GetComponent<OSC_ClickResult>()!= null)
            {
                hit.transform.gameObject.GetComponent<OSC_ClickResult>().Click(blobId);
            }
        }
                
        public void RemoveBlobFromList(string blobId)
        {
            int index = BlobList.FindIndex(x => x.GetComponent<OscBlob>().Id == blobId);
            BlobList.RemoveAt(index);
        }

        bool ConvertObjectToBool (object o)
        {
            if (Convert.ToInt32(o) == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        Vector2 StringToVector2(string x, string y)
        {
            return new Vector2 (Convert.ToSingle (x), Convert.ToSingle (y));
        }
        
        Vector3 ConvertObjectVectorThree (object ox, object oy, object oz)
        {
            Vector3 newVector = new Vector3((float)ox,(float)oy,(float)oz);
            return newVector;
        }
        
        float ConvertObjectToFloat(object o)
        {
            return (float)o;
        }
    }
}
