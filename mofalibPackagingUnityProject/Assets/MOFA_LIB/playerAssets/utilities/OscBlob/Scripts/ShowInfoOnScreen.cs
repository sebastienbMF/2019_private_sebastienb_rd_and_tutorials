﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MofaLib.OscBlob
{
    public class ShowInfoOnScreen : MonoBehaviour {

        public GameObject BackgroundImage;
        public Text TextField;

        void Start()
        {
            HideInfo();
        }

        public void HideInfo()
        {
            BackgroundImage.SetActive(false);
            TextField.text = "";
        }

        public void DisplayInfo(string textInfo, float hideDelay)
        {
            TextField.text = textInfo;
            BackgroundImage.SetActive(true);
            if(hideDelay > 0)
            {
                StartCoroutine(DelayedHide(hideDelay));
            }
        }

        IEnumerator DelayedHide(float hideDelay)
        {
            yield return new WaitForSeconds(hideDelay);
            HideInfo();
        }
    }
}