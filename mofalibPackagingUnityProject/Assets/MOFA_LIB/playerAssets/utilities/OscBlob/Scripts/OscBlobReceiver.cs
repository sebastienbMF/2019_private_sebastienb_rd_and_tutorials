﻿using UnityEngine;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using OSC.NET;

namespace MofaLib.OscBlob
{
    public class OscBlobReceiver : MonoBehaviour 
    {
        public OscBlobConfig Config;

        [Header("Debug and status")]
        public ShowInfoOnScreen OscMessageText;
        public ShowInfoOnScreen ConnectStatusMessageTxt;
        public ShowInfoOnScreen DebugMessageTxt;

        private OSCReceiver receiver;
        private Thread thread;
        private string lastOscMessage;
        private string debugMessage;
        private string connectMessage = "";
        
        private bool connected = false;
        public bool Connected {get { return connected;} }

        public delegate void OscBlobMessageReceived(OSCMessage message);
        public OscBlobMessageReceived OnOscBlobMessageReceived;

        public delegate void ConfigRefreshedDelegate(OscBlobConfig config);
        public ConfigRefreshedDelegate OnConfigRefreshed;
        
        private List<OSCMessage> messageQueue = new List<OSCMessage>();
        private const string configFileName = "OscBlobConfig.json";

        // Use this for initialization
        public void Start() 
        {
            StartCoroutine(Init());
        }

        void OnDisable()
        {
            Disconnect();
        }

        public IEnumerator Init()
        {
            yield return new WaitForSeconds(0.5f);
            LoadConfig();
            Connect();
        }

        public void LoadConfig()
        {
            Config = Config.LoadJson(configFileName);
            if(OnConfigRefreshed != null)
            {
                OnConfigRefreshed(Config);
            }
        }

        public void SaveConfig()
        {
            Config.SaveJson(configFileName);
            if(OnConfigRefreshed != null)
            {
                OnConfigRefreshed(Config);
            }
        }

        public void SaveConfig(OscBlobConfig newConfig)
        {
            Config = newConfig;
            Config.SaveJson(configFileName);
            if(OnConfigRefreshed != null)
            {
                OnConfigRefreshed(Config);
            }
        }

        public void Connect()
        {
            var port = Config.Port;
            try 
            {
                    this.connected = true;
                    receiver = new OSCReceiver(port);
                    this.thread = new Thread(new ThreadStart(Listen));
                    this.thread.Start();
                    Debug.Log("OSC reciever connected to port " + port );
                    this.connectMessage = "OSC reciever connected to port " + port;
            }
            catch (Exception e)
            {
                Debug.Log("Failed to connect to port " + port);
                this.connectMessage = "Failed to connect to port "+ port;
                Debug.Log(e.Message);
            }
        }
        
        // Update is called once per frame
        void Update () 
        {

            //processMessages has to be called on the main thread
            //so we used a shared proccessQueue full of OSC Messages
            this.debugMessage = "";
            lock(this.messageQueue)
            {
                foreach(OSCMessage message in this.messageQueue)
                {
                    if(OnOscBlobMessageReceived != null && message !=null)
                    {
                        if (message.Address == (Config.MessageAddress))
                        {
                            OnOscBlobMessageReceived(message);
                        }
                        else
                        {
                            this.debugMessage = "Wrong Adress!";
                        }
                    }
                }
                this.messageQueue.Clear();
            }

            ShowInfos();
        }

        void ShowInfos()
        {
            if(Config.DebugOscMessages)
            {
                if(this.debugMessage != null && this.debugMessage != "")
                {
                    DebugMessageTxt.DisplayInfo("OSC_Warning:  " + this.debugMessage, 0);
                }

                if( this.lastOscMessage != null)
                {
                    OscMessageText.DisplayInfo("OSC_DEBUG:  " + this.lastOscMessage, 0);
                }
            }
            else
            {
                OscMessageText.HideInfo();  
                DebugMessageTxt.HideInfo();
                this.debugMessage = "";
            }

            if(this.connectMessage != "")
            {

                if(this.connectMessage == "OSC reciever connected to port " + Config.Port)
                {
                    ConnectStatusMessageTxt.DisplayInfo(connectMessage, 5);
                }
                else
                {
                    ConnectStatusMessageTxt.DisplayInfo("Failure to connect  " + this.connectMessage, 5);
                }
                this.connectMessage = "";
            }
        }

        string ParseMessage(OSCMessage message)
        {
            string output = message.Address + " : ";
            for (int i = 0; i< message.Values.Count; i++)
            {
                try
                {
                    output += message.Values[i].ToString() + " : ";
                }
                catch(Exception e)
                {
                    Debug.Log(e);
                }
            }
            //_messageCount += 1;
            return output + " "; //+ _messageCount;
        }
        
        public void Disconnect()
        {
            if (receiver!=null)
            {
                receiver.Close();
            }
            
            receiver = null;
            connected = false;
            Debug.Log("Disconnected");
        }
        
        private void Listen() 
        {
            while(connected)
            {
                try 
                {
                    OSCPacket packet = receiver.Receive();
                    if(packet == null)
                    {
                        Debug.LogWarning("null packet");
                        return;
                    }
                    
                    lock (this.messageQueue)
                    {
                        if (packet.IsBundle()) 
                        {
                            foreach (OSCMessage message in packet.Values)
                            {
                                if(Config.DebugOscMessages)
                                {
                                    this.lastOscMessage = ParseMessage(message);
                                }
                                this.messageQueue.Add(message as OSCMessage );
                            }
                        } 
                        else
                        {
                            if(Config.DebugOscMessages)
                            {
                                this.lastOscMessage = ParseMessage((packet as OSCMessage));
                            }
                            this.messageQueue.Add(packet as OSCMessage);
                        }
                    }
                } 
                catch (Exception e)
                { 
                    Debug.LogError(e.Message);
                }
            }
        }
    }
}
