﻿using UnityEngine;
using UnityEditor;
using MofaLib.OscBlob;

[CustomEditor(typeof(OscBlobReceiver))]
public class OscBlobReceiverInspector : Editor 
{
	public override void OnInspectorGUI()
	{
		OscBlobReceiver myTarget = (OscBlobReceiver)target;
		
        var style = new GUIStyle();
        style.normal.textColor = Color.red;
        string connectedMessage = "  =/ Not Connected /=";

        if(myTarget.Connected)
        {
            connectedMessage = "=> Connected <=";
            style.normal.textColor = Color.green;
        }

        GUILayout.Space(15);
        EditorGUILayout.LabelField(connectedMessage, style);
        GUILayout.Space(5);

		DrawDefaultInspector();
		
		GUILayout.Space(20);
		if(GUILayout.Button("Save Config"))
        {
            myTarget.SaveConfig();
        }
		if(GUILayout.Button("Load Config"))
        {
            myTarget.LoadConfig();
        }
	}
}
