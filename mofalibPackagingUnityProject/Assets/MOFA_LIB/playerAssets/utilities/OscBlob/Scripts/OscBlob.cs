﻿using System;
using System.Collections;
using UnityEngine;

namespace MofaLib.OscBlob
{
    public class OscBlob : MonoBehaviour 
    {
        public string Id;
        public float LifeTime = 0.001f;
        public OscBlobManager BlobManager;
        public OscBlobZone Zone;
        public GameObject Visual;
        public int LayerMask = 1 << 4;
        public bool ShowBlob = false;
        public float Smooth = 0;
        public Camera RayCamera;

        private Vector3 newPos;
        private bool lifeTimerOn = false;
        private float lifeTimer = 0;

        public void Move(Vector2 pos)
        {
            if (Smooth == 0)
            {
                Vector3 newPos = Zone.GetPosition (pos);
                transform.position = newPos;
            }
            else
            {
                this.newPos = Zone.GetPosition(pos);
            }
            this.lifeTimer = 0;
        }

        public void ShowTheBlob()
        {
            if(ShowBlob)
            {
                StartCoroutine (ShowBlobCoroutine ());
            }
            else
            {
                GetComponentInChildren<Renderer>().enabled = false;
            }
        }

        IEnumerator ShowBlobCoroutine ()
        {
            lifeTimerOn = true;
            Visual.GetComponent<Renderer>().material.SetFloat ("_Cutoff", 0.05f);
            for (float t = 0; t <= 1; t += Time.deltaTime * 5)
            {
                transform.localScale  = Vector3.Lerp(Vector3.zero,Vector3.one,t*t);
                yield return null;
            }
            transform.localScale = Vector3.one;
        }

        IEnumerator HideBlobCoroutine ()
        {
            Vector3 blobScale = transform.localScale;
            for (float t = 0; t <= 1; t += Time.deltaTime * 5)
            {
                transform.localScale  = Vector3.Lerp(blobScale,Vector3.zero,t*t);
                yield return null;
            }
            transform.localScale = Vector3.zero;
            DestroyBlob ();
        }

        void Update()
        {
            if(Smooth > 0)
            {
                transform.position = Vector3.Lerp(transform.position, this.newPos, Time.deltaTime * Smooth);
            }

            if(this.lifeTimerOn)
            {
                this.lifeTimer += Time.deltaTime;
                if(this.lifeTimer > LifeTime)
                {
                    StartCoroutine(HideBlobCoroutine());
                }
            }
        }

        void DestroyBlob()
        {
            print("Destroy Blob!");
            if(BlobManager != null)
            {
                BlobManager.RemoveBlobFromList(Id);
            }
            Destroy (gameObject);
        }

        public void BlobClick(float clickDelay)
        {
            StartCoroutine ("BlobClickCoroutine", clickDelay);
        }
            

        IEnumerator BlobClickCoroutine(float clickDelay)
        {
            this.lifeTimerOn = true;
            float initCutoff = 0.95f;
            float targetCutoff = 0.05f;
            bool isClick = true;
            Vector3 initPos = transform.position;
            yield return new WaitForSeconds(0.5f);
            for(float f = 0; f <= clickDelay; f += Time.deltaTime)
            {
                Visual.GetComponent<Renderer>().material.SetFloat("_Cutoff",( Mathf.Lerp(initCutoff, targetCutoff, f * 1/clickDelay)));
                float dist = Mathf.Abs(Vector3.Distance(initPos, transform.position));
                if(dist > BlobManager.ClickMaxDistance)
                {
                    isClick = false;
                    f = clickDelay;
                }
                yield return null;
            }

            if(isClick)
            {
                Visual.GetComponent<Renderer>().material.SetFloat("_Cutoff",0.05f);
                Click();
                Vector3 clickScaleA = transform.localScale;
                Vector3 clickScaleB = transform.localScale * 1.2f;
                
                for (float f  = 0; f <= 1; f+= Time.deltaTime * 10)
                {
                    transform.localScale = Vector3.Lerp(clickScaleA, clickScaleB,f);
                    yield return null;
                }
                
                Vector3 endClickScale = transform.localScale;
                for (float f  = 0; f <= 1; f+= Time.deltaTime * 15)
                {
                    transform.localScale = Vector3.Lerp(endClickScale, Vector3.zero,f*f);
                    yield return null;
                }
                transform.localScale = Vector3.zero;
                ShowTheBlob();
            }
        }

        void Click()
        {
            Vector3 _cameraPoint = RayCamera.WorldToScreenPoint (transform.position);
            Ray myRay = RayCamera.ScreenPointToRay (_cameraPoint);
            RaycastHit hit;
            Debug.DrawRay(myRay.origin,myRay.direction * 100,Color.red,2);
            if(Physics.Raycast (myRay, out hit,Mathf.Infinity,~LayerMask))
            {
                // use if you want to use tags in combination with layer mask
                //if(hit.transform.tag == "OSC_Clickable")
                //{
                    if(hit.transform.gameObject.GetComponent<OSC_ClickResult>()!= null)
                    {
                        hit.transform.gameObject.GetComponent<OSC_ClickResult>().Click(Convert.ToInt32(Id));
                        Debug.DrawLine(myRay.origin,hit.point,Color.green,2);
                    }
                //}
            }
        }
    }
}
