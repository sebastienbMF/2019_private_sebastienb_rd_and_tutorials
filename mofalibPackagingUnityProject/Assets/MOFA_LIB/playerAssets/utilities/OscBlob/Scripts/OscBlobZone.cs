﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using UnityEngine;

namespace MofaLib.OscBlob
{
    [RequireComponent(typeof(OscBlobReceiver))]
    public class OscBlobZone : MonoBehaviour 
    {
        [HideInInspector]
        public OscBlobReceiver BlobReceiver;

        OscBlobConfig config;
        public OscBlobConfig Config {get { return config; } }

        // to adjust position of config window
        [HideInInspector]
        public int ConfingWindowOffsetX = 0;
        [HideInInspector]
        public int ConfingWindowOffsetY = 0;

        public void Start()
        {
            BlobReceiver = GetComponent<OscBlobReceiver> ();
            BlobReceiver.OnConfigRefreshed += ConfigUpdate;
        }

        public void OnDisable()
        {
            BlobReceiver.OnConfigRefreshed -= ConfigUpdate;
        }

        void ConfigUpdate(OscBlobConfig config)
        {
            this.config = config;
        }

        public Vector3 GetPosition(Vector2 pos)
        {
            float x = 0; 
            float y = 0;

            float flippedX = pos.x;
            float flippedY = pos.y;
            if(Config.FlipX)
            {
                flippedX = 1 - pos.x;
            }

            if(Config.FlipY)
            {
                flippedY = 1 - pos.y;
            }

            pos = new Vector2 (flippedX, flippedY);

            if (Config.SwapXY)
            {
                x = pos.y * Config.XValueScale * transform.localScale.x;
                y = pos.x * Config.YValueScale * transform.localScale.z;
            }
            else
            {
                x = pos.x * Config.XValueScale * transform.localScale.x;
                y = pos.y * Config.YValueScale * transform.localScale.z;
            }

            float scalex = transform.localScale.x * 10 / 2;
            float scaley = transform.localScale.z * 10 / 2;

            if(!Config.IsWall)
            {
                Vector3 newPos = new Vector3 (transform.position.x - scalex + x , transform.position.y, transform.position.z- scaley + y);
                return newPos;
            }
            else
            {
                Vector3 newPos = new Vector3 (transform.position.x - scalex + x , transform.position.y- scaley + y, transform.position.z);
                return newPos;
            }
        }
    }
}