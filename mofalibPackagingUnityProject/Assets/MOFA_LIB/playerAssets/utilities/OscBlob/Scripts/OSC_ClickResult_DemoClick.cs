﻿using UnityEngine;
using System.Collections;

public class OSC_ClickResult_DemoClick : OSC_ClickResult {

	public override void Click (int blobId)
	{

		base.Click (blobId);
		Vector3 rndmCol = Random.insideUnitSphere;
		Color rndmColor = new Color (rndmCol.x, rndmCol.y, rndmCol.z);
		GetComponent<Renderer> ().material.color = rndmColor;
	}
}
