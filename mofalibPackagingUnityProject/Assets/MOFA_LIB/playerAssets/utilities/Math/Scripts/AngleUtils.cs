﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory
{
    public static partial class AngleUtils
    {
        /// <summary>
        /// Get "Top View" angle in Degrees from a tranform position, adding it's forward too
        /// </summary>
        /// <param name="to">World Position</param>
        /// <returns></returns>
        public static float GetXZAngle(Transform from, Vector3 to)
        {
            float forwardAngle = GetXZAngle(Vector3.zero, from.forward);
            return Mathf.Rad2Deg * Mathf.Atan2(to.z - from.position.z, to.x - from.position.x) - forwardAngle;
        }

        /// <summary>
        /// Get "Top View" angle in Degrees from to world points
        /// </summary>
        /// <param name="p1">World Position</param>
        /// <param name="p2">World Position</param>
        /// <returns></returns>
        public static float GetXZAngle(Vector3 p1, Vector3 p2)
        {
            return Mathf.Rad2Deg * Mathf.Atan2(p2.z - p1.z, p2.x - p1.x);
        }

        /// <summary>
        /// Get Absolute angle difference in degrees from a transform forward to another transform position (considering vertical displacement!)
        /// </summary>
        /// <param name="absolute">If we need an absolute result or negative numbers where left and positive right, 0 to 180 </param>
        /// <param name="flattenXZ">If angle is planar, ignoring the vertival angle (Y)</param>
        /// <param name="useDestForward">Consider destination forward angle too (to compare two angle of views)</param>
        /// <returns></returns>
        public static float GetAngleFromTo(Transform from, Transform dest, bool flattenXZ = false, bool absolute = true, bool useDestForward = false)
        {
            Vector3 toDirection = (useDestForward) ? dest.forward : (dest.position - from.position).normalized;

            return GetAngleFromTo(from.transform.forward, toDirection, flattenXZ, absolute);
        }


        /// <summary>
        /// Get angle difference in degrees between two NORMALIZED WORLD direction vectors
        /// </summary>
        /// <param name="absolute">If we need an absolute result or negative numbers where left and positive right, 0 to 180 </param>
        /// <param name="flattenXZ">If angle is planar, ignoring the vertival angle (Y)</param>
        /// <returns>Angle in degrees</returns>
        public static float GetAngleFromTo(Vector3 fromDirection, Vector3 toDirection, bool flattenXZ = false, bool absolute = true)
        {
            // Flattened 2D XZ version
            if(flattenXZ)
            {
                fromDirection.y = 0.0f;
                toDirection.y = 0.0f;
                fromDirection = fromDirection.normalized;
                toDirection = toDirection.normalized;

                if (absolute)
                {
                    return Mathf.Rad2Deg * Mathf.Acos(fromDirection.x * toDirection.x + fromDirection.z * toDirection.z);
                } else
                {
                    float angleDirection = Vector3.Cross(toDirection, fromDirection).y > 0f ? 1f : -1f;
                    return Mathf.Rad2Deg * Mathf.Acos(fromDirection.x * toDirection.x + fromDirection.z * toDirection.z) * angleDirection;
                }
            }
            
            // 3D angle difference
            if (absolute)
            {
                return Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(toDirection, fromDirection));
            }
            else
            {
                float angleDirection = Vector3.Cross(toDirection, fromDirection).y > 0f ? 0.5f : -0.5f;
                
                return Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(toDirection, fromDirection)) * angleDirection;
            }
        }

        /// <summary>
        /// Returns the cosine of the angle between this vector and another projected onto the XZ plane (no Y). 
        /// To get angle difference in degrees, use AngleUtils.GetAngleFromTo()
        /// </summary>
        public static float Cosine2D(Vector3 a, Vector3 b)
        {
            // https://api.unrealengine.com/INT/API/Runtime/Core/Math/FVector/CosineAngle2D/index.html
            a.y = 0.0f;
            b.y = 0.0f;
            a = a.normalized;
            b = b.normalized;

            return a.x * b.x + a.z * b.z;
        }
        
    }
}