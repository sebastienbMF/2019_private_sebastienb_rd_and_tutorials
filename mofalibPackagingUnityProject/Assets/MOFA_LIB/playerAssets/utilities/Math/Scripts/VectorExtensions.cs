﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MomentFactory
{
    /// <summary>
    /// Extensions 
    /// </summary>
    public static class VectorExtensions
    {
        const float KINDA_SMALL_NUMBER = 1.175494E-38f; // Similar to Mathf.Epsilon, but a constant

        /// <summary>
        /// Gets a normalized copy of the 2D components of the vector. Y is set to zero. 
        /// </summary>
        /// <returns>Returns zero vector if vector length is too small to normalize.</returns>
        /// <param name="scaleY">If we want to keep a part of the Y value in the result</param>
        /// <returns>The planar version of the normal</returns>
        public static Vector3 Normalize2D(this Vector3 target, float scaleY = 0f, float tolerance = KINDA_SMALL_NUMBER)
        {
            if (target.sqrMagnitude == 0f) return Vector3.zero;

            // Adapted from UE4 FVector::GetSafeNormal2D() in Vector.h: 
            // https://api.unrealengine.com/INT/API/Runtime/Core/Math/FVector/GetSafeNormal2D/index.html

            // Sum without Y
            float SquareSum = (target.x * target.x) + (target.z * target.z);

            if (scaleY > 0f)
            {
                SquareSum += target.y * target.y * scaleY;
            }

            // Not sure if it's safe to add tolerance in there. Might introduce too many errors
            if (SquareSum == 1f)
            {
                return new Vector3(target.x, 0f, target.z);
            }
            else if (SquareSum < tolerance)
            {
                return Vector3.zero;
            }

            float scale = Mathf.Sqrt(SquareSum);
            //float scale = MathUtils.InvSqrt(SquareSum);

            return new Vector3(target.x * scale, 0f, target.z * scale);
        }
    }
}