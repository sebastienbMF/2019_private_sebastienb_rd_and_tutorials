﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// For StructLayout / LayoutKind
using System.Runtime.InteropServices;

namespace MomentFactory
{
    /// <summary>
    /// Generic Math Utilities
    /// 
    /// * For 3D Maths specific utilities:  Math3d.cs
    /// * For Vector3 extensions: VectorExtensions.cs
    /// * For angle-specific utilties: AngleUtils.cs
    /// * For transform-specific utilties: TransformExtensions.cs
    /// 
    /// </summary>
    public static partial class MathUtils
    {
        /// <summary>
        /// Similar to UE4 MapRangeClamped
        /// http://api.unrealengine.com/INT/BlueprintAPI/Math/Float/MapRangeClamped/
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="InRangeA"></param>
        /// <param name="InRangeB"></param>
        /// <param name="OutRangeA"></param>
        /// <param name="OutRangeB"></param>
        /// <returns></returns>
        public static float MapRangeClamped(float Value, float InRangeA, float InRangeB, float OutRangeA = 0f, float OutRangeB = 1f)
        {
            float Position = Mathf.Clamp01(GetRangePosition(InRangeA, InRangeB, Value));
            return Mathf.Lerp(OutRangeA, OutRangeB, Position);
        }

        /// <summary>
        /// Similar to UE4 MapRangeUnclamped
        /// http://api.unrealengine.com/INT/BlueprintAPI/Math/Float/MapRangeUnclamped/
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="InRangeA"></param>
        /// <param name="InRangeB"></param>
        /// <param name="OutRangeA"></param>
        /// <param name="OutRangeB"></param>
        /// <returns></returns>
        public static float MapRangeUnclamped(float Value, float InRangeA, float InRangeB, float OutRangeA = 0f, float OutRangeB = 1f)
        {
            return Mathf.Lerp(OutRangeA, OutRangeB, GetRangePosition(InRangeA, InRangeB, Value));
        }

        /// <summary>
        /// Calculates the percentage along a line from MinValue to MaxValue that Value is.
        /// </summary>
        /// <returns>From 0 to 1</returns>
        public static float GetRangePosition(float MinValue, float MaxValue, float Value)
        {
            return (Value - MinValue) / (MaxValue - MinValue);
        }

        /// <summary>
        /// Approximate Sqrt method, inspired from Quake InvSqrt()
        /// </summary>
        public static float InvSqrt(float z) 
        {
            // @see http://ncannasse.fr/blog/fast_inverse_square_root
            // @see http://blog.wouldbetheologian.com/2011/11/fast-approximate-sqrt-method-in-c.html

            if (z == 0) return 0;
            FloatIntUnion u;
            u.tmp = 0;
            u.f = z;
            u.tmp -= 1 << 23; /* Subtract 2^m. */
            u.tmp >>= 1; /* Divide by 2. */
            u.tmp += 1 << 29; /* Add ((b + 1) / 2) * 2^m. */
            return u.f;
        }

        [StructLayout(LayoutKind.Explicit)]
        private struct FloatIntUnion
        {
            [FieldOffset(0)]
            public float f;

            [FieldOffset(0)]
            public int tmp;
        }
    }
}