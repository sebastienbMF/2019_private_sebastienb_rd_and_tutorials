﻿using UnityEngine;

public class BezelRemapper : MonoBehaviour
{
    [SerializeField]
    private Texture _inputTexture; 
    [SerializeField]
    private ComputeShader _computeShader;
    [SerializeField, Tooltip("The main texture of this material will be the output of the compute shader.")]
    private Material _outputMaterial;
    [Tooltip("The size of the output texture in pixels.")]
    public Vector2Int OutputSize;
    [Tooltip("The size of a sub-window in the remapping.")]
    public Vector2Int Subsize;
    [Tooltip("The amount of pixels to crop between each sub-window, vertically and horizontally.")]
    public Vector2Int Gap = new Vector2Int(2, 2);

    // Output texture for the compute shader. Will be put as main texture in the output material.
    private RenderTexture _outputTexture;

    // The handle of the compute shader from Unity
    private int _kernelIdx;

    // Thread block values must match numthreads in the compute shader kernel
    private const int _threadBlockWidth = 8;
    private const int _threadBlockHeight = 8;
    private int _threadGroupsX;
    private int _threadGroupsY;

    void Start ()
    {
        _kernelIdx = _computeShader.FindKernel("BezelRemapper");

        _outputTexture = new RenderTexture(OutputSize.x, OutputSize.y, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
        _outputTexture.useMipMap = false;
        _outputTexture.enableRandomWrite = true;    
        _outputTexture.Create();

        _outputMaterial.mainTexture = _outputTexture;

        _computeShader.SetTexture(_kernelIdx, "InputTexture", _inputTexture);
        _computeShader.SetTexture(_kernelIdx, "OutputTexture", _outputTexture);
        _computeShader.SetInts("Subsize", new int[] { Subsize.x, Subsize.y });
        _computeShader.SetInts("Gap", new int[] { Gap.x, Gap.y });

        // Mathemagic to always get enough thread groups
        _threadGroupsX = (_outputTexture.width + (_threadBlockWidth - 1)) / _threadBlockWidth;
        _threadGroupsY = (_outputTexture.height + (_threadBlockHeight - 1)) / _threadBlockHeight;
    }

    void Update ()
    {
        _computeShader.Dispatch(_kernelIdx, _threadGroupsX, _threadGroupsY, 1);
    }
}

