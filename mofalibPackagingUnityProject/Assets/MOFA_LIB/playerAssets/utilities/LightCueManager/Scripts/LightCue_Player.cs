﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightCue_Player : MonoBehaviour 
{

	public LightCue_Behavior _default;

	public List<LightCue_LightTrack> _lightsTrack = new List<LightCue_LightTrack>();
	public Renderer _visualizer;

	public float _output;
	public float _intensityFloor;
	public bool _lockOutput;
	public Color _colorOutput;
	public List<Color> _colors = new List<Color>();
	public bool _dontPlayDefaultCue;

	void Awake()
	{
		_visualizer = GetComponent<Renderer>();
		LightCue(_default, 1, 0, false);
		SetColor(_colorOutput, 0);
	}

	public void LightCue(LightCue_Behavior behavior, float playSpeed, float xfadeDuration, bool playOnce, float offset, float offsetTranstitionDuration)
	{
		LightCue_LightTrack newTrack = new LightCue_LightTrack(behavior, playSpeed, playOnce, offset, offsetTranstitionDuration, _intensityFloor);
		_lightsTrack.Add(newTrack);
		if(_lightsTrack.Count > 1)
		{
			StartCoroutine(Crossfade(xfadeDuration));
		}
	}

	public void LightCue(LightCue_Behavior behavior, float playSpeed, float xfadeDuration, bool playOnce, Vector2 offsetRange, float offsetTranstitionDuration)
	{
		LightCue_LightTrack newTrack = new LightCue_LightTrack(behavior, playSpeed, playOnce, offsetRange, offsetTranstitionDuration, _intensityFloor);
		_lightsTrack.Add(newTrack);
		if(_lightsTrack.Count > 1)
		{
			StartCoroutine(Crossfade(xfadeDuration));
		}
	}

	public void LightCue(LightCue_Behavior behavior, float playSpeed, float xfadeDuration, bool playOnce)
	{
		LightCue_LightTrack newTrack = new LightCue_LightTrack(behavior, playSpeed, playOnce, new Vector2(0,0), 0, _intensityFloor);
		_lightsTrack.Add(newTrack);
		if(_lightsTrack.Count > 1)
		{
			StartCoroutine(Crossfade(xfadeDuration));
		}
	}

	// for color

	public void SetColor(Color col, float transitionDuration)
	{
		_colors.Add(col);
		if(_colors.Count > 1)
		{
			StartCoroutine(ChangeColorCoroutine(transitionDuration));
		}
	}

	public void ReSync()
	{
		for(int i = 0; i< _lightsTrack.Count; i++)
		{
			_lightsTrack[i].ReSync();
		}
	}

	public void ReSync(float offsetTransitionDuration)
	{
		for(int i = 0; i< _lightsTrack.Count; i++)
		{
			_lightsTrack[i].ReSync(offsetTransitionDuration);
		}
	}

	void Update()
	{
		for(int i = 0; i< _lightsTrack.Count; i++)
		{
			_lightsTrack[i].RemoteUpdate();
		}
	}

	void LateUpdate () 
	{
		if(_lightsTrack.Count > 0 && !_lockOutput)
		{
			_output = _lightsTrack[0]._intensity;
		}
		_visualizer.material.color = _colorOutput * _output;
	}

	IEnumerator Crossfade(float duration)
	{
		_lockOutput = true;
		if(duration > 0)
		{
			for(float t = 0; t< 1; t+= Time.deltaTime / duration)
			{
				_output = Mathf.Lerp(_lightsTrack[0]._intensity, _lightsTrack[1]._intensity, t);
				yield return null;
			}
		}
		_output = _lightsTrack[1]._intensity;
		_lightsTrack.RemoveAt(0);
		_lockOutput = false;
	}

	IEnumerator ChangeColorCoroutine(float crossfadeDuration)
	{
		print("Change color coroutine " + crossfadeDuration );
		if(crossfadeDuration > 0)
		{
			for(float t = 0; t< 1; t+= Time.deltaTime / crossfadeDuration)
			{
				_colorOutput = Color.Lerp(_colors[0], _colors[1], t);
				yield return null;
			}
		}
		_colorOutput = _colors[1];
		_colors.RemoveAt(0);
	}

}
