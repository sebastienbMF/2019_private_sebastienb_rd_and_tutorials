﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightCue_Manager : MonoBehaviour {

	
	public LightCue_Behavior[] _behaviors;
	public LightCue_Player[] _lights;
	public bool _hideOnStart;

	List<LightCueObject> _cueList = new List<LightCueObject>();

	void Start()
	{
		for(int i = 0; i<_lights.Length; i++)
		{
			_lights[i].GetComponent<Renderer>().enabled =  !_hideOnStart;
		}
	}
	
	public float[] GetLightInfoIntensity()
	{	
		float[] infos = new float[_lights.Length];
		for(int i = 0; i < _lights.Length; i++)
		{
			infos[i] = (_lights[i]._colorOutput * _lights[i]._output).r;
		}
		return infos;
	}

	public Color[] GetLightInfoColor()
	{	
		Color[] infos = new Color[_lights.Length];
		for(int i = 0; i < _lights.Length; i++)
		{
			infos[i] = _lights[i]._colorOutput * _lights[i]._output;
		}
		return infos;
	}

    public void LightCue(string behaviorName, float playSpeed, float crossfadeDuration, bool playOnce)
    {
		print(gameObject.name + " LightCue received: " + behaviorName);
        LightCue_Behavior behavior = Array.Find(_behaviors, p => p._name.Contains(behaviorName));
		if(StoreCue() || _cueList.Count > 0)
		{
			_cueList.Add(new LightCueObject(behavior,playSpeed, crossfadeDuration, playOnce));
		}
		else
		{
        	SetLightCue(behavior, playSpeed, crossfadeDuration, playOnce);
		}
    }

    public void LightCue(string behaviorName, float playSpeed, float crossfadeDuration, bool playOnce, Vector2 offsetRange, float offsetTransitionDuration)
    {
		print(gameObject.name + " LightCue received: " + behaviorName);
        LightCue_Behavior behavior = Array.Find(_behaviors, p => p._name.Contains(behaviorName));
		if(StoreCue() || _cueList.Count > 0)
		{
			_cueList.Add(new LightCueObject(behavior,playSpeed, crossfadeDuration, playOnce, offsetRange, offsetTransitionDuration));
		}
		else
		{
        	SetLightCue(behavior, playSpeed, crossfadeDuration, playOnce, offsetRange, offsetTransitionDuration);
		}
    }

	public void LightCue(string behaviorName, float playSpeed, float crossfadeDuration, bool playOnce, float offset, float offsetTransitionDuration)
    {
		print(gameObject.name + " LightCue received: " + behaviorName);
        LightCue_Behavior behavior = Array.Find(_behaviors, p => p._name.Contains(behaviorName));
		if(StoreCue() || _cueList.Count > 0)
		{
			_cueList.Add(new LightCueObject(behavior,playSpeed, crossfadeDuration, playOnce, offset, offsetTransitionDuration));
		}
		else
		{
        	SetLightCue(behavior, playSpeed, crossfadeDuration, playOnce, offset, offsetTransitionDuration);
		}
    }



	void SetLightCue(LightCue_Behavior behavior, float playSpeed, float crossfadeDuration, bool playOnce)
    {
        for (int i = 0; i <= _lights.Length - 1; i++)
        {
            _lights[i].LightCue(behavior, playSpeed, crossfadeDuration, playOnce);
        }
    }

	void SetLightCue(LightCue_Behavior behavior, float playSpeed, float crossfadeDuration, bool playOnce, Vector2 offsetRange, float offsetTransitionDuration)
    {
        for (int i = 0; i <= _lights.Length - 1; i++)
        {
            _lights[i].LightCue(behavior, playSpeed, crossfadeDuration, playOnce, offsetRange, offsetTransitionDuration);
        }
    }

	void SetLightCue(LightCue_Behavior behavior, float playSpeed, float crossfadeDuration, bool playOnce, float offset, float offsetTransitionDuration)
    {
        for (int i = 0; i <= _lights.Length - 1; i++)
        {
			float temp = i * offset;
			float realOffset = temp - Mathf.FloorToInt(temp);
			//print("RealOffset: " + realOffset);
			_lights[i].LightCue(behavior, playSpeed, crossfadeDuration, playOnce, realOffset, offsetTransitionDuration);
        }
    }
	// for colors

	public void ChangeColor(Color color, float transitionDuration)
	{
		for(int i = 0; i< _lights.Length; i++)
		{
			_lights[i].SetColor(color, transitionDuration);
		}
	}
	
	public void ResyncLights()
	{
		for (int i = 0; i <= _lights.Length - 1; i++)
        {
			_lights[i].ReSync();
		}
	}

	public void ResyncLights(float offsetTransitionDuration)
	{
		for (int i = 0; i <= _lights.Length - 1; i++)
        {
			_lights[i].ReSync(offsetTransitionDuration);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if(_cueList.Count > 0)
		{
			if(!StoreCue())
			{
				switch(_cueList[0].LightcueType)
				{
					case 0:
						SetLightCue(_cueList[0].Animationbehavior, _cueList[0].PlaySpeed, _cueList[0].CrossfadeDuration, _cueList[0].PlayOnce);
						break;
					case 1:
						SetLightCue(_cueList[0].Animationbehavior, _cueList[0].PlaySpeed, _cueList[0].CrossfadeDuration, _cueList[0].PlayOnce, _cueList[0].OffsetRange, _cueList[0].OffsetTransitionDuration);
						break;
					case 2:
						SetLightCue(_cueList[0].Animationbehavior, _cueList[0].PlaySpeed, _cueList[0].CrossfadeDuration, _cueList[0].PlayOnce, _cueList[0].Offset, _cueList[0].OffsetTransitionDuration);
						break;
				}
				_cueList.RemoveAt(0);
			}
		}

		if(Input.GetKeyDown(KeyCode.Alpha7))
		{
			LightCue("Black",1,2,false);
			//ChangeColor(MFColorUtilities.GetRandomColor(0.8f),2);
		}

		if(Input.GetKeyDown(KeyCode.Alpha8))
		{
			LightCue("Sinewave",5,10,false);
			//ChangeColor(MFColorUtilities.GetRandomColor(0.8f),2);
		}

		if(Input.GetKeyDown(KeyCode.Alpha9))
		{
			LightCue("Saw",1,1,false,0.5f,5);
			//ChangeColor(MFColorUtilities.GetRandomColor(0.8f),2);
		}

		if(Input.GetKeyDown(KeyCode.Alpha0))
		{
			LightCue("White",1,2,false);
			//ChangeColor(MFColorUtilities.GetRandomColor(0.8f),2);
		}


		if(Input.GetKeyDown(KeyCode.Equals))
		{
			//ChangeColor(MFColorUtilities.GetRandomColor(0.8f),2);
		}

		if(Input.GetKeyDown(KeyCode.Minus))
		{
			ResyncLights(0.5f);
		}

		if(Input.GetKeyDown(KeyCode.H))
		{
			ShowHidePreviz();
		}
	}

	void ShowHidePreviz()
	{
		for(int i = 0; i<_lights.Length; i++)
		{
			_lights[i]._visualizer.enabled = !_lights[i]._visualizer.enabled;
		}
	}

	bool StoreCue()
	{
		bool store = false;
		for(int i = 0; i< _lights.Length; i++)
		{
			if(_lights[i]._lockOutput)
			{
				store = true;
			}
		}
		return store;
	}
}

[Serializable]
public class LightCueObject
{
	public LightCue_Behavior Animationbehavior;
	public float PlaySpeed;
	public float CrossfadeDuration;
	public bool PlayOnce;

	public Vector2 OffsetRange;

	public float Offset;
	public float OffsetTransitionDuration;
	public int LightcueType;

	public LightCueObject(LightCue_Behavior behavior, float playSpeed, float crossfadeDuration, bool playOnce)
	{
		Animationbehavior = behavior;
		PlaySpeed = playSpeed;
		CrossfadeDuration = crossfadeDuration;
		PlayOnce = playOnce;
		LightcueType = 0;
	}

	public LightCueObject(LightCue_Behavior behavior, float playSpeed, float crossfadeDuration, bool playOnce, Vector2 offsetRange, float offsetTransitionDuration)
	{
		Animationbehavior = behavior;
		PlaySpeed = playSpeed;
		CrossfadeDuration = crossfadeDuration;
		PlayOnce = playOnce;
		OffsetRange = offsetRange;
		OffsetTransitionDuration = offsetTransitionDuration;
		LightcueType = 1;
	}

	public LightCueObject(LightCue_Behavior behavior, float playSpeed, float crossfadeDuration, bool playOnce, float offset, float offsetTransitionDuration)
	{
		Animationbehavior = behavior;
		PlaySpeed = playSpeed;
		CrossfadeDuration = crossfadeDuration;
		PlayOnce = playOnce;
		Offset = offset;
		OffsetTransitionDuration = offsetTransitionDuration;
		LightcueType = 2;
	}
}
