﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LightCue_Behavior
{

    [SerializeField]
    public string _name;

    [SerializeField]
    public AnimationCurve _animationCurve;

}
