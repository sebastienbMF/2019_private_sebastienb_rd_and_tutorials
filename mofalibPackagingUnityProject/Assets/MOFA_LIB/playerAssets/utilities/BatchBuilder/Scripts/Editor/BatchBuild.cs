﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;

namespace MofaLib
{
    public class BatchBuild
    {
        static bool ready = false;
        static int frameWait = 0;
        static bool nextReady = false;
        static int frameWaitNext = 0;
        static IBatchBuildable sceneManager;
        static string path = "";
        static string timeStamp;

        const string sceneManagerName = "BuildManager";
        const string executableName = "MyTestExec.exe";

        const string buildOneFolderPrefix = "PROJ_Build01";
        const string buildTwoFolderPrefix = "PROJ_Build02";
        const string buildThreeFolderPrefix = "PROJ_Build03";

        [MenuItem("MofaLib/Launch Batch Build")]
        public static void LaunchBatchBuild()
        {
            LaunchBatchBuild(BatchBuilderBuildId.BuildOne);
        }

        public static void LaunchBatchBuild(BatchBuilderBuildId buildId)
        {
            // make sure to 
            GameObject managerGO = GameObject.Find(sceneManagerName);
            if(managerGO == null)
            {
                Debug.LogWarning("No object of name -" + sceneManagerName + "- of type IBatchBuildable found, cannot launch batch build");
                return;
            }
            sceneManager = managerGO.GetComponent<IBatchBuildable>();
            ready = false;
            frameWait = 0;

            EditorApplication.update = null;
            if(EditorApplication.update != Update)
            {
                EditorApplication.update += Update;
            }

            if(buildId == BatchBuilderBuildId.BuildOne)
            {
                path = EditorUtility.SaveFolderPanel("Choose Location of Built Game", path.Replace('/','\\'), "");
                timeStamp = DateTime.Now.ToString("yyMMdd");
                timeStamp += ("_" + DateTime.Now.ToString("HHmm"));
            }
            if(path.Length == 0)
            {
                return;
            }

            Debug.Log("---PATH: " + path);

            sceneManager.ClearEventHandler();
            sceneManager.ReadyToBuildEventHandler += ReadyToBuild;
            
            sceneManager.CurrentBuildId = buildId;
            sceneManager.SetProjectBuildSettings(buildId);
        }

        public static void Update()
        {
            if(ready)
            {
                frameWait += 1;
                Debug.Log(frameWait);
                if(frameWait >= 10)
                {
                        ready = false;
                        EditorApplication.update = null;
                        Build();
                }
            }

            if(nextReady)
            {
                frameWaitNext += 1;
                Debug.Log(frameWaitNext);
                if(frameWaitNext >= 10)
                {
                        nextReady = false;
                        EditorApplication.update = null;
                        BuildNext();
                }
            }
        }

        public static void ReadyToBuild()
        {
            ready = true;
        }

        public static void Build()
        {
            sceneManager.ClearEventHandler();
        
            string currentBuildName = buildOneFolderPrefix;
            if(sceneManager.CurrentBuildId == BatchBuilderBuildId.BuildTwo)
            {
                currentBuildName = buildTwoFolderPrefix;
            }
            if(sceneManager.CurrentBuildId == BatchBuilderBuildId.BuildThree)
            {
                currentBuildName = buildThreeFolderPrefix;
            }

            string buildFolder = path + "/" + currentBuildName + timeStamp;
            Directory.CreateDirectory(buildFolder);
            Debug.Log("Build...");
            BuildReport report = BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, buildFolder + "/" + executableName, BuildTarget.StandaloneWindows64, BuildOptions.None);
            BuildSummary summary = report.summary;

            if(summary.result == BuildResult.Succeeded)
            {
                Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
                frameWaitNext = 0;
                nextReady = true;
                EditorApplication.update += Update;
            }

            if(summary.result == BuildResult.Failed)
            {
                Debug.Log("Build failed");
            }
        }


        static void BuildNext()
        {
            if(sceneManager.CurrentBuildId == BatchBuilderBuildId.BuildOne)
            {
                LaunchBatchBuild(BatchBuilderBuildId.BuildTwo);
                return;
            }

            if(sceneManager.CurrentBuildId == BatchBuilderBuildId.BuildTwo)
            {
                LaunchBatchBuild(BatchBuilderBuildId.BuildThree);
                return;
            }
            
            if(sceneManager.CurrentBuildId == BatchBuilderBuildId.BuildThree)
            {
                Debug.Log("Batch build finised!!");
                Debug.Log("---PATH: " + path);
                string pathFix = path.Replace('/','\\');
                System.Diagnostics.Process.Start("explorer.exe", pathFix);
            }
        }
}
}
