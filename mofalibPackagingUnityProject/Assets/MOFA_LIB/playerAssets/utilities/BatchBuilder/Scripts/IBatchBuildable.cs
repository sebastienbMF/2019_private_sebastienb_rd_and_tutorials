﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace MofaLib
{
    public delegate void ProjectReadyToBuild();

    // can be modified to better suit your needs / be more explicit
    public enum BatchBuilderBuildId {BuildOne, BuildTwo, BuildThree};

    public interface IBatchBuildable
    {
        BatchBuilderBuildId CurrentBuildId
        {
            get;
            set;
        }

        bool CurrentSurfaceBuildFinished
        {
            get;
            set;
        }

        void SetProjectBuildSettings(BatchBuilderBuildId buildId);
        void BatchBuildReadyCheck(int id);
        void ClearEventHandler();

        event ProjectReadyToBuild ReadyToBuildEventHandler;
    }
}

