﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MofaLib;

public class BatchBuilderExample:  MonoBehaviour, IBatchBuildable 
{
	// generic scene properties
	public Vector3 RotationSpeed;
	public Mesh[] VariousMeshes;
	public Vector3[] VariousSpeeds;

	// IBatchBuildable implementation properties
	public BatchBuilderBuildId CurrentBuildId {get; set;}
	public bool CurrentSurfaceBuildFinished  {get; set;}
	public event ProjectReadyToBuild ReadyToBuildEventHandler;

	// used to validate the project is ready to build before launching batch build
	private bool _colorReady;
	private bool _meshReady;

	// this is a required method for interface implementation
    public void SetProjectBuildSettings(BatchBuilderBuildId buildId)
	{
		// reset the ready flags
		_colorReady = false;
		_meshReady = false;

		// set project according to build requirements
		int id = (int)buildId;
		switch (id)
		{
			case 0:
        		SetSpeed(id);
				ChangeShape(id);
                // do other complicated stuff your mom would never understand.
            	break;
			
			case 1:
				SetSpeed(id);
				ChangeShape(id);
                // do other complicated stuff your mom would never understand.
				break;

			case 2:
				SetSpeed(id);
				ChangeShape(id);
                // do other complicated stuff your mom would never understand.
				break;
		}
	}

	// this is a required method for interface implementation
    public void BatchBuildReadyCheck(int id)
	{
		// validate if each steps are ready, then callback
		if(id == 0)
		{
			_colorReady = true;
		}

		if(id == 1)
		{
			_meshReady = true;
		}

		if(_colorReady && _meshReady)
        {
            if(ReadyToBuildEventHandler != null)
            {
                print("Ready callback");
                ReadyToBuildEventHandler();
            }
        }
	}
	
	// this is a required method for interface implementation
	public void ClearEventHandler()
	{
        ReadyToBuildEventHandler = null;
	}


	// example stuff	
	void Update()
	{
		transform.Rotate(RotationSpeed);
	}

	void SetSpeed(int id)
	{
		this.RotationSpeed = VariousSpeeds[id];
		BatchBuildReadyCheck(0);
	}

	void ChangeShape(int id)
	{
		GetComponent<MeshFilter>().mesh = VariousMeshes[id];
		BatchBuildReadyCheck(1);
	}

}
