﻿using System;
using System.Runtime.InteropServices;

namespace MofaLib
{
    public static class WindowRepositionCmd 
    {

        private static string[] startupArgs;

        #if UNITY_STANDALONE_WIN || UNITY_EDITOR
        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        private static extern bool SetWindowPos(IntPtr hwnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();


        private static void SetPosition(int x, int y, int resX = 0, int resY = 0)
        {
            IntPtr hWnd = GetForegroundWindow();
            SetWindowPos(hWnd, 0, x, y, resX, resY, resX * resY == 0 ? 1 : 0);
        }
        #endif


        private static string GetStartupParameter(String parameter)
        {
            if (startupArgs == null)
                startupArgs = Environment.GetCommandLineArgs();
            string s = null;
            for (int i = 0; i < startupArgs.Length; i++)
            {
                if (startupArgs[i].Equals(parameter) && startupArgs.Length >= i + 2)
                    s = startupArgs[i + 1];
            }
            return s;
        }

        public static void Init()
        {
            #if UNITY_STANDALONE_WIN || UNITY_EDITOR
            if (GetStartupParameter("-screen-position-x") == null && GetStartupParameter("-screen-position-y") == null) return;
            int x = int.Parse(GetStartupParameter("-screen-position-x"));
            int y = int.Parse(GetStartupParameter("-screen-position-y"));
            SetPosition(x, y);
            #endif
        }
    }
}