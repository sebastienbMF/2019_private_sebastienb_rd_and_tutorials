﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LoadExternalConfigExample))]
public class LoadExternalConfigExampleInspector : Editor 
{
	public override void OnInspectorGUI()
    {
        LoadExternalConfigExample myTarget = (LoadExternalConfigExample)target;
		
		DrawDefaultInspector();
		
		GUILayout.Space(20);
		if(GUILayout.Button("Save Config"))
        {
            myTarget.SaveConfig(myTarget.Config);
        }
		if(GUILayout.Button("Load Config"))
        {
            myTarget.Config = myTarget.LoadConfig(myTarget.Config);
        }
    }
}
