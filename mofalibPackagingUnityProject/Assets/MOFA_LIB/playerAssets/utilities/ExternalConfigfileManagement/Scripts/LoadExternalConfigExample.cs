﻿using System.Collections;
using UnityEngine;
using MofaLib;


public class LoadExternalConfigExample : LoadExternalConfig
{
    public MySuperConfig Config;

    void Awake()
    {
        LoadConfig(Config);
    }
}

[System.Serializable]
public class MySuperConfig  
{
    public string StringOne;
    public int NumberOne;
    public string StringTwo;
}
