﻿using System.IO;
using UnityEngine;

namespace MofaLib
{
    public class LoadExternalConfig : MonoBehaviour
    {
        public string ConfigFileName = "config.json";
        public int parentLevel = 2;

        public virtual void SaveConfig<T>(T config)
        {
            config.SaveJson(ConfigFileName);
            Debug.Log(this.name +": config saved!");
        }
        
        public virtual T LoadConfig<T>(T config)
        {   
            // if application is editor, will use config file from streaming assets
            if(Application.isEditor)
            {
                config = config.LoadJson(ConfigFileName);
                Debug.Log(this.name + ": Config Loaded Successfully!");
                return config;
            }
            
            string jsonPath = Application.dataPath;
            DirectoryInfo root = new DirectoryInfo(jsonPath);
            // will go a number of folder up according to parentLevel number 
            for(int i = 0; i< parentLevel; i++)
            {
                root = root.Parent;
            }
            jsonPath = root.FullName +  @"\" + ConfigFileName;

            if(!File.Exists(jsonPath))
            {
                Debug.LogWarning(this.name + ": cannot load " + ConfigFileName + " will use build in values");
                Debug.LogWarning(this.name + ": " + jsonPath);
                return config;
            }

            string json = System.IO.File.ReadAllText(jsonPath);
            Debug.LogWarning(this.name + ": Config Loaded Successfully!");
            config = JsonUtility.FromJson<T>(json);
            return config;
        }
    }
}