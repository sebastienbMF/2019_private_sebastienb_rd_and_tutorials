﻿using UnityEngine;

namespace MofaLib
{
    public abstract class InterpolatorBase : InterpolationBase
    {
        public AnimationCurve InterpolationCurve = AnimationCurve.Linear(0, 0, 1, 1);

        // This is set when the interpolator is referenced by a controller.
        private InterpolationController _controller;
        private bool _isControlled;
        // This is used to avoid a constant update.
        protected float _lastTInterpolation;

        // Called by the controller.
        public void Init(InterpolationController controller)
        {
            _controller = controller;
            _isControlled = true;
        }

        private void Update()
        {
            // Is a controller is assigned, its T value overrides the interpolator's.
            if (_isControlled) TInterpolation = _controller.TInterpolation;

            if (_lastTInterpolation != TInterpolation)
            {
                DoInterpolation(InterpolationCurve.Evaluate(TInterpolation));
                _lastTInterpolation = TInterpolation;
            }
        }

        protected abstract void DoInterpolation(float t);
    }
}
