﻿using UnityEngine;

namespace MofaLib
{
    /// <summary>
    /// Interpolates the position of the object.
    /// </summary>
    public class TransformPositionInterpolator : InterpolatorBase
    {
        [Tooltip("Work with position in object ( = local) space or world space.")]
        public bool UseLocal = true;
        [Space(5)]
        public bool UseStartPositionAsOrigin = true;
        public Vector3 OriginPosition;
        public bool UseStartPositionAsTarget = false;
        public Vector3 TargetPosition;

        private void Start()
        {
            if (UseStartPositionAsOrigin)
            {
                OriginPosition = UseLocal ? transform.localPosition : transform.position;
            }
            if (UseStartPositionAsTarget)
            {
                TargetPosition = UseLocal ? transform.localPosition : transform.position;
            }

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            if (!UseLocal)
            {
                transform.position = Vector3.Lerp(OriginPosition, TargetPosition, t);
            }
            else
            {
                transform.localPosition = Vector3.Lerp(OriginPosition, TargetPosition, t);
            }
        }
    }
}
