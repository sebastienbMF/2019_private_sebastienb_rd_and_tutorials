﻿using UnityEngine;

namespace MofaLib
{
    public class InterpolationController : InterpolationBase
    {
        [Tooltip("The interpolation factor of this controller will override "
            + "the interpolation factor of those interpolators.")]

        public InterpolatorBase[] Updaters;

        void Start()
        {
            // Assign this controller to every interpolators in its list.
            for (int i = 0; i < Updaters.Length; i++)
            {
                Updaters[i].Init(this);
            }
        }
    }
}
