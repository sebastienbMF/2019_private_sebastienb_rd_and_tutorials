﻿using UnityEngine;

namespace MofaLib
{
    public class MaterialAlphaInterpolator : MaterialInterpolator
    {
        [Space(5)]
        public bool ShaderUsesTintColor;

        [Range(0, 1)]
        public float AlphaOff = 0.0f;

        [Range(0, 1)]
        public float AlphaOn = 1.0f;

        private Color _color;
        private string _colorFieldName;

        protected override void Start()
        {
            base.Start();

            _colorFieldName = ShaderUsesTintColor ? "_TintColor" : "_Color";

            if (_material != null)
            {
                _color = _material.GetColor(_colorFieldName);
            }

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            var alpha = Mathf.Lerp(AlphaOff, AlphaOn, t);
            _color.a = alpha;

            if (_material != null)
            {
                _material.SetColor(_colorFieldName, _color);
            }
        }
    }
}
