﻿using UnityEngine;

namespace MofaLib
{
    [RequireComponent(typeof(Light))]
    public class LightIntensityInterpolator : InterpolatorBase
    {
        public Vector2 IntensityOffOn;

        protected Light _light;

        private void Start()
        {
            _light = GetComponent<Light>();

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            _light.intensity = Mathf.Lerp(IntensityOffOn.x, IntensityOffOn.y, t);
        }
    }
}
