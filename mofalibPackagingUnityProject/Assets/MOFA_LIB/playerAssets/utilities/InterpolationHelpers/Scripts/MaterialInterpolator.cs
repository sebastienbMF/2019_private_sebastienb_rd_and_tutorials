﻿using UnityEngine;

namespace MofaLib
{
    /// <summary>
    /// Modifies the first material assigned to a renderer.
    /// </summary>
    public abstract class MaterialInterpolator : InterpolatorBase
    {
        public Renderer MaterialRenderer;
        [Tooltip("If the component is attached to an object with a renderer, you can "
            + "auto assign it with this bool or drag and drop the desired renderer.")]
        public bool AutoUseThisRenderer = true;
        [Tooltip("CAUTION Will affect the shared material instead of the renderer's one.")]
        public bool UseSharedMaterial;

        protected Material _material;

        protected virtual void Start()
        {
            var renderer = AutoUseThisRenderer ? GetComponent<Renderer>() : MaterialRenderer;

            if (renderer == null)
            {
                Debug.LogWarningFormat("No renderer assigned to Material Updater on object {0} or "
                    + "AutoUseThisRenderer is ticked but there is no renderer on the object.", gameObject.name);
            }
            else
            {
                _material = UseSharedMaterial ? renderer.sharedMaterial : renderer.material;
            }
        }
    }
}
