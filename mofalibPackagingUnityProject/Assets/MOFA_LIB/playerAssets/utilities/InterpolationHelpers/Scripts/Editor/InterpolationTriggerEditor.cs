﻿using UnityEditor;

namespace MofaLib
{
    [CustomEditor(typeof(InterpolationTrigger))]
    [CanEditMultipleObjects]
    public class InterpolationTriggerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var trigger = target as InterpolationTrigger;

            serializedObject.Update();
            trigger.TagStr = EditorGUILayout.TagField("Trigger Tag", trigger.TagStr);
            DrawDefaultInspector();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
