﻿using UnityEditor;

namespace MofaLib
{
    [CustomEditor(typeof(InterpolationToggleTrigger))]
    [CanEditMultipleObjects]
    public class InterpolationToggleTriggerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var trigger = target as InterpolationToggleTrigger;

            serializedObject.Update();
            trigger.TagStr = EditorGUILayout.TagField("Trigger Tag", trigger.TagStr);
            DrawDefaultInspector();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
