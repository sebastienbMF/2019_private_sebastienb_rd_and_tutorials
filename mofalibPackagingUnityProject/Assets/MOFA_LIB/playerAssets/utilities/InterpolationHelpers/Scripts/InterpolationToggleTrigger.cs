﻿using UnityEngine;

namespace MofaLib
{
    /// <summary>
    /// A trigger that will toggle the TInterpolation value.
    /// If it was 0 it will change to 1, and vice versa.
    /// This does not use the interpolation curve of the interpolator.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class InterpolationToggleTrigger : MonoBehaviour
    {
        public InterpolationBase Interpolator;

        // Tag that will trigger.
        [HideInInspector]
        public string TagStr = "Untagged";

        private bool _isOn;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(TagStr))
            {
                _isOn = !_isOn;
                Interpolator.TInterpolation = _isOn ? 1.0f : 0.0f;
            }
        }
    }
}
