﻿using UnityEngine;

namespace MofaLib
{
    public class MaterialEmissionInterpolator : MaterialInterpolator
    {
        [Space(5)]
        public Color EmissionColorOn;
        public float EmissionIntensityOn;

        public Color EmissionColorOff;
        public float EmissionIntensityOff;

        protected override void Start()
        {
            base.Start();

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            var intensity = Mathf.Lerp(EmissionIntensityOff, EmissionIntensityOn, t);
            var color = Vector4.Lerp(EmissionColorOff, EmissionColorOn, t);

            if (_material != null)
            {
                _material.SetColor("_EmissionColor", color * Mathf.Pow(2.0f, intensity));
            }
        }
    }
}
