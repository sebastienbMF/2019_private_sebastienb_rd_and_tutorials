﻿using System.Collections.Generic;

using UnityEngine;

namespace MofaLib
{
    /// <summary>
    /// Will make TInterpolation loop from 0 to 1 every Duration seconds
    /// for EVERY interpolator/controller attached to the same object.
    /// </summary>
    [RequireComponent(typeof(InterpolationBase))]
    public class InterpolationLoop : MonoBehaviour
    {
        [Tooltip("Will make TInterpolation loop from 0 to 1 every Duration seconds.")]
        public float Duration = 1.0f;

        private List<InterpolationBase> _interpolators;
        private float _t = 0.0f;

        void Start()
        {
            _interpolators = new List<InterpolationBase>(GetComponents<InterpolationBase>());
        }

        // Update is called once per frame
        void Update()
        {
            _t = Mathf.Repeat(_t + Time.deltaTime / Duration, 1.0f);

            for (int i = 0; i < _interpolators.Count; i++)
            {
                _interpolators[i].TInterpolation = _t;
            }
        }

        public void RestartLoop()
        {
            _t = 0;
        }
    }
}
