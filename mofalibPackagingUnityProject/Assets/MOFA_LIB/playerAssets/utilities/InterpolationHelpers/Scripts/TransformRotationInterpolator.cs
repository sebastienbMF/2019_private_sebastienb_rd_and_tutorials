﻿using UnityEngine;

namespace MofaLib
{
    /// <summary>
    /// Interpolates the rotation of the object.
    /// </summary>
    public class TransformRotationInterpolator : InterpolatorBase
    {
        [Tooltip("Work with rotation in object ( = local) space or world space.")]
        public bool UseLocal = true;
        [Space(5)]
        public bool UseStartRotationAsOrigin = true;
        public Vector3 OriginRotation;
        public bool UseStartRotationAsTarget = false;
        public Vector3 TargetRotation;

        private Quaternion _originRotation;
        private Quaternion _targetRotation;

        private void Start()
        {
            // Rotations
            if (UseStartRotationAsOrigin)
            {
                _originRotation = UseLocal ? transform.localRotation : transform.rotation;
            }
            else
            {
                _originRotation = Quaternion.Euler(OriginRotation);
            }
            if (UseStartRotationAsTarget)
            {
                _targetRotation = UseLocal ? transform.localRotation : transform.rotation;
            }
            else
            {
                _targetRotation = Quaternion.Euler(TargetRotation);
            }

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            if (!UseLocal)
            {
                transform.rotation = Quaternion.Lerp(_originRotation, _targetRotation, t);
            }
            else
            {
                transform.localRotation = Quaternion.Lerp(_originRotation, _targetRotation, t);
            }
        }
    }
}
