﻿using UnityEngine;

namespace MofaLib
{
    public class TransformGlobalInterpolator : InterpolatorBase
    {
        public bool UseLocal = true;

        [Header("Position")]
        public bool UseStartPositionAsOrigin = true;
        public Vector3 OriginPosition;
        public bool UseStartPositionAsTarget = false;
        public Vector3 TargetPosition;

        [Header("Rotation")]
        public bool UseStartRotationAsOrigin = true;
        public Vector3 OriginRotation;
        public bool UseStartRotationAsTarget = false;
        public Vector3 TargetRotation;

        [Header("Scale")]
        public bool UseStartScaleAsOrigin = true;
        public Vector3 OriginScale = Vector3.one;
        public bool UseStartScaleAsTarget = false;
        public Vector3 TargetScale = Vector3.one;

        private Quaternion _originRotation;
        private Quaternion _targetRotation;

        private void Start()
        {
            // Positions
            if (UseStartPositionAsOrigin)
            {
                OriginPosition = UseLocal ? transform.localPosition : transform.position;
            }
            if (UseStartPositionAsTarget)
            {
                TargetPosition = UseLocal ? transform.localPosition : transform.position;
            }

            // Rotations
            if (UseStartRotationAsOrigin)
            {
                _originRotation = UseLocal ? transform.localRotation : transform.rotation;
            }
            else
            {
                _originRotation = Quaternion.Euler(OriginRotation);
            }
            if (UseStartRotationAsTarget)
            {
                _targetRotation = UseLocal ? transform.localRotation : transform.rotation;
            }
            else
            {
                _targetRotation = Quaternion.Euler(TargetRotation);
            }

            // Scales
            if (UseStartScaleAsOrigin)
            {
                OriginScale = transform.localScale;
            }
            if (UseStartScaleAsTarget)
            {
                TargetScale = transform.localScale;
            }

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            if (!UseLocal)
            {
                transform.position = Vector3.Lerp(OriginPosition, TargetPosition, t);
                transform.rotation = Quaternion.Lerp(_originRotation, _targetRotation, t);
            }
            else
            {
                transform.localPosition = Vector3.Lerp(OriginPosition, TargetPosition, t);
                transform.localRotation = Quaternion.Lerp(_originRotation, _targetRotation, t);
            }

            transform.localScale = Vector3.Lerp(OriginScale, TargetScale, t);
        }
    }
}
