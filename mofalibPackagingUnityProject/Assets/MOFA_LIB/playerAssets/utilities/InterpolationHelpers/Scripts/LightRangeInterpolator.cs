﻿using UnityEngine;

namespace MofaLib
{
    [RequireComponent(typeof(Light))]
    public class LightRangeInterpolator : InterpolatorBase
    {
        public Vector2 RangeOffOn;

        protected Light _light;

        private void Start()
        {
            _light = GetComponent<Light>();

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            _light.range = Mathf.Lerp(RangeOffOn.x, RangeOffOn.y, t);
        }
    }
}
