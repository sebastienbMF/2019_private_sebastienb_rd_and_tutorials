﻿using UnityEngine;

namespace MofaLib
{
    public class MaterialGradientInterpolator : MaterialInterpolator
    {
        [Space(5)]
        public Gradient ColourGradient;
        [Tooltip("Use for the emission colour instead of the base colour of the material.")]
        public bool UseForEmission;
        public Vector2 EmissionIntensityOffOn;

        private string _colorFieldName;

        protected override void Start()
        {
            base.Start();

            _colorFieldName = UseForEmission ? "_EmissionColor" : "_Color";

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            if (_material != null)
            {
                var intensity = Mathf.Lerp(EmissionIntensityOffOn.x, EmissionIntensityOffOn.y, t);
                _material.SetColor(_colorFieldName, ColourGradient.Evaluate(t) * Mathf.Pow(2.0f, intensity));
            }
        }
    }
}