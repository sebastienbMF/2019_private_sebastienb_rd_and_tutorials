﻿using UnityEngine;

namespace MofaLib
{
    /// <summary>
    /// Interpolates the scale of the object. Uses the local scale.
    /// </summary>
    public class TransformScaleInterpolator : InterpolatorBase
    {
        public bool UseStartScaleAsOrigin = true;
        public Vector3 OriginScale = Vector3.one;
        public bool UseStartScaleAsTarget = false;
        public Vector3 TargetScale = Vector3.one;

        private void Start()
        {
            // Scales
            if (UseStartScaleAsOrigin)
            {
                OriginScale = transform.localScale;
            }
            if (UseStartScaleAsTarget)
            {
                TargetScale = transform.localScale;
            }

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            transform.localScale = Vector3.Lerp(OriginScale, TargetScale, t);
        }
    }
}
