﻿using UnityEngine;

namespace MofaLib
{
    /// <summary>
    /// Base class for interpolation.
    /// It's the base for The InterpolatorBase and the InterpolationController.
    /// This way other components can modify the interpolation factor without
    /// discrimination for a simple interpolator or a controller (= a list of interpolators).
    /// </summary>
    public abstract class InterpolationBase : MonoBehaviour
    {
        [Tooltip("In an Interpolator, this value is used to read in the curve. " +
            "In a Controller, it's propagated to the Interpolators in the list to read in " +
            "their respective interpolation curves.")]
        [Range(0, 1)]
        public float TInterpolation;
    }
}
