﻿using System.Collections;

using UnityEngine;

namespace MofaLib
{
    /// <summary>
    /// Will trigger a transition of TInterpolation from 0 to 1 if an object
    /// with the correct tag enters the trigger.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class InterpolationTrigger : MonoBehaviour
    {
        [Tooltip("The interpolator affected by this trigger (in case several "
            + "interpolators are on the same gameobject).")]
        public InterpolationBase Interpolator;
        [Tooltip("Transition duration in seconds.")]
        public float Duration = 5;
        public bool TriggerInverseCurve = false;

        [HideInInspector]
        public string TagStr = "Untagged";

        private Coroutine _currentCoroutine;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(TagStr))
            {
                if (_currentCoroutine != null)
                {
                    StopCoroutine(_currentCoroutine);
                }
                _currentCoroutine = StartCoroutine(TransitionCoroutine());
            }
        }

        private IEnumerator TransitionCoroutine()
        {
            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / Duration)
            {
                Interpolator.TInterpolation = TriggerInverseCurve ? 1.0f - t : t;
                yield return null;
            }

            Interpolator.TInterpolation = TriggerInverseCurve ? 0.0f : 1.0f;

            _currentCoroutine = null;
        }
    }
}
