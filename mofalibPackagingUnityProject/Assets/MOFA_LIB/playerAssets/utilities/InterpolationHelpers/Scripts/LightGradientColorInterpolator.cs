﻿using UnityEngine;

namespace MofaLib
{
    [RequireComponent(typeof(Light))]
    public class LightGradientColorInterpolator : InterpolatorBase
    {
        public Gradient ColourGradient;

        protected Light _light;

        private void Start()
        {
            _light = GetComponent<Light>();

            DoInterpolation(0);
        }

        protected override void DoInterpolation(float t)
        {
            _light.color = ColourGradient.Evaluate(t);
        }
    }
}
