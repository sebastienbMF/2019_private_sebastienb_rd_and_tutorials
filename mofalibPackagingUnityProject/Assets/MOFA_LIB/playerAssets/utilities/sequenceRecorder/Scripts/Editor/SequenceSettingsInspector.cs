﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using MofaLib;

[CustomEditor(typeof(SequenceSettings))]
[CanEditMultipleObjects]
public class SequenceSettingsInspector : Editor {

    const string _tooltipSceneName = "Should be the EXACT name of the scene.";
    const string _tooltipDuration = "Seqence duration in seconds.";
    const string _tooltipFramerate = "The target framerate of the sequence.";
    const string _tooltipPreroll = "The duration for which the scene should play before starting the recording";

	public override void OnInspectorGUI()
	{
		SequenceSettings myTarget = (SequenceSettings)target;
		
		Undo.RecordObject(myTarget, "Sequence Settings change");
		myTarget.SceneName = EditorGUILayout.TextField(new GUIContent("Unity Scene Name:", _tooltipSceneName), myTarget.SceneName);
		myTarget.SequenceName = EditorGUILayout.TextField("Sequence Name:", myTarget.SequenceName);
		myTarget.SequenceDuration = EditorGUILayout.FloatField(new GUIContent("Sequence Duration (s):", _tooltipDuration), myTarget.SequenceDuration);
		myTarget.Framerate = EditorGUILayout.IntField(new GUIContent("Sequence Framerate:", _tooltipFramerate), myTarget.Framerate);
		myTarget.RecordingDelay = EditorGUILayout.FloatField(new GUIContent("Preroll (s):", _tooltipPreroll), myTarget.RecordingDelay);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		GUILayout.Space(10);
        EditorGUILayout.LabelField("Path overrides:");
        GUILayout.BeginHorizontal();
        myTarget.ImgSequencePathOverride =  GUILayout.Toggle(myTarget.ImgSequencePathOverride, "Img Sequence");
        myTarget.MediaEncoderOverride =  GUILayout.Toggle(myTarget.MediaEncoderOverride, "Encoder");
        myTarget.XAgoraMediaPathOverride =  GUILayout.Toggle(myTarget.XAgoraMediaPathOverride, "Xagora Media");
        myTarget.BackupPathOverride =  GUILayout.Toggle(myTarget.BackupPathOverride, "Media backup");

        GUILayout.EndHorizontal();

        if(myTarget.ImgSequencePathOverride)
        {
            GUILayout.BeginHorizontal();
            if(myTarget.ImgSequencePath != "")
            {
                if(GUILayout.Button("o", GUILayout.Width(20)))
                {
                    Process.Start("explorer.exe", myTarget.ImgSequencePath.Replace(@"/",@"\"));
                }
            }
            string buttonDisplay = "Img Sequence Path";
            if(myTarget.ImgSequencePath != "")
            {
                buttonDisplay = myTarget.ImgSequencePath;
            }
            if(GUILayout.Button(buttonDisplay))
            {
                myTarget.ImgSequencePath = EditorUtility.OpenFolderPanel("Img Sequence Path", "", "");
            }
            GUILayout.EndHorizontal();
        }

        if(myTarget.MediaEncoderOverride)
        {
            GUILayout.BeginHorizontal();
            if(myTarget.MediaEncoderPath != "")
            {
                if(GUILayout.Button("o", GUILayout.Width(20)))
                {
                    Process.Start("explorer.exe", myTarget.MediaEncoderPath.Replace(@"/",@"\"));
                }
            }
            string buttonDisplay = "Media Encoder Path";
            if(myTarget.MediaEncoderPath != "")
            {
                buttonDisplay = myTarget.MediaEncoderPath;
            }
            if(GUILayout.Button(buttonDisplay))
            {
                myTarget.MediaEncoderPath = EditorUtility.OpenFolderPanel("Img Sequence Path", "", "");
            }
            GUILayout.EndHorizontal();
        }
        
        if(myTarget.XAgoraMediaPathOverride)
        {
            GUILayout.BeginHorizontal();
            if(myTarget.XAgoramediaPath != "")
            {
                if(GUILayout.Button("o", GUILayout.Width(20)))
                {
                    Process.Start("explorer.exe", myTarget.XAgoramediaPath.Replace(@"/",@"\"));
                }
            }
            string buttonDisplay = "XAgora Media Path";
            if(myTarget.XAgoramediaPath != "")
            {
                buttonDisplay = myTarget.XAgoramediaPath;
            }
            if(GUILayout.Button(buttonDisplay))
            {
                myTarget.XAgoramediaPath = EditorUtility.OpenFolderPanel("XAgora Media Path", "", "");
            }
            GUILayout.EndHorizontal();
        }

        if(myTarget.BackupPathOverride)
        {
            GUILayout.BeginHorizontal();
            if(myTarget.BackupMediaPath != "")
            {
                if(GUILayout.Button("o", GUILayout.Width(20)))
                {
                    Process.Start("explorer.exe", myTarget.BackupMediaPath.Replace(@"/",@"\"));
                }
            }
            string buttonDisplay = "Backup Media Path";
            if(myTarget.BackupMediaPath != "")
            {
                buttonDisplay = myTarget.BackupMediaPath;
            }
            if(GUILayout.Button(buttonDisplay))
            {
                myTarget.BackupMediaPath = EditorUtility.OpenFolderPanel("Backup Media Path", "", "");
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(30);

        GUI.color = Color.red;
        if (GUILayout.Button("CLEAR ALL PATHS"))
        {
            if(EditorUtility.DisplayDialog("Clear all settings paths", "Are you sure you want to delete all paths for " + myTarget.name + " ?", "Yes", "No"))
            {
                myTarget.ClearOverrides();
            }
        }
        EditorUtility.SetDirty(myTarget);

	}

    
}
