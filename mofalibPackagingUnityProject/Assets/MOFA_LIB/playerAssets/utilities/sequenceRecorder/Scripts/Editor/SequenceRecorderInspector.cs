﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEditor;
using MofaLib;

[CustomEditor(typeof(SequenceRecorder))]
public class SequenceRecorderInspector : Editor 
{
	public override void OnInspectorGUI()
	{
		SequenceRecorder myTarget = (SequenceRecorder)target;

		if(myTarget.ToRender == null || myTarget.ToRender.Length != myTarget.SequenceSettings.Length)
		{
            myTarget.ToRender = new bool[myTarget.SequenceSettings.Length];
		}

   		GUILayout.Space(20);
        GUILayout.Label("-- QUICK ACCESS --");

       

        GUILayout.BeginHorizontal();

        if(myTarget.ImgSequencePath != "")
        {
            if(GUILayout.Button("Img Sequence Folder"))
            {
                Process.Start("explorer.exe", myTarget.ImgSequencePath.Replace(@"/",@"\"));
            }
        }

        if(myTarget.EncoderPath != "")
        {
            if(GUILayout.Button("Encoder Folder"))
            {
                 Process.Start("explorer.exe", myTarget.EncoderPath.Replace(@"/",@"\"));
            }
        }

        if(myTarget.XagoraMediaPath != "")
        {
            if(GUILayout.Button("Media Folder"))
            {
                Process.Start("explorer.exe", myTarget.XagoraMediaPath.Replace(@"/",@"\"));
            }
        }

        if(myTarget.XagoraMediaBackupPath != "")
        {
            if(GUILayout.Button("Backup Folder"))
            {
                Process.Start("explorer.exe", myTarget.XagoraMediaBackupPath.Replace(@"/",@"\"));
            }
        }

        GUILayout.EndHorizontal();
        GUILayout.Space(10);
        myTarget.ImgSequencePath = EditorGUILayout.TextField("Img Sequence Path: ", myTarget.ImgSequencePath);

        if(myTarget.UseXagoraEncoder)
        {
            myTarget.EncoderPath = EditorGUILayout.TextField("Encoder Path: ", myTarget.EncoderPath);
            myTarget.XagoraMediaPath = EditorGUILayout.TextField("Media Path: ", myTarget.XagoraMediaPath);
            myTarget.XagoraMediaBackupPath = EditorGUILayout.TextField("Media Backup Path: ", myTarget.XagoraMediaBackupPath);
        }
		DrawDefaultInspector();
        GUILayout.Space(10);

        if(GUILayout.Button("Add scenes to build settings"))
        {
            AddScenesToBuild(myTarget);
        }

		GUILayout.Space(10);

		if(GUILayout.Button("Select All"))
		{
			for(int i = 0; i< myTarget.SequenceSettings.Length; i++)
			{
				myTarget.ToRender[i] = true;
			}
		}

		if(GUILayout.Button("Unselect All"))
		{
			for(int i = 0; i< myTarget.SequenceSettings.Length; i++)
			{
				myTarget.ToRender[i] = false;
			}
		}
		GUILayout.Space(10);


		if(myTarget.SequenceSettings != null && myTarget.SequenceSettings.Length > 0)
		{
			for(int i = 0; i< myTarget.SequenceSettings.Length; i++)
			{
                if(myTarget.SequenceSettings[i] != null)
                {
                    var currentSettings = myTarget.SequenceSettings[i];
                    GUILayout.BeginHorizontal();
                    if(currentSettings.HasOverride())
                    {
                        GUI.color = Color.cyan;
                    }
                    myTarget.ToRender[i] = EditorGUILayout.Toggle(" ", myTarget.ToRender[i], GUILayout.ExpandWidth (false));
                    EditorGUILayout.LabelField(myTarget.SequenceSettings[i].SequenceName, GUILayout.ExpandWidth (false));
                    GUILayout.EndHorizontal();
                    GUI.color = Color.white;
                }
			}
		}

		GUILayout.Space(20);

		if(GUILayout.Button("Record Sequence"))
		{
			myTarget.StartRecording();
		}

		if(GUILayout.Button("Stop Recording"))
		{
			myTarget.StopRecording();
		}

		GUILayout.Space(20);

        EditorGUILayout.LabelField("-- PROGRESS --");
		EditorGUILayout.LabelField("Frames to render: " + myTarget.FrameIndex + "/" + myTarget.TotalFramesToRender);
		EditorGUILayout.LabelField("Sequences to render: " + myTarget.SequenceIndex + "/" + myTarget.SequenceNumber);

		GUILayout.Space(20);

		if(myTarget.FinishedSequences != null)
		{
			for(int i = 0; i< myTarget.FinishedSequences.Count; i++)
			{
				string key = myTarget.FinishedSequences.ElementAtOrDefault(i).Key;
				if(GUILayout.Button("open " + key + " folder"))
				{
					string path = myTarget.FinishedSequences[key];
					path = path.Replace(@"/", @"\");
					System.Diagnostics.Process.Start("explorer.exe", "/select," + path);
				}
			}
		}

		GUILayout.Space(20);

		if(myTarget.FinishedSequences != null && myTarget.FinishedSequences.Count > 0)
		{
			if(GUILayout.Button("open all folders"))
			{
				for(int i = 0; i< myTarget.FinishedSequences.Count; i++)
				{
					string key = myTarget.FinishedSequences.ElementAtOrDefault(i).Key;
					string path = myTarget.FinishedSequences[key];
					path = path.Replace(@"/", @"\");
					System.Diagnostics.Process.Start("explorer.exe", "/select," + path);
				}
			}
		}
	}

    void AddScenesToBuild(SequenceRecorder target)
    {
        string[] scenesGUIDs = AssetDatabase.FindAssets("t:Scene");
        string[] scenesPaths = new string[scenesGUIDs.Length];
        for(int i = 0; i< scenesGUIDs.Length; i++)
        {
            scenesPaths[i] =  AssetDatabase.GUIDToAssetPath(scenesGUIDs[i]);
        }
        
        List<string> pathsToAdd = new List<string>();
        for(int i = 0 ; i< target.SequenceSettings.Length; i++)
        {
            for(int j = 0; j < scenesPaths.Length; j++)
            {
                string[] splitPath = scenesPaths[j].Split('/');
                if(splitPath[splitPath.Length - 1] == target.SequenceSettings[i].SceneName + ".unity")
                {
                    pathsToAdd.Add(scenesPaths[j]);
                }
            }
        }
        
        List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();
        for(int i = 0; i < pathsToAdd.Count; i++)
        {
            UnityEngine.Debug.Log("Adding to build settings: " + pathsToAdd[i]);
            editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(pathsToAdd[i], true));
        }
         EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();
    }

}


