﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MofaLib
{
    [Serializable]
    [CreateAssetMenu(fileName = "Image Sequence Settings", menuName = "SequenceRecorder/SequenceSettings", order = 1)]
    public class SequenceSettings: ScriptableObject
    {
        public string SceneName;
        public string SequenceName;
        public float SequenceDuration;
        public int Framerate;
        public float RecordingDelay;


        private bool _mediaEncoderOverride;
        public bool MediaEncoderOverride {get {return _mediaEncoderOverride;} set {_mediaEncoderOverride = value;}}
        
        private bool _xAgoraMediaPathoverride;
        public bool XAgoraMediaPathOverride {get {return _xAgoraMediaPathoverride;} set {_xAgoraMediaPathoverride = value;}}
        
        private bool _imgSequencePathOverride;
        public bool ImgSequencePathOverride {get {return _imgSequencePathOverride;} set {_imgSequencePathOverride = value;}}
        
        private bool _backupMediaPathOverride;
        public bool BackupPathOverride {get {return _backupMediaPathOverride;} set {_backupMediaPathOverride = value;}}

        public string ImgSequencePath;
        public string MediaEncoderPath;
        public string XAgoramediaPath;
        public string BackupMediaPath;

        public void ClearOverrides()
        {
            XAgoraMediaPathOverride = false;
            ImgSequencePathOverride = false;
            MediaEncoderOverride = false;
            BackupPathOverride = false;

            ImgSequencePath = "";
            MediaEncoderPath = "";
            XAgoramediaPath = "";
            BackupMediaPath= "";
        }

        public bool HasOverride()
        {
            if(_mediaEncoderOverride)
            {
                return true;
            }

            if(_xAgoraMediaPathoverride)
            {
                return true;
            }

            if(_imgSequencePathOverride)
            {
                return true;
            }

            if(_backupMediaPathOverride)
            {
                return true;
            }
            return false;   
        }
    }
}
