﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SRTranslator : MonoBehaviour {
	
	public float Amp;
	// Update is called once per frame
	void Update () 
	{
		float pos = Mathf.Sin(Time.time) * Amp;
		transform.position = new Vector3(pos, 0,0);
	}
}
