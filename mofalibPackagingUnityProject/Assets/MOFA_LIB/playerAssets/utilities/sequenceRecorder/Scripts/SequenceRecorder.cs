﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;


namespace MofaLib
{
    public class SequenceRecorder : MonoBehaviour {

        [HideInInspector]
        public string ImgSequencePath;
        [HideInInspector]
        public string EncoderPath;
        [HideInInspector]
        public string XagoraMediaPath;
        [HideInInspector]
        public string XagoraMediaBackupPath;

        [Space(10)]
        public int LowrezWidth;
        public int LowrezHeight;

        [Space(10)]
        public bool UseXagoraEncoder = true;

        [Space (20)]
        public SequenceSettings[] SequenceSettings;
        public int SequenceNumber { get{ if(SequenceSettings != null) { return SequenceSettings.Length; } return 0;}}
        public Dictionary<string, string> FinishedSequences;


        private string _folderPath;
        private bool _ongoingRecordingProcess;
        private bool _record;

        // for showing status in inspector
        private int _frameIndex;
        public int FrameIndex { get {return _frameIndex;} }
        private int _totalFramesToRender;
        public int TotalFramesToRender {get {return _totalFramesToRender;}}
        private int _sequenceIndex;
        public int SequenceIndex {get {return _sequenceIndex;}}

        public Action RecordDelegate;
        public Action StopRecordDelegate;

        [HideInInspector]
        public bool[] ToRender;

        void OnEnable()
        {
                    print("Sequence recorder enable????? ++++++++++++++++++++++++++++++++++++ ");

            DontDestroyOnLoad(this.gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;
            FinishedSequences = new Dictionary<string, string>();
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        public void StartRecording()
        {
            if(_ongoingRecordingProcess == true)
            {
                Debug.LogWarning("Already Recording!");
                return;
            }
            if(FinishedSequences != null && FinishedSequences.Count > 0)
            {
                FinishedSequences.Clear();
            }
            _ongoingRecordingProcess = true;
            _sequenceIndex = 0;
            bool firstSequenceFound = false;
            for(int i = 0; i< SequenceSettings.Length; i++)
            {
                if(ToRender[i])
                {
                    firstSequenceFound = true;
                    _sequenceIndex = i;
                    break;
                }
            }
            if(!firstSequenceFound)
            {
                Debug.Log("Nothing To Render. Check your settings...");
                return;
            }
            _frameIndex = 0;
            ReadSettings();
        }

        public void StopRecording()
        {
            _record = false;
            _ongoingRecordingProcess = false;
        }

        void ReadSettings()
        {
            SequenceSettings currentSequence = SequenceSettings[_sequenceIndex];

            string sequenceMainPath = ImgSequencePath;
            if(currentSequence.ImgSequencePathOverride && currentSequence.ImgSequencePath != "")
            {
                sequenceMainPath = currentSequence.ImgSequencePath;
            }

            _folderPath = sequenceMainPath + "/" + currentSequence.SequenceName + "_" + DateTime.Now.ToString("yyyy_MM_dd_HHmm");
            Directory.CreateDirectory(_folderPath);
            Time.captureFramerate = currentSequence.Framerate;
            _totalFramesToRender = Mathf.CeilToInt(currentSequence.SequenceDuration * currentSequence.Framerate);
            if(SceneManager.GetActiveScene().name == currentSequence.SceneName)
            {
                StartCoroutine(WaitBeforeRecording());
                return;
            }
            LoadScene();
        }

        IEnumerator WaitBeforeRecording()
        {
            yield return new WaitForSeconds(SequenceSettings[_sequenceIndex].RecordingDelay);
            _record = true;
        }

        void Update()
        {
            if(_record)
            {
                RecordSequenceFrame();
            }
        }
        
        void RecordSequenceFrame()
        {
            string name = string.Format("{0}/{1}_{2:D5}.png", _folderPath, SequenceSettings[_sequenceIndex].SequenceName, _frameIndex);
            print (name);
            ScreenCapture.CaptureScreenshot(name);
            _frameIndex += 1;
            if(_frameIndex == _totalFramesToRender)
            {
                EndOfSequence();
            }
        }

        void EndOfSequence()
        {
            _record = false;
            FinishedSequences.Add(SequenceSettings[_sequenceIndex].SequenceName, _folderPath);
            if(UseXagoraEncoder)
            {
                EncodeForXagora(_sequenceIndex);
            }
            _sequenceIndex += 1;
            // look for next sequence to record....
            bool nextSequenceFound = false;
            for(int i = _sequenceIndex; i < SequenceSettings.Length; i++)
            {
                if(ToRender[i])
                {
                    nextSequenceFound = true;
                    _sequenceIndex = i;
                    break;
                }
            }
            if(nextSequenceFound)
            {
                if(_sequenceIndex < SequenceSettings.Length)
                {
                    _frameIndex = 0;
                    ReadSettings();
                    return;
                }
            }
            
            Debug.Log("Nothing Else to render...");
            _ongoingRecordingProcess = false;
            Debug.Log("RECORDING FINISHED!");

        }

        void EncodeForXagora(int isequenceIndex)
        {
            SequenceSettings currentSequence = SequenceSettings[isequenceIndex];

            string outputPath = FinishedSequences[currentSequence.SequenceName] + "/output";
            outputPath = outputPath.Replace('/', '\\');

            string sequenceEncoderPath = EncoderPath;
            if(currentSequence.MediaEncoderOverride && currentSequence.MediaEncoderPath != "")
            {
                sequenceEncoderPath = currentSequence.MediaEncoderPath;
            }

            StartCoroutine(SequenceMediaEncoder.StartConversion(LowrezWidth, LowrezHeight, 
                                                        sequenceEncoderPath,
                                                        FinishedSequences[currentSequence.SequenceName] + "\\" + currentSequence.SequenceName,
                                                        outputPath,
                                                        currentSequence.SequenceName,
                                                        currentSequence.Framerate));


    // set media backup path
            string sequenceBackupPath = XagoraMediaBackupPath;
            if(currentSequence.BackupPathOverride && currentSequence.BackupMediaPath != "")
            {
                sequenceBackupPath = currentSequence.BackupMediaPath;
            }
            SequenceMediaEncoder.DirectoryToCopy(outputPath, sequenceBackupPath, true);

    // set xagora media path
            string sequenceXagoraMediaPath = XagoraMediaPath;
            if(currentSequence.XAgoraMediaPathOverride && currentSequence.XAgoramediaPath != "")
            {
                sequenceXagoraMediaPath = currentSequence.XAgoramediaPath;
            }

            SequenceMediaEncoder.DirectoryToCopy(outputPath, sequenceXagoraMediaPath);
        }

        void LoadScene()
        {
            SceneManager.LoadScene(SequenceSettings[_sequenceIndex].SceneName);
        }

        // This will disable every GameObjects with the GameObjectDisabler component on them upon scene loading. 
        // Usefull to remove stuff that you don't want to be active while rendering.
        public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if(scene.name == SequenceSettings[_sequenceIndex].SceneName)
            {
                GameObjectDisabler[] toDisable = GameObject.FindObjectsOfType(typeof (GameObjectDisabler)) as GameObjectDisabler[];
                if(toDisable != null && toDisable.Length > 0)
                {
                    for(int i = 0; i< toDisable.Length; i++)
                    {
                        toDisable[i].DisableObject();
                    }
                    print("Disabled " + toDisable.Length + " objects before rendering.");
                }
                print("Scene loaded successfully");
                StartCoroutine(WaitBeforeRecording());
            }
        }
    }
}


