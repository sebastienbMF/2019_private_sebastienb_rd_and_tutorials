﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Text;


namespace MofaLib
{
	public class MofalibPaths : IEquatable<MofalibPaths> , IComparable<MofalibPaths>
	{
		public string path = "";
		public string folderPath = "";
		public string name = "";
		public string category = "";
		public string preview = "";
		public string link = "";
		public string videoLink = "";

		public bool Equals(MofalibPaths other) 
		{
			if (other == null)
			{
				return false;
			}

			if (this.category == other.category)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		
		public int CompareTo(MofalibPaths comparePart)
		{
			// A null value means that this object is greater. 
			if (comparePart == null)
			{
				return 1;
			}
			else
			{
				return this.category.CompareTo(comparePart.category);
			}
		}
	}


	public class MofaLib_Browser : EditorWindow 
	{
		List<MofalibPaths> _packagesPaths = new List<MofalibPaths>();

		public string _MofaLibPath = "";
		public string MofaLibConfig = "";

		string _helpFile= "";
		string _googleDriveVideos = "";
		string _searchField = "";
		bool init = false;
		Vector2 scrollPosition = Vector2.zero;


		[MenuItem("MofaLib/MofaLib Package Browser")]

		public static void ShowWindow()
		{
			MofaLib_Browser window = (MofaLib_Browser)EditorWindow.GetWindow(typeof(MofaLib_Browser));
            window.titleContent = new GUIContent("LibBrowser");
			//window.maxSize = new Vector2 (500,1000);
			window.minSize = new Vector2 (530,300);
		}

		void Update()
		{
			if(!init)
			{
				Init();
			}
		}

		void Init()
		{
			string configPath = Application.dataPath + "/mofalibConfig.txt";
			if (File.Exists (configPath)) 
			{
				string text = System.IO.File.ReadAllText(configPath);
				_MofaLibPath = text;
			}

			if (_MofaLibPath != "")
			{
				SearchForPackages();
				GetHelpFileLink();
				GetDriveVideoLocationLink();
			}
			init = true;
		}
		
		void OnGUI()
		{
			scrollPosition = GUILayout.BeginScrollView (scrollPosition,false,true,GUILayout.Width(position.width),GUILayout.Height(position.height));
			if (GUILayout.Button ("MOFALIB PATH: " + _MofaLibPath,GUILayout.Height(30)))
			{
				OpenBrowser();
			}
			if (GUILayout.Button ("Search for packages",GUILayout.Height(25)))
			{
				SearchForPackages();
				GetHelpFileLink();
				GetDriveVideoLocationLink();
			}
			if (_helpFile != "")
			{
				if (GUILayout.Button ("MofaLib Documentation"))
				{
					EditorUtility.OpenWithDefaultApp(_helpFile);
				}
			}
			if (_googleDriveVideos != "")
			{
				if (GUILayout.Button ("Google Drive Video Preview Location"))
				{
					EditorUtility.OpenWithDefaultApp(_googleDriveVideos);
				}
			}

			// search tool
			GUILayout.BeginHorizontal ();
			GUILayout.Label ("Search Keywords: ",GUILayout.Width(150));	
			_searchField = GUILayout.TextField (_searchField, 100, GUILayout.Width(300));
			GUILayout.EndHorizontal ();

			// end serach tool

			if(_packagesPaths.Count != 0)
			{
				string category = "";
				foreach(MofalibPaths path in _packagesPaths)
				{
					if(path.category != category && path.name.ToLower().Contains(_searchField.ToLower()) )
					{
						GUILayout.BeginHorizontal();
						GUILayout.Label ("");	
						GUILayout.EndHorizontal();
						//
						GUILayout.BeginHorizontal();
						GUILayout.Label (path.category,EditorStyles.boldLabel);	
						GUILayout.EndHorizontal();
						category = path.category;
					}
					GUILayout.BeginHorizontal();

					if( path.name.ToLower().Contains(_searchField.ToLower()) || _searchField == "" )
					{
				// folder button
						if (GUILayout.Button ("F",GUILayout.Width(25)))
						{
							try
							{
								Process.Start("explorer.exe",(true ? "/root," : "/select,") + path.folderPath);
							}
							catch ( System.ComponentModel.Win32Exception e )
							{
								// tried to open win explorer in mac
								// just silently skip error
								// we currently have no platform define for the current OS we are in, so we resort to this
								e.HelpLink = ""; // do anything with this variable to silence warning about not using it
							}
						}
				// package button
						if (GUILayout.Button (path.name,GUILayout.Width(275)))
						{
                            if (!File.Exists(StatsCompilerTool.GetStatsFilePath()))
                            {
                                if (EditorUtility.DisplayDialog("Can't compile Stats", "Can't compile to MOFAlib Unity Stats. Please send a Email to pascal@momentfactory.ca with the infos", "Ok"))
                                {
                                    EditorUtility.OpenWithDefaultApp(path.path);
                                }
                            }
                            else
                            {
                                StatsCompilerPopupWindow.ShowStatsConfirmationDialogue(path.path, path.name);
                            }
                            //EditorUtility.OpenWithDefaultApp(path.path);
						}

				// video preview button
						if(path.videoLink != "")
						{
							if (GUILayout.Button ("V",GUILayout.Width(25)))
							{
								EditorUtility.OpenWithDefaultApp(path.videoLink);
							}
						}
						else
						{
							GUILayout.Button (" ",GUILayout.Width(25));
						}

				// image preview button
						if(path.preview != "")
						{
							if (GUILayout.Button ("I",GUILayout.Width(25)))
							{
								MofaLib_PreviewWindow.ShowPreviewWindow();
								MofaLib_PreviewWindow.LoadPreviewImage(path.preview);
							}
						}
						else 
						{
							GUILayout.Button (" ",GUILayout.Width(25));
						}

				// redmine link button
						if(path.link != "")
						{
							if (GUILayout.Button ("RedMine",GUILayout.Width(75)))
							{
								EditorUtility.OpenWithDefaultApp(path.link);
							}
						}
						else
						{
							GUILayout.Button (" ",GUILayout.Width(75));
						}
						GUILayout.EndHorizontal();
					}
					else
					{
						GUILayout.EndHorizontal();
					}
				}

                if(GUILayout.Button("Export Stats"))
                {
                    StatsCompilerTool.ExportStatsReport(_MofaLibPath);
                }
			}
			EditorGUILayout.EndScrollView();
		}

		void OpenBrowser()
		{
			_MofaLibPath = EditorUtility.OpenFolderPanel ("MofaLib Pacakge Browser", _MofaLibPath, "mofa_u3dlib");
			File.WriteAllText(Application.dataPath+@"\mofalibConfig.txt",_MofaLibPath);
		}

		void SearchForPackages()
		{
			_packagesPaths.Clear ();
			string[] dirPaths = Directory.GetFiles(_MofaLibPath,"*.unitypackage" ,SearchOption.AllDirectories);
			foreach(string path in dirPaths)
			{
				string aPath = path.Replace(@"\","/");
				string[] newPathName = aPath.Split('/');
				string[] fullName = newPathName[newPathName.Length-1].Split('.');
				MofalibPaths pathAndName = new MofalibPaths();
				if(aPath.Contains("editorAssets"))
				{
					pathAndName.category = "Editor Asset";
				}
				else
				{
					pathAndName.category = newPathName[newPathName.Length-2];
				}
				string previewImage = aPath.Replace("unitypackage","png");
				if(File.Exists(previewImage))
				{
					pathAndName.preview = previewImage;
				}
				string redMineLink = aPath.Replace("unitypackage","url");
				if(File.Exists(redMineLink))
				{
					pathAndName.link = redMineLink;
				}
				string videoLink = aPath.Replace(".unitypackage","_Video.url");
				if(File.Exists(videoLink))
				{
					pathAndName.videoLink = videoLink;
				}

				string theFolderPath = path.Remove(path.Length - newPathName[newPathName.Length-1].Length-1);
				theFolderPath = theFolderPath.Replace("/",@"\");
				pathAndName.folderPath = theFolderPath;
				pathAndName.path = aPath;
				pathAndName.name = fullName[0];
				if(!pathAndName.name.ToLower().Contains("mofalib browser"))
				{
					_packagesPaths.Add (pathAndName);
				}
			}
			_packagesPaths.Sort ();
		}

		void GetHelpFileLink()
		{
			string[] helpFiles = Directory.GetFiles(_MofaLibPath,"MofaLib Guide.url");
			_helpFile = helpFiles[0].Replace(@"\","/");
		}

		void GetDriveVideoLocationLink()
		{
			string[] videoLocation = Directory.GetFiles(_MofaLibPath,"DriveVideoPreview.url");
			_googleDriveVideos = videoLocation[0].Replace(@"\","/");
		}

		public static void ShowDisplayDialog(string title, string message)
		{
			EditorUtility.DisplayDialog (title, message, "ok");
		}
    }
}
	
public class MofaLib_PreviewWindow : EditorWindow 
{
	static Texture2D image;

	public static void ShowPreviewWindow()
	{
		MofaLib_PreviewWindow window = (MofaLib_PreviewWindow)EditorWindow.GetWindow(typeof(MofaLib_PreviewWindow));
        window.titleContent = new GUIContent("Preview");
	}

	void OnGUI()
	{
		//if(image != null)
		//{
			GUILayout.Label(image);
		//}
	}

	public static void LoadPreviewImage(string url)
	{
		WWW www = new WWW("file://"+ url);
		for (float t = 0; t <= 1; t+= Time.deltaTime)
		{
			if(www.isDone)
			{
				break;
			}
		}

		image = new Texture2D (www.texture.width, www.texture.height);
		www.LoadImageIntoTexture (image);
	}   
}

public class StatsCompilerPopupWindow : EditorWindow
{
    string _path;
    string _name;
    string _mfProjectCode = "";

    public static void ShowStatsConfirmationDialogue(string path, string packageName)
    {
        StatsCompilerPopupWindow window = ScriptableObject.CreateInstance<StatsCompilerPopupWindow>();
        window.position = new Rect(Screen.width / 2, Screen.height / 2, 250, 300);
        window.titleContent = new GUIContent("Confirm");
        window.Show();
        window._path = path;
        window._name = packageName;
    }

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.wordWrap = true;
        style.normal.textColor = Color.white;
        GUILayout.Space(10);

        GUILayout.Label(" Do you want to count the import of ", style);
        GUILayout.Space(5);

        GUILayout.Label("- " + this._name.ToUpper() + " -", style);
        GUILayout.Space(5);

        GUILayout.Label(" in the Mofalib Stats Compiler?", style);
        GUILayout.Space(30);

        //GUILayout.BeginHorizontal();
        GUILayout.Label("ENTER MOFA PROJECT CODE HERE");
        _mfProjectCode = GUILayout.TextField(_mfProjectCode);
        //GUILayout.EndHorizontal();

        if (GUILayout.Button("Yes!"))
        {
            SaveAndImportPackage();
        }
        GUILayout.Space(5);
        if (GUILayout.Button("Nope!"))
        {
            EditorUtility.OpenWithDefaultApp(_path);
            this.Close();
        }
        GUILayout.Space(30);
        if (GUILayout.Button("CANCEL"))
        {
            this.Close();
        }
    }
    void SaveAndImportPackage()
    {
        if (_mfProjectCode == null || _mfProjectCode == "")
        {
            if (EditorUtility.DisplayDialog("Where is the MF project code?", "Are you sure you you want to import package withtout adding a project code or name? this helps a lot for stats compiling...", "No Code", "Opps, let's add a code"))
            {
                _mfProjectCode = @"N/A";
                StatsCompilerTool.SaveDataUsage(_name, _mfProjectCode);
                EditorUtility.OpenWithDefaultApp(_path);
                this.Close();
            }
        }
        else
        {
            if (EditorUtility.DisplayDialog("Is Project Code OK?", "You Confirm the code " + _mfProjectCode.ToUpper() + " is good?", "Yes, it's perfect", "Opps, let's change it..."))
            {
                StatsCompilerTool.SaveDataUsage(_name, _mfProjectCode);
                EditorUtility.OpenWithDefaultApp(_path);
                this.Close();
            }
        }
    }
}

public static class StatsCompilerTool
{
	public static string GetStatsFilePath()
	{
			string configPath = Application.dataPath + "/mofalibConfig.txt";
			string mofalibPath = "";
			if (!File.Exists (configPath)) 
			{
				return null;
			}
			mofalibPath = System.IO.File.ReadAllText(configPath);
			string fullPath = mofalibPath + "/" + "MofaLibStatsFilePath.txt";
			if(File.Exists(fullPath))
			{
				return File.ReadAllText(mofalibPath + "/" + "MofaLibStatsFilePath.txt");
			}
			return null;
	}

    public static void SaveDataUsage(string packageName, string mfCode)
    {
        MofaLib_StatsData newData = new MofaLib_StatsData();
        newData.PackageName = packageName;
        newData.ImportDate = FormatDate();
        newData.PCName = SystemInfo.deviceName;
        newData.UnityProjectName = GetProjectName();
        newData.MofaCodeName = mfCode.ToUpper();

        MofaLib_JsonStatsList dataComp = new MofaLib_JsonStatsList();
        dataComp.UsageData = new List<MofaLib_StatsData>();
		string filePath = GetStatsFilePath();
		if(filePath == null)
		{
			EditorUtility.DisplayDialog("Write Stats", "Could not find the stats file: " + filePath, "Ok");
			return;
		}

        if (File.Exists(filePath))
        {
            string loadedJson = File.ReadAllText(filePath);
            dataComp = JsonUtility.FromJson<MofaLib_JsonStatsList>(loadedJson);
        }
        dataComp.UsageData.Add(newData);

        string jsonString = JsonUtility.ToJson(dataComp);
        File.WriteAllText(filePath, jsonString);
    }

    static string FormatDate()
    {
        DateTime date = DateTime.Now;
        string dateString = String.Format("{0:yyyy-MM-dd}", date);
        return dateString;
    }

    static string GetProjectName()
    {
        string[] s = Application.dataPath.Split('/');
        string projectName = s[s.Length - 2];
        return projectName;
    }

    public static void ExportStatsReport(string mofalibPath)
    {
		
        string filePath = GetStatsFilePath();
        if (!Directory.Exists(Application.streamingAssetsPath))
        {
            Directory.CreateDirectory(Application.streamingAssetsPath);
        }
        if (File.Exists(filePath))
        {
            string loadedJson = File.ReadAllText(filePath);
            MofaLib_JsonStatsList dataComp = JsonUtility.FromJson<MofaLib_JsonStatsList>(loadedJson);
            List<string> dataStringList = new List<string>();
            
            foreach (MofaLib_StatsData data in dataComp.UsageData)
            {
                string dataString = data.PackageName + "," + data.ImportDate + "," + data.MofaCodeName + "," + data.UnityProjectName + "," + data.PCName;
                dataStringList.Add(dataString);
            }
            dataStringList.Sort((x, y) => x.CompareTo(y));

            string firstLine = "Package Name" + "," + "Import Date" + "," + "Project Code" + "," + "Unity Project Name" + "," + "Computer Name";
            dataStringList.Insert(0,firstLine);

            StringBuilder csv = new StringBuilder();
            foreach (string s in dataStringList)
            {
                csv.AppendLine(s);
            }
            string exportPath = mofalibPath + @"/mofalibStats.csv";
            File.WriteAllText(exportPath, csv.ToString());
            EditorUtility.DisplayDialog("Export Stats", "Export Successfull!  " + exportPath, "Ok");

            string folderPath = exportPath.Replace('/','\\');
            System.Diagnostics.Process.Start("explorer.exe", "/select, " + folderPath);
        }
        else
        {
            EditorUtility.DisplayDialog("Export Stats", "Could not find the stats file: " + filePath, "Ok");
        }        
    }
}

#endif
