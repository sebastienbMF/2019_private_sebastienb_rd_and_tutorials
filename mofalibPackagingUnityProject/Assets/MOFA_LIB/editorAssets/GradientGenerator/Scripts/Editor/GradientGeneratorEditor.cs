﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

public class GradientGeneratorEditor : EditorWindow
{
    private const string FILE_TYPE = ".png"; // TODO: support multiple file types

    private enum eGenerationType
    {
        GRADIENT = 0,
        CURVE
    }

    private string filePath = "MOFA_LIB/playerAssets/utilities/GradientGenerator";
    private string fileName = "Gradient";
    private bool overwriteFile = false;

    private eGenerationType generationType;

    private Gradient gradient;
    private AnimationCurve animCurve_R;
    private AnimationCurve animCurve_G;
    private AnimationCurve animCurve_B;
    private AnimationCurve animCurve_A;
    private bool splitChannels = false;

    private int textureWidth = 256; 
    private Texture2D texture;

    private GUIStyle style;
    string tooltip = "";

    [MenuItem("MofaLib/Tools/GradientGenerator")]
    private static void Init()
    {
        GradientGeneratorEditor window = (GradientGeneratorEditor)EditorWindow.GetWindow(typeof(GradientGeneratorEditor));
        window.Show();
    }

    private void Awake()
    {
        // Set Default Path
        filePath = Path.Combine(Application.dataPath, filePath);

        // Set Default Gradient
        gradient = new Gradient();
        GradientColorKey[] colorKey = new GradientColorKey[2];
        colorKey[0].color = Color.black;
        colorKey[0].time = 0.0f;
        colorKey[1].color = Color.white;
        colorKey[1].time = 1.0f;
        GradientAlphaKey[] alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 0.0f;
        alphaKey[1].time = 1.0f;
        gradient.SetKeys(colorKey, alphaKey);

        // Set Default Curves
        animCurve_R = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        animCurve_G = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        animCurve_B = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        animCurve_A = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    }

    private void OnGUI()
    {
        EditorGUIUtility.labelWidth = 120f;

        texture = new Texture2D(textureWidth, 1);

        // toggle the type of generation
        GUILayout.Space(5f);
        style = new GUIStyle(EditorStyles.toolbarButton); 
        style.fontStyle = FontStyle.Bold; 
        style.fixedHeight = 30.0f;
        string[] enumValues = System.Enum.GetNames(typeof(eGenerationType));
        GUIContent[] contents = new GUIContent[enumValues.Length];
        for (int i = 0; i < enumValues.Length; i++)
        {
            if (i == (int)eGenerationType.CURVE)
            {
                tooltip = "Generate a 1D gradient texture based on an animation curve. Split the channels to edit each channel separately.";
            }
            else if (i == (int)eGenerationType.GRADIENT)
            {
                tooltip = "Generate a 1D gradient texture based on the Unity gradient.";
            }
            contents[i] = new GUIContent(enumValues[i], tooltip);
        }
        generationType = (eGenerationType)GUILayout.Toolbar((int)generationType, contents, style);

        // TODO: load gradient png and set the gradient or curves
        //if (GUILayout.Button("Load Gradient"))
        //{
        //    string loadFilePath = EditorUtility.OpenFilePanel("Load Gradient", filePath, FILE_TYPE);
        //    if (!string.IsNullOrEmpty(loadFilePath))
        //    {
        //        LoadTexture(loadFilePath, generationType);
        //    }
        //}

        // show proper fields based on generation type
        switch (generationType)
        {
            case eGenerationType.GRADIENT:
                ShowGradientEditor();
                break;
            case eGenerationType.CURVE:
                ShowCurveEditor();
                break;
            default:
                break;
        }

        // handle file information
        GUILayout.Space(EditorGUIUtility.singleLineHeight);
        GUILayout.Label("File Information", EditorStyles.boldLabel);

        // set file name
        fileName = EditorGUILayout.TextField("Gradient Name: ", fileName);

        // set texture width
        textureWidth = Mathf.Clamp(EditorGUILayout.IntField("Width", textureWidth), 1, 4096);

        // set file path
        EditorGUILayout.BeginHorizontal();
        filePath = EditorGUILayout.TextField("File Path: ", filePath);
        if (GUILayout.Button("Browse...", GUILayout.Width(100f), GUILayout.Height(15f)))
        {
            string newFilePath = EditorUtility.OpenFolderPanel("Choose file path", filePath, "");
            if (!string.IsNullOrEmpty(newFilePath))
            {
                filePath = newFilePath;
            }
        }
        EditorGUILayout.EndHorizontal();

        // handle generating and saving gradient
        tooltip = "If toggled on, will overwrite a file of the same name in the directory. If toggled off, will create new files with '_XX' appended to the end of the file name.";
        overwriteFile = EditorGUILayout.Toggle(new GUIContent( "Overwrite Files", tooltip), overwriteFile);

        tooltip = "Generate a 1D Gradient texture. (Remember to change the import settings on the texture if needed!)";
        if (GUILayout.Button(new GUIContent("Generate Gradient", tooltip), GUILayout.Height(30f)))
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                GenerateTexture(texture, GetFilePath(filePath, fileName));
            }
        }
    }

    private void ShowGradientEditor()
    {
        // gradient title
        style = new GUIStyle(EditorStyles.boldLabel);
        style.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label("Gradient Generation", style);

        // set gradient
        gradient = EditorGUILayout.GradientField(GUIContent.none, gradient, true, GUILayout.Height(EditorGUIUtility.singleLineHeight * 2));

        // set the gradient values to the texture
        if (gradient != null)
        {
            for (int i = 0; i < textureWidth; i++)
            {
                texture.SetPixel(i, 0, gradient.Evaluate((float)i / (float)textureWidth));
            }
        }
    }

    private void ShowCurveEditor()
    {
        // anim curve title
        style = new GUIStyle(EditorStyles.boldLabel);
        style.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label("Animation Curve Generation", style);

        // split channels or not
        splitChannels = EditorGUILayout.Toggle("Use Split Channels", splitChannels);

        // set curves
        if (splitChannels)
        {
            GUILayout.Label("Red Channel");
            animCurve_R = EditorGUILayout.CurveField(animCurve_R, Color.red, Rect.zero, GUILayout.Height(50f));
            GUILayout.Label("Green Channel");
            animCurve_G = EditorGUILayout.CurveField(animCurve_G, Color.green, Rect.zero, GUILayout.Height(50f));
            GUILayout.Label("Blue Channel");
            animCurve_B = EditorGUILayout.CurveField(animCurve_B, Color.blue, Rect.zero, GUILayout.Height(50f));
        }
        GUILayout.Label(splitChannels? "Alpha Channel" : "Black/White Curve");
        animCurve_A = EditorGUILayout.CurveField(animCurve_A, Color.white, Rect.zero, GUILayout.Height(50f));

        // set the curve values to the texture
        Color color = Color.black;
        for (int i = 0; i < textureWidth; i++)
        {
            color.a = animCurve_A.Evaluate((float)i / (float)textureWidth);
            if (splitChannels)
            {
                color.r = animCurve_R.Evaluate((float)i / (float)textureWidth);
                color.g = animCurve_G.Evaluate((float)i / (float)textureWidth);
                color.b = animCurve_B.Evaluate((float)i / (float)textureWidth);
            }
            else
            {
                color.r = color.a;
                color.g = color.a;
                color.b = color.a;
            }

            texture.SetPixel(i, 0, color);
        }
    }

    private string GetFilePath(string path, string file)
    {
        string fileNamePath = Path.Combine(path, file + FILE_TYPE);
        if (!overwriteFile)
        {
            int count = 0;
            while (File.Exists(fileNamePath))
            {
                fileNamePath = Path.Combine(path, file + "_" + count.ToString("00") + FILE_TYPE);
                count++;
            }
        }

        return fileNamePath;
    }

    private void GenerateTexture(Texture2D tex, string path)
    {
        var pngData = tex.EncodeToPNG();
        if (pngData != null)
        {
            File.WriteAllBytes(path, pngData);
            AssetDatabase.Refresh();
        }
    }

    private void LoadTexture(string path, eGenerationType type)
    {
        var pngData = File.ReadAllBytes(path);
        texture.LoadImage(pngData);

        switch (type)
        {
            case eGenerationType.GRADIENT:
                LoadGradient(texture);
                break;
            case eGenerationType.CURVE:
                LoadCurves(texture);
                break;
            default:
                break;
        }
    }

    private void LoadGradient(Texture tex)
    {
        // TODO
    }

    private void LoadCurves(Texture tex)
    {
        // TODO
    }
}
