-----------------------------------------

GRADIENT GENERATOR

    documentation: https://momentfactory.atlassian.net/wiki/spaces/BIBLETECHNO/pages/732135605/Gradient+Generators
    author(s): Connie Fu
-----------------------------------------

    * VERSIONS *
1.00 - 1D Gradient textures from Unity Gradient and Animation Curves

-----------------------------------------

    * ABOUT *
MofaLib > Tools > GradientGenerator
This tool is used to create a 1D texture from the Unity Gradient or Animation Curves. 

As of V1.00, the generator cannot load gradient textures. If a custom gradient is created, it is recommended to save the new gradient preset.

-----------------------------------------

    * TODO *
- Loading of gradient textures
- Creating / loading different file types
- In Window editors for gradients and curves

-----------------------------------------


