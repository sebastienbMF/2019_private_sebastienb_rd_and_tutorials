﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour {

	public Vector3 Rotation;
	public bool RandomRotation;
	public Vector2 RandomRotationRange;

	void Start()
	{
		if(RandomRotation)
		{
			Rotation = new Vector3(Random.Range(RandomRotationRange.x, RandomRotationRange.y), Random.Range(RandomRotationRange.x, RandomRotationRange.y),Random.Range(RandomRotationRange.x, RandomRotationRange.y));
		}
	}

	void Update()
	{
		transform.Rotate(Rotation);
	}
}
