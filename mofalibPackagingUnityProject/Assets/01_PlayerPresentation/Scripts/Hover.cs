﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour {

	[Header ("Hover params")]
	public AnimationCurve HoverAnim;
	public bool _hover;
	public Vector2 _hoverRangeX;
	public Vector2 _hoverRangeY;
	public Vector2 _hoverRangeZ;

	Coroutine _hoverCoroutine;
	Vector3 _initPos;
	
	void Start()
	{
		_initPos = transform.position;
	}

	void Update()
	{
		if(_hover && _hoverCoroutine == null)
		{
			_hoverCoroutine = StartCoroutine(HoverCoroutine());
		}
	}

	public void SetHover(bool hover)
	{
		_hover = hover;
	}

	IEnumerator HoverCoroutine()
	{
		//print("Hover");
		Vector3 newPos = _initPos + new Vector3(Random.Range(_hoverRangeX.x, _hoverRangeX.y), Random.Range(_hoverRangeY.x, _hoverRangeY.y), Random.Range(_hoverRangeZ.x, _hoverRangeZ.x));
		Vector3 startPos = transform.position;
		for(float t = 0; t< 1; t += Time.deltaTime / 5)
		{
			float val = HoverAnim.Evaluate(t);
			transform.position = Vector3.Lerp(startPos, newPos, val);
			yield return null;
		}
		_hoverCoroutine = null;
	}
}
