﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class NoisyLine : MonoBehaviour
{

    public bool _runInEditor;
    public bool isRandomNoise = true;
    public float factor = 1;
    public LineRenderer _lineRenderer;
    public Transform[] _handles;
    public Transform _attractor;
    public int _pointsPerUnits;

    [Header("NoiseParam")]
    public float _noiseIntensity;
    public float _noiseScale;
    public float _noiseSpeed;
    public AnimationCurve _noiseDamp;
    public AnimationCurve _growthAnim;
    public AnimationCurve _path;
    public float _pathIntensity;
    public float _growthSpeed;
    [Range(0, 1f)]
    public float _length;

    public int _maxPoints;
    float _noiseOffset;
    float _totalDistance;
    float _generationDistance;

    public float _angle;

    float _noiseTime;

    // handlePulse
    float[] _timeOffset;
    Renderer[] _handleRenderers;
    Color _emissionColor;

    void Start()
    {

        _timeOffset = new float[2];
        if (isRandomNoise)
        {
            _noiseOffset = Random.Range(0, 10f);

            for (int i = 0; i < +_timeOffset.Length; i++)
            {
                _timeOffset[i] = Random.Range(0f, 1f);
            }
        }
        else
        {
            for (int i = 0; i < +_timeOffset.Length; i++)
            {
                _timeOffset[i] = 1f / factor;
            }
            _noiseOffset = 10f / factor;
        }

        _handleRenderers = new Renderer[2];
        if (Application.isPlaying)
        {
            for (int i = 0; i < +_handleRenderers.Length; i++)
            {
                _handleRenderers[i] = _handles[i].GetComponent<Renderer>();
            }
            _emissionColor = _handleRenderers[0].material.color;
        }
    }

    void Update()
    {
        if (!Application.isPlaying && !_runInEditor)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            for (int i = 0; i < _handleRenderers.Length; i++)
            {
                _handleRenderers[i].enabled = !_handleRenderers[i].enabled;
            }
        }
        _noiseTime += Time.deltaTime * _noiseSpeed;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //StartCoroutine(Create());
        }
        _totalDistance = Vector3.Distance(_handles[0].position, _handles[1].position);
        _generationDistance = _length * _totalDistance;
        CreatePoints();
        if (Application.isPlaying)
        {
            HandlePulse();
        }
    }

    void HandlePulse()
    {
        for (int i = 0; i < _handles.Length; i++)
        {
            float val = Mathf.Sin(Time.time + _timeOffset[i]);
            val = val.Normalize(-1, 1);
            _handleRenderers[i].material.SetColor("_EmissionColor", (1 + val * 3) * _emissionColor);
        }
    }

    public void CreatePoints()
    {
        _maxPoints = Mathf.CeilToInt(_pointsPerUnits * _totalDistance);
        int points = Mathf.CeilToInt(_pointsPerUnits * _generationDistance);
        Vector3[] pointPos = new Vector3[points];
        Vector3 endPos = Vector3.Lerp(_handles[0].position, _handles[1].position, _length);
        for (int i = 0; i < pointPos.Length; i++)
        {
            float val = (float)i / (float)pointPos.Length;
            pointPos[i] = Vector3.Lerp(_handles[0].position, endPos, val);
        }
        _lineRenderer.positionCount = points;
        pointPos = AlongPath(pointPos);
        pointPos = AddNoise(pointPos, _generationDistance);
        //pointPos = Localize(pointPos);
        _lineRenderer.SetPositions(pointPos);
    }

    IEnumerator Create()
    {
        for (float t = 0; t < 1; t += Time.deltaTime / _growthSpeed)
        {
            _length = _growthAnim.Evaluate(t);
            yield return null;
        }
        _length = 1;
    }

    Vector3[] Localize(Vector3[] posArray)
    {
        for (int i = 0; i < posArray.Length; i++)
        {
            posArray[i] = transform.TransformPoint(posArray[i]);
        }
        return posArray;
    }

    Vector3[] AlongPath(Vector3[] posArray)
    {
        for (int i = 0; i < posArray.Length; i++)
        {
            float val = ((float)i / (float)_maxPoints);
            val = _path.Evaluate(val);
            Vector3 valDir = Vector3.Cross(_handles[0].position - _handles[1].position, Vector3.forward).normalized;
            posArray[i] = posArray[i] + (valDir * val * _pathIntensity);
        }
        return posArray;
    }

    Vector3[] AddNoise(Vector3[] posArray, float distance)
    {
        Vector3 attractorPos = _attractor.position;
        for (int i = 0; i < posArray.Length; i++)
        {
            float val = ((float)i / (float)_maxPoints);
            float damp = _noiseDamp.Evaluate(val);
            float noise;

            if (isRandomNoise)
            {
                noise = Mathf.PerlinNoise((val * _noiseScale) + _noiseTime + _noiseOffset, (val * _noiseScale) + _noiseOffset) - 0.5f;
            }
            else
            {
                noise =  -(Mathf.PerlinNoise((val * _noiseScale) + _noiseTime + _noiseOffset, (val * _noiseScale) + _noiseOffset));
            }

            //Vector3 noiseDir = Vector3.Cross(_handles[0].position - _handles[1].position, Vector3.up).normalized;
            Vector3 nextPos = Vector3.zero;
            int nextPoint = i + 1;
            if (nextPoint >= posArray.Length)
            {
                nextPos = _handles[1].position;
            }
            else
            {
                nextPos = posArray[nextPoint];
            }
            Vector3 noiseDir = Vector3.Cross(posArray[i] - nextPos, Vector3.forward).normalized;
            Vector3 newPos = posArray[i] + (noiseDir * noise * _noiseIntensity * damp);

            /*float force = 20 - Mathf.Clamp(Vector3.Distance(newPos, attractorPos), 0, 20);
			force = force.Normalize(0,20);
			float forceNorm = Mathf.Lerp(0, 1, Mathf.Sin(force * Mathf.PI * 0.5f));
			Vector3 forceResult = (attractorPos - newPos).normalized * forceNorm * 10;*/
            posArray[i] = newPos; // + forceResult;
        }
        return posArray;
    }
}
